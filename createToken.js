var sha256 = require("js-sha256");
const addSubtractDate = require("add-subtract-date");

function createToken(request, response) {
  // token format:  timestamp YYYYMMDDhhmmss + token hash
  // timestamp  for Jan 4 2021 1:15pm:  20210104131500
  //
  // Azure will hash timestamp + secret text.  For example:     20210104131500X]w97u;W%yVkLuN!Gmb[Xa3,9aZMv.m*!8>hrf~V2VGz(EP5T?Gjrn%by9}DBE(dgJDUSkr-3{(_}(CS2L8M2{tt<BN*hSY*~pg
  // This is actual SHA256 hash:    b6bfe45ee602f37dd4cd0524407786b2bbf4c46eb10e061be327afad57758568
  // The token from Azure will be:  20210104131500b6bfe45ee602f37dd4cd0524407786b2bbf4c46eb10e061be327afad57758568
  //                                ^^^^^^^^^^^^^^                              ^
  //                                  timestamp                                hash
  // the process on this end will be:
  //     separate the timestamp and hash from token
  //     hash the token timestamp concatenated with the secret text
  //     compare the local hash with the token hash
  //     if they match AND the token timestamp is in the future, we are good to go.

  let requestingUser = request.params.id; //Tyler Hobbs


  if (
    requestingUser == undefined ||
    requestingUser == null ||
    requestingUser == " "
  ) {
    console.log(`User ${requestingUser} Invalid.`);
    response.status(200).send("Invalid User");
    return;
  } else {
    let requestingUserFirstName = requestingUser.split(" ")[0];
    let requestingUserLastName = requestingUser.split(" ")[1];

    //Add in check against a table for valid users
    const userAuthorizationQuery =
      "SELECT EAAUTH FROM eclaimauth WHERE eauserfn = ? and eauserln = ?";

    const userAuthorizaiton = pjs.query(userAuthorizationQuery, [
      requestingUserFirstName,
      requestingUserLastName
    ]);
    console.log(userAuthorizaiton);

    if (userAuthorizaiton.length == 0) {
      console.log(`User ${requestingUser} not found in eclaimauth table.`);
      response.status(200).send("Access Denied");
      return;
    }

    if (userAuthorizaiton[0]["eaauth"] < 60) { //60 is the minimum authorization level to create tokens. in the table, 80 allows bypass of 2 day limit and 100 allows approval of non-standard claims
      console.log(`User ${requestingUser} not authorized to create tokens.`);
      response.status(200).send("Unauthorized");
      return;
    }

    console.log(`Creating new token for ${requestingUser}`);

    const secretText =
      "X]w97u;W%yVkLuN!Gmb[Xa3,9aZMv.m*!8>hrf~V2VGz(EP5T?Gjrn%by9}DBE(dgJDUSkr-3{(_}(CS2L8M2{tt<BN*hSY*~pg";

    let tokenDate = new Date();
    tokenDate = addSubtractDate.add(tokenDate, 1, "day");

    let localTimestamp = tokenDate.toISOString();
    localTimestamp = localTimestamp.replace(/-|T|:/g, "");
    localTimestamp = localTimestamp.substring(0, 14);
    let token = localTimestamp + sha256(localTimestamp + secretText);

    response.status(200).json({ token: token });
  }
}
exports.run = createToken;
