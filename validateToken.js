
var sha256 = require('js-sha256');
function validateToken(token)
{
    // token format:  timestamp YYYYMMDDhhmmss + token hash
    // timestamp  for Jan 4 2021 1:15pm:  20210104131500
    //
    // Azure will hash timestamp + secret text.  For example:     20210104131500X]w97u;W%yVkLuN!Gmb[Xa3,9aZMv.m*!8>hrf~V2VGz(EP5T?Gjrn%by9}DBE(dgJDUSkr-3{(_}(CS2L8M2{tt<BN*hSY*~pg
    // This is actual SHA256 hash:    b6bfe45ee602f37dd4cd0524407786b2bbf4c46eb10e061be327afad57758568
    // The token from Azure will be:  20210104131500b6bfe45ee602f37dd4cd0524407786b2bbf4c46eb10e061be327afad57758568
    //                                ^^^^^^^^^^^^^^                              ^
    //                                  timestamp                                hash
    //
    
    
    //
    // the process on this end will be: 
    //     separate the timestamp and hash from token
    //     hash the token timestamp concatenated with the secret text
    //     compare the local hash with the token hash
    //     if they match AND the token timestamp is in the future, we are good to go.


    var secretText = "X]w97u;W%yVkLuN!Gmb[Xa3,9aZMv.m*!8>hrf~V2VGz(EP5T?Gjrn%by9}DBE(dgJDUSkr-3{(_}(CS2L8M2{tt<BN*hSY*~pg";
    var returnObj = {};
    returnObj.tokenIsValid = false;
    returnObj.message = "";
    
    if (token == null) 
    {
        returnObj.message = "Token is missing.";
        return returnObj;
    }

    if (token.length < 20) 
    {
        returnObj.message = "Token is corrupt.";
        return returnObj;
    }

    var tokenTimestamp = token.substring(0,14);
    var tokenHash = token.substring(14);

    
    var localHash = sha256(tokenTimestamp + secretText);  
    if (localHash != tokenHash)
    {
        returnObj.message = "Token is invalid.";
        return returnObj;
    }

    //var localTimestamp = pjs.char( pjs.timestamp(), "*iso0");
    var d = new Date();
    var localTimestamp = d.toISOString();
    localTimestamp = localTimestamp.replace(/-|T|:/g, '');
    localTimestamp = localTimestamp.substring(0,14);
    if (localTimestamp > tokenTimestamp )
    {
        returnObj.message = "Token has expired.";
        return returnObj;
    }


    returnObj.tokenIsValid = true;
    return returnObj;
   
}
exports.validateToken = validateToken;
