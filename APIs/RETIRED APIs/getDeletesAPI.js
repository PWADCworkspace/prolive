// //const addSubtractDate = require('add-subtract-date');
// function getDeletes(request,response) {

//   var directPath = 'C:/profront/modules/prolive/APPs/';

//   var log = pjs.require( directPath + 'PushToLogAPP.js');
//   var ProfrontTxtlog = pjs.require(directPath + 'PushToTxtLogAPP.js');

//   //   let numberOfDays = request.params.days;
//   //   console.log('numberOfDays:'+numberOfDays);
//   var sqlQuery;
//   var recordSet;
//   var errorMsg;
//   //   var recordSetArray = [];

//   // var searchDate = formatDate(numberOfDays);
//   sqlQuery =
//     'Select ' +
//     '@ITM as ItemNumber, ' +
//     '@CKDIG as CheckDigit, ' +
//     '@PKSIZ as PackSize, ' +
//     '@DESC as Description, ' +
//     '@SBKEY as SubKey, ' +
//     '@OFLBL as OffPack, ' +
//     '@SLOTA as Aisle, ' +
//     '@SLOT as Slot, ' +
//     '@DEPT as Department, ' +
//     '@PICOD as PickCode, ' +
//     '@VEND as VendorNumber, ' +
//     '@MYTD as MovementYTD, ' +
//     '@BUYR as Buyer, ' +
//     '@LSTYR as LastYearSales, ' +
//     '@ODHDR as OrderBookHeader, ' +
//     '@COST as Cost, ' +
//     '@SELL as Sell, ' +
//     '@BCOST as BaseCost, ' +
//     '@BSELL as BaseSell, ' +
//     '@CWFLG as CWFlag, ' +
//     '@CWCST as CWCost, ' +
//     '@CWSEL as CWSell, ' +
//     '@PRVLB as PrivateLabelFlag, ' +
//     '@B1PC as Book1Percent, ' +
//     '@B2PC as Book2Percent, ' +
//     '@B3PC as Book3Percent, ' +
//     '@B4PC as Book4Percent, ' +
//     '@B5PC as Book5Percent, ' +
//     '@B6PC as Book6Percent, ' +
//     '@B7PC as Book7Percent, ' +
//     '@B8PC as Book8Percent, ' +
//     '@B9PC as Book9Percent, ' +
//     '@B0PC as Book10Percent, ' +
//     '@B1SRP as Book1SRP, ' +
//     '@B2SRP as Book2SRP, ' +
//     '@B3SRP as Book3SRP, ' +
//     '@B4SRP as Book4SRP, ' +
//     '@B5SRP as Book5SRP, ' +
//     '@B6SRP as Book6SRP, ' +
//     '@B7SRP as Book7SRP, ' +
//     '@B8SRP as Book8SRP, ' +
//     '@B9SRP as Book9SRP, ' +
//     '@B0SRP as Book10SRP, ' +
//     '@B1BPC as Book1BasePercent, ' +
//     '@B2BPC as Book2BasePercent, ' +
//     '@B3BPC as Book3BasePercent, ' +
//     '@B4BPC as Book4BasePercent, ' +
//     '@B5BPC as Book5BasePercent, ' +
//     '@B6BPC as Book6BasePercent, ' +
//     '@B7BPC as Book7BasePercent, ' +
//     '@B8BPC as Book8BasePercent, ' +
//     '@B9BPC as Book9BasePercent, ' +
//     '@B0BPC as Book10BasePercent, ' +
//     '@B1BSR as Book1BaseSRP, ' +
//     '@B2BSR as Book2BaseSRP, ' +
//     '@B3BSR as Book3BaseSRP, ' +
//     '@B4BSR as Book4BaseSRP, ' +
//     '@B5BSR as Book5BaseSRP, ' +
//     '@B6BSR as Book6BaseSRP, ' +
//     '@B7BSR as Book7BaseSRP, ' +
//     '@B8BSR as Book8BaseSRP, ' +
//     '@B9BSR as Book9BaseSRP, ' +
//     '@B0BSR as Book10BaseSRP, ' +
//     '@UPC as UPC, ' +
//     '@DDATE as DateDeleted ' + //MMDDYY
//     'from PWAFIL.DELETES';
//   try {
//     recordSet = pjs.query(sqlQuery);
//     // console.log(recordSet);

//     if (sqlstate >= '02000') {
//       errorMsg = 'SQL Query of DELETES ended in Error. SQL State: ' + sqlstate + '.';
//       log.pushToLog('', 'DeletesAPI', errorMsg);
//       ProfrontTxtlog.pushToTxTLog("getDeletesAPI","Profront Proxy","errorMsg: " + errorMsg);
//       response.status(400).send(errorMsg);
//       return;

//     } else if (recordSet.length == 0) {
//       errorMsg = 'No records found, possible error.';
//       log.pushToLog('', 'DeletesAPI', errorMsg);
//       ProfrontTxtlog.pushToTxTLog("getDeletesAPI","Profront Proxy","errorMsg: " + errorMsg);
//       response.status(400).send(errorMsg);
//       return;

//     } else {
//       ProfrontTxtlog.pushToTxTLog("getDeletesAPI","Profront Proxy","Successfully Called.");
//       response.status(200).json(recordSet);
//     }
//   } catch (err) {
//     var errStatement = '91';
//     errorMsg = 'Sql Query error';
//     log.pushToLog(errStatement, 'getDeletesAPI', errorMsg);
//     ProfrontTxtlog.pushToTxTLog("getDeletesAPI","Profront Proxy","errorMsg: " + errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }
// }
// exports.run = getDeletes;