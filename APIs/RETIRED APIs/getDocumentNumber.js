// // WE ARE NO LONGER USING THIS!!!!!!!!!!!!

// function getDocumentNumber(request, response) {

//   pjs.define('claimNumber', {type: 'packed decimal', length: 5, decimals: 0,});  
//   var recordSet;
//   var sqlquery;
//   var errorMsg;
//   var parmListArray = [];
//   claimNumber = request.params.id;

//   if (!claimNumber || claimNumber <= 0) {
//     errorMsg = "Claim Number required ";
//     response.status(400).send(errorMsg);
//   } else {
//     // Query ENTCLM to get Document Number and Claim Date.
//     sqlquery = "select scldoc, sclmdt from PROFRONT.ENTCLM_CALLIN where sclmno = ?";
//     recordSet = pjs.query(sqlquery, [claimNumber]);

//     // If there is a record in ENTCLM return that it already exists.
//     if (recordSet.length > 0) {
//       parmListArray.push({
//         DocumentNumber: recordSet[0]["scldoc"], //assigned claim number.
//         ClaimDate: recordSet[0]["sclmdt"], //date the claim number was created.
//         claimNumber: claimNumber, //document number passed from front end.
//       });
//       response.json(parmListArray);

//       // If there is not a record in ENTCLM, proced with Claim History query.
//     } else {
//       parmListArray.push({ 
//         Status: "Claim is not in file." ,
//         claimNumber: claimNumber
//       });
//       response.json(parmListArray);
//     }
//   }
// }
// exports.run = getDocumentNumber;
