// function getBuyerCodeAPI(request, response) {
//   // Path for writting to ProfrontLog.txt
//   var directPath = 'C:/profront/modules/prolive/APPs/';
//   const ProfrontTxtlog = pjs.require(directPath + 'PushToTxtLogAPP.js');
//   const ItemNumber = request.params.id;

//   if (ItemNumber) {
//     const sqlquery = "Select BUYER from PWAFIL.INVMST where ITMCDE = ?";
//     var recordSet = pjs.query(sqlquery, [ItemNumber]);

//     if (recordSet && recordSet.length > 0) {
//       // Push results to ProfrontLog.txt
//       ProfrontTxtlog.pushToTxTLog("getBuyerCodeAPI","Profront Proxy","Buyer: " + recordSet[0]["buyer"]);
//       response.json({
//         buyer: recordSet[0]["buyer"]
//       });
//     } else {
//       let errorMessage = "No Buyer found from INVMST for Item: " + ItemNumber;
//       // Push results to ProfrontLog.txt
//       ProfrontTxtlog.pushToTxTLog("getBuyerCodeAPI","Profront Server",errorMessage);
//       response.status(400).send(errorMessage);
//     }
//   } else {
//     // Push results to ProfrontLog.txt
//     ProfrontTxtlog.pushToTxTLog("getBuyerCodeAPI","Profront Proxy","Id not received");
//     response.status(400).send("Id not received");
//   }
// }
// exports.run = getBuyerCodeAPI;
