function getorder(request, response) {
  var method = request.method;

  if (method == 'POST') 
    goInsert();
  else if (method == 'GET') 
    goSelect();
  else if (method == 'PUT') 
    goPut();
  else if (method == 'DELETE') 
    goDelete();  
  else 
    goBadMethod();

  if(method == 'GET')
    getBuyerCode()

//------------------------------------------------------------------------------------------------------
/*
  function goSelect() {
    var InvoiceNumber = request.params.id;
    var InvoiceDate = request.params.id; //--Here I need to pass two or more parameters through Get request?
    var ItemNumber = request.params.id;
    getFicheH01Record(InvoiceNumber, InvoiceDate);
    getFicheD01ItemInfo(InvoiceNumber, InvoiceDate, ItemNumber);
  }
*/
//------------------------------------------------------------------------------------------------------

  function getFicheH01Record(InvoiceNumber, InvoiceDate) {
    //var InvoiceNumber = request.params.id;
    //var InvoiceDate = request.params.id; --Here I need to know how to pass two parameters through Get request?
    console.log(InvoiceNumber, InvoiceDate);

    var recordSet;
    var sqlquery;
    if (InvoiceNumber) {
      sqlquery =
        'select FHINV, FHDATE, FHDEPT, FHDEPD from PWAFIL.FICHEH01 where FHINV = ? & FHDATE = ?'; //I need to pass multiple paramenters to query with (store number, item number, date, etc.)
      recordSet = pjs.query(sqlquery, [InvoiceNumber, InvoiceDate]);

      if (recordSet) {
        response.json({
          InvoiceNumber: recordSet[0]['FHINV'],
          InvoiceDate: recordSet[0]['FHDATE'],
          DEPT: recordSet[0]['FHDEPT'],
          DEPD: recordSet[0]['FHDEPD'],
        });
      } else {
        response
          .status(400)
          .send('No Record Found from FICHEH01 for Invoice Number ' + InvoiceNumber);
      }
    } else {
      response.status(400).send('Id not received');
    }
  }

//------------------------------------------------------------------------------------------------------

  function getFicheD01ItemInfo(InvoiceNumber, InvoiceDate, ItemNumber) {
    //var InvoiceNumber = request.params.id;
    //var InvoiceDate = request.params.id;
    console.log(InvoiceNumber, InvoiceDate, ItemNumber);

    var recordSet;
    var sqlquery;
    if (InvoiceNumber) {
      sqlquery = "select" +
        "FDINV" +
        "FDCHKD" + 
        "FDDATE" + 
        "FDPKSZ" + 
        "FDDESC" + 
        "FDQTYS" + 
        "FDDEPT" + 
        "FDSLOT" + 
        "FDSRP" + 
        "FDSELL" + 
        "FDECST" + 
        "FDCWLB" + 
        "from PWAFIL.FICHED01 where FDINV = ? & FDDATE = ? & FDITEM = ?"; //I need to pass multiple paramenters to query with (store number, item number, date, etc.)
      recordSet = pjs.query(sqlquery, [InvoiceNumber, InvoiceDate, ItemNumber]);

      if (recordSet) {
        response.json({
          InvoiceNumber: recordSet[0]['FDINV'],
          CheckDigit: recordSet[0]['FDCHKD'],
          Date: recordSet[0]['FDDATE'], 
          PackSize: recordSet[0]['FDPKSZ'], 
          Description: recordSet[0]['FDDESC'], 
          Qty: recordSet[0]['FDQTYS'], 
          Department: recordSet[0]['FDDEPT'], 
          Slot: recordSet[0]['FDSLOT'], 
          SRP: recordSet[0]['FDSRP'], 
          Sell: recordSet[0]['FDSELL'], 
          FDECST: recordSet[0]['FDECST'], 
          FDCWLB: recordSet[0]['FDCWLB'],
        });
      } else {
        response
          .status(400)
          .send('No Record Found from FICHED01 for Invoice Number ' + InvoiceNumber);
      }
    } else {
      response.status(400).send('Id not received');
    }
  }

//------------------------------------------------------------------------------------------------------  

  function getClaimHistInfo(InvoiceNumber, STNUM, ItemNumber) {
    //var InvoiceNumber = request.params.id;
    //var InvoiceDate = request.params.id; --Here I need to know how to pass two parameters through Get request?
    console.log(InvoiceNumber, STNUM, ItemNumber);

    var recordSet;
    var sqlquery;
    if (InvoiceNumber) {
      sqlquery = "select" +
        "HORDDT" +
        "HORDIN" + 
        "HCLMIT" + 
        "HCLMST" + 
        "HCLMQT" + 
        "HCLDOC" + 
        "HCLUSR" + 
        "HCLNAM" + 
        "HCLMCO" + 
        "HCLASN" + 
        "HCLCNT" +  
        "from PWAFIL.CLMHST where HORDIN = ? & HCLMST = ? & HCLMIT = ? and HCLMTY < 8";
      recordSet = pjs.query(sqlquery, [InvoiceNumber, STNUM, ItemNumber]);

      if (recordSet) { //Change names in record
        response.json({
          HORDDT: recordSet[0]['HORDDT'],
          HORDIN: recordSet[0]['HORDIN'],
          HCLMIT: recordSet[0]['HCLMIT'], 
          HCLMST: recordSet[0]['HCLMST'], 
          HCLMQT: recordSet[0]['HCLMQT'], 
          HCLDOC: recordSet[0]['HCLDOC'], 
          HCLUSR: recordSet[0]['HCLUSR'], 
          HCLNAM: recordSet[0]['HCLNAM'], 
          HCLMCO: recordSet[0]['HCLMCO'], 
          HCLASN: recordSet[0]['HCLASN'], 
          HCLCNT: recordSet[0]['HCLCNT'],
        });
      } else {
        response
          .status(400)
          .send('No Record Found from CLMHST for Invoice Number ' + InvoiceNumber);
      }
    } else {
      response.status(400).send('Id not received');
    }
  }

//------------------------------------------------------------------------------------------------------  

  function getEntClmInfo(InvoiceNumber, STNUM, ItemNumber) {
    //var InvoiceNumber = request.params.id;
    //var InvoiceDate = request.params.id; --Here I need to know how to pass two parameters through Get request?
    console.log(InvoiceNumber, STNUM, ItemNumber);

    var recordSet;
    var sqlquery;
    if (InvoiceNumber) {
      sqlquery = "select" +
        "SCLMDT" +
        "SORDIN" + 
        "SCLMIT" + 
        "SCLMST" + 
        "SCLMQT" + 
        "SCLMCO" + 
        "SCLUSR" + 
        "SCLNAM" + 
        "SCLMCO" + 
        "SCLASN" + 
        "SCLCNT" +  
        "from PWAFIL.ENTCLM where SORDIN = ? & SCLMST = ? & SCLMIT = ?";
      recordSet = pjs.query(sqlquery, [InvoiceNumber, STNUM, ItemNumber]);

      if (recordSet) {
        response.json({ //we can change the names of the recordSet.
          SCLMDT: recordSet[0]['SCLMDT'],
          SORDIN: recordSet[0]['SORDIN'],
          SCLMIT: recordSet[0]['SCLMIT'], 
          SCLMST: recordSet[0]['SCLMST'], 
          SCLMQT: recordSet[0]['SCLMQT'], 
          SCLMCO: recordSet[0]['SCLMCO'], 
          SCLUSR: recordSet[0]['SCLUSR'], 
          SCLNAM: recordSet[0]['SCLNAM'], 
          SCLMCO: recordSet[0]['SCLMCO'], //do we need this SCLMO twice?
          SCLASN: recordSet[0]['SCLASN'], 
          SCLCNT: recordSet[0]['SCLCNT'],
        });
      } else {
        response
          .status(400)
          .send('No Record Found from ENTCLM for Invoice Number ' + InvoiceNumber);
      }
    } else {
      response.status(400).send('Id not received');
    }
  }

function getStorInfo(){
  sqlquery = "select STCITY, stzip, stadrs FROM PWAFIL.ARFILE WHERE ARFKEY = username"
}

//------------------------------------------------------------------------------------------------------

/*
  function goInsert() {
    var recordSet;
    var sqlquery;
    var errorMsg;
    var parmListArray = [];

    if (!request.body.storeNumber) {
      errorMsg = 'storeNumber is required ';
      response.status(400).send(errorMsg);
    } else {
        parmListArray.push(request.body.storeNumber);
        parmListArray.push(request.body.itemNumber);
        console.log(parmListArray);

      storeNumber = request.body.storeNumber;
      console.log(storeNumber);

      sqlquery =
        'select STCITY, STZIP, STADRS from PWAFIL.ARFILE where ARFKEY = ?';
      recordSet = pjs.query(sqlquery, [storeNumber]);

      if (recordSet) {
        //console.log(recordSet);
        response.json(recordSet);
      } else {
        response
          .status(400)
          .send('No Record Found for Store Number ' + storeNumber);
      }
    }
  }
*/
//-----------------------------------------------------------------------------------------------------------

  function goBadMethod() {
    var error = 'no method received';
    response.status(400).send(error);
  }
}
exports.run = getorder;
