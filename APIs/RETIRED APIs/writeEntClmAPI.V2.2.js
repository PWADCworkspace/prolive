/* Incoming INFO:
{
  "ClaimNumber": 00000,
  "StoreNumber": 000,
  "InvoiceNumber": 00000,
  "ItemNumber": 000000,
  "ClaimType": 3,
  "isRestockFee": "N",
  "CreditType": "C",
  "QuantityOnClaim": 0,
  "CaseOrOnly": "C",
  "CatchWeight": 00.00
  "ExtendedCost": 00.00,
  "ExtenededRetail": 000.00,
  "UserKeyingClaim": "Tyler",
  "CallingName": "Hobbs"
}
*/

// VERSION V2.1-------------------------------------------------------------------------------------------------------------------------------------------------------
  // Including calculation for restock fee
// VERSION V2.2-------------------------------------------------------------------------------------------------------------------------------------------------------
  // Including Catch Weight intake and passthrough for claim.

  function writeEntClmInfo(request, response) {
    var method = request.method;

    const directPathToProlive = 'C:/profront/modules/prolive/APPs/';
    const directPathToProtest = 'C:/profront/modules/protest/APPs/';

    let log = pjs.require( directPathToProlive + 'PushToLogAPP.js');
    let ProfrontTxtlog = pjs.require(directPathToProlive + 'PushToTxtLogAPP.js');
    let FicheD01 = pjs.require( directPathToProtest + 'FicheD01APP.V2.js');
    let EntClm = pjs.require(directPathToProlive + 'EntClmAPP.js');
    let ClmHst = pjs.require(directPathToProtest + 'ClmHstAPP.V2.js');
    let ClmEnt = pjs.require(directPathToProlive + 'ClmEntAPP.js');
    let ledgerNumber = pjs.require( directPathToProlive + 'LedgerNumberAPP.js');
    var errorMsg;
  
    if (method == 'POST') goPOST();
    else goBadMethod();
    //----------------------------------------------------------------------------
    function goPOST() {
      console.log('writeEntClm v2.1 running.');
      var sqlquery;
      var successMsg;
      const invoiceNumber = request.body.InvoiceNumber;
      const storeNumber = request.body.StoreNumber;
      const itemNumberCheckDigit = request.body.ItemNumber;
      const quantityClaimed = request.body.QuantityOnClaim;
      const caseOrOnlyClaimed = request.body.CaseOrOnly;
      var itemNumber;
      const userName = request.body.UserKeyingClaim + ' ' + request.body.CallingName;
      const incommingCatchWeight = request.body.CatchWeight;
  
      pjs.define('extendedCost', {
        type: 'packed decimal',
        length: 7,
        decimals: 2,
      });
      pjs.define('extendedRetail', {
        type: 'packed decimal',
        length: 7,
        decimals: 2,
      });
  
      extendedCost = request.body.ExtendedCost; //extended cost 7s 2
  
      if (!invoiceNumber) {
        errorMsg = 'Invoice Number is required.';
        log.pushToLog(storeNumber, userName, errorMsg);
        response.status(400).send(errorMsg); 
        return;
      } else if (!itemNumberCheckDigit) {
        errorMsg = 'Item Number is required.';
        log.pushToLog(storeNumber, userName, errorMsg);
        response.status(400).send(errorMsg);
        return;
      } else if (!storeNumber) {
        errorMsg = 'Store Number is required.';
        log.pushToLog(storeNumber, userName, errorMsg);
        response.status(400).send(errorMsg);
        return;
      } else {
        itemNumber = padLeadingZeros(itemNumberCheckDigit, 6);
        itemNumber = itemNumber.slice(0, 5);
        itemNumber = parseInt(itemNumber);
  
        // Query FicheD01 to get Item info off of Invoice.
        FicheD01 = FicheD01.getFicheD01Info(
          invoiceNumber,
          storeNumber,
          itemNumber,
          incommingCatchWeight
        );
        // console.log(FicheD01);
        if (FicheD01.Message) {
          log.pushToLog(storeNumber, userName, FicheD01.Message);
          response.status(400).send(FicheD01.Message);
          console.log("Error on FicheD01 querry:" + FicheD01.Message);
          return;
        } else {
          // console.log(FicheD01.array);
  
        // Query ENTCLM to see if a claim is currently filed.
        console.log('running ENTCLM query.');
        EntClm = EntClm.getEntClmInfo(invoiceNumber, storeNumber, itemNumber);
  
        if (EntClm.array.length > 0) {
          errorMsg = `A claim has already been sumbitted for invoice: ${invoiceNumber} and item: ${itemNumber}.`;
          console.log(errorMsg);
          ProfrontTxtlog.pushToTxTLog("writeEntClmAPI.V2.1",userName, errorMsg);
          log.pushToLog(storeNumber, userName, errorMsg);
          response.status(400).send(errorMsg);
          return;
        }
  
        // Query CLMHST to see if a claim has already been filed.
        console.log('running CLMHST query.');
        ClmHst = ClmHst.getClmHstInfo(invoiceNumber, storeNumber, itemNumber, extendedCost);
  
        if (ClmHst.array.length > 0) {
          errorMsg = `A claim has already been processed for invoice: ${invoiceNumber} and item: ${itemNumber}.`;
          console.log(errorMsg);
          ProfrontTxtlog.pushToTxTLog("writeEntClmAPI.V2.1",userName, errorMsg);
          log.pushToLog(storeNumber, userName, errorMsg);
          //NOTE: TURNED OFF TO ALLOW CATCH WEIGHT CLAIMS THAT ARE ALL THE SAME. WILL ALLOW ALL DUPLICATE CLAIMS TO BE APPROVED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          // response.status(400).send(errorMsg);
          // return;
        }
  
        // Query CLMENT to see if a Markout claim was filed in house.
        console.log('running CLMENT query.');
        ClmEnt = ClmEnt.getClmEntInfo(invoiceNumber, storeNumber, itemNumber);
        
        if (ClmEnt.array.length > 0){
          errorMsg = `A Markout Claim has already been submitted for invoice: ${invoiceNumber} and item: ${itemNumber} and credit has already been issued.`;
          console.log(errorMsg);
          ProfrontTxtlog.pushToTxTLog("writeEntClmAPI.V2.1",userName, errorMsg);
          log.pushToLog(storeNumber, userName, errorMsg);
          //NOTE: TURNED OFF TO ALLOW CATCH WEIGHT CLAIMS THAT ARE ALL THE SAME. WILL ALLOW ALL DUPLICATE CLAIMS TO BE APPROVED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          // response.status(400).send(errorMsg);
          // return;
        }
  
          var recordID = 'S'; // record ID 'S'
          var claimNumber = request.body.ClaimNumber; //created by front end when new claim is written. 10001 - 99999 and repeat.
          if (isNaN(claimNumber)) {
            log.pushToLog(storeNumber, userName, 'Claim Type Incorrect.');
            response.status(400).send('Claim Type cannot contain letters.');
            return;
          }
  
          var claimDate = formatDate();
          var STNUM = request.body.StoreNumber; //store number.
          var claimType = request.body.ClaimType; // claim type '2' SCLMTY.
          var creditType = request.body.CreditType; //credit = 'C' SCLMCR.
          if (!isNaN(creditType)) {
            log.pushToLog(storeNumber, userName, 'Credit Type Incorrect.');
            response.status(400).send('Credit Type Incorrect.');
            return;
          }
  
          var department = FicheD01.array[0]['fddept']; //department 'J'.
          department = department.trim();
  
          ledgerNumber = ledgerNumber.getLedgerNumber(department);
          if (ledgerNumber.Message) {
            log.pushToLog(storeNumber, userName, ledgerNumber.Message);
            response.status(400).send(ledgerNumber.Message);
            return;
          }
          ledgerNumber = ledgerNumber.LedgerNumber;
  
          var invNumOfOrder = FicheD01.array[0]['fdinv'];
          var orderDate = FicheD01.array[0]['fddate'];
          orderDate = orderDate.toString();
          orderDate = orderDate.substr(2, 6);
          var slot = FicheD01.array[0]['fdslot']; //"123456"
          var pickCode = slot.substr(0, 1); //pick code '1'
          var slot1 = slot.substr(1, 2); // Part of slot '23'
          var slot3 = slot.substr(3, 3); // part of slot '456'
          var caseQtyOnOrder = FicheD01.array[0]['fdqtys']; // Qty on original order //2 cases
          var packSize = FicheD01.array[0]['fdpksz']; //pack size on original order //12/1OZ
          packSize = packSize.toString();
          var packDash = packSize.indexOf('/');
          packSize = packSize.slice(0, packDash);
          packSize = packSize.trim();
          var extendedPackSizeOnOrder = packSize * caseQtyOnOrder; // 12 pk * 2 cases = 24 pk
          extendedRetail = request.body.ExtenededRetail; //extended retail 7s 2
  
          if(caseOrOnlyClaimed == 'C' && quantityClaimed > caseQtyOnOrder){
              errorMsg = "Quantity on Claim (" + quantityClaimed + ") exceeds the case quantity received: (" + caseQtyOnOrder + ") for this order."
              response.status(400).send(errorMsg);
              log.pushToLog(storeNumber, userName, errorMsg);
              return;
          }
  
          if(caseOrOnlyClaimed == 'O' && quantityClaimed > extendedPackSizeOnOrder){
            errorMsg = "Quantity on Claim (" + quantityClaimed + ") exceeds the item quantity received: (" + extendedPackSizeOnOrder + ") for this order."
            response.status(400).send(errorMsg);
            log.pushToLog(storeNumber, userName, errorMsg);
            return;
          }
  
          var tobaccoTax = 0; // tax if tobacco. we can stop passing this
          var perCaseCost = FicheD01.array[0]['fdsell']; //per case cost
          var buyer = getBuyerNumber(itemNumber); //buyer
  
          // -----------------NOTE: SETTING THIS FLAG TO "P" WILL CAUSE THE 400 TO AUTO PROCESS! lEAVE BLANK UNTIL WE ARE READY FOR PRODUCTION!-------------------------------------------
          var flag = 'P';
          
          if(request.body.ClaimType == 2 && (request.body.isRestockFee == "Y" || request.body.isRestockFee == "y")){ 
            var restockCharge = extendedCost * 0.05; // If Claim is 2 (return) the restock fee is 5% of the total * qty ordered
          }else{
            restockCharge = 0;//restock charge 7s 2
          }
  
          var documentNumber = 0; //document no. created when the file is read on 400.
          var sclUser = request.body.UserKeyingClaim; //user
          var assignment = formatDate();
          var container = 0;
          var callingName = request.body.CallingName;
          // /*
          // TURN ON FOR TESTING!!!!!!
          console.log(
            'recordID: ' + recordID,
            'claimNumber: ' + claimNumber,
            'claimDate: ' + claimDate,
            'packSize: ' + packSize,
            'STNUM: ' + STNUM,
            'itemNumber: ' + itemNumber,
            'claimType: ' + claimType, //if this is a 2 then it is a return and we may charge a restock fee (SCLSTK)
            'creditType: ' + creditType,
            'department: ' + department,
            'ledgerNumber: ' + ledgerNumber,
            'invNumOfOrder: ' + invNumOfOrder,
            'orderDate: ' + orderDate,
            'slot: ' + slot,
            'pickCode: ' + pickCode,
            'slot1: ' + slot1,
            'slot3: ' + slot3,
            'quantityClaimed: ' + quantityClaimed,
            'caseOrOnly: ' + caseOrOnlyClaimed,
            'tobaccoTax: ' + tobaccoTax,
            'perCaseCost: ' + perCaseCost,
            'extendedCost: ' + extendedCost,
            'flag: ' + flag,
            'buyer: ' + buyer,
            'restockCharge: ' + restockCharge,
            'documentNumber: ' + documentNumber,
            'extendedRetail: ' + extendedRetail,
            'sclUser: ' + sclUser,
            'assignment: ' + assignment,
            'container: ' + container,
            'callingName: ' + callingName
          );
          // */
  // /*
  // TURN OFF FOR TESTING!!!!!
          var sqlquery =
            'insert into PROFRONT.ENTCLM_CALLIN (' +
            'sclmrc, ' +
            'sclmno, ' +
            'sclmdt, ' +
            'sclmst, ' +
            'sclmit, ' +
            'sclmty, ' +
            'sclmcr, ' +
            'sclmld, ' +
            'sclmdp, ' +
            'sordin, ' +
            'sorddt, ' +
            'sordpc, ' +
            'sords1, ' +
            'sords3, ' +
            'sclmqt, ' +
            'sclmco, ' +
            'sclmtx, ' +
            'sclmcs, ' +
            'sclmex, ' +
            'sclmfl, ' +
            'sclmby, ' +
            'sclstk, ' +
            'scldoc, ' +
            'sclret, ' +
            'sclusr, ' +
            'sclasn, ' +
            'sclcnt, ' +
            'sclnam' +
            ') VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) with None';
          try {
            pjs.query(sqlquery, [
              recordID,
              claimNumber,
              claimDate,
              STNUM,
              itemNumber,
              claimType,
              creditType,
              ledgerNumber,
              department,
              invNumOfOrder,
              orderDate,
              pickCode,
              slot1,
              slot3,
              quantityClaimed,
              caseOrOnlyClaimed,
              tobaccoTax,
              perCaseCost,
              extendedCost,
              flag,
              buyer,
              restockCharge,
              documentNumber,
              extendedRetail,
              sclUser,
              assignment,
              container,
              callingName,
            ]);
            if (sqlstate >= '02000') {
              errorMsg = 'SQL error during write. SQLstate: ' + sqlstate;
              log.pushToLog(storeNumber, userName, errorMsg);
              console.log(
                'Item Code ' +
                  itemNumber +
                  ' Store ' +
                  STNUM +
                  ' Error Message: ' +
                  errorMsg
              );
              response.status(400).send(errorMsg);
              return;
            }
          } catch (err) {
            errorMsg = 'Sql Insert catch error during Write.';
            log.pushToLog(storeNumber, userName, errorMsg);
            console.log(
              'Item Code ' +
                itemNumber +
                ' Store ' +
                STNUM +
                ' Error Message: ' +
                errorMsg
            );
            response.status(400).send(errorMsg);
            return;
          }
  // */
          successMsg = 'Entry Claim submitted.';
          log.pushToLog(storeNumber, userName, successMsg);
          console.log(`Item Code ${itemNumber} updated for Store ${STNUM} for for Claim Date ${claimDate}.`);
          ProfrontTxtlog.pushToTxTLog("writeEntClmAPI.V2.1",userName, successMsg);
          response.status(200).send(successMsg);
        }
      }
    }

    function goBadMethod() {
      var error = 'no method received';
      response.status(400).send(error);
    }
  }
  exports.run = writeEntClmInfo;
  
  //Get todays date for claim sumbission date. -------------------------------------------------------------------------------------------
  function formatDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
  
    if (dd < 10) {
      dd = '0' + dd;
    }
  
    if (mm < 10) {
      mm = '0' + mm;
    }
  
    today = `${yyyy}${mm}${dd}`;
    today = today.substring(2, 8);
    // console.log('today ' + today);
    return today;
  }
  
  //Get BUYER from INVMST based on Item Number. --------------------------------------------------------------------------------
  function getBuyerNumber(y) {
    var itemNumber = y;
    var recordSet;
    sqlquery = 'select buyer from PWAFIL.INVMST where ITMCDE = ?';
    recordSet = pjs.query(sqlquery, [itemNumber]);
    buyer = '';
    if (recordSet.length > 0 && recordSet != '') buyer = recordSet[0]['buyer'];
    return buyer;
  }
  
  function padLeadingZeros(num, size) {
    var s = num + '';
    while (s.length < size) s = '0' + s;
    return s;
  }
  