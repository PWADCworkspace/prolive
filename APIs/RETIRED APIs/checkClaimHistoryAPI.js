async function checkClaimHistory(request, response) {

  const directPathToProlog = "../../Prologging/";
  let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

  var sqlquery;
  var recordSet;
  var parmListArray = [];
  var errorMsg;
  var logMessage;
  claimNumber = request.params.id;
  let originalDataForLogging = "ClaimNumber: " + claimNumber;

  if (!claimNumber || claimNumber <= 0) {
    errorMsg = 'Claim Number required.';
    logMessage = 'No valid Claim Number sent to checkClaimHistory.';
    await ProfrontTxtlog.pushToTxTLog("checkClaimHistoryAPI", "automated call from eClaims", originalDataForLogging, logMessage);
    response.status(400).send(errorMsg);
    return;
  } else {
    sqlquery =
      'select HCLMDT, HCLMNO, HCLDOC from CLMHST where HCLMNO = ? and hclmdt > 20221101 order by HCLMDT desc';
    try {
      recordSet = pjs.query(sqlquery, [claimNumber]);

      if (!recordSet || recordSet.length <= 0){
        parmListArray.push({
          Status:'No record found for Claim Number: ' + claimNumber + '. If it is a new record it may be processed later today.',
          claimNumber: claimNumber
        });
        response.status(400).send(parmListArray);
        return;
      }

      if (sqlstate >= '02000') {
        errorMsg = 'SQL error during Claim History File querry. SQLstate: ' + sqlstate;
        parmListArray.push({
          Status:'SQL error during Claim History File querry. SQLstate: ' + sqlstate,
          claimNumber: claimNumber
        });
        await ProfrontTxtlog.pushToTxTLog("checkClaimHistoryAPI", "automated call from eClaims", originalDataForLogging, errorMsg);
        response.status(400).send(parmListArray);
        return;
      }

    } catch (err) {
      errorMsg = 'Sql catch error during Survey File Write. Check Claim Number and try again.';
      await ProfrontTxtlog.pushToTxTLog("checkClaimHistoryAPI", "automated call from eClaims", originalDataForLogging, errorMsg);

      parmListArray.push({
        Status:'Sql catch error during Survey File Write. Check Claim Number and try again.',
        claimNumber: claimNumber
      });
      
      response.status(400).send(parmListArray);
      return;
    }

    if (recordSet.length < 1) {
      parmListArray.push({
        Status: 'No Record Found in Claim History File.',
        claimNumber: claimNumber,
      });
      await ProfrontTxtlog.pushToTxTLog("checkClaimHistoryAPI", "automated call from eClaims", originalDataForLogging, 'No Record Found in Claim History File.');
      response.status(400).json(parmListArray);
      return;
    } else {
      parmListArray.push({
        DocumentNumber: recordSet[0]['hcldoc'], //assigned claim number.
        ClaimDate: recordSet[0]['hclmdt'], //date the claim number was created.
        claimNumber: claimNumber, //document number passed from front end.
      });
      // console.log(documentNumber);
      await ProfrontTxtlog.pushToTxTLog("checkClaimHistoryAPI", "automated call from eClaims", originalDataForLogging, parmListArray);
      response.status(200).json(parmListArray);
      return;
    }
  }
}
exports.run = checkClaimHistory;
