/*
Author: Samuel Gray

Date: 6/30/2023


The purpose of the program is to handle the approval of Nonstandardized claims. Specifically, it aims to gather all the required incoming data and identify any missing information
by throwing an error. Additionally, if an item number or invoice number is provided, the program searches through different files or calls various programs to retrieve additional 
item or invoice details. Once all the necessary information is obtained, it is written to a production table for further processing of the claims.
*/

/* Incoming INFO:
{
  "ClaimNumber": 3121,
  "StoreNumber": 700,
  "ItemNumber": 1,
  "Department": "P",
  "ClaimType": 8,
  "InvoiceNumber": 0,
  "QuantityOnClaim": 20,
  "isRestockFee": "N",
  "Buyer" : "J",
  "ApproverFirstName": "Amy",
  "ApproverLastName": "Parker",
  "BuyerFirstName": "Justin",
  "BuyerLastName": "Hartley",
  "StoreFirstName": "Gray",
  "StoreLastName": "Samuel",
  "ClaimAmount": 140.8,
  "ReasonForClaim": "Wrong Pricing."
  
}

claim type: 8 or 9

if type = 8 credit type (SCLMCR) = "C" for credit 
if type = 9 credit type (SCLMCR) = "" for charge

if type 9 restock fee = 0
*/

const addSubtractDate = require("add-subtract-date");
const dayjs = require("dayjs");

async function createMiscClaim(request, response) {

  //Set pathing for Apps.
  const directPathToProlive = "../../prolive/APPs/";
  const directPathToProtest = "../../protest/APPs/";
  const directPathToProlog = "../../prologging/";

  let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");
  let getLedgerNumber = pjs.require(directPathToProlive + "LedgerNumberAPP.js");

  //Defining the length of the incoming variables to correspond to the length for them in the database -------------------------------------------------------------------------------
  pjs.define("claimNumber", {
    type: "packed decimal",
    length: 5,
    decimals: 0
  });

  pjs.define("storeNumber", {
    type: "packed decimal",
    length: 3,
    decimals: 0
  });

  pjs.define("itemNumberCheckDigit", {
    type: "packed decimal",
    length: 6,
    decimals: 0
  });

  pjs.define("department", {
    type: "char",
    length: 1
  });

  pjs.define("claimType", {
    type: "packed decimal",
    length: 1,
    decimals: 0
  });

  pjs.define("invoiceNumber", {
    type: "packed decimal",
    length: 5,
    decimals: 0
  });

  pjs.define("quantityClaimed", {
    type: "packed decimal",
    length: 4,
    decimals: 0
  });

  pjs.define("caseOrOnlyClaimed", {
    type: "char",
    length: 1
  });

  pjs.define("isRestockFee", {
    type: "packed decimal",
    length: 7,
    decimals: 2
  });

  pjs.define("buyer", {
    type: "char",
    length: 1
  });

  pjs.define("claimAmount", {
    type: "packed decimal",
    length: 7,
    decimals: 2
  });

  pjs.define("extendedRetail", {
    type: "packed decimal",
    length: 7,
    decimals: 2
  });

  pjs.define("invNumOfOrder", {
    type: "packed decimal",
    length: 4,
    decimals: 0
  });

  pjs.define("storeCost", {
    type: "packed decimal",
    length: 8,
    decimals: 4
  });

  pjs.define("invoiceDate", {
    type: "packed decimal",
    length: 8,
    decimals: 0
  });

  // pjs.define("packSize", {
  //   type: "char",
  //   length: 10,
  // });

  pjs.define("pickCode", {
    type: "char",
    length: 1
  });

  pjs.define("slot1", {
    type: "char",
    length: 2
  });

  pjs.define("slot3", {
    type: "char",
    length: 3
  });

  //Define incomming variables ---------------------------------------------------------------------------------------------------------------------------------------------------------
  const claimNumber = request.body.ClaimNumber; //D
  const storeNumber = request.body.StoreNumber; //D
  const itemNumberCheckDigit = request.body.ItemNumber; //D
  const department = request.body.Department; //D
  const claimType = request.body.ClaimType; //D
  const invoiceNumber = request.body.InvoiceNumber; //D
  const quantityClaimed = request.body.QuantityOnClaim; //D
  const caseOrOnlyClaimed = request.body.CaseOrOnly; //D
  // const caseOrOnlyClaimed = 'C'; //TEMP FOR TESTING
  const isRestockFee = request.body.isRestockFee; //"Y" "N" //D
  const buyer = request.body.Buyer; //D
  const approverFirstName = request.body.ApproverFirstName.trim(); // Approver First Name
  const approverLastName = request.body.ApproverLastName.trim(); // Approver Last Name
  const buyerFirstName = request.body.BuyerFirstName.trim(); // Buyer first Name
  const buyerLastName = request.body.BuyerLastName.trim(); //Buyer last name
  const storeFirstName = request.body.StoreFirstName.trim(); // Buyer first Name
  const storeLastName = request.body.StoreLastName.trim();
  claimAmount = request.body.ClaimAmount;
  const reasonForClaim = request.body.ReasonForClaim;

  //Concatenating users first and last name
  const approverUserName = approverFirstName + " " + approverLastName;
  const buyerUserName = buyerFirstName + " " + buyerLastName;
  const storeUserName = storeFirstName + " " + storeLastName;

  // Setting flag to be nothing if store 700 is calling this API ----------------------------------------------------------------------------------------------------------------------
  let flag = "P";
  // let flag = "";
  if (storeNumber == 700) flag = "";

  // Setting variables that are not incoming data to 0 or string ---------------------------------------------------------------------------------------------------------------------------

  //initializing Item Information Variables
  pickCode = "";
  slotAisle = "";
  slotLevel = "";
  slot1 = "";
  slot3 = "";

  //initializing Invoice Information Variables
  extendedRetail = 0.0;
  invNumOfOrder = 0;
  storeCost = 0.0;
  invoiceDate = "";
  fdmulti = 0;
  fdsrp = 0;
  calculatedExtendedRetail = 0.0;
  let packSize = "";
  let trimmedPackSize = "";
  let pS = "";

  const recordID = "S";
  const tobaccoTax = 0;
  const documentNumber = 0;
  const container = 0;
  let ledgerNumber = 0;

  let errorMsg;
  let gatheredClaimInfoArray = [];
  let findInvoiceInfo;

  let assignment = dayjs().format("YYMMDD");
  let claimDate = dayjs().format("YYMMDD");

  let restockCharge = 0;
  if (isRestockFee == "Y" || isRestockFee == "y") {
    restockCharge = claimAmount * 0.05;
  }

  let creditType = "";
  if (claimType == 8) creditType = "C";

  if (itemNumberCheckDigit != 0) {
    var itemNumber = Math.floor(itemNumberCheckDigit / 10);
  } else {
    itemNumber = itemNumberCheckDigit;
  }

  //NOTE: 5/18/23 check CLMHST for misc claims and see if we need to carry extendedRetail. We may need it for displaying on recap data.
  const originalDataForLogging =
    "Claim Number: " +
    claimNumber +
    ", Store Number: " +
    storeNumber +
    ", Item Number with Check: " +
    itemNumberCheckDigit +
    ", Item Number: " +
    itemNumber +
    ", Department Number: " +
    department +
    ", Claim Type: " +
    claimType +
    ", Invoice Number: " +
    invoiceNumber +
    ", Quantity Claimed: " +
    quantityClaimed +
    ", Case Or Only Claimed: " +
    caseOrOnlyClaimed +
    ", Claim Amount: " +
    claimAmount +
    ", isRestockFee: " +
    isRestockFee +
    ", Buyer Number: " +
    buyer +
    ", Approver First Name: " +
    approverFirstName +
    ", Approver Last Name: " +
    approverLastName +
    ", Buyer First Name: " +
    buyerFirstName +
    ", Buyer Last Name: " +
    buyerLastName +
    ", Store First Name: " +
    storeFirstName +
    ", Store Last Name: " +
    storeLastName +
    ", Reason For Claim: " +
    reasonForClaim +
    ", Flag: " +
    flag;

  // console.log("Original Data: " + originalDataForLogging);

  //Setting Prod or Dev tables based on port---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  const currentPort = profound.settings.port;
  console.log(currentPort);

  let ENTCLMTable = "";

  if (currentPort === 80) {
    ENTCLMTable = "ENTCLM_CALLIN";
    console.log(`Running writeNonStandardClaimAPI.V1 in PROD with table: ${ENTCLMTable}.`);
  } else {
    ENTCLMTable = "ENTCLMDEV";
    console.log(`Running writeNonStandardClaimAPI.V1 in DEV with table: ${ENTCLMTable}.`);
  }

  // ERROR CHECKS FOR MISSING DATA COMMING IN---------------------------------------------------------------------------

  //if no claim number is specified
  if (!claimNumber) {
    errorMsg = "Claim Number is required";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNonStandardClaimAPI.V1",
      approverUserName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  //If no store number is specified
  if (!storeNumber) {
    errorMsg = "Store Number is required";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNonStandardClaimAPI.V1",
      approverUserName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  //If the claim type is not equal to 8(credit) or 9(for charge)
  if (!claimType || (claimType !== 8 && claimType !== 9)) {
    errorMsg = "Claim Type is required and must be 8 or 9.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNonStandardClaimAPI.V1",
      approverUserName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  //If the claimed quantity is not greater than 0
  if (!quantityClaimed || quantityClaimed == 0) {
    errorMsg = "QuantityOnClaim needs to be greater than 0.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNonStandardClaimAPI.V1",
      approverUserName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  //if no Case or Only specified
  if (!caseOrOnlyClaimed) {
    errorMsg = "Case or Only needs to be defined";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNonStandardClaimAPI.V1",
      approverUserName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  // if no approver first name or last name
  if (!approverFirstName || !approverLastName) {
    errorMsg = "Approver user first and last name is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNonStandardClaimAPI.V1",
      approverUserName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  // if no buyer first name or last name
  if (!buyerFirstName || !buyerLastName) {
    errorMsg = "Buyer user first and last name is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNonStandardClaimAPI.V1",
      approverUserName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  // if no store first name or last name
  if (!storeFirstName || !storeLastName) {
    errorMsg = "Store user first and last name is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNonStandardClaimAPI.V1",
      approverUserName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  // If no claim amount is specified
  if (!claimAmount) {
    errorMsg = "Claim amount is required";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNonStandardClaimAPI.V1",
      approverUserName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  //If buyer number is greater than one
  if (buyer.length > 1) {
    errorMsg = "Buyer number can not contain more than one value.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNonStandardClaimAPI.V1",
      approverUserName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  // END ERROR CHECKS FOR MISSING DATA COMMING IN-----------------------------------------------------------------------------------------------------------------------------------------
  //Logic for finding item number information ---------------------------------------------------------------------------------

  if (itemNumber > 0) {
    //Query if the item number is found

    const queryForItemInfo =
      "select  dept, picode, slotai, slotlv, slotno, buyer from INVMST where itmcde = ?";

    try {
      findItemInformation = await pjs.query(queryForItemInfo, [itemNumber]);
      // console.log(findItemInformation);
    } catch (err) {
      errorMsg = err;
      //console.log(errorMsg);
    }

    if (findItemInformation.length > 0) {
      pickCode = findItemInformation[0]["picode"];
      slotAisle = findItemInformation[0]["slotai"];
      slotLevel = findItemInformation[0]["slotlv"];
      slot1 = slotAisle + slotLevel;
      slot3 = findItemInformation[0]["slotno"];
      // console.log(findItemInformation);
    }

    ledgerNumber = await getLedgerNumber.getLedgerNumber(department);

    if (ledgerNumber.Message) {
      await ProfrontTxtlog.pushToTxTLog(
        "writeNonStandardClaimAPI.V1",
        approverUserName,
        originalDataForLogging,
        ledgerNumber.Message
      );
      response.status(400).send(ledgerNumber.Message);
      return;
    }

    ledgerNumber = ledgerNumber.LedgerNumber;
  }
  //Logic for finding invoice information ---------------------------------------------------------------------------------
  if (invoiceNumber > 0) {
    const queryInvoiceInfo =
      "select fdpksz, fdqtyo, fdsell, fdecst, fddate, fdmult, fdsrp from FICHED01 where fdinv = ? and fdstor = ? and fditem = ?";

    try {
      findInvoiceInfo = pjs.query(queryInvoiceInfo, [
        invoiceNumber,
        storeNumber,
        itemNumber
      ]);
    } catch (err) {
      errorMsg = err;
      console.log(errorMsg);
    }

    console.log(findInvoiceInfo);

    if (findInvoiceInfo.length > 0) {
      invNumOfOrder = findInvoiceInfo[0]["fdqtyo"];
      storeCost = findInvoiceInfo[0]["fdsell"];
      extendedRetail = findInvoiceInfo[0]["fdecst"];
      invoiceDate = findInvoiceInfo[0]["fddate"];
      invoiceDate = dayjs(invoiceDate).format("YYMMDD");
      fdmulti = findInvoiceInfo[0]["fdmult"];
      fdsrp = findInvoiceInfo[0]["fdsrp"];
      packSize = findInvoiceInfo[0]["fdpksz"];
      let packDash = packSize.indexOf("/");
      pS = packSize.slice(0, packDash);
      pS = pS.trim();
      pS = parseInt(pS);
      trimmedPackSize = pS;

      if (fdmulti != 0) {
        calculatedExtendedRetail =
          fdsrp * 100 / fdmulti * trimmedPackSize / 100;
      } else {
        calculatedExtendedRetail = fdsrp * 100 * trimmedPackSize / 100;
      }

      extendedRetail = calculatedExtendedRetail;
    }
  }
  //Console Log Results  ---------------------------------------------------------------------------------
  // /*
  // TURN ON FOR TESTING!!!!!!
  console.log(
    "recordID: " + recordID, //done
    "claimNumber: " + claimNumber, //done
    "claimDate: " + claimDate, //done
    "packSize: " + trimmedPackSize, //can grab on INVMST D
    "storeNumber: " + storeNumber, //done
    "itemNumber: " + itemNumber, //testing
    "claimType: " + claimType, //done
    "creditType: " + creditType, //done
    "department: " + department, //done
    "ledgerNumber: " + ledgerNumber, //done
    "invNumOfOrder: " + invNumOfOrder, //must get from FICHED01. For type 5 (not on INV.) use claim number D
    "orderDate: " + invoiceDate, //must get from FICHED01 D
    // "slot: " + slot, //can grab on INVMST D
    "pickCode: " + pickCode, //can grab on INVMST D
    "slot1: " + slot1, //can grab on INVMST D
    "slot3: " + slot3, //can grab on INVMST D
    "quantityClaimed: " + quantityClaimed, //done
    "caseOrOnlyClaimed: " + caseOrOnlyClaimed, //done
    "tobaccoTax: " + tobaccoTax, //done
    "storeCost: " + storeCost, //must get from FICHED01.D
    "claimAmount: " + claimAmount, //must get from FICHED01???
    "flag: " + flag, //done
    "buyer: " + buyer, //done
    "restockCharge: " + restockCharge, //done
    "documentNumber: " + documentNumber, //done
    "extendedRetail: " + extendedRetail, //must get from FICHED01.D
    "assignment: " + assignment, //done
    "container: " + container, //done
    "approverName: " + approverUserName, //done
    "buyerName: " + buyerUserName,
    "storeName: " + storeUserName,
    "reasonForClaim: " + reasonForClaim
  );

  // SQL Query to Insert Data into ENTCLM --------------------------------------------------------------------------------

  // // /*
  // TURN OFF FOR TESTING!!!!! Writing to ENTCLM File
  let sqlquery =
    `insert into ${ENTCLMTable} (` +
    "sclmrc, " +
    "sclmno, " +
    "sclmdt, " +
    "sclmst, " +
    "sclmit, " +
    "sclmty, " +
    "sclmcr, " +
    "sclmld, " +
    "sclmdp, " +
    "sordin, " +
    "sorddt, " +
    "sordpc, " +
    "sords1, " +
    "sords3, " +
    "sclmqt, " +
    "sclmco, " +
    "sclmtx, " +
    "sclmcs, " +
    "sclmex, " +
    "sclmfl, " +
    "sclmby, " +
    "sclstk, " +
    "scldoc, " +
    "sclret, " +
    "sclusr, " +
    "sclasn, " +
    "sclcnt, " +
    "sclnam" +
    ") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) with None";
  try {
    pjs.query(sqlquery, [
      recordID,
      claimNumber,
      claimDate,
      storeNumber,
      itemNumber,
      claimType,
      creditType,
      ledgerNumber,
      department,
      invNumOfOrder,
      invoiceDate,
      pickCode,
      slot1,
      slot3,
      quantityClaimed,
      caseOrOnlyClaimed,
      tobaccoTax,
      storeCost,
      claimAmount,
      flag,
      buyer,
      restockCharge,
      documentNumber,
      extendedRetail,
      approverFirstName,
      assignment,
      container,
      approverLastName
    ]);
    if (sqlstate >= "02000") {
      errorMsg = "SQL error during write. SQLstate: " + sqlstate;
      await ProfrontTxtlog.pushToTxTLog(
        "writeNonStandardClaimAPI.V1",
        approverUserName,
        originalDataForLogging,
        errorMsg
      );
      response.status(400).send(errorMsg);
      return;
    }
  } catch (err) {
    errorMsg = "Sql Insert catch error during Write to entclm.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNonStandardClaimAPI.V1",
      approverUserName,
      originalDataForLogging,
      errorMsg + ":" + err
    );
    response.status(400).send(errorMsg);
    return;
  }
  // */

  //Writing to CMENTSUPF File
  //Getting Current Date and Formatting it for SQL Table---
  const dateTime = new Date();
  writeDate = dayjs(dateTime).format("YYYY-MM-DD");
  writeTime = dayjs(dateTime).format("h.mm ");

  longClaimNum = claimNumber;

  let sqlquery2 =
    "insert into cmentsupf (" +
    "csclaim, " + //1
    "csclaiml," + //2
    "csstore," + //3
    "csdept," + //4
    "csctype," + //5
    "cscredty," + //6
    "csitem," + //7
    "csinv#," + //8
    "csqty," + //9
    "csrestock," + //10
    "csbuyer," + //11
    "csapprover," + //12
    "csbuyernam," + //13
    "csstoreusr," + //14
    "csclaimamt," + //15
    "csclaimrsn," + //16
    "csdate," + //17
    "cstime" + //18
    ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) with NONE";

  try {
    pjs.query(sqlquery2, [
      claimNumber,
      longClaimNum, //long claim num
      storeNumber,
      department,
      claimType,
      creditType,
      itemNumber,
      invoiceNumber,
      quantityClaimed,
      isRestockFee,
      buyer,
      approverUserName,
      buyerUserName,
      storeUserName,
      claimAmount,
      reasonForClaim,
      writeDate,
      writeTime
    ]);
    if (sqlstate >= "02000") {
      errorMsg = "SQL error during write. SQLstate: " + sqlstate;
      await ProfrontTxtlog.pushToTxTLog(
        "writeNonStandardClaimAPI.V1",
        approverUserName,
        //originalDataForLogging,
        errorMsg
      );
      response.status(400).send(errorMsg);
      return;
    }
  } catch (err) {
    errorMsg = "Sql Insert catch error during Write to cmentsupf.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNonStandardClaimAPI.V1",
      approverUserName,
      //originalDataForLogging,
      errorMsg + ":" + err
    );
    console.log(err);
    response.status(400).send(errorMsg);
    return;
  }

  let successMsg = "Entry Claim submitted.";
  await ProfrontTxtlog.pushToTxTLog(
    "writeNonStandardClaimAPI.V1",
    approverUserName,
    originalDataForLogging,
    successMsg
  );
  console.log(
    `Item Code ${itemNumber} updated for Store ${storeNumber} for for Claim Date ${claimDate}.`
  );
  response.status(200).send(successMsg);
}
exports.run = createMiscClaim;
