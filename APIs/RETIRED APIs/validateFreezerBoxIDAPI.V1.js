/*
    Author: Samuel Gray
    Date: 12/01/2023

    Purpose: 
    This API serves as an intermediate step between the 'getFBID' API and the 'createFBTote' API. It is used to validate whether the freezer box ID is present 
    in the 'FBCLAIMLOG' Table. This API primarily handles the FB IDs that store users input into eClaims. Its main purpose is to ensure that the FB ID exists in the table
    before eClaims proceeds to create a claim for it.

    
    // validateFreezerBoxIDAPI Version 1
    
    app.post("/v1/validateFreezerBox",
    profoundjs.express("prolive/APIs/validateFreezerBoxIDAPI.V1"));

    eClaims URL call: v1/validateFreezerBox

    Incoming Data: 
    {
        "ItemNumber" : 30 or 70
        "FreezerBoxID": 1006,
    }

    Change Log:
    JIRA TICKET PROCLM-45: 
      -- Added in dev table and adjusted sql queries to use template literals using the claimLogTable.
*/

async function validateFBID(request, response) {

  console.log("validateFreezerBoxIDAPI is running...");

  const directPathToProlog = "../../prologging/";
  let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

  //Initializing Variables-------------------------------------------------------------
  var returnObj = {};
  returnObj.freezerBoxId = 0;
  returnObj.error = "";

  //Defining Incoming Variables--------------------------------------------------------
  const itemNumber = request.body.ItemNumber; 
  const freezerBoxID = request.body.FreezerBoxID; 

  const incomingDataForLogging = `Item Number: ${itemNumber}, Freezer Box ID: ${freezerBoxID}`;
  console.log("Incoming Data: " + incomingDataForLogging);

  const currentPort = profound.settings.port;
  console.log(currentPort);

  let claimLogTable = "";

  if (currentPort === 80) { 
    console.log("Running validateFreezerBoxIDAPI in PROD...");
    claimLogTable = "FBCLAIMLOG";
  } else {
    console.log("Running validateFreezerBoxIDAPI in DEV...");
    claimLogTable = "FBCLMLOGCY";
  }

  //Error Checks for Incoming Data-----------------------------------------------------
  
  if (itemNumber !== 70 && itemNumber !== 30) {
    returnObj.error = "An Item Number of either 70 (Freezer Box) or 30 (Tote) is required.";

    await ProfrontTxtlog.pushToTxTLog(
      "validateFreezerBoxIDs.V1",
      "automated call during FB claim processing",
      incomingDataForLogging,
      returnObj.error
    );

    response.status(400).send(returnObj);
    return;
  }
  
  if (!freezerBoxID && itemNumber == 70) {
    returnObj.error = "Freezer Box ID is required.";

    await ProfrontTxtlog.pushToTxTLog(
      "validateFreezerBoxIDs.V1",
      "automated call during FB claim processing",
      incomingDataForLogging,
      returnObj.error
    );

    response.status(400).send(returnObj);
    return;
  }

  // Freezer box Logic------------------------------------------------------------------------------------------------------
  if (itemNumber == 70) {
    //Grabbing fb id based on incoming fb id where there is no credit given and no claim information
    try {
      var result = pjs.query(
        `SELECT fbcfbid FROM ${claimLogTable} WHERE fbcfbid = ${freezerBoxID} and fbccrdf = '' and fbcclmd = 0`
      );
    } catch (err) {
      console.log("Validate FB ID API Sql Select catch error: " + err);

      returnObj.error = `CATCH SQL Select error during fetch for FB ID: ${freezerBoxID} in validateFreezerBoxIDs.V1`;

      await ProfrontTxtlog.pushToTxTLog(
        "validateFreezerBoxIDs.V1",
        "automated call during FB claim processing",
        incomingDataForLogging,
        returnObj.error + " " + err
      );

      response.status(400).send(returnObj);
      return;
    }

    //If there are no open freezer boxes from query then return 400 status code
    if (result.length === 0) {
      let errorMesage = `Unable to locate freezer box id: ${freezerBoxID}. Please contact Amy Parker.`;
      console.log("Validate FB ID API Error: " + errorMesage);

      returnObj.error = errorMesage;

      await ProfrontTxtlog.pushToTxTLog(
        "validateFreezerBoxIDs.V1",
        "automated call during FB claim processing",
        incomingDataForLogging,
        returnObj.error
      );

      response.status(400).send(returnObj);
      return;
    }

    //If there is an open fb id then return 200 response
    const fbID = result[0]["fbcfbid"];

    console.log(
      `Incoming Freezer Box ID: ${freezerBoxID} vs. FBCLAIMLOG Freezer Box ID: ${fbID} `
    );

    returnObj.freezerBoxId = fbID;
  }

  await ProfrontTxtlog.pushToTxTLog(
    "validateFreezerBoxIDs.V1",
    "automated call during FB claim processing",
    incomingDataForLogging,
    returnObj.freezerBoxId
  );

  response.status(200).send(returnObj);
  return;
}
exports.run = validateFBID;