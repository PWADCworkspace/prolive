// V2.1 ADD PICK SELECTOR INFORMATION.

const addSubtractDate = require('add-subtract-date');
function getEntClmInfo(request, response) {
  var directPath = 'C:/profront/modules/';
  var FicheD01 = pjs.require( directPath + 'protest/APPs/FicheD01APP.js');
  var buyerName = pjs.require( directPath + 'protest/APPs/BuyerNameAPP.js');
  var EntClm = pjs.require( directPath + 'protest/APPs/EntClmAPP.js');
  var ClmHst = pjs.require( directPath + 'protest/APPs/ClmHstAPP.js');
  var log = pjs.require( directPath + 'prolive/APPs/PushToLogAPP.js');
  var selector = pjs.require( directPath + 'protest/APPs/PickSelectorAPPV2.js');
  var errorMsg;
  var ItemInfoListArray = [];
  var deliveryDate;
  var packSize;

  var invoiceNumber = request.body.invoiceNumber;
  var storeNumber = request.body.storeNumber;
  var itemNumberCheckDigit = request.body.itemNumber;
  var userName = request.body.UserKeyingClaim + " " + request.body.CallingName;
  var itemNumber;

  if (!invoiceNumber) {
    errorMsg = 'Invoice Number is required.';
    log.pushToLog(storeNumber, userName, errorMsg );
    response.status(400).send(errorMsg);
    return;

  } else if (!itemNumberCheckDigit) {
    errorMsg = 'Item Number is required.';
    log.pushToLog(storeNumber, userName, errorMsg );
    response.status(400).send(errorMsg);
    return;

  } else if (!storeNumber) {
    errorMsg = 'Store Number is required.';
    log.pushToLog(storeNumber, userName, errorMsg );
    response.status(400).send(errorMsg);
    return;

  } else {
    itemNumber = padLeadingZeros(itemNumberCheckDigit, 6);
    itemNumber = itemNumber.slice(0, 5);
    itemNumber = parseInt(itemNumber);

    // call getBuyerNameApp to get Buyer first and last name and apply it to buyerNum to add into returned array.
    buyerName = buyerName.getBuyerName(itemNumber);
    if (buyerName.errorMsg) {
      response.status(400).send(buyerName.errorMsg);
      log.pushToLog(storeNumber, userName, buyerName.errorMsg);
      return;
    }

    //get Pick Selector Info.
    selector = selector.getPickSelectorInfo(invoiceNumber, itemNumberCheckDigit, itemNumber);
    if (selector.ErrMessage) {
      selector.Message = "Selector not available.";
      //response.status(400).send(selector.ErrMessage);
      log.pushToLog(storeNumber, userName, selector.ErrMessage);
    }
    console.log(selector.Message);

    // Query FicheD01 to get Item info off of Invoice.
    FicheD01 = FicheD01.getFicheD01Info(invoiceNumber, storeNumber, itemNumber);
    //console.log(FicheD01.array);

    if (FicheD01.Message) {
      response.status(400).send(FicheD01.Message);
      log.pushToLog(storeNumber, userName, FicheD01.Message);
      return;
    }

    if (FicheD01.array > 0 && FicheD01.array[0]['fdqtys'] <= 0) {
      errorMsg = "Quantity was zero meaning Item was an Out and a Claim cannot be filed on such. GCIP"
      response.status(400).send(errorMsg);
      log.pushToLog(storeNumber, userName, errorMsg);
    } else {
      //get date and run formatDate function to +1 the day.
      var invoiceDate = FicheD01.array[0]['fddate'];
      deliveryDate = formatDate(invoiceDate);

      // get Pack Size and run formatPackSize to just get pack amount.
      packSize = FicheD01.array[0]['fdpksz'];
      packSize = formatPackSize(packSize);

      console.log('records gathered from FICHED01.');
      //console.log(ItemInfoListArray);
      console.log('running ENTCLM query.');

      // Query ENTCLM to see if a claim is currently filed.
      EntClm = EntClm.getEntClmInfo(invoiceNumber, storeNumber, itemNumber);
      //console.log(EntClm.array);

      // Query CLMHST to see if a claim has already been filed.
      ClmHst = ClmHst.getClmHstInfo(invoiceNumber, storeNumber, itemNumber);
      //console.log(ClmHst.array);

      // STOPHERE-----------------------------------------------------------------------------------------------------------------------------------------------------------
      if (ClmHst.array.length > 0) {
        console.log('Claim found in Claim History.');

        ItemInfoListArray.push({
          Selector: selector.Message,
          Buyer: buyerName.value,
          DeliveryDate: deliveryDate,
          PackSize: packSize,
          Status: 'CLAIM-ALREADY-EXISTS', //DO NOT CHANGE - MAGIC STRING
          OrderDate: ClmHst.array[0]['horddt'],
          InvoiceNumber: ClmHst.array[0]['hordin'],
          ItemNumber: ClmHst.array[0]['hclmit'],
          StoreNumber: ClmHst.array[0]['hclmst'],
          QuantityOnClaim: ClmHst.array[0]['hclmqt'],
          DocumentNumber: ClmHst.array[0]['hcldoc'],
          User: ClmHst.array[0]['hclusr'],
          CallingName: ClmHst.array[0]['hclnam'],
          CaseOrOnly: ClmHst.array[0]['hclmco'],
          Assignment: ClmHst.array[0]['hclasn'],
          Container: ClmHst.array[0]['hclcnt'],
        });

        //console.log(ItemInfoListArray);
        response.json(ItemInfoListArray);
        //log.pushToLog(storeNumber, userName, "Claim found in Claim History." );
      } else if (EntClm.array.length > 0) {
        console.log('Claim found in ENTCLM.');

        ItemInfoListArray.push({
          Selector: selector.Message,
          Buyer: buyerName.value,
          Status: 'CLAIM-ALREADY-EXISTS', //DO NOT CHANGE - MAGIC STRING
          RecordID: EntClm.array[0]['sclmrc'], // record ID 'S'
          ClaimNumber: EntClm.array[0]['sclmno'], //claim number
          ClaimDate: EntClm.array[0]['sclmdt'], //claim date
          StoreNumber: EntClm.array[0]['sclmst'], //store number 'session.StoreNum#'
          ItemNumber: EntClm.array[0]['sclmit'], //item Number '70'
          ClaimType: EntClm.array[0]['sclmty'], // claim type '2'
          CreditType: EntClm.array[0]['sclmcr'], //creit: 'C'
          LedgerNumber: EntClm.array[0]['sclmld'], //ledger number '213'
          Department: EntClm.array[0]['sclmdp'], //department 'J'
          InvoiceNumberOfOrder: EntClm.array[0]['sordin'], //invoice number of order '0'
          OrderDate: EntClm.array[0]['sorddt'], //order date'0'
          DeliveryDate: deliveryDate,
          PackSize: packSize,
          PickCode: EntClm.array[0]['sordpc'], //pick code ''
          Slot1: EntClm.array[0]['sords1'],
          Slot3: EntClm.array[0]['sords3'],
          QuantityOnClaim: EntClm.array[0]['sclmqt'], //Qty on claim
          CaseOrOnly: EntClm.array[0]['sclmco'], //case or only
          TobaccoTax: EntClm.array[0]['sclmtx'], // tax if tobacco
          perCaseCost: EntClm.array[0]['sclmcs'], //per case cost
          ExtendedCost: EntClm.array[0]['sclmex'], //extended cost
          Flag: EntClm.array[0]['sclmfl'], //flag
          Buyer: EntClm.array[0]['sclmby'], //buyer
          RestockCharge: EntClm.array[0]['sclstk'], //restock charge
          DocumentNumber: EntClm.array[0]['scldoc'], //document no.
          ExtendedRetail: EntClm.array[0]['sclret'], //extended retail
          UserKeyingClaim: EntClm.array[0]['sclusr'], //user
          Assignment: EntClm.array[0]['sclasn'], //assignment
          Container: EntClm.array[0]['sclcnt'], //container
          CallingName: EntClm.array[0]['sclnam'], //calling name
        });

        log.pushToLog(storeNumber, userName, "Claim found in ENTCLM." );
        response.json(ItemInfoListArray);
      } else if (FicheD01.array.length > 0) {
        console.log('Claim found in FicheD01.');

        ItemInfoListArray.push({
          Selector: selector.Message,
          Buyer: buyerName.value,
          Status: 'READY-TO-WRITE-NEW-CLAIM', //DO NOT CHANGE - MAGIC STRING
          ItemNumber: FicheD01.array[0]['fditem'],
          InvoiceNumber: FicheD01.array[0]['fdinv'],
          CheckDigit: FicheD01.array[0]['fdchkd'],
          InvoiceDate: FicheD01.array[0]['fddate'],
          DeliveryDate: deliveryDate,
          PackSize: packSize,
          Description: FicheD01.array[0]['fddesc'],
          Quantity: FicheD01.array[0]['fdqtys'],
          Department: FicheD01.array[0]['fddept'],
          StoreRetialPrice: FicheD01.array[0]['fdsrp'],
          StoreCost: FicheD01.array[0]['fdsell'],
          ExtendedCost: FicheD01.array[0]['fdecst'],
          CatchWeightInPounds: FicheD01.array[0]['fdcwlb'],
          Slot: FicheD01.array[0]['fdslot'],
        });

        log.pushToLog(storeNumber, userName, "Claim found in FicheD01." );
        response.json(ItemInfoListArray);
      } else {
        response.json('Error, no records found.');
        log.pushToLog(storeNumber, userName, 'Error, no records found. GCIA');
      }
    }
  }
}
exports.run = getEntClmInfo;

function formatDate(date) {
  let s = date.toString();
  let invoiceYear = s.substr(0, 4);
  let invoiceMonth = s.substr(4, 2);
  let invoiceDay = s.substr(6, 2);
  let invoiceDate = new Date(invoiceYear, invoiceMonth, invoiceDay);
  deliveryDate = addSubtractDate.subtract(invoiceDate, 1, 'month');
  deliveryDate = addSubtractDate.add(invoiceDate, 1, 'day');
  deliveryDate = deliveryDate.toISOString();
  deliveryDate = deliveryDate.replace(/-|T|:/g, '');
  deliveryDate = deliveryDate.substring(0, 8);
  deliveryDate = parseInt(deliveryDate);
  return deliveryDate;
}

function formatPackSize(packSize) {
  packSize = packSize.toString();
  var packDash = packSize.indexOf('/');
  packSize = packSize.slice(0, packDash);
  packSize = packSize.trim();
  packSize = parseInt(packSize);
  return packSize;
}

function padLeadingZeros(num, size) {
  var s = num + '';
  while (s.length < size) s = '0' + s;
  return s;
}