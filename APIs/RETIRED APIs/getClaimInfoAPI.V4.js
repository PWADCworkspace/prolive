// /* Incoming INFO:
// {
//     "invoiceNumber" : 00000,
//     "storeNumber" : 000,
//     "itemNumber" : 000000,
//     "UserKeyingClaim": "Tyler",
//     "CallingName": "Hobbs"
//     "CatchWeight": 00.00
// }
// */

// // VERSION V3.2-------------------------------------------------------------------------------------------------------------------------------------------------------
// // Including Catch Weight intake and calculations for extended cost.

// async function getEntClmInfo(request, response) {
//   console.log("getClaimInfoAPI.V4");

//   const directPathToProlive = "../../prolive/APPs/";
//   const directPathToProtest = "../../protest/APPs/";
//   const directPathToProlog = "../../Prologging/";

//   // let log = pjs.require(directPathToProlive + "PushToLogAPP.js");
//   let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");
//   let getClaimInfo = pjs.require(directPathToProlive + "getClaimInfo.V1.js");
//   let getCatchWeightClaimInfo = pjs.require(directPathToProlive + "getCatchWeightClaimInfo.V1.js");
//   let validateCheckDigitAPP = pjs.require(directPathToProtest + "ValidateCheckDigitAPP.js");

//   //Predefine variables.
//   let errorMsg;
//   const invoiceNumber = request.body.InvoiceNumber;
//   const storeNumber = request.body.StoreNumber;
//   const itemNumberCheckDigit = request.body.ItemNumber;
//   const userName = request.body.UserKeyingClaim + " " + request.body.CallingName;
//   // const incommingCatchWeight = request.body.catchWeight;
//   const incommingCatchWeight = request.body.CatchWeight;
//   const quantityOnClaim = request.body.QuantityOnClaim;
//   const caseOrOnly = request.body.CaseOrOnly;

//   const originalDataForLogging = 
//   'InvoiceNumber: '+ invoiceNumber +
//   ', StoreNumber: '+ storeNumber +
//   ', ItemNumber: '+ itemNumberCheckDigit +
//   ', IncomingCatchWeight: '+ incommingCatchWeight +
//   ', QuantityOnClaim: '+ quantityOnClaim +
//   ', CaseOrOnly: '+ caseOrOnly;

//   console.log(`Incoming Data: ${originalDataForLogging}`);

//   // ERROR CHECKS FOR MISSING DATA COMMING IN---------------------------------------------------------------------------
//   if (!invoiceNumber) {
//     errorMsg = "Invoice Number is required";
//     await ProfrontTxtlog.pushToTxTLog(
//       "getClaimInfoAPI.V4", 
//       userName, 
//       originalDataForLogging,
//       errorMsg
//     );
//     // log.pushToLog(storeNumber, userName, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }

//   if (!itemNumberCheckDigit) {
//     errorMsg = "Item Number is required";
//     await ProfrontTxtlog.pushToTxTLog(
//       "getClaimInfoAPI.V4", 
//       userName,
//       originalDataForLogging,
//       errorMsg
//     );
//     // log.pushToLog(storeNumber, userName, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }

//   let checkDigitValidation = await validateCheckDigitAPP.validateCheckDigit(itemNumberCheckDigit);

//   if (checkDigitValidation == false) {
//     errorMsg = "This item code does not seem to be valid, please double check that it is entered correctly.";
//     await ProfrontTxtlog.pushToTxTLog(
//       "getClaimInfoAPI.V4", 
//       userName, 
//       originalDataForLogging,
//       errorMsg
//     );
//     // log.pushToLog(storeNumber, userName, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }

//   if (!storeNumber) {
//     errorMsg = "Store Number is required";
//     await ProfrontTxtlog.pushToTxTLog(
//       "getClaimInfoAPI.V4", 
//       userName,
//       originalDataForLogging,
//       errorMsg
//     );
//     // log.pushToLog(storeNumber, userName, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }

//   if (invoiceNumber.toString().length > 5) {
//     errorMsg =
//       "Invoice Number: " +
//       invoiceNumber +
//       " may be incorrect. It should have a length of 5";
//     await ProfrontTxtlog.pushToTxTLog(
//       "getClaimInfoAPI.V4", 
//       userName, 
//       originalDataForLogging,
//       errorMsg
//     );
//     // log.pushToLog(storeNumber, userName, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }

//   if (!quantityOnClaim || quantityOnClaim == 0) {
//     errorMsg = "QuantityOnClaim needs to be greater than 0.";
//     await ProfrontTxtlog.pushToTxTLog(
//       "getCatchWeightClmInfoAPI.V1",
//       userName,
//       originalDataForLogging,
//       errorMsg
//     );
//     // log.pushToLog(storeNumber, userName, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }
 
//   if (incommingCatchWeight > 0) {
    
//     if(caseOrOnly == "O" || caseOrOnly == "o"){
//       errorMsg = "Catch Weight is only allowed for a case claim.";
//       await ProfrontTxtlog.pushToTxTLog(
//         "getCatchWeightClmInfoAPI.V1",
//         userName,
//         originalDataForLogging,
//         errorMsg
//       );
//       // log.pushToLog(storeNumber, userName, errorMsg);
//       response.status(400).send(errorMsg);
//       return;
//     }

//     if(quantityOnClaim > 1){
//       errorMsg = "Quantity on Claim must be 1 for Catch Weight Items.";
//       await ProfrontTxtlog.pushToTxTLog(
//         "getCatchWeightClmInfoAPI.V1",
//         userName,
//         originalDataForLogging,
//         errorMsg
//       );
//       // log.pushToLog(storeNumber, userName, errorMsg);
//       response.status(400).send(errorMsg);
//       return;
//     }

//     console.log(`Calling Catch Weight Claim Info Program.`);
//     let gatheredClaimInfo = await getCatchWeightClaimInfo.getCatchWeightClaimInfo(
//       storeNumber,
//       invoiceNumber,
//       itemNumberCheckDigit,
//       userName,
//       incommingCatchWeight,
//       quantityOnClaim
//     );

//     if(gatheredClaimInfo.error == ""){ 
//       await ProfrontTxtlog.pushToTxTLog(
//         "getClaimInfoAPI.V4",
//         userName,
//         originalDataForLogging,
//         `Success Status:${gatheredClaimInfo.array[0]['Status']}`
//       );
//       // console.log(gatheredClaimInfo);
//       response.json(gatheredClaimInfo.array);
//       return;
//     } else {
//       await ProfrontTxtlog.pushToTxTLog(
//         "getClaimInfoAPI.V4",
//         userName, 
//         originalDataForLogging,
//         `Error Status:${gatheredClaimInfo.error}`
//       );
//       // console.log(gatheredClaimInfo);
//       response.status(400).send(gatheredClaimInfo.error);
//       return;
//     }
//   } else {
//     console.log(`Calling Claim Info Program.`);
//     let gatheredClaimInfo = await getClaimInfo.getClaimInfo(
//       storeNumber,
//       invoiceNumber,
//       itemNumberCheckDigit,
//       userName,
//       quantityOnClaim,
//       caseOrOnly
//     );

//     if(gatheredClaimInfo.error == ""){ 
//       await ProfrontTxtlog.pushToTxTLog(
//         "getClaimInfoAPI.V4",
//         userName, 
//         originalDataForLogging,        
//         `Success Status:${gatheredClaimInfo.array[0]['Status']}`
//       );
//       // console.log(gatheredClaimInfo);
//       response.json(gatheredClaimInfo.array);
//       return;
//     } else {
//       await ProfrontTxtlog.pushToTxTLog(
//         "getClaimInfoAPI.V4",
//         userName, 
//         originalDataForLogging,     
//         `Error Status:${gatheredClaimInfo.error}`
//       );
//       response.status(400).send(gatheredClaimInfo.error);
//       return;
//     }
//   }
// }
// exports.run = getEntClmInfo;
