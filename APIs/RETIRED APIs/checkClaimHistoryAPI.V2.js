const dayjs = require("dayjs");

async function checkClaimHistory(request, response) {
  console.log("checkClaimHistoryAPI.V2");
  
  const directPathToProlog = "../../Prologging/";
  const ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

  pjs.define("claimNumber", { type: 'packed decimal', length: 6, decimals: 0 });
  claimNumber = request.params.id;

  //NOTE: Later addition: Use InvoiceDate in SQL query to filter out claims that are older than 3 months.
    // InvoiceDate =  request.body.InvoiceDate;
    // console.log(InvoiceDate);

  let threeMonthsAgo = dayjs().subtract(3, "month").format("YYYYMMDD");
  let clmhstRecordSet;
  let parmListArray = [];
  let statusMessage;
  let originalDataForLogging = "ClaimNumber: " + claimNumber;

  if (!claimNumber || claimNumber <= 0) {
    statusMessage = "No valid Claim Number sent to checkClaimHistory.";
    await ProfrontTxtlog.pushToTxTLog("checkClaimHistoryAPI.V2", "automated call from eClaims", originalDataForLogging, statusMessage);
    response.status(400).send(statusMessage);
    return;
  }

  //NOTE: 05162023 - This addition is to trim claim IDs that are greater than 99,999 (5 digits) to fit our system.
  let trimmedClaimNumber = claimNumber.toString();
    if (trimmedClaimNumber.length > 5) {
      trimmedClaimNumber = trimmedClaimNumber.substring(1);
    }
  trimmedClaimNumber= parseInt(trimmedClaimNumber);
  //NOTE: 05162023 - This addition is to trim claim IDs that are greater than 99,999 (5 digits) to fit our system.

  const clmhstSqlQuery =
  "select HCLMDT, HCLMNO, HCLDOC, HCLMST, HORDIN, HORDDT, HCLMIT from CLMHST where HCLMNO = ? and hclmdt > ? order by HCLMDT desc";

  try {
    clmhstRecordSet = pjs.query(clmhstSqlQuery, [trimmedClaimNumber, threeMonthsAgo]);
    // console.log(clmhstRecordSet);

    if (!clmhstRecordSet || clmhstRecordSet.length < 1) {
      statusMessage = "No record found for Claim Number: " + trimmedClaimNumber + ". If it is a new record it may be processed later today.";
      parmListArray.push({
        Status: statusMessage,
        ClaimNumber: trimmedClaimNumber
      });
      response.status(400).send(parmListArray);
      return;
    }

    if (sqlstate >= "02000") {
      statusMessage = "SQL error during Claim History File querry. SQLstate: " + sqlstate;
      parmListArray.push({
        Status: statusMessage,
        ClaimNumber: trimmedClaimNumber
      });      
      await ProfrontTxtlog.pushToTxTLog("checkClaimHistoryAPI.V2", "automated call from eClaims", originalDataForLogging, statusMessage);
      response.status(400).send(parmListArray);
      return;
    }
  } catch (err) {
    statusMessage = "Sql catch error while grabbing claim history records. Check Claim Number and try again." + err;
    await ProfrontTxtlog.pushToTxTLog("checkClaimHistoryAPI.V2", "automated call from eClaims", originalDataForLogging, statusMessage);
    parmListArray.push({ 
      Status: statusMessage, 
      ClaimNumber: trimmedClaimNumber 
    });
    response.status(400).send(parmListArray);
    return;
  }

  let claimDate = clmhstRecordSet[0]["hclmdt"]; //This is the claim date on the invoice in YYMMDD format.
    claimDate = claimDate.toString();
  let formattedClaimDate = claimDate.substring(2, 8);
  let claimDateCentury = claimDate.substring(0, 2);
  let documentNumber = clmhstRecordSet[0]["hcldoc"];
  let storeNumber = clmhstRecordSet[0]["hclmst"]; //This is the store number on the invoice.
  
  let recaphSqlQuery = '';
  let recaphRecordSet;
  let recaphRecordParms = [];

  // let invoiceNumber = clmhstRecordSet[0]["hordin"]; //This is the invoice number on the invoice.
  // let invoiceDate = clmhstRecordSet[0]["horddt"]; //This is the order date on the invoice in YYMMDD format.
  // invoiceDate = invoiceDate.toString();
  // let invoiceDateMMDDYY =
  //   invoiceDate.substring(2, 4) +
  //   invoiceDate.substring(4, 6) +
  //   invoiceDate.substring(0, 2);
  // console.log(`invoiceDateMMDDYY: ${invoiceDateMMDDYY}`);
  // console.log(`storeNumber: ${storeNumber}`);
  // console.log(`invoiceNumber: ${invoiceNumber}`);

  // if hclmit = 70 or 30 
  // if(clmhstRecordSet[0]["hclmit"] == 70 || clmhstRecordSet[0]["hclmit"] == 30){
  //   // recaphSqlQuery = "select distinct RHNUMB, RHDATE FROM RECAPH WHERE RHSTOR = ? and RHINVN = ? order by rhdate desc";
  //   // recaphRecordParms = [storeNumber, documentNumber];
  //   recaphSqlQuery = "select distinct RHNUMB, RHDATE FROM RECAPH WHERE RHSTOR = ? and RHINVN = ? and rhdate >= ? order by rhdate desc";
  //   recaphRecordParms = [storeNumber, documentNumber, claimDate];

  // } else {
  //     // recaphSqlQuery = "select distinct RHNUMB, RHDATE FROM RECAPH WHERE RHSTOR = ? and RHINVN = ? and rhinvd = ?";
  //     // recaphRecordParms = [storeNumber, documentNumber, invoiceDateMMDDYY];

  //     recaphSqlQuery = "select distinct RHNUMB, RHDATE FROM RECAPH WHERE RHSTOR = ? and RHINVN = ? and rhdate >= ? order by rhdate desc";
  //     recaphRecordParms = [storeNumber, documentNumber, claimDate];
  // }

  recaphSqlQuery = "select distinct RHNUMB, RHDATE FROM RECAPH WHERE RHSTOR = ? and RHINVN = ? and rhdate >= ? order by rhdate desc";
  recaphRecordParms = [storeNumber, documentNumber, formattedClaimDate];

  //Dottis query that I need to replicate:
  // 'select distinct rhnumb, rhdate into :g_recap#, :g_recapdate  from recaph where rhstor = :g_store# and rhinvn=:g_doc# and rhdate >= :g_useclmdate'; 

  try {
    recaphRecordSet = pjs.query(recaphSqlQuery, recaphRecordParms);
    // console.log(recaphRecordSet);

    if (!recaphRecordSet || recaphRecordSet.length <= 0) {
      statusMessage = "No reference number or date could be found for Claim Number: " + trimmedClaimNumber +". If it is a new record it may be processed later today.";
      await ProfrontTxtlog.pushToTxTLog("checkClaimHistoryAPI.V2", "automated call from eClaims", originalDataForLogging, statusMessage);
      parmListArray.push({
        Status: statusMessage,
        ClaimNumber: trimmedClaimNumber,
        ClaimDate: formattedClaimDate,
        documentNumber
      });
      response.status(200).send(parmListArray);
      return;
    }

    if (sqlstate >= "02000") {
      statusMessage = "SQL error during Claim History File RECAP querry. SQLstate: " + sqlstate;
      await ProfrontTxtlog.pushToTxTLog("checkClaimHistoryAPI.V2", "automated call from eClaims", originalDataForLogging, statusMessage);
      parmListArray.push({
        Status: statusMessage,
        ClaimNumber: trimmedClaimNumber,
        ClaimDate: formattedClaimDate,
        documentNumber
      });
      response.status(400).send(parmListArray);
      return;
    }
  } catch (err) {
    statusMessage = "Sql catch error while grabbing RECAP data for claim history records. Check Claim Number and try again." + err;
    await ProfrontTxtlog.pushToTxTLog("checkClaimHistoryAPI.V2", "automated call from eClaims", originalDataForLogging, statusMessage);
    parmListArray.push({ 
      Status: statusMessage, 
      ClaimNumber: trimmedClaimNumber, 
      ClaimDate: formattedClaimDate,
      documentNumber
    });
    response.status(400).send(parmListArray);
    return;
  }

    let recapDate = claimDateCentury + recaphRecordSet[0]["rhdate"]; //Here we are taking the century from the claim date and adding it to the recap date for formatting.
    recapDate = parseInt(recapDate);

    statusMessage = "Claim History data found for Claim Number: " + trimmedClaimNumber;

    parmListArray.push({
      Status: statusMessage,
      ClaimDate: claimDate, //date the claim number was created.
      ClaimNumber: trimmedClaimNumber, //document number passed from front end.
      documentNumber, //assigned claim number.
      RecapNumber: recaphRecordSet[0]["rhnumb"], //recap number from recap file.
      RecapDate: recapDate //recap date from recap file.
    });

    await ProfrontTxtlog.pushToTxTLog("checkClaimHistoryAPI.V2", "automated call from eClaims", originalDataForLogging, statusMessage);
    response.status(200).json(parmListArray);

}
exports.run = checkClaimHistory;
