/* Incoming INFO:
{
    "ClaimNumber": 99993,
    "StoreNumber" : 700,
    "ItemNumber" : 821645,
    "QuantityOnClaim" : 1,
    "ExtendedCost": 52.99,
    "UserKeyingClaim": "Tyler",
    "CallingName": "Hobbs"
}
*/

async function writeEntClmInfo(request, response) {

  const directPathToProlive = "../../prolive/APPs/";
  const directPathToProtest = "../../protest/APPs/";
  const directPathToProlog = "../../Prologging/";

  let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");
  let ledgerNumber = pjs.require(directPathToProlive + "LedgerNumberAPP.js");
  let validateCheckDigitAPP = pjs.require(
    directPathToProlive + "ValidateCheckDigitAPP.js"
  );

  //Define incomming variables.
  const claimNumber = request.body.ClaimNumber;
  const storeNumber = request.body.StoreNumber;
  const itemNumberCheckDigit = request.body.ItemNumber;
  const quantityClaimed = request.body.QuantityOnClaim;
  const sclUser = request.body.UserKeyingClaim.trim();
  const callingName = request.body.CallingName.trim();
  const userName = sclUser + " " + callingName;

  //Predefine variables.
  let errorMsg;
  let gatheredClaimInfoArray = [];
  let itemNumber = Math.floor(itemNumberCheckDigit / 10);

  pjs.define("extendedCost", {
    type: "packed decimal",
    length: 7,
    decimals: 2
  });

  pjs.define("extendedRetail", {
    type: "packed decimal",
    length: 7,
    decimals: 2
  });

  extendedCost = request.body.ExtendedCost; //extended cost 7s 2
  extendedRetail = 0.0; //Place holder since we cannot pull the retail information for this item for this store.

  const originalDataForLogging =
    "ClaimNumber: " +
    claimNumber +
    ", StoreNumber: " +
    storeNumber +
    ", ItemNumber: " +
    itemNumberCheckDigit +
    ", QuantityOnClaim: " +
    quantityClaimed +
    ", UserName: " +
    userName +
    ", extendedCost: " +
    extendedCost;

  //Setting Prod or Dev tables based on port---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  const currentPort = profound.settings.port;
  console.log(currentPort);

  let ENTCLMTable = "";

  if (currentPort === 80) {
    ENTCLMTable = "ENTCLM_CALLIN";
    console.log(`Running writeNotOnInvoiceKeptClaimAPI.V1 in PROD with table: ${ENTCLMTable}.`);
  } else {
    ENTCLMTable = "ENTCLMDEV";
    console.log(`Running writeNotOnInvoiceKeptClaimAPI.V1 in DEV with table: ${ENTCLMTable}.`);
  }

  // ERROR CHECKS FOR MISSING DATA COMMING IN---------------------------------------------------------------------------
  if (!claimNumber) {
    errorMsg = "Claim Number is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNotOnInvoiceKeptClaimAPI.V1",
      userName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  if (!itemNumberCheckDigit) {
    errorMsg = "Item Number is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNotOnInvoiceKeptClaimAPI.V1",
      userName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  let checkDigitValidation = await validateCheckDigitAPP.validateCheckDigit(
    itemNumberCheckDigit
  );
  if (checkDigitValidation == false) {
    errorMsg =
      "This item code does not seem to be valid, please double check that it is entered correctly.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNotOnInvoiceKeptClaimAPI.V1",
      userName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  if (!storeNumber) {
    errorMsg = "Store Number is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNotOnInvoiceKeptClaimAPI.V1",
      userName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  if (!quantityClaimed || quantityClaimed == 0) {
    errorMsg = "QuantityOnClaim needs to be greater than 0.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNotOnInvoiceKeptClaimAPI.V1",
      userName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }
  // END ERROR CHECKS FOR MISSING DATA COMMING IN---------------------------------------------------------------------------

  console.log(
    `Processing a Not On Invoice: Returned claim. Checking Inventory Master for item ${itemNumber}`
  );

  let sqlquery =
    "select " +
    "ITMCDE as ItemCode, " +
    "CHKDIG as CheckDigit, " +
    "ITMDSC as Description, " +
    "DEPT as Department, " +
    "PICODE as PickCode, " +
    "SLOTAI as SlotAisle, " +
    "SLOTLV as SlotLevel, " +
    "SLOTNO as SlotBin, " +
    "sell as StoreCost, " +
    "cost as Cost " +
    "from INVMST where ITMCDE = ?";

  gatheredClaimInfoArray = pjs.query(sqlquery, [itemNumber]);
  console.log(gatheredClaimInfoArray);

  if (gatheredClaimInfoArray.length < 1) {
    errorMsg = `No item found for item number ${itemNumber}`;
    await ProfrontTxtlog.pushToTxTLog(
      "writeNotOnInvoiceKeptClaimAPI.V1",
      userName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  let department = gatheredClaimInfoArray[0]["department"];
  department = department.trim();
  console.log(`Department: ${department}`);

  ledgerNumber = await ledgerNumber.getLedgerNumber(department);

  if (ledgerNumber.Message) {
    await ProfrontTxtlog.pushToTxTLog(
      "writeNotOnInvoiceKeptClaimAPI.V1",
      userName,
      originalDataForLogging,
      ledgerNumber.Message
    );
    response.status(400).send(ledgerNumber.Message);
    return;
  }
  ledgerNumber = ledgerNumber.LedgerNumber;
  console.log(`Ledger Number: ${ledgerNumber}`);

  const invoiceNumber = claimNumber;
  const caseOrOnlyClaimed = "C";
  // const isRestockFee = "N"; // setting default to "N" to not generate a restock fee.
  const restockCharge = 0;
  const creditType = ""; // setting default to "" to generate an invoice instead of a credit.
  const claimType = 5; // setting default to 5 for Not On Invoice: Kept
  const recordID = "S"; // record ID 'S'
  const tobaccoTax = 0; // tax if tobacco. we can stop passing this
  const documentNumber = 0; //document no. created when the file is read on 400.
  const container = 0;
  let storeCost = gatheredClaimInfoArray[0]["storecost"];
  let buyer = getBuyerNumber(itemNumber);
  let pickCode = gatheredClaimInfoArray[0]["pickcode"];
  let slot3 = gatheredClaimInfoArray[0]["slotbin"];
  let slotAisle = gatheredClaimInfoArray[0]["slotaisle"];
  let slotLevel = gatheredClaimInfoArray[0]["slotlevel"];
  let slot1 = slotAisle + slotLevel;
  slot1 = slot1.trim();
  console.log(`Slot1: ${slot1}`);
  let assignment = formatDate();
  let claimDate = formatDate();
  let invoiceDate = claimDate;
  let flag = "P";
  if (storeNumber == 700) flag = "";

  // /*
  // TURN ON FOR TESTING!!!!!!
  console.log(
    "recordID: " + recordID,
    "claimNumber: " + claimNumber,
    "claimDate: " + claimDate,
    "storeNumber: " + storeNumber,
    "itemNumber: " + itemNumber,
    "claimType: " + claimType,
    "creditType: " + creditType,
    "department: " + department,
    "ledgerNumber: " + ledgerNumber,
    "invNumOfOrder: " + invoiceNumber, //set to claim number
    "orderDate: " + invoiceDate, //set to claim date
    "pickCode: " + pickCode,
    "slot1: " + slot1,
    "slot3: " + slot3,
    "quantityClaimed: " + quantityClaimed,
    "caseOrOnlyClaimed: " + caseOrOnlyClaimed,
    "tobaccoTax: " + tobaccoTax,
    "storeCost: " + storeCost,
    "extendedCost: " + extendedCost,
    "flag: " + flag,
    "buyer: " + buyer,
    "restockCharge: " + restockCharge,
    "documentNumber: " + documentNumber,
    "extendedRetail: " + extendedRetail,
    "sclUser: " + sclUser,
    "assignment: " + assignment,
    "container: " + container,
    "callingName: " + callingName
  );
  // */
  // /*
  // TURN OFF FOR TESTING!!!!!
  let insertsqlquery =
    `insert into ${ENTCLMTable} (` +
    "sclmrc, " +
    "sclmno, " +
    "sclmdt, " +
    "sclmst, " +
    "sclmit, " +
    "sclmty, " +
    "sclmcr, " +
    "sclmld, " +
    "sclmdp, " +
    "sordin, " +
    "sorddt, " +
    "sordpc, " +
    "sords1, " +
    "sords3, " +
    "sclmqt, " +
    "sclmco, " +
    "sclmtx, " +
    "sclmcs, " +
    "sclmex, " +
    "sclmfl, " +
    "sclmby, " +
    "sclstk, " +
    "scldoc, " +
    "sclret, " +
    "sclusr, " +
    "sclasn, " +
    "sclcnt, " +
    "sclnam" +
    ") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) with None";
  try {
    pjs.query(insertsqlquery, [
      recordID,
      claimNumber,
      claimDate,
      storeNumber,
      itemNumber,
      claimType,
      creditType,
      ledgerNumber,
      department,
      invoiceNumber,
      invoiceDate,
      pickCode,
      slot1,
      slot3,
      quantityClaimed,
      caseOrOnlyClaimed,
      tobaccoTax,
      storeCost,
      extendedCost,
      flag,
      buyer,
      restockCharge,
      documentNumber,
      extendedRetail,
      sclUser,
      assignment,
      container,
      callingName
    ]);
    if (sqlstate >= "02000") {
      errorMsg =
        "SQL error during write on writeNotOnInvoiceKeptClaim API. SQLstate: " +
        sqlstate;
      await ProfrontTxtlog.pushToTxTLog(
        "writeNotOnInvoiceKeptClaimAPI.V1",
        userName,
        originalDataForLogging,
        errorMsg
      );
      console.log(
        "Item Code " +
          itemNumber +
          " Store " +
          storeNumber +
          " Error Message: " +
          errorMsg
      );
      response.status(400).send(errorMsg);
      return;
    }
  } catch (err) {
    errorMsg =
      "Sql Insert catch error during write on writeNotOnInvoiceKeptClaim API.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNotOnInvoiceKeptClaimAPI.V1",
      userName,
      originalDataForLogging,
      errorMsg
    );
    console.log(
      "Item Code " +
        itemNumber +
        " Store " +
        storeNumber +
        " Error Message: " +
        errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }
  // */
  let successMsg =
    "Entry Claim submitted to generate an invoice for the kept item.";
  console.log(
    `Item Code ${itemNumber} updated for Store ${storeNumber} for for Claim Date ${claimDate}.`
  );
  await ProfrontTxtlog.pushToTxTLog(
    "writeNotOnInvoiceKeptClaimAPI.V1",
    userName,
    originalDataForLogging,
    successMsg
  );
  response.status(200).send(successMsg);
}
exports.run = writeEntClmInfo;

//Get todays date for claim sumbission date. -------------------------------------------------------------------------------------------
function formatDate() {
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1;
  var yyyy = today.getFullYear();

  if (dd < 10) {
    dd = "0" + dd;
  }

  if (mm < 10) {
    mm = "0" + mm;
  }

  today = `${yyyy}${mm}${dd}`;
  today = today.substring(2, 8);
  return today;
}

//Get BUYER from INVMST based on Item Number. --------------------------------------------------------------------------------
function getBuyerNumber(y) {
  let itemNumber = y;
  let recordSet;
  let sqlquery = "select buyer from INVMST where ITMCDE = ?";
  recordSet = pjs.query(sqlquery, [itemNumber]);
  buyer = "";
  if (recordSet.length > 0 && recordSet != "") buyer = recordSet[0]["buyer"];
  return buyer;
}
