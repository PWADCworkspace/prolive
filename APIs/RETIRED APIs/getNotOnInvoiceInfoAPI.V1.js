// const gtin = require('gtin');

// function getNonInvoiceInfoAPI(request, response) {
  
//   //Set pathing for Apps.
//   //local
//   // const directPathToProlive = "C:/tyler/modules/prolive/APPs/";
//   // const directPathToProtest = "C:/tyler/modules/protest/APPs/";
//   //production
//   const directPathToProlive = "C:/profront/modules/prolive/APPs/";
//   const directPathToProtest = "C:/profront/modules/protest/APPs/";

//   let buyerNameAPP = pjs.require(directPathToProlive + "BuyerNameAPP.js");
//   let validateCheckDigitAPP = pjs.require(directPathToProtest + "ValidateCheckDigitAPP.js");
//   let ProfrontTxtlog = pjs.require(directPathToProlive + "PushToTxtLogAPP.js");

//   console.log(`Running getNonInvoiceInfoAPI.V1.js for entered number: ${request.body.EnteredNumber} and quantity on claim: ${request.body.QuantityOnClaim}`);

//   let enteredNumber = request.body.EnteredNumber;
//   let quantityOnClaim = request.body.QuantityOnClaim;

//   const originalDataForLogging = 'enteredNumber: '+ enteredNumber + ', quantityOnClaim: '+ quantityOnClaim;

//   let isUPC = false;
//   let isItemCode = false;
//   let isPackedUPC = false;
//   let upc = -1;
//   let itemNumber = -1;
//   let errorMsg;

//   if (String(enteredNumber).length < 4 || isNaN(enteredNumber)) {
//     errorMsg = "Entered Number is too short or is not a Number";
//     ProfrontTxtlog.pushToTxTLog("getNotOnInvoiceAPI.V1", "Automatic API call", originalDataForLogging, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }
//   if (String(enteredNumber).length <= 6) isItemCode = true;
//   else if (String(enteredNumber).length == 8) isPackedUPC = true;
//   else if (String(enteredNumber).length == 10) isUPC = true;
//   else if (String(enteredNumber).length == 12) {
//     enteredNumber = enteredNumber.substring(1, 11);
//     isUPC = true;
//   } else {
//     errorMsg = "Entered Number is invalid, please double check that it is entered correctly either as a full item code or a full upc.";
//     ProfrontTxtlog.pushToTxTLog("getNotOnInvoiceAPI.V1", "Automatic API call", originalDataForLogging, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }

//   if (isItemCode) {
//     let checkDigitValidation = validateCheckDigitAPP.validateCheckDigit(enteredNumber);
//     if (checkDigitValidation == false) {
//       errorMsg = "This item code does not seem to be valid, please double check that it is entered correctly with the check digit included.";
//       ProfrontTxtlog.pushToTxTLog("getNotOnInvoiceAPI.V1", "Automatic API call", originalDataForLogging, errorMsg);
//       response.status(400).send(errorMsg);
//       return;
//     }
//     itemNumber = enteredNumber.substring(0, enteredNumber.length - 1);
//   }

//   if (isPackedUPC) {
//     enteredNumber = gtin.upcE.expand(enteredNumber);
//     isUPC = true;
//   }

//   if (isUPC) upc = enteredNumber;

//   let querysql;
//   let paramater;
//   if (isUPC) {
//     console.log(`Running INVMST query for UPC: ${upc}`);
//     querysql = "select * from invmst where upc = ?";
//     paramater = upc;
//   } else {
//     console.log(`Running INVMST query for item code: ${itemNumber}`);
//     querysql = "select * from invmst where itmcde = ?";
//     paramater = itemNumber;
//   }

//   let recordSet = pjs.query(querysql, [paramater]);

//   if (!recordSet || recordSet.length == 0) {
//     errorMsg = "No record found where " + (isUPC ? "upc = " : "item code = ") + paramater;
//     console.log(errorMsg);
//     ProfrontTxtlog.pushToTxTLog("getNotOnInvoiceAPI.V1", "Automatic API call", originalDataForLogging, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }

//   if (recordSet && recordSet.length > 1) {
//     errorMsg = "Error - more than one matching item found in inventory master file";
//     console.log(errorMsg);
//     ProfrontTxtlog.pushToTxTLog("getNotOnInvoiceAPI.V1", "Automatic API call", originalDataForLogging, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }

//   let outputArray = [];
//   let outputJSON = {};

//   let packSize = "";
//   let splitArray = recordSet[0]["pksize"].split("/");
//   if (splitArray.length != 2) packSize = recordSet[0]["pksize"];
//   if (splitArray.length == 2) packSize = splitArray[0];
//   packSize = Number(packSize.trim());

//   let buyerName;
//   let buyerNameJSON = buyerNameAPP.getBuyerName(recordSet[0]["itmcde"]);
//   if (buyerNameJSON.errorMsg == "") buyerName = buyerNameJSON.value;
//   if (buyerNameJSON.errorMsg != "")
//     buyerName = "ERROR GETTING BUYER NAME: " + buyerNameJSON.errorMsg;

//   let itemNumberCheckDigit = "" + recordSet[0]["itmcde"] + recordSet[0]["chkdig"];
//   itemNumberCheckDigit = Number(itemNumberCheckDigit);

//   let calculatedExtendedCost = recordSet[0]["sell"] * quantityOnClaim;
//   calculatedExtendedCost = Number(calculatedExtendedCost.toFixed(2));

//   outputJSON["ItemNumber"] = itemNumberCheckDigit;
//   outputJSON["UPCNumber"] = recordSet[0]["upc"];
//   outputJSON["CheckDigit"] = recordSet[0]["chkdig"];
//   outputJSON["Buyer"] = buyerName;
//   outputJSON["AppMessage"] = "success";
//   outputJSON["PackSize"] = packSize;
//   outputJSON["Description"] = recordSet[0]["itmdsc"];
//   outputJSON["Department"] = recordSet[0]["dept"];
//   outputJSON["perCaseCost"] = recordSet[0]["sell"];
//   outputJSON["QuantityOnClaim"] = quantityOnClaim;
//   outputJSON["ExtendedCost"] = calculatedExtendedCost;

//   outputArray.push(outputJSON);
//   response.json(outputArray);

//   ProfrontTxtlog.pushToTxTLog("getNotOnInvoiceAPI.V1", "Automatic API call", originalDataForLogging, outputArray);

// }
// exports.run = getNonInvoiceInfoAPI;
