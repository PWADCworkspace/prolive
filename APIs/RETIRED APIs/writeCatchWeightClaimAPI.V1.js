// /* Incoming INFO:
// {
//   "ClaimNumber": 00000,
//   "StoreNumber": 000,
//   "InvoiceNumber": 00000,
//   "ItemNumber": 000000,
//   "ClaimType": 3,
//   "isRestockFee": "N",
//   "CreditType": "C",
//   "QuantityOnClaim": 0,
//   "CaseOrOnly": "C",
//   "CatchWeight": 00.00
//   "ExtendedCost": 00.00,
//   "ExtenededRetail": 000.00,
//   "UserKeyingClaim": "Tyler",
//   "CallingName": "Hobbs"
// }
// */

// // VERSION V2.1-------------------------------------------------------------------------------------------------------------------------------------------------------
// // Including calculation for restock fee
// // VERSION V2.2-------------------------------------------------------------------------------------------------------------------------------------------------------
// // Including Catch Weight intake and passthrough for claim.

// function writeCatchWeightEntClmInfo(request, response) {
//   console.log("writeCatchWeightClaim API V1 running.");

//   //Set pathing for Apps.
//   const directPathToProlive = "C:/profront/modules/prolive/APPs/";
//   const directPathToProtest = "C:/profront/modules/protest/APPs/";
//   let FicheD01 = pjs.require(directPathToProtest + "CatchWeightFicheD01APP.V1.js"); //New app call is set
//   let EntClm = pjs.require(directPathToProtest + "CatchWeightEntClmAPP.V1.js"); //New app call is set
//   let ClmHst = pjs.require(directPathToProtest + "CatchWeightClmHstAPP.V1.js"); //New app call is set
//   let ClmEnt = pjs.require(directPathToProtest + "CatchWeightClmEntAPP.V1.js"); //New app call is set
//   let ledgerNumber = pjs.require(directPathToProlive + "LedgerNumberAPP.js");
//   let log = pjs.require(directPathToProlive + "PushToLogAPP.js");
//   let ProfrontTxtlog = pjs.require(directPathToProlive + "PushToTxtLogAPP.js");

//   //Predefine variables.
//   let successMsg;
//   let itemNumber;
//   let errorMsg;
//   pjs.define("extendedCost", {type: "packed decimal",length: 7,decimals: 2});
//   pjs.define("extendedRetail", {type: "packed decimal",length: 7,decimals: 2});
//   pjs.define("incommingCatchWeight", {type: "packed decimal",length: 7,decimals: 2});
//   extendedCost = request.body.ExtendedCost; //extended cost 7s 2
//   extendedRetail = request.body.ExtenededRetail; //extended retail 7s 2
//   incommingCatchWeight = request.body.CatchWeight; //catch weight 7s 2
//   // const incommingCatchWeight = request.body.CatchWeight;
//   const tobaccoTax = 0; // tax if tobacco. we can stop passing this
//   const recordID = "S";

//   //Get the incoming JSON objects.
//   const invoiceNumber = request.body.InvoiceNumber;
//   const storeNumber = request.body.StoreNumber;
//   const itemNumberCheckDigit = request.body.ItemNumber;
//   const quantityClaimed = request.body.QuantityOnClaim;
//   const caseOrOnlyClaimed = request.body.CaseOrOnly;
//   const userName = request.body.UserKeyingClaim + " " + request.body.CallingName;
//   const claimNumber = request.body.ClaimNumber;
//   const STNUM = request.body.StoreNumber; //store number.
//   const claimType = request.body.ClaimType; // claim type '2' SCLMTY.
//   const creditType = request.body.CreditType; //credit = 'C' SCLMCR.
//   const sclUser = request.body.UserKeyingClaim;
//   const callingName = request.body.CallingName;
//   const restockFee = request.body.RestockFee;
//   const flag = "";// NOTE: SETTING THIS FLAG TO "P" WILL CAUSE THE 400 TO AUTO PROCESS! lEAVE BLANK UNTIL WE ARE READY FOR PRODUCTION!
//   const documentNumber = 0; //document no. created when the file is read on 400.
//   const container = 0;
//   var assignment = formatDate();
//   itemNumber = padLeadingZeros(itemNumberCheckDigit, 6);
//   itemNumber = itemNumber.slice(0, 5);
//   itemNumber = parseInt(itemNumber);

//   //Check validation of incoming data.
//   if (!invoiceNumber) {
//     errorMsg = "Invoice Number is required.";
//     ProfrontTxtlog.pushToTxTLog("writeCatchWeightEntClmAPI.V1",userName,errorMsg);
//     log.pushToLog(storeNumber, userName, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }
//   if (!itemNumberCheckDigit) {
//     errorMsg = "Item Number is required.";
//     ProfrontTxtlog.pushToTxTLog("writeCatchWeightEntClmAPI.V1",userName,errorMsg);
//     log.pushToLog(storeNumber, userName, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }
//   if (!storeNumber) {
//     errorMsg = "Store Number is required.";
//     ProfrontTxtlog.pushToTxTLog("writeCatchWeightEntClmAPI.V1",userName,errorMsg);
//     log.pushToLog(storeNumber, userName, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }
//   if (!incommingCatchWeight || incommingCatchWeight == 0) {
//     errorMsg = `Catch Weight: ${incommingCatchWeight} may be incorrect.`;
//     ProfrontTxtlog.pushToTxTLog("writeCatchWeightEntClmAPI.V1",userName,errorMsg);
//     log.pushToLog(storeNumber, userName, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }

//   // Query FicheD01 to get Item info off of Invoice.
//   FicheD01 = FicheD01.getCatchWeightFicheD01Info(invoiceNumber,storeNumber,itemNumber,incommingCatchWeight);
//   console.log("records gathered from FICHED01.");
//   // console.log(FicheD01);
//   let numberOfItemsOnInvoice = Object.keys(FicheD01.array).length;
//   console.log(`member key size: ${numberOfItemsOnInvoice}`);

//   if (FicheD01.Message) {
//     ProfrontTxtlog.pushToTxTLog("writeCatchWeightEntClmAPI.V1",userName,FicheD01.Message);
//     log.pushToLog(storeNumber, userName, FicheD01.Message);
//     response.status(400).send(FicheD01.Message);
//     console.log("Error on FicheD01 querry:" + FicheD01.Message);
//     return;
//   }

//   // START check for claims already filed.-----------------------------------------------------------------------------------------------------------------------------

//   // Query ENTCLM to see if a claim is currently filed.
//   console.log(`Calling ENTCLM query for item: ${itemNumber}`);
//   let EntClmCall = EntClm.getCatchWeightEntClmInfo(invoiceNumber,storeNumber,itemNumber,extendedCost);
//   console.log(EntClmCall);
//   let numberOfItemsInENTCLM = Object.keys(EntClmCall.array).length;
//   console.log(`member key size: ${numberOfItemsInENTCLM}`);

//   // if (EntClm.array.length > 0) {
//   //   errorMsg = `A claim has already been sumbitted for invoice: ${invoiceNumber} and item: ${itemNumber}.`;
//   //   console.log(errorMsg);
//   //   ProfrontTxtlog.pushToTxTLog("writeEntClmAPI.V2.1",userName, errorMsg);
//   //   log.pushToLog(storeNumber, userName, errorMsg);
//   //   response.status(400).send(errorMsg);
//   //   return;
//   // }

//   // Query CLMHST to see if a claim has already been filed.
//   console.log(`Calling CLMHST query for item: ${itemNumber}`);
//   let ClmHstCall = ClmHst.getCatchWeightClmHstInfo(invoiceNumber,storeNumber,itemNumber,extendedCost);
//   console.log(ClmHstCall);
//   let numberOfItemsInCLMHST = Object.keys(ClmHstCall.array).length;
//   console.log(`member key size: ${numberOfItemsInCLMHST}`);

//   // if (ClmHst.array.length > 0) {
//   //   errorMsg = `A claim has already been processed for invoice: ${invoiceNumber} and item: ${itemNumber}.`;
//   //   console.log(errorMsg);
//   //   ProfrontTxtlog.pushToTxTLog("writeEntClmAPI.V2.1",userName, errorMsg);
//   //   log.pushToLog(storeNumber, userName, errorMsg);
//   //   response.status(400).send(errorMsg);
//   //   return;
//   // }

//   // Query CLMENT to see if a Markout claim was filed in house.
//   console.log(`Calling CLMENT query for item: ${itemNumber}`);
//   let ClmEntCall = ClmEnt.getCatchWeightClmEntInfo(invoiceNumber,storeNumber,itemNumber,extendedCost);
//   console.log(ClmEntCall);
//   let numberOfItemsInCLMENT = Object.keys(ClmEntCall.array).length;
//   console.log(`member key size: ${numberOfItemsInCLMENT}`);

//   let numberOfClaimsFiled = numberOfItemsInENTCLM + numberOfItemsInCLMHST + numberOfItemsInCLMENT;
//   console.log(`Number of claims filed: ${numberOfClaimsFiled}`);

//   // if (ClmEnt.array.length > 0){
//   //   errorMsg = `A Markout Claim has already been submitted for invoice: ${invoiceNumber} and item: ${itemNumber} and credit has already been issued.`;
//   //   console.log(errorMsg);
//   //   ProfrontTxtlog.pushToTxTLog("writeEntClmAPI.V2.1",userName, errorMsg);
//   //   log.pushToLog(storeNumber, userName, errorMsg);
//   //   response.status(400).send(errorMsg);
//   //   return;
//   // }

//   // if(numberOfItemsOnInvoice == numberOfClaimsFiled) {
//   //   errorMsg = 'All items on invoice have already been claimed.';
//   //   //return a message here that shows how many of each claim is in each file so the store knows. aka 2 markouts paid + 1 pending + 1 paid = 4 claims.
//   //   ProfrontTxtlog.pushToTxTLog("getCatchWeightClmInfoAPI.V1",userName, errorMsg);
//   //   log.pushToLog(storeNumber, userName, errorMsg);
//   //   response.status(400).send(errorMsg);
//   // }

//   let maxNumberOfClaimableItems = numberOfItemsOnInvoice - numberOfClaimsFiled; //remainder that can be claimed.

//   if (quantityClaimed > maxNumberOfClaimableItems) {
//     // errorMsg = "Quantity on claim is greater than the number of claimable items on invoice.";
//     errorMsg = 
//       `There were ${numberOfItemsOnInvoice} items on the invoice. 
//       ${numberOfItemsInENTCLM} claims are currently pending.
//       ${numberOfItemsInCLMHST} claims have already been applied to account.
//       ${numberOfItemsInCLMENT} Mark-Out claims have already been filed.
//       No additional claims can be filed for this item on this invoice.`;
//     ProfrontTxtlog.pushToTxTLog("getCatchWeightClmInfoAPI.V1",userName,errorMsg);
//     log.pushToLog(storeNumber, userName, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }

//   // END check for claims already filed.-----------------------------------------------------------------------------------------------------------------------------

//   // START formatting data for claim.-----------------------------------------------------------------------------------------------------------------------------

//   let claimDate = formatDate();
//   let department = FicheD01.array[0]["fddept"]; //department 'J'.
//     department = department.trim();

//   ledgerNumber = ledgerNumber.getLedgerNumber(department);
//     if (ledgerNumber.Message) {
//       log.pushToLog(storeNumber, userName, ledgerNumber.Message);
//       response.status(400).send(ledgerNumber.Message);
//       return;
//     }
//   ledgerNumber = ledgerNumber.LedgerNumber;

//   let invNumOfOrder = FicheD01.array[0]["fdinv"];
//   let orderDate = FicheD01.array[0]["fddate"];
//     orderDate = orderDate.toString();
//     orderDate = orderDate.substr(2, 6);
//   let slot = FicheD01.array[0]["fdslot"]; //"123456"
//   let pickCode = slot.substr(0, 1); //pick code '1'
//   let slot1 = slot.substr(1, 2); // Part of slot '23'
//   let slot3 = slot.substr(3, 3); // part of slot '456'
//   // var caseQtyOnOrder = FicheD01.array[0]["fdqtys"]; // Qty on original order //2 cases
//   let packSize = FicheD01.array[0]["fdpksz"]; //pack size on original order //12/1OZ
//     packSize = packSize.toString();
//   let packDash = packSize.indexOf("/");
//     packSize = packSize.slice(0, packDash); 
//     packSize = packSize.trim();
//   // var extendedPackSizeOnOrder = packSize * caseQtyOnOrder; // 12 pk * 2 cases = 24 pk

//   // if (caseOrOnlyClaimed == "C" && quantityClaimed > caseQtyOnOrder) {
//   //   errorMsg =
//   //     "Quantity on Claim (" +
//   //     quantityClaimed +
//   //     ") exceeds the case quantity received: (" +
//   //     caseQtyOnOrder +
//   //     ") for this order.";
//   //   response.status(400).send(errorMsg);
//   //   log.pushToLog(storeNumber, userName, errorMsg);
//   //   return;
//   // }

//   // if (caseOrOnlyClaimed == "O" && quantityClaimed > extendedPackSizeOnOrder) {
//   //   errorMsg =
//   //     "Quantity on Claim (" +
//   //     quantityClaimed +
//   //     ") exceeds the item quantity received: (" +
//   //     extendedPackSizeOnOrder +
//   //     ") for this order.";
//   //   response.status(400).send(errorMsg);
//   //   log.pushToLog(storeNumber, userName, errorMsg);
//   //   return;
//   // }

//   let perCaseCost = FicheD01.array[0]["fdsell"]; //per case cost
//   let buyer = getBuyerNumber(itemNumber); //buyer

//   if ( claimType == 2 && (restockFee == "Y" || restockFee == "y")) {
//     console.log("Restock fee is being charged.");
//     var restockCharge = extendedCost * 0.05; // If Claim is 2 (return) the restock fee is 5% of the total * qty ordered
//   } else {
//     restockCharge = 0; //restock charge 7s 2
//   }
  
//   // /*
//   // TURN ON FOR TESTING!!!!!!
//   console.log(
//     "recordID: " + recordID,
//     "claimNumber: " + claimNumber,
//     "claimDate: " + claimDate,
//     "packSize: " + packSize,
//     "STNUM: " + STNUM,
//     "itemNumber: " + itemNumber,
//     "claimType: " + claimType, //if this is a 2 then it is a return and we may charge a restock fee (SCLSTK)
//     "creditType: " + creditType,
//     "department: " + department,
//     "ledgerNumber: " + ledgerNumber,
//     "invNumOfOrder: " + invNumOfOrder,
//     "orderDate: " + orderDate,
//     "slot: " + slot,
//     "pickCode: " + pickCode,
//     "slot1: " + slot1,
//     "slot3: " + slot3,
//     "quantityClaimed: " + quantityClaimed,
//     "caseOrOnly: " + caseOrOnlyClaimed,
//     "tobaccoTax: " + tobaccoTax,
//     "perCaseCost: " + perCaseCost,
//     "extendedCost: " + extendedCost,
//     "flag: " + flag,
//     "buyer: " + buyer,
//     "restockCharge: " + restockCharge,
//     "documentNumber: " + documentNumber,
//     "extendedRetail: " + extendedRetail,
//     "sclUser: " + sclUser,
//     "assignment: " + assignment,
//     "container: " + container,
//     "callingName: " + callingName
//   );
//   // */
//   /*
//   // TURN OFF FOR TESTING!!!!!
//   let sqlquery =
//     "insert into PROFRONT.ENTCLM_CALLIN (" +
//     "sclmrc, " +
//     "sclmno, " +
//     "sclmdt, " +
//     "sclmst, " +
//     "sclmit, " +
//     "sclmty, " +
//     "sclmcr, " +
//     "sclmld, " +
//     "sclmdp, " +
//     "sordin, " +
//     "sorddt, " +
//     "sordpc, " +
//     "sords1, " +
//     "sords3, " +
//     "sclmqt, " +
//     "sclmco, " +
//     "sclmtx, " +
//     "sclmcs, " +
//     "sclmex, " +
//     "sclmfl, " +
//     "sclmby, " +
//     "sclstk, " +
//     "scldoc, " +
//     "sclret, " +
//     "sclusr, " +
//     "sclasn, " +
//     "sclcnt, " +
//     "sclnam" +
//     ") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) with None";
//   try {
//     pjs.query(sqlquery, [
//       recordID,
//       claimNumber,
//       claimDate,
//       STNUM,
//       itemNumber,
//       claimType,
//       creditType,
//       ledgerNumber,
//       department,
//       invNumOfOrder,
//       orderDate,
//       pickCode,
//       slot1,
//       slot3,
//       quantityClaimed,
//       caseOrOnlyClaimed,
//       tobaccoTax,
//       perCaseCost,
//       extendedCost,
//       flag,
//       buyer,
//       restockCharge,
//       documentNumber,
//       extendedRetail,
//       sclUser,
//       assignment,
//       container,
//       callingName
//     ]);
//     if (sqlstate >= "02000") {
//       errorMsg = "SQL error during write. SQLstate: " + sqlstate;
//       log.pushToLog(storeNumber, userName, errorMsg);
//       console.log(
//         "Item Code " +
//           itemNumber +
//           " Store " +
//           STNUM +
//           " Error Message: " +
//           errorMsg
//       );
//       response.status(400).send(errorMsg);
//       return;
//     }
//   } catch (err) {
//     errorMsg = "Sql Insert catch error during Write.";
//     log.pushToLog(storeNumber, userName, errorMsg);
//     console.log(
//       "Item Code " +
//         itemNumber +
//         " Store " +
//         STNUM +
//         " Error Message: " +
//         errorMsg
//     );
//     response.status(400).send(errorMsg);
//     return;
//   }
//   */
//   successMsg = "Entry Claim submitted.";
//   log.pushToLog(storeNumber, userName, successMsg);
//   console.log(`Item Code ${itemNumber} updated for Store ${STNUM} for for Claim Date ${claimDate}.`);
//   ProfrontTxtlog.pushToTxTLog("writeEntClmAPI.V2.1", userName, successMsg);
//   response.status(200).send(successMsg);
// }
// exports.run = writeCatchWeightEntClmInfo;

// //Get todays date for claim sumbission date. -------------------------------------------------------------------------------------------
// function formatDate() {
//   let today = new Date();
//   let dd = today.getDate();
//   let mm = today.getMonth() + 1;
//   let yyyy = today.getFullYear();

//   if (dd < 10) {
//     dd = "0" + dd;
//   }

//   if (mm < 10) {
//     mm = "0" + mm;
//   }

//   today = `${yyyy}${mm}${dd}`;
//   today = today.substring(2, 8);
//   // console.log('today ' + today);
//   return today;
// }

// //Get BUYER from INVMST based on Item Number. --------------------------------------------------------------------------------
// function getBuyerNumber(y) {
//   let itemNumber = y;
//   let recordSet;
//   sqlquery = "select buyer from PWAFIL.INVMST where ITMCDE = ?";
//   recordSet = pjs.query(sqlquery, [itemNumber]);
//   buyer = "";
//   if (recordSet.length > 0 && recordSet != "") buyer = recordSet[0]["buyer"];
//   return buyer;
// }

// function padLeadingZeros(num, size) {
//   let s = num + "";
//   while (s.length < size) s = "0" + s;
//   return s;
// }
