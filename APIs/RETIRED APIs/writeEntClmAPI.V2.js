// VERSION V2 -------------------------------------------------------------------------------------------------------------------------------------------------------
function writeEntClmInfo(request, response) {
  var method = request.method;
  var directPath = 'C:/profront/modules/';
  var log = pjs.require(directPath + 'prolive/APPs/PushToLogAPP.js');
  var FicheD01 = pjs.require(directPath + 'protest/APPs/FicheD01APP.js');
  var ledgerNumber = pjs.require(directPath + 'protest/APPs/LedgerNumberAPP.js');
  var errorMsg;

  if (method == 'POST') goPOST();
  else goBadMethod();
  //----------------------------------------------------------------------------
  function goPOST() {
    var sqlquery;

    var successMsg;
    var invoiceNumber = request.body.InvoiceNumber;
    var storeNumber = request.body.StoreNumber;
    var itemNumberCheckDigit = request.body.ItemNumber;
    var itemNumber;
    var userName =
      request.body.UserKeyingClaim + ' ' + request.body.CallingName;

    if (!invoiceNumber) {
      errorMsg = 'Invoice Number is required.';
      log.pushToLog(storeNumber, userName, errorMsg);
      response.status(400).send(errorMsg);
      return;
    } else if (!itemNumberCheckDigit) {
      errorMsg = 'Item Number is required.';
      log.pushToLog(storeNumber, userName, errorMsg);
      response.status(400).send(errorMsg);
      return;
    } else if (!storeNumber) {
      errorMsg = 'Store Number is required.';
      log.pushToLog(storeNumber, userName, errorMsg);
      response.status(400).send(errorMsg);
      return;
    } else {
      itemNumber = padLeadingZeros(itemNumberCheckDigit, 6);
      itemNumber = itemNumber.slice(0, 5);
      itemNumber = parseInt(itemNumber);

      // Query FicheD01 to get Item info off of Invoice.
      FicheD01 = FicheD01.getFicheD01Info(
        invoiceNumber,
        storeNumber,
        itemNumber
      );
      console.log(FicheD01);
      if (FicheD01.Message) {
        log.pushToLog(storeNumber, userName, FicheD01.Message);
        response.status(400).send(FicheD01.Message);
        return;
      } else {
        // console.log(FicheD01.array);

        pjs.define('extendedCost', {
          type: 'packed decimal',
          length: 7,
          decimals: 2,
        });
        pjs.define('extendedRetail', {
          type: 'packed decimal',
          length: 7,
          decimals: 2,
        });

        var recordID = 'S'; // record ID 'S'
        var claimNumber = request.body.ClaimNumber; //created by front end when new claim is written. 10001 - 99999 and repeat.
        if (isNaN(claimNumber)) {
          log.pushToLog(storeNumber, userName, 'Claim Type Incorrect.');
          response.status(400).send('Claim Type cannot contain letters.');
          return;
        }

        var claimDate = 0; //claim date created when document# generated.

        var packSize = FicheD01.array[0]['fdpksz'];
        packSize = packSize.toString();
        var packDash = packSize.indexOf('/');
        packSize = packSize.slice(0, packDash);
        packSize = packSize.trim();

        var STNUM = request.body.StoreNumber; //store number.
        var claimType = request.body.ClaimType; // claim type '2' SCLMTY.
        var creditType = request.body.CreditType; //credit = 'C' SCLMCR.
        if (!isNaN(creditType)) {
          log.pushToLog(storeNumber, userName, 'Credit Type Incorrect.');
          response.status(400).send('Credit Type Incorrect.');
          return;
        }

        var department = FicheD01.array[0]['fddept']; //department 'J'.
        department = department.trim();

        ledgerNumber = ledgerNumber.getLedgerNumber(department);
        if (ledgerNumber.Message) {
          log.pushToLog(storeNumber, userName, ledgerNumber.Message);
          response.status(400).send(ledgerNumber.Message);
          return;
        }
        ledgerNumber = ledgerNumber.LedgerNumber;

        var invNumOfOrder = FicheD01.array[0]['fdinv']; //invoice number of order '0'.

        var orderDate = FicheD01.array[0]['fddate']; //order date'0'.
        orderDate = orderDate.toString();
        orderDate = orderDate.substr(2, 6);

        var slot = FicheD01.array[0]['fdslot']; //"123456"
        var pickCode = slot.substr(0, 1); //pick code '1'
        var slot1 = slot.substr(1, 2); // Part of slot '23'
        var slot3 = slot.substr(3, 3); // part of slot '456'
        var qtyOnClaim = request.body.QuantityOnClaim; //Qty on claim
        var qtyOnOrder = FicheD01.array[0]['fdqtys']; // Qty on original order

        // CHECK that claim quanity does not exceed quanity that was shipped to store.
        // console.log("qtyOnOrder:" + qtyOnOrder + " qtyOnClaim:" + qtyOnClaim);
        if (qtyOnClaim > qtyOnOrder){
          errorMsg = "Quantity on Claim (" + qtyOnClaim + ") exceeds quantity received (" + qtyOnOrder + ") for this order."
          response.status(400).send(errorMsg);
          log.pushToLog(storeNumber, userName, errorMsg);
          return;
        }

        var caseOrOnly = request.body.CaseOrOnly; //case or only
        var tobaccoTax = 0; // tax if tobacco. we can stop passing this
        var perCaseCost = FicheD01.array[0]['fdsell']; //per case cost
        extendedCost = request.body.ExtendedCost; //extended cost 7s 2

        // -----------------NOTE: SETTING THIS FLAG TO "P" WILL CAUSE THE 400 TO AUTO PROCESS! lEAVE BLANK UNTIL WE ARE READY FOR PRODUCTION!-------------------------------------------
        var flag = ''; //flag this should always be "P"?
        
        var buyer = getBuyerNumber(itemNumber); //buyer
        var restockCharge = 0; //restock charge 7s 2
        var documentNumber = 0; //document no. created when the file is read on 400.
        extendedRetail = request.body.ExtenededRetail; //extended retail 7s 2
        var sclUser = request.body.UserKeyingClaim; //user
        var assignment = 0; //assignment
        var container = formatDate(); //container
        var callingName = request.body.CallingName; //calling name
        // /*
        console.log(
          'recordID: ' + recordID,
          'claimNumber: ' + claimNumber,
          'claimDate: ' + claimDate,
          'packSize: ' + packSize,
          'STNUM: ' + STNUM,
          'itemNumber: ' + itemNumber,
          'claimType: ' + claimType, //if this is a 2 then it is a return and we may charge a restock fee (SCLSTK)
          'creditType: ' + creditType,
          'department: ' + department,
          'ledgerNumber: ' + ledgerNumber,
          'invNumOfOrder: ' + invNumOfOrder,
          'orderDate: ' + orderDate,
          'slot: ' + slot,
          'pickCode: ' + pickCode,
          'slot1: ' + slot1,
          'slot3: ' + slot3,
          'qtyOnClaim: ' + qtyOnClaim,
          'caseOrOnly: ' + caseOrOnly,
          'tobaccoTax: ' + tobaccoTax,
          'perCaseCost: ' + perCaseCost,
          'extendedCost: ' + extendedCost,
          'flag: ' + flag,
          'buyer: ' + buyer,
          'restockCharge: ' + restockCharge, // = 5% of (total cost (extendedRetail??) * qty (qtyOnClaim??))
          'documentNumber: ' + documentNumber,
          'extendedRetail: ' + extendedRetail,
          'sclUser: ' + sclUser,
          'assignment: ' + assignment,
          'container: ' + container,
          'callingName: ' + callingName
        );
// */
// /*
        var sqlquery =
          'insert into PROFRONT.ENTCLM_CALLIN (' +
          'sclmrc, ' +
          'sclmno, ' +
          'sclmdt, ' +
          'sclmst, ' +
          'sclmit, ' +
          'sclmty, ' +
          'sclmcr, ' +
          'sclmld, ' +
          'sclmdp, ' +
          'sordin, ' +
          'sorddt, ' +
          'sordpc, ' +
          'sords1, ' +
          'sords3, ' +
          'sclmqt, ' +
          'sclmco, ' +
          'sclmtx, ' +
          'sclmcs, ' +
          'sclmex, ' +
          'sclmfl, ' +
          'sclmby, ' +
          'sclstk, ' +
          'scldoc, ' +
          'sclret, ' +
          'sclusr, ' +
          'sclasn, ' +
          'sclcnt, ' +
          'sclnam' +
          ') VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) with None';
        try {
          pjs.query(sqlquery, [
            recordID,
            claimNumber,
            claimDate,
            STNUM,
            itemNumber,
            claimType,
            creditType,
            ledgerNumber,
            department,
            invNumOfOrder,
            orderDate,
            pickCode,
            slot1,
            slot3,
            qtyOnClaim,
            caseOrOnly,
            tobaccoTax,
            perCaseCost,
            extendedCost,
            flag,
            buyer,
            restockCharge,
            documentNumber,
            extendedRetail,
            sclUser,
            assignment,
            container,
            callingName,
          ]);
          if (sqlstate >= '02000') {
            errorMsg = 'SQL error during write. SQLstate: ' + sqlstate;
            log.pushToLog(storeNumber, userName, errorMsg);
            console.log(
              'Item Code ' +
                itemNumber +
                ' Store ' +
                STNUM +
                ' Error Message: ' +
                errorMsg
            );
            response.status(400).send(errorMsg);
            return;
          }
        } catch (err) {
          errorMsg = 'Sql Insert catch error during Write.';
          log.pushToLog(storeNumber, userName, errorMsg);
          console.log(
            'Item Code ' +
              itemNumber +
              ' Store ' +
              STNUM +
              ' Error Message: ' +
              errorMsg
          );
          response.status(400).send(errorMsg);
          return;
        }
// */
        successMsg = 'Entry Claim submitted.';
        log.pushToLog(storeNumber, userName, successMsg);
        console.log(
          'Item Code ' +
            itemNumber +
            ' updated to Store ' +
            STNUM +
            ' for Claim Date ' +
            claimDate
        );
        response.status(200).send(successMsg);
      }
    }
  }
  //----------------------------------
  function goBadMethod() {
    var error = 'no method received';
    response.status(400).send(error);
  }
}
exports.run = writeEntClmInfo;

//Get todays date for claim sumbission date. -------------------------------------------------------------------------------------------
function formatDate() {
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1;
  var yyyy = today.getFullYear();

  if (dd < 10) {
    dd = '0' + dd;
  }

  if (mm < 10) {
    mm = '0' + mm;
  }

  today = dd + '/' + mm + '/' + yyyy;
  //today = today.replace(/-|T|:/g, "");
  today = today.replace('/', '');
  today = today.replace('/', '');
  today = today.substring(0, 10);
  console.log('today ' + today);

  return today;
}
//---------------------------------------------------------------------------------------------------------------------------------------

//Get BUYER from INVMST based on Item Number. --------------------------------------------------------------------------------
function getBuyerNumber(y) {
  var itemNumber = y;
  var recordSet;

  sqlquery = 'select buyer from PWAFIL.INVMST where ITMCDE = ?';
  recordSet = pjs.query(sqlquery, [itemNumber]);
  buyer = recordSet[0]['buyer'];
  return buyer;
}

function padLeadingZeros(num, size) {
  var s = num + '';
  while (s.length < size) s = '0' + s;
  return s;
}
