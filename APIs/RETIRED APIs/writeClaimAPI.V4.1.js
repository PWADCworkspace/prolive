/* Incoming INFO:
{
    "ClaimNumber": 15653,
    "StoreNumber": 568,
    "InvoiceNumber": 36489,
    "ItemNumber": 883172,
    "ClaimType": 1,
    "isRestockFee": "N",
    "CreditType": "C",
    "QuantityOnClaim": 1,
    "CaseOrOnly": "C",
    "CatchWeight" : 37.90,
    "ExtendedCost": 56.58,
    "ExtenededRetail": 64.05,
    "UserKeyingClaim": "Tyler",
    "CallingName": "Hobbs"
}
*/

async function writeEntClmInfo(request, response) {
  console.log("writeEntClm V4 running.");
  //Set pathing for Apps.
  const directPathToProlive = "../../prolive/APPs/";
  const directPathToProtest = "../../protest/APPs/";
  const directPathToProlog = "../../prologging/";

  let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");
  let ledgerNumber = pjs.require(directPathToProlive + "LedgerNumberAPP.js");

  //Define incomming variables.
  const invoiceNumber = request.body.InvoiceNumber;
  const storeNumber = request.body.StoreNumber;
  const itemNumberCheckDigit = request.body.ItemNumber;
  const quantityClaimed = request.body.QuantityOnClaim;
  const caseOrOnlyClaimed = request.body.CaseOrOnly;
  const claimNumber = request.body.ClaimNumber;
  const claimType = request.body.ClaimType;
  const creditType = request.body.CreditType;
  const isRestockFee = request.body.isRestockFee;
  const sclUser = request.body.UserKeyingClaim.trim();
  const callingName = request.body.CallingName.trim();
  const userName = sclUser + " " + callingName;
  const incommingCatchWeight = request.body.CatchWeight;
  let flag = "P";
  if (storeNumber == 700) flag = "";

  let errorMsg;
  let gatheredClaimInfoArray = [];
  let itemNumber = Math.floor(itemNumberCheckDigit / 10);

  pjs.define("extendedCost", {
    type: "packed decimal",
    length: 7,
    decimals: 2
  });
  pjs.define("extendedRetail", {
    type: "packed decimal",
    length: 7,
    decimals: 2
  });

  extendedCost = request.body.ExtendedCost; //extended cost 7s 2
  extendedRetail = request.body.ExtendedRetail; //extended retail 7s 2

  const originalDataForLogging =
    "InvoiceNumber: " +
    invoiceNumber +
    ", StoreNumber: " +
    storeNumber +
    ", ItemNumber: " +
    itemNumberCheckDigit +
    ", QuantityOnClaim: " +
    quantityClaimed +
    ", CaseOrOnly: " +
    caseOrOnlyClaimed +
    ", ClaimNumber: " +
    claimNumber +
    ", ClaimType: " +
    claimType +
    ", CreditType: " +
    creditType +
    ", isRestockFee: " +
    isRestockFee +
    ", IncomingCatchWeight: " +
    incommingCatchWeight +
    ", Flag: " +
    flag +
    ", ExtendedCost: " +
    extendedCost +
    ", ExtendedRetail: " +
    extendedRetail;

  console.log(originalDataForLogging);

  const currentPort = profound.settings.port;
  console.log(currentPort);

  let ENTCLMTable = "";

  if (currentPort === 80) {
    ENTCLMTable = "ENTCLM_CALLIN";
    console.log(
      `Running writeClaimAPI.V4.1 in PROD with table: ${ENTCLMTable}.`
    );
  } else {
    ENTCLMTable = "ENTCLMDEV";
    console.log(
      `Running writeClaimAPI.V4.1 in DEV with table: ${ENTCLMTable}.`
    );
  }

  // ERROR CHECKS FOR MISSING DATA COMMING IN---------------------------------------------------------------------------
  if (!invoiceNumber) {
    errorMsg = "Invoice Number is required";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.1",
      userName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  if (!itemNumberCheckDigit) {
    errorMsg = "Item Number is required";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.1",
      userName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  if (!storeNumber) {
    errorMsg = "Store Number is required";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.1",
      userName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  if (invoiceNumber.toString().length > 5) {
    errorMsg =
      "Invoice Number: " +
      invoiceNumber +
      " may be incorrect. It should have a length of 5";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.1",
      userName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  if (!quantityClaimed || quantityClaimed == 0) {
    errorMsg = "QuantityOnClaim needs to be greater than 0.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.1",
      userName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }
  // END ERROR CHECKS FOR MISSING DATA COMMING IN---------------------------------------------------------------------------

  //START CATCH WEIGHT QUERY HERE
  if (incommingCatchWeight > 0) {
    if (caseOrOnlyClaimed == "O" || caseOrOnlyClaimed == "o") {
      errorMsg = "Catch Weight is only allowed for a case claim.";
      await ProfrontTxtlog.pushToTxTLog(
        "writeClaimAPI.V4.1",
        userName,
        originalDataForLogging,
        errorMsg
      );
      response.status(400).send(errorMsg);
      return;
    }

    if (quantityClaimed != 1) {
      errorMsg = "Quantity on Claim must be 1 for Catch Weight Items.";
      await ProfrontTxtlog.pushToTxTLog(
        "writeClaimAPI.V4.1",
        userName,
        originalDataForLogging,
        errorMsg
      );
      response.status(400).send(errorMsg);
      return;
    }

    console.log(
      `Running catch weight query for item ${itemNumber} on invoice ${invoiceNumber} at store ${storeNumber} with a catch weight of ${incommingCatchWeight}`
    );

    sqlquery =
      "select " +
      "FDITEM as ItemCode, " +
      "FDINV as InvoiceNumber, " +
      "FDCHKD as CheckDigit, " +
      "FDDATE as InvoiceDate, " +
      "FDPKSZ as PackSize, " +
      "FDDESC as Description, " +
      "FDQTYS as QuantityShipped, " +
      "FDDEPT as Department, " +
      "FDSLOT as Slot, " +
      "FDSELL as StoreCost, " +
      "FDECST as Cost " + //this is extended cost.
      "from FICHED01 where FDINV = ? and FDSTOR = ? and FDITEM = ? and FDCWLB = ?";
    // gatheredClaimInfo.array = pjs.query(sqlquery, [invoiceNumber, storeNumber, itemNumber, incommingCatchWeight]);
    gatheredClaimInfoArray = pjs.query(sqlquery, [
      invoiceNumber,
      storeNumber,
      itemNumber,
      incommingCatchWeight
    ]);

    if (gatheredClaimInfoArray.length < 1) {
      errorMsg = `No item info was found for item number ${itemNumber} for invoice ${invoiceNumber} for store ${storeNumber} with a catch weight of ${incommingCatchWeight}.`;
      await ProfrontTxtlog.pushToTxTLog(
        "writeClaimAPI.V4.1",
        userName,
        originalDataForLogging,
        errorMsg
      );
      response.status(400).send(errorMsg);
      return;
    }
    writeClaim(gatheredClaimInfoArray);
  } else {
    //START NON-CATCH WEIGHT QUERY HERE

    console.log(
      `Running non-catch weight query for item ${itemNumber} on invoice ${invoiceNumber} at store ${storeNumber}`
    );

    sqlquery =
      "select " +
      "FDITEM as ItemCode, " +
      "FDINV as InvoiceNumber, " +
      "FDCHKD as CheckDigit, " +
      "FDDATE as InvoiceDate, " +
      "FDPKSZ as PackSize, " +
      "FDDESC as Description, " +
      "FDQTYS as QuantityShipped, " +
      "FDDEPT as Department, " +
      "FDSLOT as Slot, " +
      "FDSELL as StoreCost, " +
      "FDECST as Cost " +
      "from FICHED01 where FDINV = ? and FDSTOR = ? and FDITEM = ?";
    //  gatheredClaimInfo.array = pjs.query(sqlquery, [invoiceNumber, storeNumber, itemNumber]);
    gatheredClaimInfoArray = pjs.query(sqlquery, [
      invoiceNumber,
      storeNumber,
      itemNumber
    ]);

    if (gatheredClaimInfoArray.length < 1) {
      errorMsg = `No item found for item number ${itemNumber} for invoice ${invoiceNumber} for store ${storeNumber}.`;
      await ProfrontTxtlog.pushToTxTLog(
        "writeClaimAPI.V4.1",
        userName,
        originalDataForLogging,
        errorMsg
      );
      response.status(400).send(errorMsg);
      return;
    }
    writeClaim(gatheredClaimInfoArray);
  }

  async function writeClaim(gatheredClaimInfoIn) {
    let department = gatheredClaimInfoIn[0]["department"];
    department = department.trim();
    department = department.substr(0, 1);

    ledgerNumber = await ledgerNumber.getLedgerNumber(department);
    if (ledgerNumber.Message) {
      await ProfrontTxtlog.pushToTxTLog(
        "writeClaimAPI.V4.1",
        userName,
        originalDataForLogging,
        ledgerNumber.Message
      );
      response.status(400).send(ledgerNumber.Message);
      return;
    }
    ledgerNumber = ledgerNumber.LedgerNumber;

    let invNumOfOrder = gatheredClaimInfoIn[0]["invoicenumber"];
    let invoiceDate1 = gatheredClaimInfoIn[0]["invoicedate"];
    let invoiceDate = invoiceDate1.toString();
    invoiceDate = invoiceDate.substr(2, 6);
    let slot = gatheredClaimInfoIn[0]["slot"]; //"123456"
    let pickCode = slot.substr(0, 1); //pick code '1'
    let slot1 = slot.substr(1, 2); // Part of slot '23'
    let slot3 = slot.substr(3, 3); // part of slot '456'
    let packSize;
    packSize = gatheredClaimInfoIn[0]["packsize"];
    packSize = formatPackSize(packSize);
    let storeCost = gatheredClaimInfoIn[0]["storecost"];
    let buyer = getBuyerNumber(itemNumber); //buyer

    if (claimType == 2 && (isRestockFee == "Y" || isRestockFee == "y")) {
      var restockCharge = extendedCost * 0.05;
      extendedCost = extendedCost - restockCharge;
    } else {
      restockCharge = 0;
    }

    const recordID = "S";
    const tobaccoTax = 0;
    const documentNumber = 0;
    const container = 0;
    let assignment = formatDate();
    let claimDate = formatDate();

    /*
    // TURN ON FOR TESTING!!!!!!
    console.log(
      "recordID: " + recordID, //done 
      "claimNumber: " + claimNumber, //done 
      "claimDate: " + claimDate, //done 
      "packSize: " + packSize, //can grab on INVMST
      "storeNumber: " + storeNumber, //done
      "itemNumber: " + itemNumber, //testing
      "claimType: " + claimType, //done
      "creditType: " + creditType, //done
      "department: " + department, //done
      "ledgerNumber: " + ledgerNumber, //done
      "invNumOfOrder: " + invNumOfOrder, //must get from FICHED01. For type 5 (not on INV.) use claim number
      "orderDate: " + invoiceDate, //must get from FICHED01
      "slot: " + slot, //can grab on INVMST
      "pickCode: " + pickCode, //can grab on INVMST
      "slot1: " + slot1, //can grab on INVMST
      "slot3: " + slot3, //can grab on INVMST
      "quantityClaimed: " + quantityClaimed, //done
      "caseOrOnlyClaimed: " + caseOrOnlyClaimed, //done
      "tobaccoTax: " + tobaccoTax, //done
      "storeCost: " + storeCost, //must get from FICHED01.
      "extendedCost: " + extendedCost, //must get from FICHED01???
      "flag: " + flag, //done 
      "buyer: " + buyer, //done 
      "restockCharge: " + restockCharge, //done 
      "documentNumber: " + documentNumber, //done
      "extendedRetail: " + extendedRetail, //must get from FICHED01.
      "sclUser: " + sclUser, //done
      "assignment: " + assignment, //done 
      "container: " + container, //done
      "callingName: " + callingName //done
    );
    */
    let sqlquery =
      `insert into ${ENTCLMTable} (` +
      "sclmrc, " +
      "sclmno, " +
      "sclmdt, " +
      "sclmst, " +
      "sclmit, " +
      "sclmty, " +
      "sclmcr, " +
      "sclmld, " +
      "sclmdp, " +
      "sordin, " +
      "sorddt, " +
      "sordpc, " +
      "sords1, " +
      "sords3, " +
      "sclmqt, " +
      "sclmco, " +
      "sclmtx, " +
      "sclmcs, " +
      "sclmex, " +
      "sclmfl, " +
      "sclmby, " +
      "sclstk, " +
      "scldoc, " +
      "sclret, " +
      "sclusr, " +
      "sclasn, " +
      "sclcnt, " +
      "sclnam" +
      ") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) with None";
    try {
      pjs.query(sqlquery, [
        recordID,
        claimNumber,
        claimDate,
        storeNumber,
        itemNumber,
        claimType,
        creditType,
        ledgerNumber,
        department,
        invNumOfOrder,
        invoiceDate,
        pickCode,
        slot1,
        slot3,
        quantityClaimed,
        caseOrOnlyClaimed,
        tobaccoTax,
        storeCost,
        extendedCost,
        flag,
        buyer,
        restockCharge,
        documentNumber,
        extendedRetail,
        sclUser,
        assignment,
        container,
        callingName
      ]);
      if (sqlstate >= "02000") {
        errorMsg = "SQL error during write. SQLstate: " + sqlstate;
        await ProfrontTxtlog.pushToTxTLog(
          "writeClaimAPI.V4.1",
          userName,
          originalDataForLogging,
          errorMsg
        );
        response.status(400).send(errorMsg);
        return;
      }
    } catch (err) {
      errorMsg = "Sql Insert catch error during Write.";
      await ProfrontTxtlog.pushToTxTLog(
        "writeClaimAPI.V4.1",
        userName,
        originalDataForLogging,
        errorMsg + ":" + err
      );
      response.status(400).send(errorMsg);
      return;
    }

    let successMsg = "Entry Claim submitted.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.1",
      userName,
      originalDataForLogging,
      successMsg
    );
    console.log(
      `Item Code ${itemNumber} updated for Store ${storeNumber} for for Claim Date ${claimDate}.`
    );
    response.status(200).send(successMsg);
  }
}
exports.run = writeEntClmInfo;

//Get todays date for claim sumbission date. -------------------------------------------------------------------------------------------
function formatDate() {
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1;
  var yyyy = today.getFullYear();

  if (dd < 10) {
    dd = "0" + dd;
  }

  if (mm < 10) {
    mm = "0" + mm;
  }

  today = `${yyyy}${mm}${dd}`;
  today = today.substring(2, 8);
  // console.log('today ' + today);
  return today;
}

//Get BUYER from INVMST based on Item Number. --------------------------------------------------------------------------------
function getBuyerNumber(y) {
  var itemNumber = y;
  var recordSet;
  let sqlquery = "select buyer from INVMST where ITMCDE = ?";
  recordSet = pjs.query(sqlquery, [itemNumber]);
  buyer = "";
  if (recordSet.length > 0 && recordSet != "") buyer = recordSet[0]["buyer"];
  return buyer;
}

// function padLeadingZeros(num, size) {
//   var s = num + "";
//   while (s.length < size) s = "0" + s;
//   return s;
// }

function formatPackSize(packSizeIn) {
  pS = packSizeIn.toString();
  var packDash = pS.indexOf("/");
  pS = pS.slice(0, packDash);
  pS = pS.trim();
  pS = parseInt(pS);
  return pS;
}

// function formatDeliveryDate(date) {
//   console.log(typeof date);
//   let s = date.toString();
//   let invoiceYear = s.substr(0, 4);
//   let invoiceMonth = s.substr(4, 2);
//   let invoiceDay = s.substr(6, 2);
//   let invoiceDate = new Date(invoiceYear, invoiceMonth, invoiceDay);
//   deliveryDate = addSubtractDate.subtract(invoiceDate, 1, "month"); //Becuase .js months start at 00.
//   deliveryDate = addSubtractDate.add(invoiceDate, 1, "day"); //Delivery date is always one day after the invoice date.
//   deliveryDate = deliveryDate.toISOString();
//   deliveryDate = deliveryDate.replace(/-|T|:/g, "");
//   deliveryDate = deliveryDate.substring(0, 8);
//   deliveryDate = parseInt(deliveryDate);
//   return deliveryDate;
// }
