// /* Incoming INFO:
// {
//     "ClaimNumber": 15653,
//     "StoreNumber": 568,
//     "InvoiceNumber": 36489,
//     "ItemNumber": 883172,
//     "ClaimType": 1,
//     "isRestockFee": "N",
//     "CreditType": "C",
//     "QuantityOnClaim": 1,
//     "CaseOrOnly": "C",
//     "CatchWeight" : 37.90,
//     "ExtendedCost": 56.58,
//     "ExtenededRetail": 64.05,
//     "UserKeyingClaim": "Tyler",
//     "CallingName": "Hobbs"
// }
// */

// async function writeEntClmInfo(request, response) {
//   console.log("writeEntClm V4 running.");

//   const directPathToProlive = "../../prolive/APPs/";
//   const directPathToProtest = "../../protest/APPs/";
//   const directPathToProlog = "../../Prologging/";

//   // let log = pjs.require(directPathToProlive + "PushToLogAPP.js");
//   let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");
//   let ledgerNumber = pjs.require(directPathToProlive + "LedgerNumberAPP.js");
//   let getClaimInfo = pjs.require(directPathToProlive + "getClaimInfo.V1.js");
//   let getCatchWeightClaimInfo = pjs.require(directPathToProlive + "getCatchWeightClaimInfo.V1.js");

//   //Define incomming variables.
//   const invoiceNumber = request.body.InvoiceNumber;
//   const storeNumber = request.body.StoreNumber;
//   const itemNumberCheckDigit = request.body.ItemNumber;
//   const quantityClaimed = request.body.QuantityOnClaim;
//   const caseOrOnlyClaimed = request.body.CaseOrOnly;
//   const claimNumber = request.body.ClaimNumber;
//   const claimType = request.body.ClaimType; // claim type '2' SCLMTY.
//   const creditType = request.body.CreditType; //credit = 'C' SCLMCR.
//   const isRestockFee = request.body.isRestockFee; //restock fee 'Y' SCLMRF.
//   const sclUser = request.body.UserKeyingClaim; //user
//   const callingName = request.body.CallingName;
//   const userName = sclUser + " " + callingName;
//   const incommingCatchWeight = request.body.CatchWeight;
//   // -----------------NOTE: SETTING THIS FLAG TO "P" WILL CAUSE THE 400 TO AUTO PROCESS! lEAVE BLANK UNTIL WE ARE READY FOR PRODUCTION!-------------------------------------------
//   let flag = "P";
//   if(storeNumber == 700) flag = "";

//   const originalDataForLogging = 
//     'InvoiceNumber: '+ invoiceNumber +
//     ', StoreNumber: '+ storeNumber +
//     ', ItemNumber: '+ itemNumberCheckDigit +
//     ', QuantityOnClaim: '+ quantityClaimed +
//     ', CaseOrOnly: '+ caseOrOnlyClaimed +
//     ', ClaimNumber: '+ claimNumber +
//     ', ClaimType: '+ claimType +
//     ', CreditType: '+ creditType +
//     ', isRestockFee: '+ isRestockFee +
//     ', IncomingCatchWeight: '+ incommingCatchWeight +
//     ', Flag: '+ flag;

//   //Predefine variables.
//   // let sqlquery;
//   let successMsg;
//   let errorMsg;
//   const recordID = "S"; // record ID 'S'
//   const tobaccoTax = 0; // tax if tobacco. we can stop passing this
//   const documentNumber = 0; //document no. created when the file is read on 400.
//   const container = 0;
//   let assignment = formatDate();
//   let claimDate = formatDate();

//   pjs.define("extendedCost", {
//     type: "packed decimal",
//     length: 7,
//     decimals: 2
//   });
//   pjs.define("extendedRetail", {
//     type: "packed decimal",
//     length: 7,
//     decimals: 2
//   });

//    extendedCost = request.body.ExtendedCost; //extended cost 7s 2
//    extendedRetail = request.body.ExtenededRetail; //extended retail 7s 2

//   // ERROR CHECKS FOR MISSING DATA COMMING IN---------------------------------------------------------------------------
//   if (!invoiceNumber) {
//     errorMsg = "Invoice Number is required";
//     await ProfrontTxtlog.pushToTxTLog("writeClaimAPI.V4", userName, originalDataForLogging, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }

//   if (!itemNumberCheckDigit) {
//     errorMsg = "Item Number is required";
//     await ProfrontTxtlog.pushToTxTLog("writeClaimAPI.V4", userName, originalDataForLogging, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }

//   if (!storeNumber) {
//     errorMsg = "Store Number is required";
//     await ProfrontTxtlog.pushToTxTLog("writeClaimAPI.V4", userName, originalDataForLogging, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }

//   if (invoiceNumber.toString().length > 5) {
//     errorMsg =
//       "Invoice Number: " +
//       invoiceNumber +
//       " may be incorrect. It should have a length of 5";
//       await ProfrontTxtlog.pushToTxTLog("writeClaimAPI.V4", userName, originalDataForLogging, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }

//   if (!quantityClaimed || quantityClaimed == 0) {
//     errorMsg = "QuantityOnClaim needs to be greater than 0.";
//     await ProfrontTxtlog.pushToTxTLog("writeClaimAPI.V4", userName, originalDataForLogging, errorMsg);
//     response.status(400).send(errorMsg);
//     return;
//   }
//   // END ERROR CHECKS FOR MISSING DATA COMMING IN---------------------------------------------------------------------------

//   if (incommingCatchWeight > 0) {
//     if (caseOrOnlyClaimed == "O" || caseOrOnlyClaimed == "o") {
//       errorMsg = "Catch Weight is only allowed for a case claim.";
//       await ProfrontTxtlog.pushToTxTLog("writeClaimAPI.V4", userName, originalDataForLogging, errorMsg);
//       response.status(400).send(errorMsg);
//       return;
//     }

//     if (quantityClaimed != 1) {
//       errorMsg = "Quantity on Claim must be 1 for Catch Weight Items.";
//       await ProfrontTxtlog.pushToTxTLog("writeClaimAPI.V4", userName, originalDataForLogging, errorMsg);
//       response.status(400).send(errorMsg);
//       return;
//     }

//     console.log(`Calling Catch Weight Claim Info Program.`);
//     let gatheredClaimInfo = await getCatchWeightClaimInfo.getCatchWeightClaimInfo(
//       storeNumber,
//       invoiceNumber,
//       itemNumberCheckDigit,
//       userName,
//       incommingCatchWeight,
//       quantityClaimed
//     );
//     // console.log(gatheredClaimInfo);

//     if (gatheredClaimInfo.error == "") {
//       writeClaim(gatheredClaimInfo);
//     } else {
//       await ProfrontTxtlog.pushToTxTLog(
//         "writeClaimAPI.V4",
//         userName,
//         originalDataForLogging,
//         "Error Status: " + gatheredClaimInfo.error
//       );
//       console.log(gatheredClaimInfo);
//       response.status(400).send(gatheredClaimInfo.error);
//       return;
//     }
//   } else {
//     console.log(`Calling Claim Info Program.`);
//     let gatheredClaimInfo = await getClaimInfo.getClaimInfo(
//       storeNumber,
//       invoiceNumber,
//       itemNumberCheckDigit,
//       userName,
//       quantityClaimed,
//       caseOrOnlyClaimed
//     );
//     // console.log(gatheredClaimInfo);

//     if (gatheredClaimInfo.error == "") { 
//       writeClaim(gatheredClaimInfo);
//     } else {
//       await ProfrontTxtlog.pushToTxTLog(
//         "writeClaimAPI.V4",
//         userName,
//         originalDataForLogging,
//         "Error Status: " + gatheredClaimInfo.error
//       );
//       console.log(gatheredClaimInfo);
//       response.status(400).send(gatheredClaimInfo.error);
//       return;
//     }
//   }

//   async function writeClaim(gatheredClaimInfoIn) {
//     console.log(gatheredClaimInfoIn);

//     let itemNumber = padLeadingZeros(gatheredClaimInfoIn.array[0]["ItemNumber"], 6);
//     itemNumber = itemNumber.slice(0, 6);
//     itemNumber = parseInt(itemNumber);

//     let department = gatheredClaimInfoIn.array[0]["Department"];
//     department = department.trim();
//     department = department.substr(0, 1);
//     console.log(`Department: ${department}`);

//     ledgerNumber = await ledgerNumber.getLedgerNumber(department);
//     if (ledgerNumber.Message) {
//       await ProfrontTxtlog.pushToTxTLog("writeClaimAPI.V4", userName, originalDataForLogging, ledgerNumber.Message);
//       response.status(400).send(ledgerNumber.Message);
//       return;
//     }
//     ledgerNumber = ledgerNumber.LedgerNumber;
//     console.log(`Ledger Number: ${ledgerNumber}`);

//     let invNumOfOrder = gatheredClaimInfoIn.array[0]["InvoiceNumber"];
//     console.log(`Invoice Number: ${invNumOfOrder}`);
//     let orderDate = gatheredClaimInfoIn.array[0]["OrderDate"];
//     console.log(`Order Date: ${orderDate}`);
//     orderDate = orderDate.toString();
//     orderDate = orderDate.substr(2, 6);
//     let slot = gatheredClaimInfoIn.array[0]["Slot"]; //"123456"
//     console.log(`Slot: ${slot}`);
//     let pickCode = slot.substr(0, 1); //pick code '1'
//     let slot1 = slot.substr(1, 2); // Part of slot '23'
//     let slot3 = slot.substr(3, 3); // part of slot '456'
//     let packSize = gatheredClaimInfoIn.array[0]["PackSize"];
//     console.log(`Pack Size: ${packSize}`);
//     let storeCost = gatheredClaimInfoIn.array[0]["StoreCost"];
//     console.log(`Store Cost: ${storeCost}`);
//     let buyer = getBuyerNumber(itemNumber); //buyer

//     if (claimType == 2 && (isRestockFee == "Y" || isRestockFee == "y")) {
//       var restockCharge = extendedCost * 0.05; // If Claim is 2 (return) the restock fee is 5% of the total * qty ordered
//       console.log("restockCharge: " + restockCharge);
//     } else {
//       restockCharge = 0; //restock charge 7s 2
//     }

//     // /*
//     // TURN ON FOR TESTING!!!!!!
//     console.log(
//       "recordID: " + recordID, //done 
//       "claimNumber: " + claimNumber, //done 
//       "claimDate: " + claimDate, //done 
//       "packSize: " + packSize,
//       "storeNumber: " + storeNumber, //done
//       "itemNumber: " + itemNumber,
//       "claimType: " + claimType, //done
//       "creditType: " + creditType, //done
//       "department: " + department, //done
//       "ledgerNumber: " + ledgerNumber, //done
//       "invNumOfOrder: " + invNumOfOrder,
//       "orderDate: " + orderDate,
//       "slot: " + slot,
//       "pickCode: " + pickCode,
//       "slot1: " + slot1,
//       "slot3: " + slot3,
//       "quantityClaimed: " + quantityClaimed, //done
//       "caseOrOnlyClaimed: " + caseOrOnlyClaimed, //done
//       "tobaccoTax: " + tobaccoTax, //done
//       "storeCost: " + storeCost,
//       "extendedCost: " + extendedCost,
//       "flag: " + flag, //done 
//       "buyer: " + buyer, //done 
//       "restockCharge: " + restockCharge,
//       "documentNumber: " + documentNumber, //done
//       "extendedRetail: " + extendedRetail,
//       "sclUser: " + sclUser, //done
//       "assignment: " + assignment, //done 
//       "container: " + container, //done
//       "callingName: " + callingName //done
//     );
//     // */
//     // /*
//   // TURN OFF FOR TESTING!!!!!
//           let sqlquery =
//             'insert into PROFRONT.ENTCLM_CALLIN (' +
//             'sclmrc, ' +
//             'sclmno, ' +
//             'sclmdt, ' +
//             'sclmst, ' +
//             'sclmit, ' +
//             'sclmty, ' +
//             'sclmcr, ' +
//             'sclmld, ' +
//             'sclmdp, ' +
//             'sordin, ' +
//             'sorddt, ' +
//             'sordpc, ' +
//             'sords1, ' +
//             'sords3, ' +
//             'sclmqt, ' +
//             'sclmco, ' +
//             'sclmtx, ' +
//             'sclmcs, ' +
//             'sclmex, ' +
//             'sclmfl, ' +
//             'sclmby, ' +
//             'sclstk, ' +
//             'scldoc, ' +
//             'sclret, ' +
//             'sclusr, ' +
//             'sclasn, ' +
//             'sclcnt, ' +
//             'sclnam' +
//             ') VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) with None';
//           try {
//             pjs.query(sqlquery, [
//               recordID,
//               claimNumber,
//               claimDate,
//               storeNumber,
//               itemNumber,
//               claimType,
//               creditType,
//               ledgerNumber,
//               department,
//               invNumOfOrder,
//               orderDate,
//               pickCode,
//               slot1,
//               slot3,
//               quantityClaimed,
//               caseOrOnlyClaimed,
//               tobaccoTax,
//               storeCost,
//               extendedCost,
//               flag,
//               buyer,
//               restockCharge,
//               documentNumber,
//               extendedRetail,
//               sclUser,
//               assignment,
//               container,
//               callingName,
//             ]);
//             if (sqlstate >= '02000') {
//               errorMsg = 'SQL error during write. SQLstate: ' + sqlstate;
//               await ProfrontTxtlog.pushToTxTLog("writeClaimAPI.V4", userName, originalDataForLogging, errorMsg);
//               console.log(
//                 'Item Code ' +
//                   itemNumber +
//                   ' Store ' +
//                   storeNumber +
//                   ' Error Message: ' +
//                   errorMsg
//               );
//               response.status(400).send(errorMsg);
//               return;
//             }
//           } catch (err) {
//             errorMsg = 'Sql Insert catch error during Write.';
//             await ProfrontTxtlog.pushToTxTLog("writeClaimAPI.V4", userName, originalDataForLogging, errorMsg);
//             console.log(
//               'Item Code ' +
//                 itemNumber +
//                 ' Store ' +
//                 storeNumber +
//                 ' Error Message: ' +
//                 errorMsg
//             );
//             response.status(400).send(errorMsg);
//             return;
//           }
//   // */
//   successMsg = `Success: Claim:${claimNumber}, item:${itemNumber}, clmtype:${claimType}, QTY:${quantityClaimed}, value:${extendedCost} clmdate:${claimDate}`;

//   console.log(`Item Code ${itemNumber} updated for Store ${storeNumber} for for Claim Date ${claimDate}.`);
//   await ProfrontTxtlog.pushToTxTLog("writeClaimAPI.V4", userName, originalDataForLogging, successMsg);

//   response.status(200).send("Entry Claim/s submitted.");
//   }
// }
// exports.run = writeEntClmInfo;

// //Get todays date for claim sumbission date. -------------------------------------------------------------------------------------------
// function formatDate() {
//   var today = new Date();
//   var dd = today.getDate();
//   var mm = today.getMonth() + 1;
//   var yyyy = today.getFullYear();

//   if (dd < 10) {
//     dd = "0" + dd;
//   }

//   if (mm < 10) {
//     mm = "0" + mm;
//   }

//   today = `${yyyy}${mm}${dd}`;
//   today = today.substring(2, 8);
//   // console.log('today ' + today);
//   return today;
// }

// //Get BUYER from INVMST based on Item Number. --------------------------------------------------------------------------------
// function getBuyerNumber(y) {
//   var itemNumber = y;
//   var recordSet;
//   let sqlquery = "select buyer from PWAFIL.INVMST where ITMCDE = ?";
//   recordSet = pjs.query(sqlquery, [itemNumber]);
//   buyer = "";
//   if (recordSet.length > 0 && recordSet != "") buyer = recordSet[0]["buyer"];
//   return buyer;
// }

// function padLeadingZeros(num, size) {
//   var s = num + "";
//   while (s.length < size) s = "0" + s;
//   return s;
// }
