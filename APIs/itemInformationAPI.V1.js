const dayjs = require("dayjs");

async function itemInformation(request, response) {
  console.log(`itemInformation V1 is running....${dayjs()}`);

  const directPathToProlog = "../../Prologging/";

  let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

  //Setting table and query
  const table = `zapwhseoh`;
  const program = `ZARWHSEOH`;
  let dataQuery = `select zadata from ${table} where zaguid = ?`;
  let data;

  //Setting API response
  var returnObj = {};
  returnObj.errorMessage = "";

  

  //Defining variables
  pjs.define("uuid", { type: "char", length: 36 });
  pjs.define("item", { type: "packed decimal", length: 5, decimals: 0 });
  pjs.define("ErrorMessageOut", { type: 'char', length: 256 });
  pjs.define("upc", { type: "packed decimal", length: 16, decimals: 0 });


  //Setting values
  uuid = pjs.newUUID();
  let barcode = request.query.barcode; //Will be either item code or upc
  let codeType = request.query.codeType;
  let inputMethod = request.query.inputMethod;

  upc = 0;
  item = 0;
  ErrorMessageOut = "";

  const incomingDataForLogging = `code : ${barcode}, codeType: ${codeType}, inputMethod: ${inputMethod}`;
  console.log("itemInformation V1 Incoming Data: " + incomingDataForLogging);

  //Data validation and manipulation
  if(!Number(barcode) === 0 || barcode.length == 0){
    returnObj.errorMessage = "Please ensure to enter a code that is not zero.";
    console.log(returnObj.errorMessage);
    await ProfrontTxtlog.pushToTxTLog(
      "itemInformation V1",
      `Code: ${barcode}`,
      incomingDataForLogging,
      returnObj.errorMessage
    );
    response.status(400).send(returnObj);
    return;
  }

  if(!codeType && !inputMethod){
    returnObj.errorMessage = "The Code Type and Input Method are required.";
    console.log(returnObj.errorMessage);
    await ProfrontTxtlog.pushToTxTLog(
      "itemInformation V1",
      `Code: ${barcode}`,
      incomingDataForLogging,
      returnObj.errorMessage
    );
    response.status(400).send(returnObj);
    return;
  }

  //UPC-A Check
  if(codeType == "UPC-A" && barcode.length != 12){
    returnObj.errorMessage = "Please enter a valid 12 digit UPC-A code.";
    console.log(returnObj.errorMessage);
    await ProfrontTxtlog.pushToTxTLog(
      "itemInformation V1",
      `Code: ${barcode}`,
      incomingDataForLogging,
      returnObj.errorMessage
    );
    response.status(400).send(returnObj);
    return;
  }

    //UPC-A Check
    if(codeType == "UPC-E" && barcode.length != 8){
      returnObj.errorMessage = "Please enter a valid 8 digit UPC-E code.";
      console.log(returnObj.errorMessage);
      await ProfrontTxtlog.pushToTxTLog(
        "itemInformation V1",
        `Code: ${barcode}`,
        incomingDataForLogging,
        returnObj.errorMessage
      );
      response.status(400).send(returnObj);
      return;
    }

    //Item Number Check
    if(codeType == "ItemCode" && barcode.length > 6){
      returnObj.errorMessage = "Please enter a valid item number of up to 6 digits.";
      console.log(returnObj.errorMessage);
      await ProfrontTxtlog.pushToTxTLog(
        "itemInformation V1",
        `Code: ${barcode}`,
        incomingDataForLogging,
        returnObj.errorMessage
      );
      response.status(400).send(returnObj);
      return;
    }

  if(codeType == 'ItemCode') {
    item = Number(barcode.slice(0, -1));
  } else if(codeType == 'UPC-A'){
    upc = Number(barcode.substring(0, barcode.length - 1)); //Removing first digit
  } else if(codeType == 'UPC-E') {
    barcode = await convertUPCEtoUPCA(barcode);
    upc = Number(barcode.substring(0, barcode.length - 1)); //Removing first digit

  } else{
    returnObj.errorMessage = "Please ensure the CodeType is either ItemCode, UPC-E, or UPC-A.";
    console.log(returnObj.errorMessage);
    await ProfrontTxtlog.pushToTxTLog(
      "itemInformation V1",
      `Code: ${barcode}`,
      incomingDataForLogging,
      returnObj.errorMessage
    );
    response.status(400).send(returnObj);
    return;
  }


  const rpgParms = `
    uuid: ${uuid},
    upc: ${upc},
    item: ${item},
    ErrorMessageOut: ${ErrorMessageOut}
    `;
  console.log("itemInformation V1 RPG Parms: " + rpgParms);


  //Calling RPG Program
  try {
    console.log("Calling ZARWHSEOH program...");
    pjs.call(
      `${program}`, //done
      pjs.parm("uuid"), //guid for zarclmhst
      pjs.parm("upc"), //store for zarclmhst
      pjs.parm("item"), //item for zarclmhst
      pjs.refParm("ErrorMessageOut") //invoice for zarclmhst
    );
  } catch (err) {
    console.log(err);
    returnObj.errorMessage = `Program Call Catch error. If the issue persists please contact retail systems.`;
    await ProfrontTxtlog.pushToTxTLog(
      "itemInformation V1",
      `UPC: ${upc}`,
      incomingDataForLogging,
      returnObj.errorMessage + err
    );
    response.status(400).send(returnObj);
    return;
   
  }



  if (ErrorMessageOut.trim().length > 0) {
    returnObj.errorMessage = ErrorMessageOut.trim();
    console.log(`ZARWHSEOH error response: ${returnObj.errorMessage}`);
    await ProfrontTxtlog.pushToTxTLog(
      "itemInformation V1",
      `UPC: ${upc}`,
      incomingDataForLogging,
      returnObj.errorMessage
    );
    response.status(404).send(returnObj);
    return;

  } else {
    try {
        console.log(`Querying ZAPWHSEOH table for results....`);
        data = pjs.query(dataQuery, [uuid]);
      } catch (err) {
        returnObj.errorMessage = `SQL catch error while grabbing results. If the issue persists please contact retail systems.`;
    
        await ProfrontTxtlog.pushToTxTLog(
            "itemInformation V1",
            `UPC: ${upc}`,
            incomingDataForLogging,
            returnObj.errorMessage + err
          );
          response.status(400).send(returnObj);
          return;
      }
  }

  if (data && data.length > 1 ){
    returnObj.errorMessage = `There seems to be two records with the same UPC number.`;

    await ProfrontTxtlog.pushToTxTLog(
        "itemInformation V1",
        `UPC: ${upc}`,
        incomingDataForLogging,
        returnObj.errorMessage
      );
      response.status(400).send(returnObj);
      return;
  }

  if (data && data.length > 0)
  {
      for(var i = 0 ; i<data.length ;  i++)
      {
          try{
            returnObj = JSON.parse(data[i]["zadata"]);

            // Remove leading zeros from UPC# and WhseOH
            returnObj['UPC#'] = parseInt(returnObj['UPC#']).toString();
            returnObj['WhseOH'] = parseInt(returnObj['WhseOH']).toString();

            //Combining Item Number and Check Digit
            returnObj['ItemNumber'] += returnObj['CheckDigit'];

            //Removing any white spaces
            returnObj['UPC#'] = returnObj['UPC#'].trim();
            returnObj['WhseOH'] = returnObj['WhseOH'].trim();
            returnObj['ItemNumber'] = returnObj['ItemNumber'].trim();
            returnObj['Description'] = returnObj['Description'].trim();
            returnObj['PackSize'] = returnObj['PackSize'].trim();
            returnObj['Status'] = returnObj['Status'].trim();
            returnObj['UnitCost'] = returnObj['UnitCost'].trim();
            returnObj['StatusInfo'] = returnObj['StatusInfo'].trim();
            returnObj['Group'] = returnObj['Group'].trim();
            returnObj['GroupDesc'] = returnObj['GroupDesc'].trim();
          }
          catch (err){
            returnObj.errorMessage = "An error occured while the parsing data. If the issue persists, please contact Retail Systems.";
            await ProfrontTxtlog.pushToTxTLog(
              "itemInformation V1",
              `UPC: ${upc}`,
              incomingDataForLogging,
              returnObj.errorMessage
            );              
            console.log("bad json:"+data[i]["zadata"] + "error" + err);
            response.status(400).send(returnObj);
            return;
          }
      }
  }

  console.log("itemInformation V1 Response ");

  console.log(returnObj);

  response.status(200).send(returnObj);

  console.log(`itemInformation V1 is done running....${dayjs()}`);

 //This function is to convert a upc-e to a upc-a code to match the format in the 'i'
 function convertUPCEtoUPCA(upce) {
  console.log('Inside of convertUPCEtoUPCA');
  console.log(`UPC-E: ${upce}`);
  //EXAMPLE: 05244410

  // Extract "1"
  var last_digit_upce = upce.charAt(6);

  //Extract '0'
  var checkDigit = upce.charAt(7);

  var upca;

  // Determine the conversion scenario based on the last digit of the UPC-E code
  switch (last_digit_upce) {
    case '0':
    case '1':
    case '2':
        // Scenario 1: Concate two digits(52), last digit(1), and 4 zeros (0000)
        upca = '0' + upce.substring(1, 3) + last_digit_upce + '0000' + upce.substring(3, 6) + checkDigit;
        break;
    case '3':
        // Scenario 2: Concate three digits(524), 5 zeros, and last two digits()
        upca = upce.substring(0, 4) + '00000' + upce.substring(4, 6) + '0';
        break;
    case '4':
        // Scenario 3: Concate 4 digits(5244), 5 zeros, and last digit of manufacturer code
        upca = upce.substring(0, 5) + '00000' + last_digit_upce + '0';
        break;
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
        // Scenario 4: Concate five digits(52244), 4 zeros, and last digit of UPC-E
        upca = upce.substring(0, 6) + '0000' + last_digit_upce + '0';
        break;
    default:
        console.log('Invalid UPC-E code');
    return 0;
    }

    console.log('UPC-A:', upca);
    return upca;
  }

}
exports.run = itemInformation;
