/*

Incoming Data: 
{
    "FreezerBoxID": 1006,
    "StoreNumber": 700,
    "ItemNumber": 70,
    "ClaimNumber": 199997,
    "ClaimDate" : '092122'
    "ReceivedDate": '102222',
    "QuantityOnClaim": 30,
    "UserFirstName": "Tyler",
    "UserLastName": "Hobbs"
    "ReceivedMethod" : "Retired"
}

Author: Samuel Gray
Date: 10/27/2023
Purpose: Processes tote claims to write to entclm file

// writeFreezerBoxToteAPI Version 2.1
  app.post("/v2.1/writeFreezerBoxToteAPI/", 
  profoundjs.express("protest/APIs/writeFreezerboxTotesAPI.V2.1"));

    Change Log:
     JIRA Ticket PROCLM-34: https://pwadc.atlassian.net/browse/PROCLM-34:
        ---Integrated functionality to write to the new CMXREFP table, including support for long claim numbers.
        -- Introduced Invoice Number with a default setting of 0 for new table entries.
        -- Updated UserKeyingClaim to UserFirstName and CallingName to UserLastName.
        -- Refactored all instances from writeFreezerboxTotesAPI.V2 to writeFreezerboxTotesAPI.V2.1.
        -- Adjusted sql query to fbclaimlog table to include billed store when flagging

      JIRA TICKET PROCLM-42:
        -- Removed receiver id from requesting body
        -- Added receivedMethod to know method of credit(recycled, retired, or repurpose)
        -- Removed store update variable b/c this we will be done in getLucas API
        -- Replaced receiverId with userName in fbclaimlog sql update
      
      JIRA TICKET PROCLM-40:
        -- Formated the claim date to be yymmdd format for enclm file
    
      JIRA TICKET PROCLM-45: 
      -- Added in dev table and adjusted sql queries to use template literals using the claimLogTable.
      -- Renamed array to 'success' and made it a string.
      -- Replaces all instances of returnObj with returnObj.error when writing to txt log.

      JIRA TICKET PROCLM-83:
        -- Removed any logic that pertained to freezer boxes
        -- Added additonal logging
        -- Renamed API from writeFreezerBoxToteClaim to writeToteClaim
*/
async function writeToteClaim(request, response) {

  //Setting modules
  const dayjs = require("dayjs");

  //Setting paths
  const directPathToProlive = "../../Prologging/";
  const ProfrontTxtlog = pjs.require(directPathToProlive + "PushToTxtLogAPP.js");

  //Grabbing current port
  const currentPort = profound.settings.port;
  const programName = "writeToteAPI.V2.1";


  //Setting tables
  let ENTCLMTable = "PROFRNTDEV.ENTCLMDEV";
  
  if (currentPort === 80) { 
    ENTCLMTable = "ENTCLM_CALLIN";
  }

  //Setting API response
  let returnObj = {};
  returnObj.success = ""; 
  returnObj.error = "";

  console.log(`${programName} is running on ${currentPort} with the following tables: ${ENTCLMTable}...${dayjs().format('YYYY-MM-DD HH:mm:ss')} `);

  //Grabbing incoming request body
  const storeNumber = request.body.StoreNumber; //Store number
  const itemNumber = request.body.ItemNumber; //Item number ( 30 for tote)
  const claimNumber = request.body.ClaimNumber; //Long claim number
  let claimDate = request.body.ClaimDate; //Unformatted claim date
  const qtyOnClaim = request.body.QuantityOnClaim; //Quantity on claim
  const extendedCost = request.body.ExtendedCost;
  const storeCost = request.body.StoreCost;
  const userFirstName = request.body.UserFirstName.trim(); //
  const userLastName = request.body.UserLastName.trim(); // 
  const userName = userFirstName + " " + userLastName;

  const incomingDataForLogging =  `StoreNumber: ${storeNumber}, ItemNumber: ${itemNumber}, ClaimNumber: ${claimNumber}, ClaimDate: ${claimDate}, 
  StoreCost: ${storeCost}, FinalClaimAmount: ${extendedCost} QuantityOnClaim: ${qtyOnClaim}, UserName: ${userName}
  `;

  console.log("Incoming data: " + incomingDataForLogging);

  const shortClaimNumber = String(claimNumber).slice(-5); //Seperating claim number into short
    
  //Initializing other key variables
  //These variable will be used for inserting into entclm file - Majority of these variables won't be updated for this API
  const recordID = "S";
  const claimType = 2;
  const creditType = "C";
  const ledgerNumber = 213;
  const department = "J";
  const invNumOfOrder = 0;
  const orderDate = 0;
  const pickCode = 0;
  const slot1 = 0;
  const slot3 = 0;
  const caseOrOnly = "";
  const tobaccoTax = 0.0;
  const buyer = "";
  const restockCharge = 0.0;
  const documentNumber = 0;
  const extendedRetail = 0.0;
  const assignment = 0;
  const container = 0;
  const invoiceNumber = 0;
  const perCaseCost = storeCost;
  // let extendedCost = 0.0;
    
  console.log(`Performing error checks on incoming data...`);

  //If item number is not 30 then throw error
  if (itemNumber !== 30) {
    returnObj.error = `An Item Number of 30 (Tote) is required.`;
    await ProfrontTxtlog.pushToTxTLog(programName, incomingDataForLogging, returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }
    
  //If store number is missing or greater than 3 digits then throw error
  if (!storeNumber || storeNumber.toString().length > 3) {
    returnObj.error = `A valid Store Number is required.`;
    await ProfrontTxtlog.pushToTxTLog(programName, incomingDataForLogging, returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }
    
  //If claim number is missing then throw error
  if (!claimNumber) {
    returnObj.error = "A valid claim number is required.";
    await ProfrontTxtlog.pushToTxTLog(programName, incomingDataForLogging, returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }
    
  //If claim date is missing then throw error
  if (!claimDate) {
    returnObj.error = "A valid claim date is required.";
    await ProfrontTxtlog.pushToTxTLog(programName, incomingDataForLogging, returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }
    
  //If received date is missing then throw error
  if (!extendedCost) {
    returnObj.error = "The tote claim amount is required.";
    await ProfrontTxtlog.pushToTxTLog(programName, incomingDataForLogging, returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }
    
  //If quantity on claim is missing then throw error
  if (!qtyOnClaim) {
    returnObj.error = "A valid quantity is required.";
    await ProfrontTxtlog.pushToTxTLog(programName, incomingDataForLogging, returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }

  //If first name or last name are missing then throw error
  if (!userFirstName || userFirstName == "") {
    returnObj.error = "Users first and last name are required.";
    await ProfrontTxtlog.pushToTxTLog(programName, incomingDataForLogging, returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }

  //Setting processing flag
  //A value of 'P' means the record will be processed in entclm file 
  let flag = "P"; // TO DO: When release to PROD need to set to 'P'
  if (storeNumber == 700) flag = "";
    
  claimDate = dayjs(claimDate).format("YYYYMMDD");

  let trimmedClaimDate = claimDate.slice(2);

  claimDate = parseInt(claimDate); //This field will be used in CMXREFP file
  trimmedClaimDate = parseInt(trimmedClaimDate); // This field will be used in ENTCLM file

  /*
    Write claim to CMXREFP file to document the long claim number
    If sqlstate >= "02000" or catch error throw error back to user
  */

  console.log(`Inserting claim ${claimNumber} into CMXREFP file...`);
  let cmxrefpSQLQuery =
    "INSERT into CMXREFP (" +
    "cxclmlong, " + //Long Claim Number
    "cxclm, " + //Short Claim Number
    "cxstore, " + //Store Number
    "cxdate, " + //Claim Date
    "cxitem, " + //Item Number
    "cxinv " + //Invoice Number
    ") VALUES(?,?,?,?,?,?) with None"
  ;

  try {
    pjs.query(cmxrefpSQLQuery, [
      claimNumber,
      shortClaimNumber,
      storeNumber,
      trimmedClaimDate,
      itemNumber,
      invoiceNumber
    ]);

    if (sqlstate >= "02000") {
      returnObj.error = `SQL Insert error occurred while inserting claim ${claimNumber} into the CMXREFP TABLE. SQL state: ${sqlstate}`;
      await ProfrontTxtlog.pushToTxTLog(programName, userName, incomingDataForLogging, returnObj.error);
      response.status(400).send(returnObj.error);
      return;
    }
  } catch (err) {
    returnObj.error = `CATCH SQL Insert error while inserting claim ${claimNumber} into the CMXREFP TABLE.`;
    await ProfrontTxtlog.pushToTxTLog(programName, userName, incomingDataForLogging, returnObj.error + ":" + err);
    response.status(400).send(returnObj.error);
    return;
  }

  console.log("recordID: " + recordID, "shortClaimNumber: " + shortClaimNumber, "trimmedClaimDate: " + trimmedClaimDate, "storeNumber: " + storeNumber,
    "itemNumber: " + itemNumber, "claimType: " +  claimType, "creditType: " +  creditType, "ledgerNumber: " +  ledgerNumber, "department: " +  department,
    "invNumOfOrder: " +  invNumOfOrder, "orderDate: " +  orderDate, "pickCode: " +  pickCode, "slot1: "  + slot1, "slot3: "  + slot3, 
    "qtyOnClaim: " +  qtyOnClaim, "caseOrOnly: " +  caseOrOnly, "tobaccoTax: " +  tobaccoTax, "perCaseCost: " +  perCaseCost, "extendedCost: " +  extendedCost,
    "flag: " +  flag, "buyer: " +  buyer, "restockCharge: " +  restockCharge, "documentNumber: " +  documentNumber, "extendedRetail: " +  extendedRetail, 
    "userFirstName: " +  userFirstName, "assignment: " +  assignment, "container: " +  container, "userLastName: " +  userLastName
  );

  /*
    Write claim to entclm file to process the claim (payout the store)
    If sqlstate >= "02000" or catch error throw error back to user
  */
  console.log(`Inserting claim ${claimNumber} into ${ENTCLMTable} file...`);
  const entclmQuery =
    `insert into ${ENTCLMTable} (` +
    "sclmrc, " + // record ID
    "sclmno, " + // Claim No.
    "sclmdt, " + // Claim Date
    "sclmst, " + // Store
    "sclmit, " + // Item Number
    "sclmty, " + // Claim Type
    "sclmcr, " + // Credit
    "sclmld, " + // Ledger Number
    "sclmdp, " + // Department
    "sordin, " + // Invoice Number of Order
    "sorddt, " + // Order Date
    "sordpc, " + // Pick Code
    "sords1, " + // Slot 1
    "sords3, " + // Slot 3
    "sclmqt, " + // Quantity on Claim
    "sclmco, " + // Case or Only
    "sclmtx, " + // Tax if Tobacco
    "sclmcs, " + // Per Case Cost
    "sclmex, " + // Extended Cost
    "sclmfl, " + // Flag
    "sclmby, " + // Buyer
    "sclstk, " + // Restock Charge
    "scldoc, " + // Document Number
    "sclret, " + // Extended Retail
    "sclusr, " + // User
    "sclasn, " + // Assignment
    "sclcnt, " + // Container
    "sclnam" + // Callin Name
    ") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) with None"
  ;

  try {
    pjs.query(entclmQuery, [
      recordID,
      Number(shortClaimNumber), 
      trimmedClaimDate,
      storeNumber,
      itemNumber,
      claimType,
      creditType,
      ledgerNumber,
      department,
      invNumOfOrder,
      orderDate,
      pickCode,
      slot1,
      slot3,
      qtyOnClaim,
      caseOrOnly,
      tobaccoTax,
      perCaseCost,
      extendedCost,
      flag,
      buyer,
      restockCharge,
      documentNumber,
      extendedRetail,
      userFirstName,
      assignment,
      container,
      userLastName
    ]);

    if (sqlstate >= "02000") {
      returnObj.error = `SQL Insert error occurred while inserting into the ENTCLM TABLE. SQL state: ${sqlstate}`;
      await ProfrontTxtlog.pushToTxTLog(programName, userName, incomingDataForLogging, returnObj.error);
      response.status(400).send(returnObj.error);
      return;
    }
  } catch (err) {
    returnObj.error = "CATCH SQL Insert error occurred while inserting into the ENTCLM FILE";
    await ProfrontTxtlog.pushToTxTLog(programName, userName, incomingDataForLogging, returnObj.error + ":" + err);
    response.status(400).send(returnObj.error);
    return;
  }   
  
  returnObj.success = `Tote claim ${claimNumber} has been submitted for processing`;


  await ProfrontTxtlog.pushToTxTLog(programName,incomingDataForLogging,returnObj);

  console.log(returnObj.success);
  console.log(`${programName} is done running on ${currentPort}...${dayjs().format('YYYY-MM-DD HH:mm:ss')} `);

  response.status(200).send(returnObj.success);
  return;
}
exports.run = writeToteClaim;
  