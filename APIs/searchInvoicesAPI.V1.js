const dayjs = require("dayjs");

async function invoices_V1(request, response) {

    const directPathToProlog = "../../Prologging/";
    let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

    const programName = "invoices_V1";
    const currentPort = profound.settings.port;
    const currentDate = dayjs().format("YYYYMMDD");

    console.log(`${programName} is running....${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);

    //Declare API response
    let invoicesArray = [];
    let errorMessageArray = [];
    
    //Retrieve incoming request query parameters
    const storeNumber = request.query.storeNumber; 
    let startDateRange = request.query.startDateRange;
    let endDateRange = request.query.endDateRange;

    let message;

    const incomingDataForLogging = `storeNumber : ${storeNumber}, startDateRange: ${startDateRange}, endDateRange: ${endDateRange}`;

    console.log(incomingDataForLogging);

    if (!storeNumber) {
        message = 'Store Number is required';
        constructErrorResponse('MISSING_PARAMS', message,'');
        await ProfrontTxtlog.pushToTxTLog(
            programName,
            incomingDataForLogging,
            errorMessageArray
        );
        response.status(400).send(errorMessageArray);
        return;
    }

    if (!startDateRange) {
        message = 'Start Date Range is required';
        constructErrorResponse('MISSING_PARAMS',message ,'');
        await ProfrontTxtlog.pushToTxTLog(
            programName,
            incomingDataForLogging,
            message
        );
        response.status(400).send(errorMessageArray);
        return;
    }

    if (!endDateRange) {
        message = 'End Date Range is required';
        constructErrorResponse('MISSING_PARAMS', message,'');
        await ProfrontTxtlog.pushToTxTLog(
            programName,
            incomingDataForLogging,
            message
        );
        response.status(400).send(errorMessageArray);
        return;
    }

    const getInvoicesQuery = `
        SELECT 
            fhstor AS Store,
            fhinv AS InvoiceNumber,
            fhdate AS InvoiceDate,
            fhdept AS InvoiceDepartment,
            fhtamt AS InvoiceTotal,
            fhcwfg as CatchWeightInvoice
        FROM pwafil.ficheh01
        WHERE
            fhstor = ${storeNumber} AND
            fhdate between '${(Number(startDateRange))}' AND '${(Number(endDateRange))}'
        ORDER BY fhdate DESC
    `;

    console.log(getInvoicesQuery);

    try{
        var invoiceResults = pjs.query(getInvoicesQuery);

        if(sqlstate == '02000'){
            message = `No invoices were found for store ${storeNumber} between ${startDateRange} and ${endDateRange}`;
            constructErrorResponse('NOT_FOUND', message, 'SQL STATE: ' + sqlstate);
            await ProfrontTxtlog.pushToTxTLog(
              programName,
              incomingDataForLogging,
              message
            );
            response.status(404).send(errorMessageArray);
            return;
        }

        if(sqlstate > '02000'){
            message = `SQl state error occurred while grabbing invoices for store ${storeNumber}. Please try again.`;
            constructErrorResponse('SQL_ERROR', message, 'SQL STATE: ' + sqlstate);
            await ProfrontTxtlog.pushToTxTLog(
              programName,
              incomingDataForLogging,
              errorMessageArray
            );
            response.status(500).send(errorMessageArray);
            return;
        }        

    } catch(err){
        message = `Catch error occurred while grabbing invoices for store ${storeNumber}. Please try again.`;
        constructErrorResponse('Catch_ERROR', message,'Catch Error:' +err);
        await ProfrontTxtlog.pushToTxTLog(
          programName,
          incomingDataForLogging,
          message + err
        );
        response.status(500).send(errorMessageArray);
        return;        
    }

    console.log(`Number of invoices found for store ${storeNumber}: ${invoiceResults.length}`);

    invoiceResults.forEach(invoice => {
        let invoiceClaimable = false;
        let invoiceType = invoice.catchweightinvoice != '' ? 'CatchWeight' : 'Non-CatchWeight';

        if(!(Number(currentDate) - invoice.invoicedate > 2)){
            invoiceClaimable = true;
        }

        invoicesArray.push({
            invoiceNumber : invoice.invoicenumber,
            details: {
                invoiceDate : invoice.invoicedate,
                invoiceDepartment : invoice.invoicedepartment,
                invoiceTotal : invoice.invoicetotal,
                invoiceType : invoiceType,
                invoiceClaimable : invoiceClaimable,
            }
        });
    });     


    console.log(`${programName} is done running....${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);
    response.status(200).send(invoicesArray);

    function constructErrorResponse(errorCode,errorMessage,exceptionMessage){
        return errorMessageArray.push({
            code: errorCode,
            message: errorMessage,
            details: {
                enviroment: currentPort,
                timeStamp: dayjs().format('YYYY-MM-DD HH:mm:ss'),
                exception: exceptionMessage
            }

        });            
    }
}


exports.run = invoices_V1;

