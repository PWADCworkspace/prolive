/* Incoming INFO:
{
    "invoiceNumber" : 00000,
    "storeNumber" : 000,
}
*/

async function getEntClmInfo(request, response) {

  const directPathToProlog = "../../Prologging/";
  let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

  const invoiceNumber = request.body.InvoiceNumber;
  const storeNumber = request.body.StoreNumber;
  const originalDataForLogging = 'InvoiceNumber: ' + invoiceNumber + ', StoreNumber: ' + storeNumber;
  let returnMsg = "";

  if (!invoiceNumber) {
    returnMsg = "Invoice Number is required";
    await ProfrontTxtlog.pushToTxTLog("getInvoiceTypeAPI.V1", "automated call from eClaims", originalDataForLogging, returnMsg);
    response.status(400).send(returnMsg);
    return;
  }

  if (invoiceNumber.toString().length > 5) {
    returnMsg = "Invoice Number is too long";
    await ProfrontTxtlog.pushToTxTLog("getInvoiceTypeAPI.V1", "automated call from eClaims", originalDataForLogging, returnMsg);
    response.status(400).send("Invoice Number is too long, please enter a valid invoice number.");
    return;
  }

  if (!storeNumber) {
    returnMsg = "Store Number is required";
    await ProfrontTxtlog.pushToTxTLog("getInvoiceTypeAPI.V1", "automated call from eClaims", originalDataForLogging, returnMsg);
    response.status(400).send(returnMsg);
    return;
  }

  console.log(`Checking invoice type for INV:${invoiceNumber} for store:${storeNumber}.`);

  sqlquery =
    "select fdcwfg from FICHED01 where FDINV = ? and FDSTOR = ? ORDER BY FDDATE DESC";
  let InvoiceCatchWeightCheck = pjs.query(sqlquery, [
    invoiceNumber,
    storeNumber
  ]);

  if (InvoiceCatchWeightCheck.length < 1) {
    returnMsg = `No invoice:${invoiceNumber} found for store:${storeNumber}.`;
    await ProfrontTxtlog.pushToTxTLog("getInvoiceTypeAPI.V1", "automated call from eClaims", originalDataForLogging, returnMsg);
    response.status(400).send(returnMsg);
    return;
  }

  let check = InvoiceCatchWeightCheck[0].fdcwfg == "C" ? true : false;

  if (check) {
    returnMsg = "Catch Weight";
    console.log(returnMsg);
    await ProfrontTxtlog.pushToTxTLog("getInvoiceTypeAPI.V1", "automated call from eClaims", originalDataForLogging, returnMsg);
    response.status(200).send(returnMsg);
    return;
  } else {
    returnMsg = "Non-Catch Weight";
    console.log(returnMsg);
    await ProfrontTxtlog.pushToTxTLog("getInvoiceTypeAPI.V1", "automated call from eClaims", originalDataForLogging, returnMsg);
    response.status(200).send(returnMsg);
    return;
  }
}
exports.run = getEntClmInfo;
