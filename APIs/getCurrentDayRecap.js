/*
Author: Jake Lovett
Date: 01/30/2025

Purpose: This API will return a list of stores which ran recaps for the current day.

Modified by: Jake Lovett
Modified Date: 01/31/2025
*/

// #region Imports
const dayjs = require("dayjs");
// #endregion

async function main(request, response) {

  // #region Function-level variables
  const currentPort = profound.settings.port;
  const programName = "Get_Current_Day_Recap";

  //Retrieve the current day of week value
  const currentWeekday = dayjs().day();

  //Declare API response
  let storesArray = []; 
  let errorMessageArray = [];
  // #endregion

  console.log(`${programName} is running on ${currentPort} with currentWeekday: ${currentWeekday}... ${dayjs().format("YYYY-MM-DD HH:mm:ss")}`);

  //Retrieve the stores that recap today
  try {
    const query = `SELECT strnum FROM samuel.storelistw WHERE strrecap = ${currentWeekday}`;

    let tempStores = pjs.query(query);

    //If sql state error occurs, respond with a 500 Internal Error
    if (sqlstate > 2000) {
      message = `Sqlstate error occurred while fetching stores.`;
      constructErrorResponse("SQL_ERROR", message, "");
      response.status(500).send(errorMessageArray);
      return;
    }

    //Create a new array containing the list of stores
    if (tempStores) {
      storesArray = tempStores.map((store) => store.strnum);
    }

    console.log(`${programName} is done running on ${currentPort} with currentWeekday: ${currentWeekday}... ${dayjs().format("YYYY-MM-DD HH:mm:ss")}`);

    //Respond with a 200 OK status and return the store list
    response.status(200).send(storesArray);
    return;
  } 
  //If catch error occurs while retrieving the stores, respond with a 500 Internal Error
  catch (err) 
  {
    message = "Catch error occured while fetching stores.";
    constructErrorResponse("CATCH_ERROR", message, err.message);
    response.status(500).send(errorMessageArray);
    return;
  }

  //Contructs the APIs error response
  async function constructErrorResponse(errorCode, errorMessage, exceptionMessage) {
    console.log(`${programName}: ${errorCode} ${errorMessage} ${exceptionMessage}`);
    return errorMessageArray.push({
      code: errorCode,
      message: errorMessage,
      details: {
        environment: currentPort,
        timeStamp: dayjs().format("YYYY-MM-DD HH:mm:ss"),
        exception: exceptionMessage,
      },
    });
  }
}

exports.run = main;
