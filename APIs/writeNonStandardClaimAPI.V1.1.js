/*
Created By: Samuel Gray
Create Date: 6/30/2023
Modified By: Samuel Gray
Modified Date: 07/17/2024
Purpose: This API processes Non-Standard claims originating from the eClaim system, ensuring the store gets credit or charged.

/* 
NOTES: 

Incoming INFO:
{
  "ClaimNumber": 813411,
  "StoreNumber": 700,
  "ItemNumber": 0,
  "Department": "P",
  "ClaimType": 8,
  "InvoiceNumber": 0,
  "QuantityOnClaim": 4,
  "isRestockFee": "N",
  "Buyer" : "J",
  "ApproverFirstName": "Amy",
  "ApproverLastName": "Parker",
  "BuyerFirstName": "Justin",
  "BuyerLastName": "Hartley",
  "StoreFirstName": "Samuel",
  "StoreLastName": "Gray",
  "ClaimAmount": 30,
  "ReasonForClaim": "Wrong Pricing.",
  "CaseOrOnly" : "C",
  "ClaimDate" : "2023-09-21",
  "isVMC" : false,
  "isRecall" : false,

}


claim type: 8 or 9

if type = 8 credit type (SCLMCR) = "C" for credit 
if type = 9 credit type (SCLMCR) = "" for charge & restock fee = 0

*/

async function createMiscClaim(request, response) {

  //Import external modules
  const addSubtractDate = require("add-subtract-date");
  const dayjs = require("dayjs");

  //Set path to APPs.
  const directPathToProlive = "../../prolive/APPs/";
  const directPathToProtest = "../../protest/APPs/";
  const directPathToProlog = "../../prologging/";

  //Declare APPs
  let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");
  let getLedgerNumber = pjs.require(directPathToProlive + "LedgerNumberAPP.js");

  const programName = 'writeNonStandardClaimAPI.V1.1';
  const currentPort = profound.settings.port; //current port the pgm is running on

  //Declare Response Object 
  let returnObj = {}; 
  returnObj.success = ""; 
  returnObj.error = "";    

  //Set dev tables
  let ENTCLMTable = "ENTCLMDEV";

  //Overide with prod tables
  if(currentPort === 80){
    ENTCLMTable = "ENTCLM_CALLIN";
  }

  console.log(`${programName} is running on ${currentPort} using the following table: ${ENTCLMTable}...${dayjs().format()}`);

  //Declare variable data types
  pjs.define("claimNumber", {
    type: "packed decimal",
    length: 5,
    decimals: 0,
  });

  pjs.define("storeNumber", {
    type: "packed decimal",
    length: 3,
    decimals: 0,
  });

  // pjs.define("itemNumberCheckDigit", {
  //   type: "packed decimal",
  //   length: 6,
  //   decimals: 0,
  // });

  pjs.define("department", {
    type: "char",
    length: 1,
  });

  pjs.define("claimType", {
    type: "packed decimal",
    length: 1,
    decimals: 0,
  });

  pjs.define("invoiceNumber", {
    type: "packed decimal",
    length: 5,
    decimals: 0,
  });

  pjs.define("quantityClaimed", {
    type: "packed decimal",
    length: 4,
    decimals: 0,
  });

  pjs.define("caseOrOnlyClaimed", {
    type: "char",
    length: 1,
  });

  pjs.define("isRestockFee", {
    type: "packed decimal",
    length: 7,
    decimals: 2,
  });

  pjs.define("buyer", {
    type: "char",
    length: 1,
  });

  pjs.define("claimAmount", {
    type: "packed decimal",
    length: 7,
    decimals: 2,
  });

  pjs.define("extendedRetail", {
    type: "packed decimal",
    length: 7,
    decimals: 2,
  });

  pjs.define("invNumOfOrder", {
    type: "packed decimal",
    length: 4,
    decimals: 0,
  });

  pjs.define("storeCost", {
    type: "packed decimal",
    length: 8,
    decimals: 4,
  });

  pjs.define("invoiceDate", {
    type: "packed decimal",
    length: 8,
    decimals: 0,
  });

  // pjs.define("packSize", {
  //   type: "char",
  //   length: 10,
  // });

  pjs.define("pickCode", {
    type: "char",
    length: 1,
  });

  pjs.define("slot1", {
    type: "char",
    length: 2,
  });

  pjs.define("slot3", {
    type: "char",
    length: 3,
  });

  //Retrieve Incoming Data
  const claimNumber = request.body.ClaimNumber; //int
  const storeNumber = request.body.StoreNumber; //int  
  const claimDate = request.body.ClaimDate; 
  const itemCode = request.body.ItemNumber; //D
  const isVMC = request.body.isVMC; //boolean
  const isRecall = request.body.isRecall; //boolean
  const claimType = request.body.ClaimType; //string
  const invoiceNumber = request.body.InvoiceNumber; //int
  const quantityClaimed = request.body.QuantityOnClaim; //int
  const caseOrOnlyClaimed = request.body.CaseOrOnly; //int
  const isRestockFee = request.body.isRestockFee; //string "Y" "N"
  const buyer = request.body.Buyer != undefined ? request.body.Buyer.trim() : ''; //string
  const approverFirstName = request.body.ApproverFirstName != undefined ? request.body.ApproverFirstName.trim() : ''; //string
  const approverLastName = request.body.ApproverLastName != undefined ? request.body.ApproverLastName.trim() : ''; //string
  const buyerFirstName = request.body.BuyerFirstName != undefined ? request.body.BuyerFirstName.trim() : ''; //string
  const buyerLastName = request.body.BuyerLastName != undefined ? request.body.BuyerLastName.trim() : ''; //string
  const storeFirstName = request.body.StoreFirstName != undefined ? request.body.StoreFirstName.trim() : ''; //string
  const storeLastName = request.body.StoreLastName != undefined ? request.body.StoreLastName.trim() : ''; //string

  let reasonForClaim = request.body.ReasonForClaim != undefined ? request.body.ReasonForClaim.trim() : '';  //string
  let ledgerNumber = request.body.ledgerNumber != undefined ? request.body.ledgerNumber : 0; //int
  let department = request.body.Department !=undefined ? request.body.Department.trim() : '' ; //string

  claimAmount = request.body.ClaimAmount !=undefined ? request.body.ClaimAmount : 0; //float  

  const approverUserName = approverFirstName.trim() + " " + approverLastName.trim();
  const buyerUserName = buyerFirstName.trim() + " " + buyerLastName.trim();
  const storeUserName = storeFirstName + " " + storeLastName;

  const maxLength = 300;
  reasonForClaim = reasonForClaim.length > maxLength ? reasonForClaim.substring(0, maxLength) : reasonForClaim;

  const incomingDataForLogging  =
    " claimNumber: " + claimNumber +
    ", storeNumber: " + storeNumber +
    ", itemCode: " + itemCode +
    ", department: " + department +
    ", claimType: " + claimType +
    ", invoiceNumber: " + invoiceNumber +
    ", quantityClaimed: " + quantityClaimed +
    ", caseOrOnlyClaimed: " + caseOrOnlyClaimed +
    ", claimAmount: " + claimAmount +
    ", isRestockFee: " + isRestockFee + 
    ", claimDate: " + claimDate +
    ", buyer: " + buyer +
    ", approverFirstName: " + approverFirstName +
    ", approverLastName: " + approverLastName +
    ", buyerFirstName: " + buyerFirstName +
    ", buyerLastName: " + buyerLastName +
    ", storeFirstName: " + storeFirstName +
    ", storeLastName: " + storeLastName +
    ", reasonForClaim: " + reasonForClaim +
    ", ledgerNumber: " + ledgerNumber +
    ", isVMC: " + isVMC +
    ", isRecall: " + isRecall 
  ;
  console.log("incomingDataForLogging: " + incomingDataForLogging );

  //Throw error if the claimNumber is not provided
  if (!claimNumber) {
    returnObj.error = "claimNumber is required";
    await ProfrontTxtlog.pushToTxTLog(programName,approverUserName,incomingDataForLogging,returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }

  const shortClaimNumber = String(claimNumber).slice(-5);

  //Throw error if the storeNumber is not provided
  if (!storeNumber) {
    returnObj.error = "storeNumber is required";
    await ProfrontTxtlog.pushToTxTLog(programName,approverUserName,incomingDataForLogging,returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }

  //Throw error if the claimType is not 8 (credit) or 9 (for charge)
  if (!claimType || (claimType !== 8 && claimType !== 9)) {
    returnObj.error = "claimType is required and must be 8 or 9.";
    await ProfrontTxtlog.pushToTxTLog(programName,approverUserName,incomingDataForLogging,returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }  

  //Throw error if the quantityClaimed is not greater than 0
  if (!quantityClaimed || quantityClaimed == 0) {
    returnObj.error = "quantityClaimed is required and needs to be greater than 0.";
    await ProfrontTxtlog.pushToTxTLog(programName,approverUserName,incomingDataForLogging,returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }  

  //Throw error if the caseOrOnlyClaimed is not provied
  if (!caseOrOnlyClaimed) {
    returnObj.error = "caseOrOnlyClaimed is required.";
    await ProfrontTxtlog.pushToTxTLog(programName,approverUserName,incomingDataForLogging,returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }  
  
  //Throw error if the buyer number is greater than one
  if(!buyer.length > 1) {
    returnObj.error = "buyer can not contain more than one value."
    await ProfrontTxtlog.pushToTxTLog(programName,approverUserName,incomingDataForLogging,returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }  

  //Throw error if the approverLastName or approverFirstName are not provided
  if (!approverFirstName || !approverLastName || approverFirstName.length == 0 || approverLastName.length == 0) {
    returnObj.error = "approverFirstName & approverLastName are required.";
    await ProfrontTxtlog.pushToTxTLog(programName,approverUserName,incomingDataForLogging,returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }

  //Throw error if the buyerFirstName or buyerLastName are not provided
  if (!buyerFirstName || !buyerLastName) {
    returnObj.error = "buyerFirstName & buyerLastName are required.";
    await ProfrontTxtlog.pushToTxTLog(programName,approverUserName,incomingDataForLogging,returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }

  //Throw error if the storeFirstName or storeLastName are not provided
  if (!storeFirstName || !storeLastName) {
    returnObj.error = "storeFirstName & storeLastName are required.";
    await ProfrontTxtlog.pushToTxTLog(programName,approverUserName,incomingDataForLogging,returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }   

  //Throw error if the reasonForClaim is not provided
  if(!reasonForClaim || reasonForClaim.length == 0){
    returnObj.error = "reasonForClaim is required.";
    await ProfrontTxtlog.pushToTxTLog(programName,approverUserName,incomingDataForLogging,returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }

  //Throw error if the claimDate is not provided
  if (!claimDate) {
    returnObj.error = "claimDate is required";
    await ProfrontTxtlog.pushToTxTLog(programName,approverUserName,incomingDataForLogging,returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }

  //Throw error if the isVMC or isRecall are not provided
  if (isVMC == undefined) {
    returnObj.error = "isVMC is required";
    await ProfrontTxtlog.pushToTxTLog(programName,approverUserName,incomingDataForLogging,returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }

    //Throw error if the isVMC or isRecall are not provided
    if (isRecall == undefined) {
      returnObj.error = "isRecall is required";
      await ProfrontTxtlog.pushToTxTLog(programName,approverUserName,incomingDataForLogging,returnObj.error);
      response.status(400).send(returnObj.error);
      return;
    }

  const formattedClaimDate = dayjs(claimDate).format("YYYYMMDD"); //Formating claim date
  let trimmedClaimDate = formattedClaimDate.slice(2); //Formating claim date

  //Initialize variables
  const recordID = "S";
  const tobaccoTax = 0;
  const documentNumber = 0;
  const container = 0;

  let assignment = dayjs().format("YYMMDD");
  writeDate = dayjs().format("YYYY-MM-DD");
  writeTime = dayjs().format("hmm");

  let itemNumber = 0;
  let slotAisle = ""; //ITEM INFORMATION
  let slotLevel = ""; //ITEM INFORMATION
  let packSize = ""; //INVOICE INFORMATION
  let trimmedPackSize = ""; //INVOICE INFORMATION
  let pS = ""; //INVOICE INFORMATION
  let findInvoiceInfo; //INVOICE INFORMATION

  slot1 = ""; //ITEM INFORMATION
  slot3 = ""; //ITEM INFORMATION
  extendedRetail = 0.0; //INVOICE INFORMATION
  invNumOfOrder = 0; //INVOICE INFORMATION
  storeCost = 0.0; //INVOICE INFORMATION
  invoiceDate = ""; //INVOICE INFORMATION
  fdmulti = 0; //INVOICE INFORMATION
  fdsrp = 0; //INVOICE INFORMATION
  calculatedExtendedRetail = 0.0; //INVOICE INFORMATION
  pickCode = ""; //ITEM INFORMATION


  let flag = "P"; //Processing Flag
  if (storeNumber == 700) flag = ""; //Remove flag for test stores

  //Calculate restock fee if set to 'Y'
  let restockCharge = 0;
  if (isRestockFee == "Y" || isRestockFee == "y") {
    restockCharge = claimAmount * 0.05;
  }

  //Set credit type based on claimtype
  let creditType = "";
  if (claimType == 8) creditType = "C";

  //Remove check digit from item code
  let code = 0;

  if(itemCode != 0) { 
    code = Math.floor(itemCode / 10);
    console.log(`Original Code: ${itemCode} vs Code without Checkdigit: ${code}`);
  } else {
    code = itemCode;
  }

  //If the claim is a VMC then set dept to empty string and set ledger number to 218
  if(isVMC){
    console.log(`This is a VMC Nonstandard claim... setting department to an empty string and ledger number to 218`);
    department = "";
    ledgerNumber = 218;
  }

  //If the claim is a Recall do nothing
  if(isRecall){
    console.log(`This is a Recall Nonstandard claim`);
  }  

  if(!isVMC && !isRecall){
    console.log(`This is a regular Nonstandard claim`);
  }


  //Retrieve item information based on code for NON VMC CLAIMS 
  if (code > 0 && isVMC == false) {

    console.log(`Grabbing item information for code ${code} `);

    let dynamicItemInfoQuery = 'SELECT itmcde, dept, picode, slotai, slotlv, slotno, buyer from INVMST';

    if(String(code).length <= 5){
      dynamicItemInfoQuery += ' WHERE itmcde = ?';
    } else {
      dynamicItemInfoQuery += ' WHERE upc = ?';
    }

    console.log(dynamicItemInfoQuery);

    try {
      var findItemInformation = await pjs.query(dynamicItemInfoQuery, [code]);
    } catch (err) {
      returnObj.error = `Catch error occurred while grabbing item information for code.`
      console.log(returnObj.error + err);
      await ProfrontTxtlog.pushToTxTLog(programName,incomingDataForLogging,returnObj.error + err);
      response.status(400).send(returnObj.error);
      return;
    }

    if (findItemInformation.length > 0) {
      itemNumber = findItemInformation[0]["itmcde"];
      pickCode = findItemInformation[0]["picode"];
      slotAisle = findItemInformation[0]["slotai"];
      slotLevel = findItemInformation[0]["slotlv"];
      slot1 = slotAisle + slotLevel;
      slot3 = findItemInformation[0]["slotno"];
      department = findItemInformation[0]["dept"];
    }

    ledgerNumber = await getLedgerNumber.getLedgerNumber(department);

    if (ledgerNumber.Message && isRecall == false ) {
      returnObj.error = ledgerNumber.Message;
      await ProfrontTxtlog.pushToTxTLog(programName,incomingDataForLogging,returnObj.error);
      response.status(400).send(returnObj.error);
      return;
    }

    ledgerNumber = ledgerNumber.LedgerNumber;
  }

  //Retrieve invoice information based on invoice and code for NON VMC CLAIMS 
  if (invoiceNumber > 0 && itemNumber > 0 && isVMC == false) {
    console.log(`Grabbing invoice information for invoice ${invoiceNumber} and code ${code}.`);

    const invoiceInfoQuery = "SELECT fdpksz, fdqtyo, fdsell, fdecst, fddate, fdmult, fdsrp FROM FICHED01 WHERE fdinv = ? AND fdstor = ? AND fditem = ?";

    try {
      findInvoiceInfo = pjs.query(invoiceInfoQuery, [
        invoiceNumber,
        storeNumber,
        itemNumber,
      ]);
    } catch (err) {
      returnObj.error = `Catch error occurred while grabbing invoice information for code ${code} and invoice ${invoiceNumber}.`
      console.log(returnObj.error + err);
      await ProfrontTxtlog.pushToTxTLog(programName,incomingDataForLogging,returnObj.error + err);
      response.status(400).send(returnObj.error);
      return;
    }

    if (findInvoiceInfo.length > 0) {
      invNumOfOrder = findInvoiceInfo[0]["fdqtyo"];
      storeCost = findInvoiceInfo[0]["fdsell"];
      extendedRetail = findInvoiceInfo[0]["fdecst"];
      invoiceDate = findInvoiceInfo[0]["fddate"];
      invoiceDate = dayjs(invoiceDate).format("YYMMDD");
      fdmulti = findInvoiceInfo[0]["fdmult"];
      fdsrp = findInvoiceInfo[0]["fdsrp"];
      packSize = findInvoiceInfo[0]["fdpksz"];
      let packDash = packSize.indexOf("/");
      pS = packSize.slice(0, packDash);
      pS = pS.trim();
      pS = parseInt(pS);
      trimmedPackSize = pS;

      if (fdmulti != 0) {
        calculatedExtendedRetail =
          (((fdsrp * 100) / fdmulti) * trimmedPackSize) / 100;
      } else {
        calculatedExtendedRetail = (fdsrp * 100 * trimmedPackSize) / 100;
      }
      extendedRetail = calculatedExtendedRetail;
    }
  }

  console.log(
    "recordID: " + recordID, //done
    "claimNumber: " + claimNumber, //INCOMING DATA
    "claimDate: " + claimDate, //INCOMING DATA
    "trimmedClaimDate" + trimmedClaimDate,
    "trimmedPackSize: " + trimmedPackSize, //can grab on INVMST D
    "storeNumber: " + storeNumber, //INCOMING DATA
    "itemNumber: " + itemNumber, //INCOMING DATA
    "claimType: " + claimType, //INCOMING DATA
    "creditType: " + creditType, //done
    "department: " + department, //INCOMING DATA
    "ledgerNumber: " + ledgerNumber, //done
    "invNumOfOrder: " + invNumOfOrder, //must get from FICHED01. For type 5 (not on INV.) use claim number D
    "invoiceDate: " + invoiceDate, //must get from FICHED01 D
    "pickCode: " + pickCode, //can grab on INVMST D
    "slot1: " + slot1, //can grab on INVMST D
    "slot3: " + slot3, //can grab on INVMST D
    "quantityClaimed: " + quantityClaimed, //INCOMING DATA
    "caseOrOnlyClaimed: " + caseOrOnlyClaimed, //INCOMING DATA
    "tobaccoTax: " + tobaccoTax, //done
    "storeCost: " + storeCost, //must get from FICHED01.D
    "claimAmount: " + claimAmount, //INCOMING DATA
    "flag: " + flag, //done
    "buyer: " + buyer, //done
    "restockCharge: " + restockCharge, //done
    "documentNumber: " + documentNumber, //done
    "extendedRetail: " + extendedRetail, //must get from FICHED01.D
    "assignment: " + assignment, //done
    "container: " + container, //done
    "approverName: " + approverUserName, //INCOMING DATA
    "buyerName: " + buyerUserName, //INCOMING DATA
    "storeName: " + storeUserName, //INCOMING DATA
    "reasonForClaim: " + reasonForClaim, //INCOMING DATA
    "isVMC: " + isVMC,
    "isRecall: " + isRecall
  );

  //Insert nonstandard information into CMENTSUPF file (nonstandard file) 
  console.log(`Inserting nonstandard record to cmentsupf table`);

  let cmentsupfSQLQuery = "insert into cmentsupf (" +
  "csclaim, " + //SHORT CLAIM NUM
  "csclaiml," + //LONG CLAIM NUM
  "csstore," + //STORE
  "csdept," + //DEPARTMENT
  "csctype," + //CLAIM TYPE (EITHER 8 OR 9)
  "cscredty," + //CREDIT TYPE ("" OR C)
  "csitem," + //ITEM NUMBER
  "csinv#," + //INVOICE NUMBER
  "csqty," + // CLAIMED QTY
  "csrestock," + //RESTOCK FEE APPLIED (N OR Y)
  "csbuyer," + //BUYER NUMBER
  "csapprover," + //APPROVER NAME
  "csbuyernam," + //BUYER NAME
  "csstoreusr," + //REQUESTER NAME
  "csclaimamt," + //CLAIM AMOUNT
  "csclaimrsn," + //REASON FOR CLAIM
  "csdate," + // INSERT DATE
  "cstime" + //INSERT TIME
  ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) with NONE";

  try {
   pjs.query(cmentsupfSQLQuery, [
    shortClaimNumber, 
    claimNumber, 
    storeNumber, 
    department,
    claimType,
    creditType,
    itemNumber,
    invoiceNumber,
    quantityClaimed,
    isRestockFee,
    buyer,
    approverUserName,
    buyerUserName,
    storeUserName,
    claimAmount,
    reasonForClaim,
    writeDate,
    writeTime
  ]);
  if (sqlstate >= "02000") {
    returnObj.error = `SQL state error occurred while writing nonstandard claim to cmentsupf file SQLstate: ${sqlstate}`;
    await ProfrontTxtlog.pushToTxTLog(programName, incomingDataForLogging, returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }
  } catch (err) {
    returnObj.error = "Catch error occurred while writing nonstandard claim to cmentsupf file.";
    console.log(returnObj.error + err);
    await ProfrontTxtlog.pushToTxTLog(programName,incomingDataForLogging,returnObj.error + err);
    response.status(400).send(returnObj.error);
    return;
  }

 //Insert nonstandard information into CMXREFP file (claim cross reference file) 
 console.log(`Inserting nonstandard record to CMXREFP table.`);

  let cmxrefpSQLQuery =
    "INSERT into CMXREFP (" +
    "cxclmlong, " + //LONG CLAIM NUM
    "cxclm, " + //SHORT CLAIM NUM
    "cxstore, " + //STORE 
    "cxdate, " + //CLAIM DATE
    "cxitem, " + //ITEM NUMBER
    "cxinv " + //INVOICE NUMBER
    ") VALUES(?,?,?,?,?,?) with None"
  ;

  try {
    pjs.query(cmxrefpSQLQuery, [
      claimNumber,
      shortClaimNumber,
      storeNumber,
      trimmedClaimDate,
      itemNumber,
      invoiceNumber
    ]);

    if (sqlstate >= "02000") {
      returnObj.error = `SQL state error occurred while writing nonstandard claim to CMXREFP file SQLstate: ${sqlstate}`;
      await ProfrontTxtlog.pushToTxTLog(programName, incomingDataForLogging, returnObj.error);
      response.status(400).send(returnObj.error);
      return;
    }
  } 
  catch (err) {
    returnObj.error = "Catch error occurred while writing nonstandard claim to CMXREFP file.";
    console.log(returnObj.error + err);
    await ProfrontTxtlog.pushToTxTLog(programName,incomingDataForLogging,returnObj.error + err);
    response.status(400).send(returnObj.error);
    return;
  }


  //Insert nonstandard information into ENTCLM file to process(credit or charge) the claim
  console.log(`Inserting new record in ${ENTCLMTable} File`);

  let entclmSQLQuery =
    `insert into ${ENTCLMTable} (` +
    "sclmrc, " + //RECORD ID
    "sclmno, " + //SHORT CLAIM NUM
    "sclmdt, " + //TRIMMED CLAIM DATE
    "sclmst, " + //STORE NUMBER
    "sclmit, " + //ITEM NUMBER
    "sclmty, " + //CLAIM TYPE (8 OR 9)
    "sclmcr, " + //CREDIT TYPE ("" OR C)
    "sclmld, " + //LEDGER NUM
    "sclmdp, " + //DEPARTMENT NUM
    "sordin, " + //INVOICE QUANTITY ORDERED
    "sorddt, " + //INVOICE DATE
    "sordpc, " + //PICK CODE
    "sords1, " + //SLOT 1
    "sords3, " + //SLOT 3
    "sclmqt, " + //CLAIMED QTY
    "sclmco, " + //CASE OR UNIT (C OR U?)
    "sclmtx, " + //TOBACCO TAX
    "sclmcs, " + // STORE COST
    "sclmex, " + //CLAIM AMOUNT/EXTENDED COST
    "sclmfl, " + //FLAG 
    "sclmby, " + //BUYER NUM
    "sclstk, " + //RESTOCK FEE (N OR Y)
    "scldoc, " + //DOCUMENT NUMBER
    "sclret, " + //EXTENDED RETAIL
    "sclusr, " + //APPROVER FIRST NAME
    "sclasn, " + //ASSIGNMENT NUM
    "sclcnt, " + //CONTAINER NUM
    "sclnam" + //APPROVER LAST NAME
    ") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) with None"
  ;

  try {
    pjs.query(entclmSQLQuery, [
      recordID, 
      shortClaimNumber, //CHANGED TO SHORT CLAIM NUMBER
      trimmedClaimDate, //CHANGED TO TRIMMED DATE
      storeNumber,
      itemNumber,
      claimType,
      creditType,
      ledgerNumber,
      department,
      invoiceNumber,
      invoiceDate,
      pickCode,
      slot1,
      slot3,
      quantityClaimed,
      caseOrOnlyClaimed,
      tobaccoTax,
      storeCost,
      claimAmount,
      flag,
      buyer,
      restockCharge,
      documentNumber,
      extendedRetail,
      approverFirstName,
      assignment,
      container,
      approverLastName,
    ]);
    if (sqlstate >= "02000") {
      returnObj.error = `SQL State error occurred while writing nonstandard claim to ${ENTCLMTable} file. SQL State: ${sqlstate}`;
      await ProfrontTxtlog.pushToTxTLog(programName,incomingDataForLogging,returnObj.error);
      response.status(400).send(returnObj.error);
      return;
    }
  } catch (err) {
    returnObj.error = `Catch error occurred while writing nonstandard to ${ENTCLMTable} file.`;
    console.log(returnObj.error + err);
    await ProfrontTxtlog.pushToTxTLog(programName,incomingDataForLogging,returnObj.error + err);
    response.status(400).send(returnObj.error);
    return;
  }

  returnObj.success = "Entry Claim submitted.";
  await ProfrontTxtlog.pushToTxTLog(programName,incomingDataForLogging,returnObj);
  response.status(200).send(returnObj.success);
}
exports.run = createMiscClaim;
