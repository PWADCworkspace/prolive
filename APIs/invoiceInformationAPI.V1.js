
/*
  Created By: Samuel Gray
  Date Create: 08/12/2024
  Modified By: Samuel Gray
  Date Modified: 11/04/2024

  Purpose: Retrieve invoice information for claiming items on the invoice
*/

//Importing NPM modules
const dayjs = require("dayjs");
const addSubtractDate = require("add-subtract-date");

async function invoiceInformation(request, response) {

  //Setting paths apps
  const directPathToProlog = "../../Prologging/";
  const directPathToProlive = "../../prolive/APPs/";

  const programName = "invoice_information_V1";
  const currentPort = profound.settings.port;
  const currentDate = dayjs().format("YYYYMMDD");

  console.log(`${programName} is running....${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);

  //Declare apps used in the pgm
  let FicheD01 = pjs.require(directPathToProlive + "FicheD01APP.V2.js"); //Invoice Lookup
  let EntClm = pjs.require(directPathToProlive + "EntClmAPP.V2.js"); //Existing 
  let ClmHst = pjs.require(directPathToProlive + "ClmHstAPP.V2.js"); //New app call is set
  let ClmEnt = pjs.require(directPathToProlive + "ClmEntAPP.V2.js"); //New app call is set
  let validateCheckDigitAPP = pjs.require(directPathToProlive + "ValidateCheckDigitAPP.js");
  let selector = pjs.require(directPathToProlive + "PickSelectorAPPV2.js");
  let buyerName = pjs.require(directPathToProlive + "BuyerNameAPP.js");
  let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");
  let CatchWeightFicheD01 = pjs.require(directPathToProlive + "CatchWeightFicheD01APP.V2.js");
  let CatchWeightEntClm = pjs.require(directPathToProlive + "CatchWeightEntClmAPP.V2.js");
  let CatchWeightClmHst = pjs.require(directPathToProlive + "CatchWeightClmHstAPP.V2.js");
  let CatchWeightClmEnt = pjs.require(directPathToProlive + "CatchWeightClmEntAPP.V2.js");

  //Declare API response
  let invoiceReturnObj = {
    invoice: {
      invoiceNumber: 0,
      invoiceDate: "",
      invoiceStore: 0,
      invoiceTotal: 0,
      invoiceType: '',
      invoiceCW: false,
      denyInvoice: false
    },
    invoiceItems: []    
  };

  let errorMessageArray = [];
  let message = "";
 
  //Defining query parameters data types
  // pjs.define("invoiceNumber", { type: "packed decimal", length: 5, decimals: 0 }); // how we define our data types 
  // pjs.define("storeNumber", { type: "packed decimal", length: 3, decimals: 0 });
  // pjs.define("userFirstName", {type: "string"});
  // pjs.define("lastFirstName", {type: "string"});

  //Retrieve incoming request query parameters
  const storeNumber = request.query.storeNumber; 
  const invoiceNumber = request.query.invoiceNumber;
  const invoiceDate = request.query.invoiceDate; 
  const userFirstName = request.query.userFirstName == undefined ? '' : request.query.userFirstName.trim().toUpperCase(); 
  const userLastName = request.query.userLastName == undefined ? '' : request.query.userLastName.trim().toUpperCase(); 
  const userName = userFirstName + " " + userLastName;

  const incomingDataForLogging = `invoiceNumber : ${invoiceNumber}, storeNumber: ${storeNumber}, user: ${userFirstName}, callingName: ${userLastName}`;  
  console.log(incomingDataForLogging);

  const numericInvoiceNumber = Number(storeNumber);
  const numericStoreNumber = Number(invoiceNumber);

  //Data validation 
  if(!invoiceNumber ||invoiceNumber == 0 || invoiceNumber > 99999){
    constructErrorResponse('MISSING_PARAMS',"Invoice Number is required." ,'');
    await ProfrontTxtlog.pushToTxTLog(
      programName,
      incomingDataForLogging,
      message
    );
    response.status(400).send(errorMessageArray);
    return;
  }    
  if(storeNumber == 0 || !storeNumber || storeNumber > 999){
    constructErrorResponse('MISSING_PARAMS',"Store Number is required." ,'');
    await ProfrontTxtlog.pushToTxTLog(
      programName,
      incomingDataForLogging,
      message
    );
    response.status(400).send(errorMessageArray);
    return;
  }

  if(userFirstName.length == 0 || userLastName.length == 0){
    constructErrorResponse('MISSING_PARAMS',"User first and last names are required." ,'');
    await ProfrontTxtlog.pushToTxTLog(
      programName,
      incomingDataForLogging,
      message
    );
    response.status(400).send(errorMessageArray);
    return;
  }

  const invoiceHeaderQuery = `
    SELECT 
      fhstor AS Store,
      fhinv AS InvoiceNumber,
      fhdate AS InvoiceDate,
      fhdept AS InvoiceDepartment,
      fhcwfg AS CatchWeightInvoice,
      fhtamt AS InvoiceTotal
    FROM pwafil.ficheH01
    WHERE 
      fhstor = ? AND 
      fhinv = ?  
  `;

  try{
    var invoiceHeaderResults = pjs.query(invoiceHeaderQuery, [numericInvoiceNumber, numericStoreNumber]);

    //Return 404 (Not Found) if invoice is not found
    if(sqlstate == 2000){
      message = `Invoice ${invoiceNumber} was not found for store ${storeNumber}.`;
      constructErrorResponse('NOT_FOUND', message,'');
      await ProfrontTxtlog.pushToTxTLog(
        programName,
        incomingDataForLogging,
        message
      );
      response.status(404).send(errorMessageArray);
      return;
    }
    
    //Return Bad Request for other sql state errors
    if(sqlstate > 2000){
      message = `SQL State error occurred while fetching invoice header information for ${invoiceNumber} for store ${storeNumber}.`;
      constructErrorResponse('SQL_ERROR', message,'');
      await ProfrontTxtlog.pushToTxTLog(
        programName,
        incomingDataForLogging,
        message
      );
      response.status(500).send(errorMessageArray);
      return;
    }
  } 
  catch(err)
  {
    console.log(err);
    message = `Catch error occurred while fetching invoice header information for ${invoiceNumber} for store ${storeNumber}.`;
    constructErrorResponse('Catch_ERROR', message,'');
    await ProfrontTxtlog.pushToTxTLog(
      programName,
      incomingDataForLogging,
      message + err
    );
    response.status(500).send(errorMessageArray);
    return;
  }

  if(invoiceHeaderResults.length > 1){
    message = `There were multiple invoices records for invoice ${invoiceNumber} and store ${storeNumber}. Please try again.`;
    constructErrorResponse('SQL_ERROR', message,'');
    await ProfrontTxtlog.pushToTxTLog(
      programName,
      incomingDataForLogging,
      message
    );
    response.status(500).send(errorMessageArray);
    return;
  }

  let invoiceClaimable = false;
  let isCWInvoice = invoiceHeaderResults[0].catchweightinvoice == '' ? false : true;
  let invoiceType = isCWInvoice == false ? 'Non-CatchWeight' : 'CatchWeight';

  if((Number(currentDate) - invoiceHeaderResults[0].invoicedate > 2)){
    invoiceClaimable = true;
  }

  invoiceReturnObj.invoice.invoiceNumber = invoiceHeaderResults[0].invoicenumber;
  invoiceReturnObj.invoice.invoiceDate = invoiceHeaderResults[0].invoicedate;
  invoiceReturnObj.invoice.invoiceStore = invoiceHeaderResults[0].store;
  invoiceReturnObj.invoice.invoiceTotal = invoiceHeaderResults[0].invoicetotal;
  invoiceReturnObj.invoice.invoiceType = invoiceType;
  invoiceReturnObj.invoice.invoiceCW = isCWInvoice;
  invoiceReturnObj.invoice.denyInvoice = invoiceClaimable;

  console.log(`Fetching invoice information for invoice ${invoiceNumber} and store ${storeNumber}`);

  const invoiceQuery = `
    SELECT 
      F.*, 
      CONCAT(I.itmcde, I.chkdig) AS ItemNumber 
    FROM pwafil.FICHED01 AS F 
    LEFT JOIN pwafil.invmst AS I ON F.fditem = I.itmcde
    WHERE  
      F.FDINV = ? AND F.FDSTOR = ? 
    ORDER BY F.fditem
  `;

  try{
    var invoiceResults = pjs.query(invoiceQuery, [invoiceNumber, storeNumber]);

    //Return 404 (Not Found) if invoice is not found
    if(sqlstate == 2000){
      message = `Invoice ${invoiceNumber} was not found for store ${storeNumber}.`;
      constructErrorResponse('NOT_FOUND', message,'');
      await ProfrontTxtlog.pushToTxTLog(
        programName,
        incomingDataForLogging,
        message
      );
      response.status(404).send(errorMessageArray);
      return;
    }
    
    //Return Bad Request for other sql state errors
    if(sqlstate > 2000){
      message = `SQL State error occurred while fetching invoice information for ${invoiceNumber} for store ${storeNumber}.`;
      constructErrorResponse('SQL_ERROR', message,'');
      await ProfrontTxtlog.pushToTxTLog(
        programName,
        incomingDataForLogging,
        message
      );
      response.status(500).send(errorMessageArray);
      return;
    }
  } 
  catch(err)
  {
    message = `Catch error occurred while fetching invoice information for ${invoiceNumber} for store ${storeNumber}.`;
    constructErrorResponse('Catch_ERROR', message,'');
    await ProfrontTxtlog.pushToTxTLog(
      programName,
      incomingDataForLogging,
      message
    );
    response.status(500).send(errorMessageArray);
    return;
  }

  console.log(`There are ${invoiceResults.length} line items on invoice ${invoiceNumber} (${invoiceType}) for store ${storeNumber}`);
  console.log(`Looping through each line item on invoice ${invoiceNumber} for store ${storeNumber}`);

  //Loop through each line item on the invoice
  for(let i = 0; i < invoiceResults.length; i++){
    
    //Setting flags
    let isOut = false; 
    let isMarkout = false;
    let denyItem = false;
    let reasonForDenial = "";
    
    let quantityShipped = invoiceResults[i].fdqtys;
    //set isOut and denyItem to true since item was already credited
    //don't let user claim item at all
    if (quantityShipped <= 0){
      isOut = true;
      denyItem = true;
      // reasonForDenial = "The quantity shipped was zero meaning the item was an Out and a claim cannot be filed on such. Any questions please call 205-209-6309.";
      reasonForDenial = `Item was marked as an OUT and can not be filed as a claim.`;
    }
    
    //Call function to retrieve the pack amount from the Pack Size
    let itemPackSize = invoiceResults[i].fdpksz.trim();   
    itemSize = formatPackSize(itemPackSize);

    //Call function to +1 the invoice date
    let invoiceDate = invoiceResults[i].fddate; 
    let deliveryDate = formatDate(invoiceDate);
    
    //Call function to determine if the invoice is claimable based on two day business rule and bybass functionality
    let deliveryVSInvoiceDateMessage = compareTest(deliveryDate,userFirstName,userLastName);
    //If the message returned is greater than 0 then don't let user try to claim item
    if(deliveryVSInvoiceDateMessage.length > 0){
      reasonForDenial = deliveryVSInvoiceDateMessage;
      denyItem = true;
    }

    let extendedCostForQueries = invoiceResults[i]["fdecst"];
    let itemSRP = invoiceResults[i]["fdsrp"]; 
    let itemMulti = invoiceResults[i]["fdmult"];
    let itemExtendedRetail = 0.00;

    if(isCWInvoice){
      itemExtendedRetail = extendedCostForQueries;
    } else {
      if (itemMulti != 0) {
        itemExtendedRetail = itemSRP * 100 / itemMulti * itemSize / 100;
      } else {
        itemExtendedRetail = itemSRP * 100 * itemSize / 100;
      }    

    }

    itemExtendedRetail = +itemExtendedRetail.toFixed(2);

    //Call Buyer APP to retrieve the item's buyer name
    buyerNameResults = await buyerName.getBuyerName(invoiceResults[i].fditem);
    if (buyerNameResults.errorMsg) {
      if(buyerNameResults.errorMsg.includes("No Buyer was found")){ //'Item Number is required.'
        denyItem = true;
        reasonForDenial = buyerNameResults.errorMsg;
      } 

      if(buyerNameResults.errorMsg.includes("Item Number is required.")){
        message = `An error occurred please try again.`;
        constructErrorResponse('SQL_ERROR', message,'');
        await ProfrontTxtlog.pushToTxTLog(
          programName,
          incomingDataForLogging,
          message
        );
        response.status(500).send(errorMessageArray);
        return;
      }

      else
      {
        returnObj.error = buyerNameResults.errorMsg;
        message = `SQl state error occurred while grabbing buyer information. Please try again.`;
        constructErrorResponse('SQL_ERROR', message, 'SQL STATE: ');
        await ProfrontTxtlog.pushToTxTLog(
          programName,
          incomingDataForLogging,
          message
        );
        response.status(500).send(errorMessageArray);
        return;
      }
    }

    /*
     Summary: Fetching existing credits on the items on the invoice
      --  Calling getEntClmInfo, getClmEntInfo, getClmHstInfo Apps
      --  subtract total credit qty from shipped qty to get remaining claiming qty
      --  create list of claimIds from each app
      --  determine if item can be claimed
    */

    let entclmCredits;
    let clmhstCredits;
    let clmentCredits;

    let pendingCLMENTUnitTotal = 0;
    let pendingENTCLMUnitTotal = 0;
    let appliedCLMHSTUnitTotal = 0;
    let numberOfUnitClaimsFiled = 0;
    let numberOfUnitsShipped = 0;
    let cwQuantity = 0.00;

    if(isCWInvoice){
      //Check for existing claims in entclm file------------------------------------------------------------------------------
      cwQuantity = invoiceResults[i].fdcwlb;
      entclmCredits = await CatchWeightEntClm.getCatchWeightEntClmInfo(invoiceResults[i].fdinv,invoiceResults[i].fdstor,invoiceResults[i].fditem,extendedCostForQueries);

      if(entclmCredits.Array.length > 0){
        pendingENTCLMUnitTotal = itemSize;
      }

      //Check for existing claims in clment file------------------------------------------------------------------------------
          
      clmentCredits = await CatchWeightClmEnt.getCatchWeightClmEntInfo(invoiceResults[i].fdinv,invoiceResults[i].fdstor,invoiceResults[i].fditem,extendedCostForQueries);
      
      if(clmentCredits.Array.length > 0){
        pendingCLMENTUnitTotal = itemSize;
        isMarkout = true;
      }
      
      //Check for existing claims in clmhst file------------------------------------------------------------------------------

      clmhstCredits = await CatchWeightClmHst.getCatchWeightClmHstInfo(invoiceResults[i].fdinv,invoiceResults[i].fdstor,invoiceResults[i].fditem,extendedCostForQueries);

      if(clmhstCredits.Array.length > 0){
        appliedCLMHSTUnitTotal = itemSize;
      }


    } else {
      
      //Check for existing claims in entclm file------------------------------------------------------------------------------    
      entclmCredits = await EntClm.getEntClmInfo(invoiceResults[i].fdinv,invoiceResults[i].fdstor,invoiceResults[i].fditem);
      pendingENTCLMUnitTotal = sumTotalUnits(entclmCredits); //Summing claim quantities on markout claims -- already credited      

      //Check for existing claims in clment file------------------------------------------------------------------------------
          
      clmentCredits = await ClmEnt.getClmEntInfo(invoiceResults[i].fdinv,invoiceResults[i].fdstor,invoiceResults[i].fditem); 

      pendingCLMENTUnitTotal = sumTotalUnits(clmentCredits); //Summing claim quantities on existing approved claims -- in process of being credited 

      if(clmentCredits.Array.length > 0){
        isMarkout = true;
      }    
  
      //Check for existing claims in clmhst file------------------------------------------------------------------------------
      
      clmhstCredits = await ClmHst.getClmHstInfo(invoiceResults[i].fdinv,invoiceResults[i].fdstor,invoiceResults[i].fditem);

      appliedCLMHSTUnitTotal = sumTotalUnits(clmhstCredits); //Summing claim quantities on existing credited claims  -- already credited    
    }

    numberOfUnitClaimsFiled = pendingENTCLMUnitTotal + pendingCLMENTUnitTotal + appliedCLMHSTUnitTotal;
    numberOfUnitsShipped = quantityShipped * itemSize;

    // console.log(`Number of units shipped: ${numberOfUnitsShipped}`);

    // console.log(`pendingENTCLMUnitTotal: ${pendingENTCLMUnitTotal}`);
    // console.log(`pendingCLMENTUnitTotal: ${pendingCLMENTUnitTotal}`);
    // console.log(`appliedCLMHSTUnitTotal: ${appliedCLMHSTUnitTotal}`);
    // console.log(`Total number existing credit quantities: ${numberOfUnitClaimsFiled}`);

    let numberOfUnitsRemainingToBeClaimed = numberOfUnitsShipped - numberOfUnitClaimsFiled;
    let numberOfCasesRemainingToBeClaims = Math.floor(numberOfUnitsRemainingToBeClaimed / itemSize);
    let numberOfCasesClaimed = Math.floor(numberOfUnitClaimsFiled / itemSize);

    // console.log(`Number of units remaining to be claimed: ${numberOfUnitsRemainingToBeClaimed}`);
    // console.log(`Number of cases remaining to be claimed: ${numberOfCasesRemainingToBeClaims}`);
    // console.log(`Number of cases already credited: ${numberOfCasesClaimed}`);

    if(numberOfUnitClaimsFiled > numberOfUnitsRemainingToBeClaimed){
      denyItem = true;

      reasonForDenial = 
      (clmhstCredits.Array.length ? `Claims have been given credit.` : "") +
      (entclmCredits.Array.length ? `Claims have been submitted to accounting for payment.` : "") +
      (clmentCredits.Array.length ? "MarkOut claims have already been applied and can be found on your Markout form in eDocs. " : "") 
    }

    invoiceReturnObj.invoiceItems.push({
      itemNumber : invoiceResults[i].fditem.toString() + invoiceResults[i].fdchkd.toString(),
      costPerCase: invoiceResults[i].fdsell,
      quantityShipped: quantityShipped,
      cwQuantity,
      extendedCost: extendedCostForQueries,
      itemAttributes: {
        itemName: invoiceResults[i].fddesc.trim(),
        itemSRP: itemSRP,
        itemBuyer: buyerNameResults.name,
        itemPackSize: itemPackSize.trim(),
        itemDepartment: invoiceResults[i].fddept.trim(),
      },
      claimDetails: {
        remainingCasesToBeClaimed: numberOfCasesRemainingToBeClaims,
        remainingUnitsToBeClaimed: numberOfUnitsRemainingToBeClaimed,
        isOut: isOut,
        isMarkout: isMarkout,
        denyItem: denyItem,
        reasonForDenial: reasonForDenial
      }
    });
  }

  response.status(200).send(invoiceReturnObj);
  return;

  function constructErrorResponse(errorCode,errorMessage,exceptionMessage){
    return errorMessageArray.push({
        code: errorCode,
        message: errorMessage,
        details: {
            enviroment: currentPort,
            timeStamp: dayjs().format('YYYY-MM-DD HH:mm:ss'),
            exception: exceptionMessage
        }
  
    });            
  }

}


function createClaimList(claimArray){
  return claimArray.Array.map(claim => claim.ClaimID).join(", ");
}

function sumTotalUnits(claimsArray){
  totalUnits =  claimsArray.Array.reduce((totalUnitCount, claim) => {
    return totalUnitCount + (claim.CaseOrOnly === "C" ? claim.QuantityOnClaim * itemSize : claim.QuantityOnClaim);
  }, 0);
  return totalUnits;
} 

//Function to parse the meaure and units from pack size
function formatPackSize(packSize) {
  return parseInt(packSize.toString().split('/')[0].trim(), 10);
}
//Function to calculate delivery date
function formatDate(date) {
  const formattedDate = dayjs(date.toString(), 'YYYYMMDD')
    .subtract(1, 'month') // Subtract one month
    .add(1, 'day') // Add one day
    .format('YYYYMMDD'); // Format to 'YYYYMMDD'
      
  return parseInt(formattedDate, 10);
}
//Function to validate if user can create claim based on 2 business day rule and authority
function compareTest(deliveryDateIn,userFirstName,userLastName) {
  let message = "";
  console.log(deliveryDateIn);
  console.log(userFirstName);
  console.log(userLastName);
  var today = new Date();
  let baseDate = deliveryDateIn.toString();
  let year = baseDate.substr(0, 4);
  let month = baseDate.substr(4, 2);
  let day = baseDate.substr(6, 2);
  let compareDate = new Date(year, month, day);
  let comparisonDeliveryDate = addSubtractDate.subtract(
    compareDate,
    1,
    "month"
  ); //Becuase .js months start at 00.
  // console.log(`comparisonDeliveryDate: ${comparisonDeliveryDate}`);
  const comparisonDayOfWeek = comparisonDeliveryDate.getDay();
  if (comparisonDayOfWeek == 6) {
    var deadlineDateOut = addSubtractDate.add(comparisonDeliveryDate, 4, "day");
  } else if (comparisonDayOfWeek == 4 || comparisonDayOfWeek == 5) {
    deadlineDateOut = addSubtractDate.add(comparisonDeliveryDate, 5, "day");
  } else {
    deadlineDateOut = addSubtractDate.add(comparisonDeliveryDate, 3, "day");
  }
  userFirstName = "%" + userFirstName + "%";
  userLastName = "%" + userLastName + "%";
  let approvedUsers = pjs.query("SELECT eaauth FROM eclaimauth WHERE UPPER(eauserfn) LIKE ? and UPPER(eauserln) LIKE ?",[userFirstName, userLastName]
  );
  if (approvedUsers.length == 0 || approvedUsers[0]["eaauth"] < 80) {
    //60 is the minimum authorization level to create tokens. in the table, 80 allows bypass of 2 day limit and 100 allows approval of non-standard claims
    if (today > deliveryDateIn) {
      let displayDate = addSubtractDate.subtract(deadlineDateOut, 1, "day");
      displayDate = displayDate.toString().slice(0, 15);
      message = `The deadline for filing a claim for this invoice ended at 11:59 PM on ${displayDate}, two business days after delivery.`;
      //  ProfrontTxtlog.pushToTxTLog(
      //   "createClaimAPI.V5",
      //   userName,
      //   originalDataForLogging,
      //   returnObj.error
      // );
      // response.status(400).send(returnObj.error);
      // return;
    }
  }
  return message;
}
exports.run = invoiceInformation;