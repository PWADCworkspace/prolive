async function queryMobileFrame(request, response) {
  console.log('mobileFrame API called.');

  // const directPathToProlive = "../../prolive/APPs/";
  // const directPathToProtest = "../../protest/APPs/";
  const directPathToProlog = "../../Prologging/";
  const ProfrontTxtlog = pjs.require(directPathToProlog + 'PushToTxtLogAPP.js');

  let StoreNumber = request.params.id;
  let getOrders = [];
  let ordersArray = [];
  const originalDataForLogging = 'StoreNumber: ' + StoreNumber;

  if (!StoreNumber || StoreNumber == '') {
    errorMsg = 'Store Number is required.';
    await ProfrontTxtlog.pushToTxTLog("MobileFrameQuery", "automated call from eClaims", originalDataForLogging, errorMsg);
    response.status(400).send(errorMsg);
    return;
  }

  let params = { StoreNo: StoreNumber };

  const sqlquery =
    'Select x.OrderNo, x.GUID, x.ORDER_DT, x.Store, x.Type, x.Vendor, x.Lines, y.Cases From ' +
    '(Select oh.id As OrderNo, oh.ORDER_DT, oh.STORE_NO AS Store, ' +
    "Case oh.ORDER_TYPE when 'O' Then 'Warehouse' when 'T' Then 'Tag' When 'X' Then 'Cross Dock' End As TYPE, " +
    'oh.GUID, oh.LINE_COUNT As Lines, vnd.VENDOR ' +
    'From PW_ORDER_HEADER oh Inner Join PW_VENDOR vnd On oh.VENDOR_NO = vnd.VENDOR_NO ' +
    "Where oh.deleted_by < 0 And oh.ORDER_DT > '2017-08-14 00:01:00.000' " +
    'And oh.STORE_NO = @StoreNo) As x ' +
    'Inner Join (Select SUM(od.QUANTITY) As Cases, od.ORDER_HEADER_GUID From PW_ORDER_DETAIL od ' +
    'Inner Join PW_ORDER_HEADER oh on oh.GUID = od.ORDER_HEADER_GUID ' +
    'Group By ORDER_HEADER_GUID) As y on x.GUID = y.ORDER_HEADER_GUID Order By ORDER_DT Desc ';
  try {
    getOrders = pjs.query(pjs.getDB('Server2012-MFS'), sqlquery, params);

    if (!getOrders || getOrders.length <= 0) {
      await ProfrontTxtlog.pushToTxTLog(
        "MobileFrameQuery", 
        "automated call from eClaims", 
        originalDataForLogging, 
        `No records found for store: ${StoreNumber}`
      );
      response.status(400).send(`No records found for store: ${StoreNumber}`);
      return;
    } else {
      console.log('get order ran successfully.');
    }
  } catch (err) {
    await ProfrontTxtlog.pushToTxTLog("MobileFrameQuery", "automated call from eClaims", originalDataForLogging, err);
    response.json('Error, no records found.');
    return;
  }

  // loop through the getOrders Array and look for Type. if type === warehouse set OrderDept = getOrders.type. Else run new SQL.
  for (let i = 0; i < getOrders.length; i++) {
    let orderType = getOrders[i]['Type'];
    let guid = getOrders[i]['GUID'];
    let orderLineCount = getOrders[i]['Lines'];
    let C = getOrders[i]['Cases'];
    let ODT = getOrders[i]['ORDER_DT'];
    let ONO = getOrders[i]['OrderNo'];
    let S = getOrders[i]['Store'];
    let V = getOrders[i]['Vendor'];
    let OrderDept = 'Mixed Departments';

    if (orderType === 'Warehouse') {
      let params2 = {guidNum: guid};
      const itemDeptQuery =
        'Select Department, Count(DEPARTMENT) As DeptLineTotal ' +
        'From PW_ITEM itm Inner Join PW_ORDER_DETAIL od ' +
        'on itm.ITEM_CODE = od.LINE_ITEM_NO ' +
        'Where od.ORDER_HEADER_GUID = @guidNum ' +
        'Group By DEPARTMENT ' +
        'Order By DeptLineTotal DESC';

      let getItemDeptType = [];
      getItemDeptType = pjs.query(
        pjs.getDB('Server2012-MFS'), itemDeptQuery, params2);

      for (let i = 0; i < getItemDeptType.length; i++) {
        let DeptLineTotal = getItemDeptType[i]['DeptLineTotal'];
        let ratio = DeptLineTotal/orderLineCount;

        if(ratio >= 0.6){
          OrderDept = getItemDeptType[i]['Department'];
        }else{
          OrderDept = 'Mixed Departments';
        }        
      }
    } 
    // else {
    //   OrderDept = orderType; //IF TYPE IS NOT WAREHOUSE THEN THE MOBILE FRAME PAGE SHOULD NOT HAVE A Primary Item Type TO DISPALY.
    // }

    ordersArray.push({
      Cases: C,
      GUID: guid,
      Lines: orderLineCount,
      ORDER_DT: ODT,
      OrderNo: ONO,
      Store: S,
      Type: orderType,
      Vendor: V,
      PrimaryDepartment: OrderDept 
    });

  }
  await ProfrontTxtlog.pushToTxTLog("MobileFrameQuery", "automated call from eClaims", originalDataForLogging, 'ordersArray built successfully.');
  response.status(200).json(ordersArray);
}

exports.run = queryMobileFrame;
