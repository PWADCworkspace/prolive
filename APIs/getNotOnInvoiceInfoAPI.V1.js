const gtin = require('gtin');
async function getNonInvoiceInfoAPI(request, response) {
  
  const directPathToProlive = "../../prolive/APPs/";
  const directPathToProtest = "../../protest/APPs/";
  const directPathToProlog = "../../Prologging/";

  let buyerNameAPP = pjs.require(directPathToProlive + "BuyerNameAPP.js");
  let validateCheckDigitAPP = pjs.require(directPathToProlive + "ValidateCheckDigitAPP.js");
  let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

  console.log(`Running getNonInvoiceInfoAPI.V1.js for entered number: ${request.body.EnteredNumber} and quantity on claim: ${request.body.QuantityOnClaim}`);

  let enteredNumber = request.body.EnteredNumber;
  let quantityOnClaim = request.body.QuantityOnClaim;
  let catchWeight = request.body.CatchWeight;

  const originalDataForLogging = 'enteredNumber: '+ enteredNumber + ', quantityOnClaim: '+ quantityOnClaim + ', catchWeight: '+ catchWeight;

  let isUPC = false;
  let isItemCode = false;
  let isPackedUPC = false;
  let upc = -1;
  let itemNumber = -1;
  let errorMsg;

  if (String(enteredNumber).length < 4 || isNaN(enteredNumber)) {
    errorMsg = "Entered Number is too short or is not a Number";
    await ProfrontTxtlog.pushToTxTLog("getNotOnInvoiceAPI.V1", "Automatic API call", originalDataForLogging, errorMsg);
    response.status(400).send(errorMsg);
    return;
  }
  if (String(enteredNumber).length <= 6) isItemCode = true;
  else if (String(enteredNumber).length == 8) isPackedUPC = true;
  else if (String(enteredNumber).length == 10) isUPC = true;
  else if (String(enteredNumber).length == 11) isUPC = true;
  else if (String(enteredNumber).length == 12) {
    enteredNumber = enteredNumber.substring(1, 11);
    isUPC = true;
  } else {
    errorMsg = "Entered Number is invalid, please double check that it is entered correctly either as a full item code or a full upc.";
    await ProfrontTxtlog.pushToTxTLog("getNotOnInvoiceAPI.V1", "Automatic API call", originalDataForLogging, errorMsg);
    response.status(400).send(errorMsg);
    return;
  }

  if (isItemCode) {
    let checkDigitValidation = await validateCheckDigitAPP.validateCheckDigit(enteredNumber);
    if (checkDigitValidation == false) {
      errorMsg = "This item code does not seem to be valid, please double check that it is entered correctly with the check digit included.";
      await ProfrontTxtlog.pushToTxTLog("getNotOnInvoiceAPI.V1", "Automatic API call", originalDataForLogging, errorMsg);
      response.status(400).send(errorMsg);
      return;
    }
    itemNumber = enteredNumber.substring(0, enteredNumber.length - 1);
  }

  if (isPackedUPC) {
    enteredNumber = gtin.upcE.expand(enteredNumber);
    isUPC = true;
  }

  if (isUPC) upc = enteredNumber;

  let querysql;
  let paramater;
  if (isUPC) {
    console.log(`Running INVMST query for UPC: ${upc}`);
    querysql = "select * from invmst where upc = ?";
    paramater = upc;
  } else {
    console.log(`Running INVMST query for item code: ${itemNumber}`);
    querysql = "select * from invmst where itmcde = ?";
    paramater = itemNumber;
  }

  let recordSet = pjs.query(querysql, [paramater]);

  if (!recordSet || recordSet.length == 0) {
    errorMsg = "No record found where " + (isUPC ? "upc = " : "item code = ") + paramater;
    console.log(errorMsg);
    await ProfrontTxtlog.pushToTxTLog("getNotOnInvoiceAPI.V1", "Automatic API call", originalDataForLogging, errorMsg);
    response.status(400).send(errorMsg);
    return;
  }

  if (recordSet && recordSet.length > 1) {
    errorMsg = "Error - more than one matching item found in inventory master file";
    console.log(errorMsg);
    await ProfrontTxtlog.pushToTxTLog("getNotOnInvoiceAPI.V1", "Automatic API call", originalDataForLogging, errorMsg);
    response.status(400).send(errorMsg);
    return;
  }

  let outputArray = [];
  let outputJSON = {};

  let packSize = "";
  let splitArray = recordSet[0]["pksize"].split("/");

  //turned off because some pack sizes have multi '/' included.
  // if (splitArray.length != 2) packSize = recordSet[0]["pksize"];
  // if (splitArray.length == 2) 

  packSize = splitArray[0];
  packSize = Number(packSize.trim());

  let buyerName;
  let buyerNameJSON = await buyerNameAPP.getBuyerName(recordSet[0]["itmcde"]);
  if (buyerNameJSON.errorMsg == "") buyerName = buyerNameJSON.name;
  if (buyerNameJSON.errorMsg != "")
    buyerName = "ERROR GETTING BUYER NAME: " + buyerNameJSON.errorMsg;

  let itemNumberCheckDigit = "" + recordSet[0]["itmcde"] + recordSet[0]["chkdig"];
  itemNumberCheckDigit = Number(itemNumberCheckDigit);

  let calculatedExtendedCost = 0;
  let perUnitCost = 0;

  if (recordSet[0]["sell"] == 0) { //This implys that it is a catch weight item
    if(catchWeight <= 0){
      errorMsg = "This appears to be a catch weight item but the catch weight is not set. Please set the catch weight and try again.";
      console.log(errorMsg);
      await ProfrontTxtlog.pushToTxTLog("getNotOnInvoiceAPI.V1", "Automatic API call", originalDataForLogging, errorMsg);
      response.status(400).send(errorMsg);
      return;
    } else { //This is a catch weight item and they have set the catch weight so we can calculate the price
      console.log(`This is a catch weight item so we are factoring the price as lbs * catch weight sell price`);
      perUnitCost = recordSet[0]["cwsell"];
      calculatedExtendedCost = perUnitCost * quantityOnClaim * catchWeight;
    }
  } else {
    if(catchWeight > 0){ //This is a regular item but they have set the catch weight so we can't calculate the price
      errorMsg = "This appears to be a non-catch weight item but the catch weight is set. Please clear the catch weight and try again.";
      console.log(errorMsg);
      await ProfrontTxtlog.pushToTxTLog("getNotOnInvoiceAPI.V1", "Automatic API call", originalDataForLogging, errorMsg);
      response.status(400).send(errorMsg);
      return;
    } else{ //This is a regular item and they have not set the catch weight so we can calculate the price
      console.log(`This is not a catch weight item so we are factoring the price as lbs * regular sell price`);
      perUnitCost = recordSet[0]["sell"];
      calculatedExtendedCost = perUnitCost * quantityOnClaim;
    }
  }

  calculatedExtendedCost = Number(calculatedExtendedCost.toFixed(2));

  outputJSON["ItemNumber"] = itemNumberCheckDigit;
  outputJSON["UPCNumber"] = recordSet[0]["upc"];
  outputJSON["CheckDigit"] = recordSet[0]["chkdig"];
  outputJSON["Buyer"] = buyerName;
  outputJSON["AppMessage"] = "success";
  outputJSON["PackSize"] = packSize;
  outputJSON["Description"] = recordSet[0]["itmdsc"];
  outputJSON["Department"] = recordSet[0]["dept"];
  outputJSON["PerUnitCost"] = perUnitCost;
  outputJSON["QuantityOnClaim"] = quantityOnClaim;
  outputJSON["ExtendedCost"] = calculatedExtendedCost;

  outputArray.push(outputJSON);
  response.json(outputArray);

  await ProfrontTxtlog.pushToTxTLog("getNotOnInvoiceAPI.V1", "Automatic API call", originalDataForLogging, outputArray);

}
exports.run = getNonInvoiceInfoAPI;
