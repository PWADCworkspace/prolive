async function createFreezerBoxToteClaim(request, response) {

  //Set pathing for Apps.  
  const directPathToProlive = "../../prolive/APPs/";
  const directPathToProtest = "../../protest/APPs/";
  const directPathToProlog = "../../prologging/";

  let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

  //Predefine variables.
  const storeNumber = request.body.StoreNumber;
  let itemNumber = request.body.ItemNumber;
  const userName =
  request.body.UserKeyingClaim.trim() + " " + request.body.CallingName.trim();
  const quantityOnClaim = request.body.QuantityOnClaim;
  var returnObj = {};
  returnObj.array = [];
  returnObj.error = "";

  const originalDataForLogging = 
    'StoreNumber: '+ storeNumber +
    ', ItemNumber: '+ itemNumber +
    ', QuantityOnClaim: '+ quantityOnClaim + 
    ', UserKeyingClaim: '+ userName;

  console.log("Incoming Data: " + originalDataForLogging);

  if (!storeNumber) {
    returnObj = "Store Number is required.";
    await ProfrontTxtlog.pushToTxTLog(
        "createFreezerBoxToteClaimAPI.V1",
        userName,
        originalDataForLogging,
        returnObj
      );
    response.status(400).send(returnObj);
    return;
  }

  if (itemNumber !== 70 && itemNumber !== 30) {
    returnObj = "An Item Number of either 70 (Freezer Box) or 30 (Tote) is required.";
    await ProfrontTxtlog.pushToTxTLog(
        "createFreezerBoxToteClaimAPI.V1",
        userName,
        originalDataForLogging,
        returnObj
      );
    response.status(400).send(returnObj);
    return;
  }

  if (!quantityOnClaim || quantityOnClaim == 0) {
    returnObj = "QuantityOnClaim needs to be greater than 0.";
    await ProfrontTxtlog.pushToTxTLog(
        "createFreezerBoxToteClaimAPI.V1",
        userName,
        originalDataForLogging,
        returnObj
      );
    response.status(400).send(returnObj);
    return;
  }

  //Note: Tyler Hobbs 03/11/24: Turning off the update process to CMPFRZBX as it is no longer needed and was replaced with the LucasFBGetR.js process and FBCLAIMLOG table
  // let quantityToWrite = 0;
  // let writeCMPFRZBXQTYquery = "";

  // const checkCMPFRZBXQTYquery = "SELECT fbpndqty FROM cmpfrzbx WHERE fbstore = ? and fbitem = ?";
  // const checkCMPFRZBXQTY = pjs.query(checkCMPFRZBXQTYquery, [storeNumber, itemNumber]);
  // console.log(checkCMPFRZBXQTY);

  // if(checkCMPFRZBXQTY.length == 0){
  //   writeCMPFRZBXQTYquery = "INSERT INTO cmpfrzbx (fbpndqty, fbstore, fbitem) VALUES (?, ?, ?)";
  //   quantityToWrite = quantityOnClaim;
  // } else {
  //   writeCMPFRZBXQTYquery = "UPDATE cmpfrzbx SET fbpndqty = ? where fbstore = ? and fbitem = ?";
  //   quantityToWrite = checkCMPFRZBXQTY[0].fbpndqty + quantityOnClaim;
  // }

  // try {
  //   pjs.query(writeCMPFRZBXQTYquery, [quantityToWrite, storeNumber, itemNumber]);
    returnObj = "File successfully updated.";
  //   await ProfrontTxtlog.pushToTxTLog(
  //       "createFreezerBoxToteClaimAPI.V1",
  //       userName,
  //       originalDataForLogging,
  //       returnObj
  //     );
  //   response.status(200).send(returnObj);
  //   return;
  // } catch (error) {
  //   returnObj = error;
  //   await ProfrontTxtlog.pushToTxTLog(
  //       "createFreezerBoxToteClaimAPI.V1",
  //       userName,
  //       originalDataForLogging,
  //       returnObj
  //     );
    response.status(200).send(returnObj);
  //   return;
  // }
}
exports.run = createFreezerBoxToteClaim;
