async function storeAddOn(request, response) {
  console.log("storeAddOnAPI.js");

  const directPathToProlive = "../../prolive/APPs/";
  const directPathToProtest = "../../protest/APPs/";
  const directPathToProlog = "../../Prologging/";
  let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

  let ArFileRead = "AR Record Set Array";
  let InvMstITMCDE;
  let itemDept;
  let PrivateLabelCode;
  let uu1;
  let storeState;
  let checkDigit;
  let storeNumber = request.body.storeNumber;
  let itemNumberCheckDigit = request.body.itemNumber;
  let quantity = request.body.quantity;
  let userName = request.body.UserKeyingClaim + " " + request.body.CallingName;
  let itemNumber;
  let itemVendor;
  let errorMsg;
  const originalDataForLogging =
    "storeNumber: " +
    storeNumber +
    ", itemNumberCheckDigit: " +
    itemNumberCheckDigit +
    ", quantity: " +
    quantity +
    ", userName: " +
    userName;

  if (!itemNumberCheckDigit) {
    errorMsg = "Item Number is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "storeAddOnAPI",
      userName,
      originalDataForLogging,
      errorMsg
    );
    // log.pushToLog(storeNumber, userName, errorMsg);
    response.status(400).send(errorMsg);
    return;
  }

  if (!storeNumber) {
    errorMsg = "Store Number is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "storeAddOnAPI",
      userName,
      originalDataForLogging,
      errorMsg
    );
    // log.pushToLog(storeNumber, userName, errorMsg);
    response.status(400).send(errorMsg);
    return;
  }

  if (!quantity) {
    errorMsg = "Quantity is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "storeAddOnAPI",
      userName,
      originalDataForLogging,
      errorMsg
    );
    // log.pushToLog(storeNumber, userName, errorMsg);
    response.status(400).send(errorMsg);
    return;
  }
  // would need to loop through
  let len = String(itemNumberCheckDigit).length;
  itemNumber = String(itemNumberCheckDigit).substr(0, len - 1);
  console.log("item Number=" + itemNumber);
  checkDigit = String(itemNumberCheckDigit).substr(len - 1, 1);

  console.log("check digit=" + checkDigit);

  //Get todays date to insert into Bultin File
  let today = new Date();
  let date = `${today.getFullYear()}${today.getMonth() + 1}${today.getDate()}`;
  // let numericDate = parseFloat(date);
  let todaysHour = today.getHours();

  // todaysHour = 9; //######################################################### FOR TESTING AFTER 11AM ONLY, LEAVE COMMENTED OUT.

  console.log("todaysHour: " + todaysHour);
  let todayDay = today.getDay();
  todayDay = todayDay.toString();
  console.log("todayDay: " + todayDay);
  if (todayDay == "0") {
    todayDay = "7";
  }

  let todayYMD;
  let pjsDate = pjs.date();
  todayYMD =
    pjsDate.extractYear() * 10000 +
    pjsDate.extractMonth() * 100 +
    pjsDate.extractDay();

  // Run data collecting queries ---------------------------------------------------------------------------------------------------------------------
  // AR FIle: if UU1 != Y then that store cannot get Private Label P.W. items ("P").
  const ARquery = "select UU1, BUSTER from ARFILE where ARFKEY = ?";
  try {
    ArFileRead = pjs.query(ARquery, [storeNumber]);
    uu1 = ArFileRead[0]["uu1"];
    storeState = ArFileRead[0]["buster"];
    console.log("uu1: " + uu1);
  } catch (err) {
    // console.log(err);
    errorMsg = "Error on ARFILE Query, check store number.";
    await ProfrontTxtlog.pushToTxTLog(
      "storeAddOnAPI",
      userName,
      originalDataForLogging,
      errorMsg
    );
    // log.pushToLog(storeNumber, userName, errorMsg);
    response.status(400).send(errorMsg);
    return;
  }

  // would need to loop through. ???if sending multiple items???
  // INVMST
  const INVMSTquery =
    "select BALOH, ITMCDE, CHKDIG, PKSIZE, ITMDSC, DEPT, PVTLAB, VENDNO from INVMST WHERE ITMCDE = ? ";
  let invmstRecordSet = pjs.query(INVMSTquery, [itemNumber]);
  if (invmstRecordSet && invmstRecordSet.length > 0) {
    InvMstITMCDE = invmstRecordSet[0]["itmcde"];
    itemDept = invmstRecordSet[0]["dept"];
    PrivateLabelCode = invmstRecordSet[0]["pvtlab"];
    itemVendor = invmstRecordSet[0]["vendno"];
  } else {
    errorMsg = "Error on Inventory Master File Query.";
    console.log(errorMsg);
    await ProfrontTxtlog.pushToTxTLog(
      "storeAddOnAPI",
      userName,
      originalDataForLogging,
      errorMsg
    );
    // log.pushToLog(storeNumber, userName, errorMsg);
    response
      .status(400)
      .send(
        errorMsg + " Please check that your Item Code is correct and try again."
      );
    return;
  }

  console.log("InvMstITMCDE = " + InvMstITMCDE);
  console.log("itemDept = " + itemDept);
  console.log("PrivateLabelCode = " + PrivateLabelCode);
  // would need to loop through
  // check for private label item and private label approved store. uu1 = Y or Y :this is a piggly store and allowed to get private label items
  if (
    (PrivateLabelCode == "p" || PrivateLabelCode == "P") &&
    uu1 !== "y" &&
    uu1 !== "Y"
  ) {
    errorMsg =
      "Piggly Wiggly Private Label Items cannot be ordered by this store.";
    await ProfrontTxtlog.pushToTxTLog(
      "storeAddOnAPI",
      userName,
      originalDataForLogging,
      errorMsg
    );
    console.log("Error Message" + errorMsg);
    response.status(400).send(errorMsg);
    return;
  }

  if (todaysHour > 11) {
    writeToTomorrowFile();
    console.log(
      "It is after 11am. This item will be added for your next truck."
    );
    await ProfrontTxtlog.pushToTxTLog(
      "storeAddOnAPI",
      userName,
      originalDataForLogging,
      "It is after 11am. This item will be added for your next truck."
    );
    response
      .status(200)
      .send("It is after 11am. This item will be added for your next truck.");
    return;
  }

  //  todayDay = 4;  //####################################################################### FOR TESTING FOR DAYS OF WEEK ONLY. LEAVE COMMENTED OUT.
  // would need to loop through
  const truckQuery =
    "select  BWDEPT, BPULL " +
    "from BULPOLF  " +
    "where BSTR = ? and bwdept = ? and bpull = ?";
  let truckingRecordSet = pjs.query(truckQuery, [
    storeNumber,
    itemDept,
    todayDay
  ]);

  if (truckingRecordSet && truckingRecordSet.length > 0) {
    console.log("calling writeToTodayFile");
    writeToTodayFile();
    await ProfrontTxtlog.pushToTxTLog(
      "storeAddOnAPI",
      userName,
      originalDataForLogging,
      "This item will be added for delivery tomorrow."
    );
    response.status(200).send("This item will be added for delivery tomorrow.");
  } else {
    //WRITE TO TOMORROWS FILE
    console.log("calling write tomorrow");
    writeToTomorrowFile();
    await ProfrontTxtlog.pushToTxTLog(
      "storeAddOnAPI",
      userName,
      originalDataForLogging,
      "This item will be added to your next available truck."
    );
    response
      .status(200)
      .send("This item will be added to your next available truck.");
  }

  async function writeToTodayFile() {
    let oldQty;
    let newQty;
    let todayQuery =
      "select qty from srtent " + " where strno = ? and code = ?"; //NOTE: WILL CHANGE TYLER.SRTENT TO PWAFIL.SRTENT ONCE WE START PRODUCTION!!!!!!!!!!!!
    let todayRecord = pjs.query(todayQuery, [storeNumber, itemNumber]);
    if (todayRecord && todayRecord.length > 0) {
      oldQty = todayRecord[0]["qty"];
      newQty = oldQty + quantity;
      todayQuery =
        "UPDATE srtent " + //NOTE: WILL CHANGE TYLER.SRTENT TO PWAFIL.SRTENT ONCE WE START PRODUCTION!!!!!!!!!!!!
        "SET qty = ? " +
        "WHERE strno=? and code = ? and qty = ?";
      try {
        pjs.query(todayQuery, [newQty, storeNumber, itemNumber, oldQty]);
        console.log(
          "UPDATING TODAY FILE: STORE NUMBER = " +
            storeNumber +
            "   Item Number = " +
            itemNumber +
            "   New Quantity = " +
            newQty
        );
      } catch (err) {
        errorMsg = "Sql Update error on SRTENT File."; //NOTE: WILL CHANGE TYLER.SRTENT TO PWAFIL.SRTENT ONCE WE START PRODUCTION!!!!!!!!!!!!
        await ProfrontTxtlog.pushToTxTLog(
          "storeAddOnAPI",
          userName,
          originalDataForLogging,
          errorMsg
        );
        // log.pushToLog(storeNumber, userName, errorMsg);
        response.status(400).send(errorMsg);
        return;
      }
    } else {
      todayQuery =
        "insert into srtent " + //NOTE: WILL CHANGE TYLER.SRTENT TO PWAFIL.SRTENT ONCE WE START PRODUCTION!!!!!!!!!!!!
        "(strno, qty, code, bors, stcde, ckdig) " +
        "values (?,?,?,?,?,?)";
      try {
        pjs.query(todayQuery, [
          storeNumber,
          quantity,
          itemNumber,
          "",
          storeState,
          checkDigit
        ]);
        if (sqlstate >= "02000") {
          errorMsg = "sqlstate:" + sqlstate;
          await ProfrontTxtlog.pushToTxTLog(
            "storeAddOnAPI",
            userName,
            originalDataForLogging,
            errorMsg
          );
          // log.pushToLog(storeNumber, userName, errorMsg);
          return;
        }
        console.log(
          "INSERTING NEW RECORD INTO TODAY FILE: STORE NUMBER = " +
            storeNumber +
            "   Item Number = " +
            itemNumber +
            "   Quantity = " +
            quantity
        );
      } catch (err) {
        errorMsg = "Sql Insert error on TYLER.SRTENT File."; //NOTE: WILL CHANGE TYLER.SRTENT TO PWAFIL.SRTENT ONCE WE START PRODUCTION!!!!!!!!!!!!
        await ProfrontTxtlog.pushToTxTLog(
          "storeAddOnAPI",
          userName,
          originalDataForLogging,
          errorMsg
        );
        // log.pushToLog(storeNumber, userName, errorMsg);
        response.status(400).send(errorMsg);
        return;
      }
    }
  }

  async function writeToTomorrowFile() {
    let oldQty;
    let newQty;
    let tomorrowQuery =
      "select bqty from bultin " + " where bstore = ? and bitem = ?"; //NOTE: WILL CHANGE TYLER.BULTIN TO PWAFIL.BULTIN ONCE WE START PRODUCTION!!!!!!!!!!!!
    let tomorrRecord = pjs.query(tomorrowQuery, [storeNumber, itemNumber]);
    if (tomorrRecord && tomorrRecord.length > 0) {
      oldQty = tomorrRecord[0]["bqty"];
      newQty = oldQty + quantity;

      tomorrowQuery =
        "UPDATE bultin " + //NOTE: WILL CHANGE TYLER.BULTIN TO PWAFIL.BULTIN ONCE WE START PRODUCTION!!!!!!!!!!!!
        "SET bqty = ? " +
        "WHERE bstore=? and bitem = ? and bqty = ?";
      try {
        pjs.query(tomorrowQuery, [newQty, storeNumber, itemNumber, oldQty]); //NOT TESTED  NOT TESTED NOT TESTED NOT TESTED NOT TESTED NOT TESTED NOT TESTED NOT TESTED
      } catch (err) {
        errorMsg = "Sql Update error on TYLER.BULTIN File."; //NOTE: WILL CHANGE TYLER.BULTIN TO PWAFIL.BULTIN ONCE WE START PRODUCTION!!!!!!!!!!!!
        await ProfrontTxtlog.pushToTxTLog(
          "storeAddOnAPI",
          userName,
          originalDataForLogging,
          errorMsg
        );
        // log.pushToLog(storeNumber, userName, errorMsg);
        response.status(400).send(errorMsg);
        return;
      }
    } else {
      console.log("inserting into tomorrow file");
      tomorrowQuery =
        "insert into bultin " + //NOTE: WILL CHANGE TYLER.BULTIN TO PWAFIL.BULTIN ONCE WE START PRODUCTION!!!!!!!!!!!!
        "(BFLAG, BSTORE, BITEM, BDATE, BQTY, BDEPT, BSTATE, BEDAT, BFLG1, BFLG2, BVND) " +
        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
      try {
        pjs.query(tomorrowQuery, [
          "",
          storeNumber,
          itemNumber,
          todayYMD,
          quantity,
          itemDept,
          storeState,
          todayYMD,
          "",
          "",
          itemVendor
        ]);
        // BFLAG,     BSTORE,       BITEM,      BDATE,        BQTY,     BDEPT,        BSTATE,              BEDAT,     BFLG1,     BFLG2,BVND
        if (sqlstate >= "02000") {
          errorMsg = "sqlstate:" + sqlstate;
          await ProfrontTxtlog.pushToTxTLog(
            "storeAddOnAPI",
            userName,
            originalDataForLogging,
            errorMsg
          );
          // log.pushToLog(storeNumber, userName, errorMsg);
          response
            .status(400)
            .send("Sql Insert error on TYLER.BULTIN File. " + errorMsg); //NOTE: WILL CHANGE TYLER.BULTIN TO PWAFIL.BULTIN ONCE WE START PRODUCTION!!!!!!!!!!!!
          return;
        }
      } catch (err) {
        errorMsg = "Sql Insert error on TYLER.BULTIN File."; //NOTE: WILL CHANGE TYLER.BULTIN TO PWAFIL.BULTIN ONCE WE START PRODUCTION!!!!!!!!!!!!
        await ProfrontTxtlog.pushToTxTLog(
          "storeAddOnAPI",
          userName,
          originalDataForLogging,
          errorMsg
        );
        // log.pushToLog(storeNumber, userName, errorMsg);
        response.status(400).send(errorMsg);
        return;
      }
    }
  }
}
exports.run = storeAddOn;
