/* 
Author: Samuel Gray
Date: 08/02/2023

Purpose: This API will return all of the FBs IDs back to eClaims for the store to select which box they are filing a claim on.

// getFreezerBoxIDsAPI Version 1
app.get("/v1/FreezerBox",
profoundjs.express("prolive/APIs/getFreezerBoxIDs.V1"));

eClaims URL call: v1/FreezerBox?storeId=900


Change Log:
  JIRA Ticket PROCLM-43: https://pwadc.atlassian.net/browse/PROCLM-43:
    -- Updated API URL call to be /v1/FreezerBox
      -- Added storeId as a query parameter for the API
    -- Added store number validation check
    -- Renamed returnObj.array to returnObj.freezerboxes
      -- Added storeID and freezerBoxId containing the store number and freezer box id values
    -- Removed 'No freezer box ids for store ${}' message from API response
      -- If there are no ids for the store there response fields will be empty

  JIRA TICKET PROCLM-45: 
    -- Added in dev table and adjusted sql queries to use template literals using the claimLogTable.
    -- Replace returnObj in push to txt log to returnObj.error
*/

async function fetchFB(request, response) {
  console.time("getFreezerBoxIDs.V1 API is running");

  //Defining Paths---------------------------------------------------------------------
  const directPathToProlog = "../../prologging/";
  let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

  //Initializing Variables-------------------------------------------------------------
  var returnObj = {};
  returnObj.freezerboxes = [];
  returnObj.error = "";

  //Defining Incoming Variables--------------------------------------------------------
  const storeNumber = parseInt(request.query.storeId);

  const incomingDataForLogging = `Store Number ${storeNumber}`;
  console.log("Incoming Data: " + incomingDataForLogging);

  const currentPort = profound.settings.port;
  console.log(currentPort);

  let claimLogTable = "FBCLAIMLOG";

  if (currentPort === 80) { 
    console.log("Running validateFreezerBoxIDAPI in PROD...");
    // claimLogTable = "FBCLAIMLOG";
  } else {
    console.log("Running validateFreezerBoxIDAPI in DEV...");
    // claimLogTable = "FBCLMLOGCY";
  }

  //Error Checks for Incoming Data-----------------------------------------------------
  if (!storeNumber) {
    returnObj.error = "Store number is required.";
    console.log(returnObj.error);
    await ProfrontTxtlog.pushToTxTLog(
      "getFreezerBoxIDs.V1",
      incomingDataForLogging, 
      returnObj.error);
    response.status(400).send(returnObj);
    return;
  }

  //Executing Fetch Query to grab all open FB associated with store number where there there is no claim info
  try {
    console.log(`Fetching freezer box IDs for store ${storeNumber}`);
    var fbData = pjs.query(`SELECT fbcstnm, fbcfbid FROM ${claimLogTable} WHERE fbcstnm = ${storeNumber} and fbccrdf = '' and fbcclmd = 0`); //11/8/223: Replaced Credit Method with Claim Date b/c we don't want to grab records that have claim info
    
    if(sqlstate > "02000") {
      error = `A sql error occured while retrieving freezer box data. SQL State: ${sqlstate}`
      returnObj.error = `A sql catch error occurred while retrieving freezer box data.`;
      console.log(returnObj.error);
      await ProfrontTxtlog.pushToTxTLog(
        "getFreezerBoxIDs.V1",
        "Automated call during Claim", 
        incomingDataForLogging, 
        error);
      response.status(400).send(returnObj);
      return;
    }  
  } catch (err) {
    console.log(err);

    error = `Sql Select catch error during Fetch: + ${err}`
    returnObj.error = `A sql catch error occurred while retrieving freezer box data.`;
    console.log(returnObj.error);
    await ProfrontTxtlog.pushToTxTLog(
      "getFreezerBoxIDs.V1",
      "Automated call during Claim", 
      incomingDataForLogging, 
      error);
    response.status(400).send(returnObj);
    return;
  }

  //If no records are found return empty 200 response
  if (fbData.length === 0) {
    returnObj.error =`There are no open freezer boxes for store ${storeNumber}. Please contact Retail Systems.`;
    console.log(returnObj.error);
    await ProfrontTxtlog.pushToTxTLog(
      "getFreezerBoxIDs.V1",
      "Automated call during Claim", 
      incomingDataForLogging, 
      returnObj.error);

    response.status(400).send(returnObj);
    return;
  } else {
    //Grabbing all of the indivudal FB Ids and pushing to return freezerboxes array
    for (let id of fbData) {
        var storeId = id.fbcstnm
        var freezerBoxID = id.fbcfbid;
        // console.log(`store ${storeId}, freezerbox id ${freezerBoxID}`)
        returnObj.freezerboxes.push({ "storeID": storeId, "freezerBoxId" : freezerBoxID});
    }

    console.timeEnd("getFreezerBoxIDs.V1 API is running");

    console.log(returnObj);
    await ProfrontTxtlog.pushToTxTLog(
      "getFreezerBoxIDs.V1",
      "Automated call during Claim", 
      incomingDataForLogging, 
      returnObj);

    response.status(200).send(returnObj);
    return;
  }
}
exports.run = fetchFB;
