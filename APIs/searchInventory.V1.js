const dayjs = require("dayjs");

/*
  Created By: Samuel Gray
  Date Create: 08/12/2024
  Modified By: Samuel Gray
  Date Modified: 11/04/2024

  Purpose: Lookup item information based on passed in item number, UPC, or description.
*/

async function searchItem(request, response) {

    const directPathToProlog = "../../Prologging/";
    const directPathToProlive = "../../prolive/APPs/";
    let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

    const programName = "searchInventoryAPI_V1";
    const currentPort = profound.settings.port;

    // const filterTypes = Object.freeze({
    //     itemNumber: 'itemNumber',
    //     upc: 'upc',
    //     itemDescription: 'itemDescription'
    // });

    const filterTypes = [
        'itemNumber',
        'upc',
        'itemDescription'    
    ];

    console.log(`${programName}  is running on port ${currentPort}....${dayjs()}`);

    var inventoryItemsArray = []; //200k Return Array 
    var errorMessageArray = []; //Error Return Array

    let errorObject = {};
    let message = '';

    //Retrieving passed in query parameters from the Client
    const searchType = request.query.searchType == undefined ? '' : request.query.searchType.trim(); 
    let searchFilter = request.query.searchFilter == undefined ? '' : request.query.searchFilter.trim();

    incomingDataForLogging = `searchType: ${searchType}, searchFilter: ${searchFilter}`;
    console.log(incomingDataForLogging);

    
    //Throw error if searchType and searchFilter are empty strings
    if(searchType.length == 0 || searchFilter.length == 0){
        message = 'Please ensure to specify a searchType and searchFilter.';

        errorObject = constructErrorResponse('MISSING_PARAMS', message);
        errorMessageArray.push(errorObject);
        await ProfrontTxtlog.pushToTxTLog(
          programName,
          incomingDataForLogging,
          message
        );
        response.status(400).send(errorMessageArray);
        return;
    }

    //Throw error if the passed searchType is NOT in the filterTypes list
    if(!filterTypes.includes(searchType)){
        message = 'Please ensure to specify a valid searchType';

        errorObject = constructErrorResponse('MISSING_PARAMS', message);
        errorMessageArray.push(errorObject);
        await ProfrontTxtlog.pushToTxTLog(
          programName,
          incomingDataForLogging,
          message
        );
        response.status(400).send(errorMessageArray);
        return;
    }

    if(isNaN(searchFilter) && (searchType == 'itemNumber' || searchType == 'upc')){
        message = 'Please ensure to specify numeric values when searching by the item number or upc';

        errorObject = constructErrorResponse('MISSING_PARAMS', message);
        errorMessageArray.push(errorObject);
        await ProfrontTxtlog.pushToTxTLog(
          programName,
          incomingDataForLogging,
          message
        );
        response.status(400).send(errorMessageArray);
        return;
    }


    let dynamicSearchItemsQuery = `
        SELECT 
        itmcde AS shortItemNumber, 
        chkdig as checkDigit, 
        concat(itmcde,chkdig) as itemNumber, 
        upc ,
        itmdsc as itemDescription, 
        posdec as posDescription,
        dept as departmentNumber,
        buyer as buyerNumber,
        pksize as packSize,
        oh.hdesc as orderHeaderDescription,
        CONCAT(buy.bfname,buy.blname) as buyerFullName
        FROM INVMST AS test
        LEFT JOIN buytab AS buy ON buyer = buy.bcode
        LEFT JOIN ordhdr AS oh ON test.ordhdr = oh.hdr#
        WHERE 
    `;

    //Add WHERE clause to dynamic query based on the searchType
    switch(searchType){
        case "itemNumber":
            dynamicSearchItemsQuery += `ITMCDE LIKE '%${searchFilter}' or CONCAT(ITMCDE, CHKDIG) LIKE '%${searchFilter}%'`;
            break;
        case "upc":
            dynamicSearchItemsQuery += ` UPC LIKE '%${searchFilter}%'`;
            break;
        case "itemDescription":
            searchFilter = searchFilter.toUpperCase();
            dynamicSearchItemsQuery += ` ITMDSC LIKE '%${searchFilter}%'`;
            break;
    }

    dynamicSearchItemsQuery += ' ORDER BY DEPT,ITMCDE';

    console.log(`Querying invmst to retrieve items.`);

    try{
        var itemsResults = pjs.query(dynamicSearchItemsQuery);  

        //Throw error if there were no records found for the entered criteria
        if(sqlstate == '02000'){
            message = 'No records were found for the entered criteria';
            errorObject = constructErrorResponse('NOT_FOUND', message, 'SQL STATE: ' + sqlstate);
            errorMessageArray.push(errorObject);

            await ProfrontTxtlog.pushToTxTLog(
              programName,
              incomingDataForLogging,
              `${message}. SQL State: ${sqlstate}`
            );
            response.status(404).send(errorMessageArray);
            return;
        }

        //Throw error if the sql state is > 02000
        if(sqlstate > '02000'){
            message = 'SQL State error occurred while grabbing items. Please try again.';
            errorObject = constructErrorResponse('SQL_ERROR', message, `SQL STATE: ${sqlstate} `);
            errorMessageArray.push(errorObject);

            await ProfrontTxtlog.pushToTxTLog(
              programName,
              incomingDataForLogging,
              `${message}. SQL State: ${sqlstate}`
            );
            response.status(404).send(errorMessageArray);
            return;
        }
    } 
    //Throw error if there is an catch error
    catch(err){
        console.log(err);
        message = 'Catch error occurrred while grabbing items. Please try again.';
        errorObject = constructErrorResponse('CATCH_ERROR',message, 'Catch Error: ' + err);
        errorMessageArray.push(errorObject);

        await ProfrontTxtlog.pushToTxTLog(
            programName,
            incomingDataForLogging,
            `${message}. Catch Error: ${err}`
        );
        response.status(400).send(errorMessageArray);
        return;
    }

    console.log(`Records pulled from invmst: ${itemsResults.length}`);
    
    //Loop through each item and push information to return array
    itemsResults.forEach(item => {
        const itemObject = {
            name : item.itemdescription,
            identifiers: {
                itemNumber: item.itemnumber,
                itemNumberShort: item.shortitemnumber,
                checkDigit: item.checkdigit,
                upc: item.upc
            },
            attributes: [
                {
                    id: 'pack_size',
                    description: 'Pack Size',
                    value: item.packsize
                },
                {
                    id: 'item_department',
                    description: 'Department Number',
                    value: item.departmentnumber
                },
                {
                    id: 'item_buyer_name',
                    description: 'Buyer Name',
                    value: item.buyerfullname
                },
                {
                    id: 'item_order_header',
                    description: 'Order Header',
                    value: item.orderheaderdescription
                },
                {
                    id: 'pos_description',
                    description: 'Point of Sale Description',
                    value: item.posdescription
                },
            ]
        }
        inventoryItemsArray.push(itemObject);
    });

    console.log(`${programName} V1 is done running....${dayjs()}`);
    response.status(200).send(inventoryItemsArray);

    //Contructs error message object
    async function constructErrorResponse(errorCode,errorMessage,exceptionMessage){
        return {
            code: errorCode,
            message: errorMessage,
            details : {
                enviroment: currentPort,
                timesStamp: dayjs().format('YYYY-MM-DD HH:mm:ss'),
                exception: exceptionMessage
            }

        }
    }
}
exports.run = searchItem;