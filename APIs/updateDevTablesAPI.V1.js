/*
    Notes: This API is used to adjust the Dev tables in the PROFRNTDEV Library for testing purposes for the eClaims system.
    This API will take in the following parameters:
        StoreNumber: The store number to be used for the update. If blank the API will exclude storenumber from the select queires.
        ClearTables: A Yes or No value to determine if the tables should be cleared before the update.
        RecordCount: The number of records to be updated in the tables, if 0 the API will default to 1000 records.
        DateOrder: The order in which the records should be updated, either Asc or Ascending.
        StartDate: The start date for the records to be updated as YYYYMMDD.
        EndDate: The end date for the records to be updated as YYYYMMDD.

    The start.js entry for this API is as follows:
        //updateDevTablesAPI.V1  
        app.post("/v1/updateDev", profoundjs.express("prolive/APIs/updateDevTablesAPI.V1"));

    The Client-Side JSON for this API is as follows:
        {
            "StoreNumber": 000,
            "ClearTables": "Yes",
            "RecordCount": 1000,
            "DateOrder": "Ascending",
            "StartDate": "01/01/2020",
            "EndDate": "01/01/2021"
        }
*/

async function updateDevTables(request, response) {
  const directPathToProlog = "../../prologging/";

  let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

  //Predefine variables:
  let clearTables = request.body.ClearTables;
  let storeNumber = request.body.StoreNumber;
  let recordCount = request.body.RecordCount;
  let dateOrder = request.body.DateOrder;
  let startDate = request.body.StartDate;
  let endDate = request.body.EndDate;
  let returnObj = {};
  returnObj.array = [];
  returnObj.error = "";

  //Define Production Tables:
  const entryClaimTable = "PROFRONT.ENTCLM_CALLIN"; //I hard coded the PROFRONT Lib so I could continue to exclude it from the Dev instnace pathlist.
  const claimHistoryTable = "CLMHST";
  const claimEntryTable = "CLMENT";
  const ficheDetail01Table = "FICHED01";

  //Define Dev Tables:
  const devEntclmTable = "ENTCLMDEV";
  const devClmhstTable = "CLMHSTDEV";
  const devClmentTable = "CLMENTDEV";
  const devFiched01Table = "FICHEDDEV";

  const originalDataForLogging =
    "StoreNumber: " +
    storeNumber +
    ", ClearTables: " +
    clearTables +
    ", RecordCount: " +
    recordCount +
    ", DateOrder: " +
    dateOrder +
    ", StartDate: " +
    startDate +
    ", EndDate: " +
    endDate;

  console.log("Incoming Data: " + originalDataForLogging);

  //Clear Tables:
  if (clearTables.toUpperCase() == "YES") {
    console.log("Clearing all data in the 4 Dev Tables!");

    try {
      await pjs.query(`DELETE FROM ${devEntclmTable}`);
      await pjs.query(`DELETE FROM ${devClmhstTable}`);
      await pjs.query(`DELETE FROM ${devClmentTable}`);
      await pjs.query(`DELETE FROM ${devFiched01Table}`);
      returnObj.array.push("Data cleared in the 4 Dev Tables!");
    } catch (error) {
      returnObj.error = `Error deleting the dev tables: ${error.message}`;
      await ProfrontTxtlog.pushToTxTLog("updateDevTablesAPI.V1", "System", originalDataForLogging, returnObj.error);
    }
  }

  //Initial Select Queries:
  let entclmQuery = `SELECT * FROM ${entryClaimTable}`;
  let clmhstQuery = `SELECT * FROM ${claimHistoryTable}`;
  let clmentQuery = `SELECT * FROM ${claimEntryTable}`;
  let fiched01Query = `SELECT * FROM ${ficheDetail01Table}`;

  //Select Queries:
  let multiSearchFlag = "No";

  if (storeNumber !== 0 || startDate !== 0 || endDate !== 0) {
    entclmQuery += " where ";
    clmhstQuery += " where ";
    clmentQuery += " where ";
    fiched01Query += " where ";

    //If Store Number is provided, add it to the search parameters:
    if (storeNumber !== 0) {
      console.log(`Store Number Provided: ${storeNumber}`);

      entclmQuery += `SCLMST = ${storeNumber}`;
      clmhstQuery += `HCLMST = ${storeNumber}`;
      clmentQuery += `CLMSTR = ${storeNumber}`;
      fiched01Query += `FDSTOR = ${storeNumber}`;

      if (multiSearchFlag !== "Yes") {
        multiSearchFlag = "Yes";
      }
    }

    //If Start Date is provided, add it to the search parameters:
    if (startDate !== 0) {
      console.log(`Start Date Provided: ${startDate}`);

      if (multiSearchFlag == "Yes") {
        entclmQuery += " and ";
        clmhstQuery += " and ";
        clmentQuery += " and ";
        fiched01Query += " and ";
      }

      // For entclmQuery, drop the first 2 numbers from the startDate
      let modifiedStartDate = Math.floor(startDate % 1000000);
      entclmQuery += `SCLMDT >= ${modifiedStartDate}`; //Date Format: YYMMDD
      clmhstQuery += `HCLMDT >= ${startDate}`; //Date Format: YYYYMMDD
      clmentQuery += `CLMDT >= ${startDate}`; //Date Format: YYYYMMDD
      fiched01Query += `FDDATE >= ${startDate}`; //Date Format: YYYYMMDD

      if (multiSearchFlag !== "Yes") {
        multiSearchFlag = "Yes";
      }
    }

    //If End Date is provided, add it to the search parameters:
    if (endDate !== 0) {
      console.log(`End Date Provided: ${endDate}`);

      if (multiSearchFlag == "Yes") {
        entclmQuery += " and ";
        clmhstQuery += " and ";
        clmentQuery += " and ";
        fiched01Query += " and ";
      }

      // For entclmQuery, drop the first 2 numbers from the endDate
      let modifiedEndDate = Math.floor(endDate % 1000000);
      entclmQuery += `SCLMDT <= ${modifiedEndDate}`; //Date Format: YYMMDD
      clmhstQuery += `HCLMDT <= ${endDate}`; //Date Format: YYYYMMDD
      clmentQuery += `CLMDT <= ${endDate}`; //Date Format: YYYYMMDD
      fiched01Query += `FDDATE <= ${endDate}`; //Date Format: YYYYMMDD

      if (multiSearchFlag !== "Yes") {
        multiSearchFlag = "Yes";
      }
    }
  }

  //If Date Order is provided, add it to the search parameters: (Default to Descending)
  if (dateOrder.toUpperCase() == "ASC" || dateOrder.toUpperCase() == "ASCENDING") {
    console.log("Date Order: Ascending");

    entclmQuery += " ORDER BY SCLMDT";
    clmhstQuery += " ORDER BY HCLMDT";
    clmentQuery += " ORDER BY CLMDT";
    fiched01Query += " ORDER BY FDDATE";
  } else {
    console.log("Date Order: Descending");

    entclmQuery += " ORDER BY SCLMDT DESC";
    clmhstQuery += " ORDER BY HCLMDT DESC";
    clmentQuery += " ORDER BY CLMDT DESC";
    fiched01Query += " ORDER BY FDDATE DESC";
  }

  //Set SQL limit to record count, if its blank, default to 1000 records:
  if (recordCount == 0) {
    recordCount = 1000;
  }

  entclmQuery += ` LIMIT ${recordCount}`;
  clmhstQuery += ` LIMIT ${recordCount}`;
  clmentQuery += ` LIMIT ${recordCount}`;
  fiched01Query += ` LIMIT ${recordCount}`;

  //Return the Queries for Testing:
  returnObj.array.push(entclmQuery);
  returnObj.array.push(clmhstQuery);
  returnObj.array.push(clmentQuery);
  returnObj.array.push(fiched01Query);

  //Run the Select Queries:
  try {
    let entclmResults = await pjs.query(entclmQuery);
    let clmhstResults = await pjs.query(clmhstQuery);
    let clmentResults = await pjs.query(clmentQuery);
    let fiched01Results = await pjs.query(fiched01Query);

    returnObj.array.push("Select Queries Ran");

    //Push the number of records returned for each query to the returnObj:
    returnObj.array.push(`records found in entclmQuery: ${entclmResults.length}`);
    returnObj.array.push(`records found in clmhstQuery: ${clmhstResults.length}`);
    returnObj.array.push(`records found in clmentQuery: ${clmentResults.length}`);
    returnObj.array.push(`records found in fiched01Query: ${fiched01Results.length}`);

    //Turn on if you want to see the results for testing:
    // returnObj.array.push(entclmResults);
    // returnObj.array.push(clmhstResults);
    // returnObj.array.push(clmentResults);
    // returnObj.array.push(fiched01Results);

    //Insert the Results into the Dev Tables:
    try {
      for (let record of entclmResults) {
        await pjs.query(`INSERT INTO ${devEntclmTable} SET ?`, record);
      }
      //   for (let record of clmhstResults) {
      //       await pjs.query(`INSERT INTO ${devClmhstTable} SET ?`, record);
      //   }
      //   for (let record of clmentResults) {
      //       await pjs.query(`INSERT INTO ${devClmentTable} SET ?`, record);
      //   }
      //   for (let record of fiched01Results) {
      //       await pjs.query(`INSERT INTO ${devFiched01Table} SET ?`, record);
      //   }

      returnObj.array.push("Dev Tables Updated");
    } catch (error) {
      returnObj.error = `Error updating the dev tables: ${error.message}`;
      await ProfrontTxtlog.pushToTxTLog("updateDevTablesAPI.V1", "System", originalDataForLogging, returnObj.error);
    }
  } catch (error) {
    returnObj.error = `Error running the select queries: ${error.message}`;
    await ProfrontTxtlog.pushToTxTLog("updateDevTablesAPI.V1", "System", originalDataForLogging, returnObj.error);
  }

  response.status(200).send(returnObj);
}
exports.run = updateDevTables;
