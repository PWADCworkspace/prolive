/* Incoming INFO:
{
    "InvoiceNumber" : 1,
    "StoreNumber" : 700,
    "ItemNumber" : 13425,
    "UserKeyingClaim": "   Tyler",
    "userLastName": "Hobbs",
    "CatchWeight": 0,
    "QuantityOnClaim" : 1,
    "CaseOrOnly" : "C",
    "ClaimNumber": 123456,
    "ClaimType": 1,
    "ClaimDate": "2023-09-25",
    "isRestockFee": "N",
    "CreditType": "C",
    "ExtendedCost": 47.57,
    "ExtenededRetail": 47.57
}
*/

/*
V4.2 adjustments: 
  Date: 10/27/2023
  Author: Tyler Hobbs
  Calling Config:   
    //writeEntClmAPI V4.2
    app.post(
      "/v4.2/writeEntryClaim/",
      profoundjs.express("prolive/APIs/writeClaimAPI.V4.2")
    );
  Changes:
   - Added in Claim Date as a requested parm instead of using Todays Date.
   - Using Dayjs for date formatting to YYMMDD.
   - Removed the previous formatDate function that was used to set the assignment and claim date to the proper YYMMDD format.
   - Added in wrtting to the new CMXREFP table with the long claim number. PROCLM-25 task in Jira

  Additional updated on 11/1/23
   - Replaced UserKeyingClaim with userFirstName
   - Replaced CallingName with userLastName
   - Moved formatting for claimDateFormatted to after the missing data error checks
   - Moved formatting for shortClaimNumber to after the missing data error checks
   - Added error checking for all incoming parms
   - Removed errorMsg and replaced with returnObj.error
   - Replaced all returning variables with returnObj, a combination of .error and .array
   - Replaced originalDataForLogging with incomingDataForLogging to be consistent with the other APIs
*/
const dayjs = require("dayjs");

async function writeClaimInfo(request, response) {
  //Set pathing for Apps.
  const directPathToProlive = "../../prolive/APPs/";
  const directPathToProtest = "../../protest/APPs/";
  const directPathToProlog = "../../prologging/";

  let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");
  let ledgerNumber = pjs.require(directPathToProlive + "LedgerNumberAPP.js");

  //Define incomming variables.
  const invoiceNumber = request.body.InvoiceNumber;
  const storeNumber = request.body.StoreNumber;
  const itemNumberCheckDigit = request.body.ItemNumber;
  const quantityClaimed = request.body.QuantityOnClaim;
  const caseOrOnlyClaimed = request.body.CaseOrOnly;
  const claimNumber = request.body.ClaimNumber;
  const claimType = request.body.ClaimType;
  const creditType = request.body.CreditType;
  const isRestockFee = request.body.isRestockFee;
  const userFirstName = request.body.UserFirstName.trim();
  const userLastName = request.body.UserLastName.trim();
  const userName = userFirstName + " " + userLastName;
  const incommingCatchWeight = request.body.CatchWeight;
  const claimDate = request.body.ClaimDate; //incommming will be like "2023-09-25"

  let flag = "P";
  if (storeNumber == 700) flag = "";

  let returnObj = {};
  returnObj.array = [];
  returnObj.error = "";
  let gatheredClaimInfoArray = [];
  let itemNumber = Math.floor(itemNumberCheckDigit / 10);

  pjs.define("extendedCost", {
    type: "packed decimal",
    length: 7,
    decimals: 2
  });
  pjs.define("extendedRetail", {
    type: "packed decimal",
    length: 7,
    decimals: 2
  });

  extendedCost = request.body.ExtendedCost; //extended cost 7s 2
  extendedRetail = request.body.ExtenededRetail; //extended retail 7s 2

  const incomingDataForLogging =
    "InvoiceNumber: " +
    invoiceNumber +
    ", StoreNumber: " +
    storeNumber +
    ", ItemNumber: " +
    itemNumberCheckDigit +
    ", QuantityOnClaim: " +
    quantityClaimed +
    ", CaseOrOnly: " +
    caseOrOnlyClaimed +
    ", ClaimNumber: " +
    claimNumber +
    ", ClaimType: " +
    claimType +
    ", CreditType: " +
    creditType +
    ", ClaimDate: " +
    claimDate +
    ", isRestockFee: " +
    isRestockFee +
    ", IncomingCatchWeight: " +
    incommingCatchWeight +
    ", Flag: " +
    flag +
    ", ExtendedCost: " +
    extendedCost +
    ", ExtendedRetail: " +
    extendedRetail;

  //Setting Prod or Dev tables based on port---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  const currentPort = profound.settings.port;
  console.log(currentPort);

  let ENTCLMTable = "";

  if (currentPort === 80) {
    ENTCLMTable = "ENTCLM_CALLIN";
    console.log(`Running writeClaimAPI.V4.2 in PROD with table: ${ENTCLMTable}`);
  } else {
    ENTCLMTable = "ENTCLMDEV";
    console.log(`Running writeClaimAPI.V4.2 in DEV with table: ${ENTCLMTable}`);
  }

  // ERROR CHECKS FOR MISSING DATA COMMING IN---------------------------------------------------------------------------
  if (!invoiceNumber) {
    returnObj.error = "Invoice Number is required";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  if (!itemNumberCheckDigit) {
    returnObj.error = "Item Number is required";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  if (!storeNumber) {
    returnObj.error = "Store Number is required";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  if (invoiceNumber.toString().length > 5) {
    returnObj.error =
      "Invoice Number: " +
      invoiceNumber +
      " may be incorrect. It should have a length of 5";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  if (!quantityClaimed || quantityClaimed == 0) {
    returnObj.error = "QuantityOnClaim needs to be greater than 0.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  if (!caseOrOnlyClaimed) {
    returnObj.error = "CaseOrOnly is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  if (!claimNumber) {
    returnObj.error = "ClaimNumber is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  if (!claimType) {
    returnObj.error = "ClaimType is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  if (!creditType) {
    returnObj.error = "CreditType is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  if (!isRestockFee) {
    returnObj.error = "isRestockFee is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  if (
    !userFirstName ||
    userFirstName == "" ||
    (!userLastName || userLastName == "")
  ) {
    returnObj.error = "Users first and last name is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  if (!claimDate) {
    returnObj.error = "Claim Date is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  if (!extendedCost || extendedCost == 0) {
    returnObj.error = "Extended Cost is required and must be greater than 0.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  if (extendedRetail < 0 || isNaN(extendedRetail)) {
    returnObj.error = "Extended Retail is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }
  // END ERROR CHECKS FOR MISSING DATA COMMING IN---------------------------------------------------------------------------

  //Use dayjs to format the incomming date to YYMMDD and then convert it to a number
  let claimDateFormatted = dayjs(claimDate).format("YYMMDD");
  claimDateFormatted = parseInt(claimDateFormatted);

  //drop the first number of the claim number and set it to shortClaimNumber
  let shortClaimNumber = claimNumber.toString();
  shortClaimNumber = shortClaimNumber.substr(1, 5);
  shortClaimNumber = parseInt(shortClaimNumber);

  //START CATCH WEIGHT QUERY HERE
  if (incommingCatchWeight > 0) {
    if (caseOrOnlyClaimed == "O" || caseOrOnlyClaimed == "o") {
      returnObj.error = "Catch Weight is only allowed for a case claim.";
      await ProfrontTxtlog.pushToTxTLog(
        "writeClaimAPI.V4.2",
        userName,
        incomingDataForLogging,
        returnObj.error
      );
      response.status(400).send(returnObj);
      return;
    }

    if (quantityClaimed != 1) {
      returnObj.error = "Quantity on Claim must be 1 for Catch Weight Items.";
      await ProfrontTxtlog.pushToTxTLog(
        "writeClaimAPI.V4.2",
        userName,
        incomingDataForLogging,
        returnObj.error
      );
      response.status(400).send(returnObj);
      return;
    }

    console.log(
      `Running catch weight query for item ${itemNumber} on invoice ${invoiceNumber} at store ${storeNumber} with a catch weight of ${incommingCatchWeight}`
    );

    sqlquery =
      "select " +
      "FDITEM as ItemCode, " +
      "FDINV as InvoiceNumber, " +
      "FDCHKD as CheckDigit, " +
      "FDDATE as InvoiceDate, " +
      "FDPKSZ as PackSize, " +
      "FDDESC as Description, " +
      "FDQTYS as QuantityShipped, " +
      "FDDEPT as Department, " +
      "FDSLOT as Slot, " +
      "FDSELL as StoreCost, " +
      "FDECST as Cost " + //this is extended cost.
      "from FICHED01 where FDINV = ? and FDSTOR = ? and FDITEM = ? and FDCWLB = ?";
    // gatheredClaimInfo.array = pjs.query(sqlquery, [invoiceNumber, storeNumber, itemNumber, incommingCatchWeight]);
    gatheredClaimInfoArray = pjs.query(sqlquery, [
      invoiceNumber,
      storeNumber,
      itemNumber,
      incommingCatchWeight
    ]);

    if (gatheredClaimInfoArray.length < 1) {
      returnObj.error = `No item info was found for item number ${itemNumber} for invoice ${invoiceNumber} for store ${storeNumber} with a catch weight of ${incommingCatchWeight}.`;
      await ProfrontTxtlog.pushToTxTLog(
        "writeClaimAPI.V4.2",
        userName,
        incomingDataForLogging,
        returnObj.error
      );
      response.status(400).send(returnObj);
      return;
    }
    writeClaim(gatheredClaimInfoArray);
  } else {
    //START NON-CATCH WEIGHT QUERY HERE

    console.log(
      `Running non-catch weight query for item ${itemNumber} on invoice ${invoiceNumber} at store ${storeNumber}`
    );

    sqlquery =
      "select " +
      "FDITEM as ItemCode, " +
      "FDINV as InvoiceNumber, " +
      "FDCHKD as CheckDigit, " +
      "FDDATE as InvoiceDate, " +
      "FDPKSZ as PackSize, " +
      "FDDESC as Description, " +
      "FDQTYS as QuantityShipped, " +
      "FDDEPT as Department, " +
      "FDSLOT as Slot, " +
      "FDSELL as StoreCost, " +
      "FDECST as Cost " +
      "from FICHED01 where FDINV = ? and FDSTOR = ? and FDITEM = ?";
    //  gatheredClaimInfo.array = pjs.query(sqlquery, [invoiceNumber, storeNumber, itemNumber]);
    gatheredClaimInfoArray = pjs.query(sqlquery, [
      invoiceNumber,
      storeNumber,
      itemNumber
    ]);

    if (gatheredClaimInfoArray.length < 1) {
      returnObj.error = `No item found for item number ${itemNumber} for invoice ${invoiceNumber} for store ${storeNumber}.`;
      await ProfrontTxtlog.pushToTxTLog(
        "writeClaimAPI.V4.2",
        userName,
        incomingDataForLogging,
        returnObj.error
      );
      response.status(400).send(returnObj);
      return;
    }
    writeClaim(gatheredClaimInfoArray);
  }

  async function writeClaim(gatheredClaimInfoIn) {
    let department = gatheredClaimInfoIn[0]["department"];
    department = department.trim();
    department = department.substr(0, 1);

    ledgerNumber = await ledgerNumber.getLedgerNumber(department);
    if (ledgerNumber.Message) {
      returnObj.error = ledgerNumber.Message;
      await ProfrontTxtlog.pushToTxTLog(
        "writeClaimAPI.V4.2",
        userName,
        incomingDataForLogging,
        ledgerNumber.Message
      );
      response.status(400).send(returnObj);
      return;
    }
    ledgerNumber = ledgerNumber.LedgerNumber;

    let invNumOfOrder = gatheredClaimInfoIn[0]["invoicenumber"];
    let invoiceDate1 = gatheredClaimInfoIn[0]["invoicedate"];
    let invoiceDate = invoiceDate1.toString();
    invoiceDate = invoiceDate.substr(2, 6);
    let slot = gatheredClaimInfoIn[0]["slot"]; //"123456"
    let pickCode = slot.substr(0, 1); //pick code '1'
    let slot1 = slot.substr(1, 2); // Part of slot '23'
    let slot3 = slot.substr(3, 3); // part of slot '456'
    let packSize;
    packSize = gatheredClaimInfoIn[0]["packsize"];
    packSize = formatPackSize(packSize);
    let storeCost = gatheredClaimInfoIn[0]["storecost"];
    let buyer = getBuyerNumber(itemNumber); //buyer

    if (claimType == 2 && (isRestockFee == "Y" || isRestockFee == "y")) {
      var restockCharge = extendedCost * 0.05;
      extendedCost = extendedCost - restockCharge;
    } else {
      restockCharge = 0;
    }

    const recordID = "S";
    const tobaccoTax = 0;
    const documentNumber = 0;
    const container = 0;
    let assignment = dayjs().format("YYMMDD");

    /*
    // TURN ON FOR TESTING!!!!!!
    console.log(
      "recordID: " + recordID, //done 
      "claimNumber: " + claimNumber, //done 
      "shortClaimNumber: " + shortClaimNumber, //done
      "claimDate: " + claimDate, //done 
      "packSize: " + packSize, //can grab on INVMST
      "storeNumber: " + storeNumber, //done
      "itemNumber: " + itemNumber, //testing
      "claimType: " + claimType, //done
      "creditType: " + creditType, //done
      "department: " + department, //done
      "ledgerNumber: " + ledgerNumber, //done
      "invNumOfOrder: " + invNumOfOrder, //must get from FICHED01. For type 5 (not on INV.) use claim number
      "orderDate: " + invoiceDate, //must get from FICHED01
      "slot: " + slot, //can grab on INVMST
      "pickCode: " + pickCode, //can grab on INVMST
      "slot1: " + slot1, //can grab on INVMST
      "slot3: " + slot3, //can grab on INVMST
      "quantityClaimed: " + quantityClaimed, //done
      "caseOrOnlyClaimed: " + caseOrOnlyClaimed, //done
      "tobaccoTax: " + tobaccoTax, //done
      "storeCost: " + storeCost, //must get from FICHED01.
      "extendedCost: " + extendedCost, //must get from FICHED01???
      "flag: " + flag, //done 
      "buyer: " + buyer, //done 
      "restockCharge: " + restockCharge, //done 
      "documentNumber: " + documentNumber, //done
      "extendedRetail: " + extendedRetail, //must get from FICHED01.
      "userFirstName: " + userFirstName, //done
      "assignment: " + assignment, //done 
      "container: " + container, //done
      "userLastName: " + userLastName //done
    );
    */
    // /*
    // TURN OFF FOR TESTING!!!!!

    //TH: 10-26-2023 - Adding in method to write to the new claim table with the long claim ID. PROCLM-25 in Jira
    let cmxrefpSQLQuery =
      "INSERT into CMXREFP (" +
      "cxclmlong, " + //Long Claim Number
      "cxclm, " + //Short Claim Number
      "cxstore, " + //Store Number
      "cxdate, " + //Claim Date
      "cxitem, " + //Item Number
      "cxinv " + //Invoice Number
      ") VALUES(?,?,?,?,?,?) with None";

    try {
      pjs.query(cmxrefpSQLQuery, [
        claimNumber,
        shortClaimNumber,
        storeNumber,
        claimDateFormatted,
        itemNumber,
        invNumOfOrder
      ]);
      if (sqlstate >= "02000") {
        returnObj.error =
          "SQLstate error during CMXREFP write. SQLstate: " + sqlstate;
        await ProfrontTxtlog.pushToTxTLog(
          "writeClaimAPI.V4.2",
          userName,
          incomingDataForLogging,
          returnObj.error
        );
        response.status(400).send(returnObj);
        return;
      }
    } catch (err) {
      returnObj.error = "Sql Insert catch error during CMXREFP Write.";
      await ProfrontTxtlog.pushToTxTLog(
        "writeClaimAPI.V4.2",
        userName,
        incomingDataForLogging,
        returnObj.error + ":" + err
      );
      response.status(400).send(returnObj);
      return;
    }

    let entclmSQLQuery =
      `insert into ${ENTCLMTable} (` +
      "sclmrc, " +
      "sclmno, " +
      "sclmdt, " +
      "sclmst, " +
      "sclmit, " +
      "sclmty, " +
      "sclmcr, " +
      "sclmld, " +
      "sclmdp, " +
      "sordin, " +
      "sorddt, " +
      "sordpc, " +
      "sords1, " +
      "sords3, " +
      "sclmqt, " +
      "sclmco, " +
      "sclmtx, " +
      "sclmcs, " +
      "sclmex, " +
      "sclmfl, " +
      "sclmby, " +
      "sclstk, " +
      "scldoc, " +
      "sclret, " +
      "sclusr, " +
      "sclasn, " +
      "sclcnt, " +
      "sclnam" +
      ") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) with None";

    try {
      pjs.query(entclmSQLQuery, [
        recordID,
        shortClaimNumber,
        claimDateFormatted,
        storeNumber,
        itemNumber,
        claimType,
        creditType,
        ledgerNumber,
        department,
        invNumOfOrder,
        invoiceDate,
        pickCode,
        slot1,
        slot3,
        quantityClaimed,
        caseOrOnlyClaimed,
        tobaccoTax,
        storeCost,
        extendedCost,
        flag,
        buyer,
        restockCharge,
        documentNumber,
        extendedRetail,
        userFirstName,
        assignment,
        container,
        userLastName
      ]);
      if (sqlstate >= "02000") {
        returnObj.error =
          "SQLstate error during ENTCLM write. SQLstate: " + sqlstate;
        await ProfrontTxtlog.pushToTxTLog(
          "writeClaimAPI.V4.2",
          userName,
          incomingDataForLogging,
          returnObj.error
        );
        response.status(400).send(returnObj);
        return;
      }
    } catch (err) {
      returnObj.error = "Sql Insert catch error during ENTCLM Write.";
      await ProfrontTxtlog.pushToTxTLog(
        "writeClaimAPI.V4.2",
        userName,
        incomingDataForLogging,
        returnObj.error + ":" + err
      );
      response.status(400).send(returnObj);
      return;
    }
    // */
    let successMsg = "Entry Claim submitted.";
    returnObj.array = successMsg;
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.2",
      userName,
      incomingDataForLogging,
      successMsg
    );
    console.log(
      `Item Code ${itemNumber} updated for Store ${storeNumber} for for Claim Date ${claimDate}.`
    );
    response.status(200).send(returnObj);
  }
}
exports.run = writeClaimInfo;

//Get BUYER from INVMST based on Item Number. --------------------------------------------------------------------------------
function getBuyerNumber(y) {
  var itemNumber = y;
  var recordSet;
  let sqlquery = "select buyer from INVMST where ITMCDE = ?";
  recordSet = pjs.query(sqlquery, [itemNumber]);
  buyer = "";
  if (recordSet.length > 0 && recordSet != "") buyer = recordSet[0]["buyer"];
  return buyer;
}

function formatPackSize(packSizeIn) {
  pS = packSizeIn.toString();
  var packDash = pS.indexOf("/");
  pS = pS.slice(0, packDash);
  pS = pS.trim();
  pS = parseInt(pS);
  return pS;
}
