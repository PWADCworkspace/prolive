/* Incoming INFO:
{
    "ClaimNumber": 987654,
    "StoreNumber" : 700,
    "ItemNumber" : 840165,
    "QuantityOnClaim" : 2,
    "ExtendedCost": 0,
    "ClaimDate": "2023-09-25",
    "UserFirstName": "Tyler",
    "UserLastName": "Hobbs"
}
*/

/*
V4.2 adjustments: 
  Date: 10/27/2023
  Author: Tyler Hobbs
  Calling Config:   
    //writeNotOnInvoiceKeptClaimAPI V1.2
    app.post(
      "/v1.2/writeNotOnInvoiceKeptClaim/",
      profoundjs.express("prolive/APIs/writeNotOnInvoiceKeptClaimAPI.V1.2")
    );
  Changes:
   - Added in Claim Date as a requested parm instead of using Todays Date.
   - Changed the assignment number to get todays date and format it to YYMMDD using Dayjs.
   - Changed the insert statement to use the new claim date and assignment number.
   - Removed the old formatDate function and replaced it with Dayjs.
   - Changed the incomming user first name from "sclUser" to "UserFirstName"
   - Changed the incomming user last name from "callingName" to "UserLastName"
*/

const dayjs = require("dayjs");

async function writeNotOnInvoiceKeptClaimInfo(request, response) {

  const directPathToProlive = "../../prolive/APPs/";
  const directPathToProtest = "../../protest/APPs/";
  const directPathToProlog = "../../Prologging/";

  let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");
  let ledgerNumber = pjs.require(directPathToProlive + "LedgerNumberAPP.js");
  let validateCheckDigitAPP = pjs.require(
    directPathToProlive + "ValidateCheckDigitAPP.js"
  );

  //Define incomming variables.
  const claimNumber = request.body.ClaimNumber;
  const storeNumber = request.body.StoreNumber;
  const itemNumberCheckDigit = request.body.ItemNumber;
  const quantityClaimed = request.body.QuantityOnClaim;
  const userFirstName = request.body.UserFirstName.trim();
  const userLastName = request.body.UserLastName.trim();
  const userName = userFirstName + " " + userLastName;
  const claimDate = request.body.ClaimDate; //incommming will be like "2023-09-25"

  pjs.define("extendedCost", {
    type: "packed decimal",
    length: 7,
    decimals: 2
  });

  pjs.define("extendedRetail", {
    type: "packed decimal",
    length: 7,
    decimals: 2
  });

  //Predefine variables.
  let returnObj = {};
  returnObj.array = [];
  returnObj.error = "";
  let gatheredClaimInfoArray = [];

  extendedCost = request.body.ExtendedCost; //extended cost 7s 2
  extendedRetail = 0.0; //Place holder since we cannot pull the retail information for this item for this store.

  const incomingDataForLogging =
    "ClaimNumber: " +
    claimNumber +
    ", StoreNumber: " +
    storeNumber +
    ", ItemNumber: " +
    itemNumberCheckDigit +
    ", QuantityOnClaim: " +
    quantityClaimed +
    ", UserName: " +
    userName +
    ", ClaimDate: " +
    claimDate +
    ", extendedCost: " +
    extendedCost;

  //Setting Prod or Dev tables based on port---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  const currentPort = profound.settings.port;
  console.log(currentPort);

  let ENTCLMTable = "";

  if (currentPort === 80) {
    ENTCLMTable = "ENTCLM_CALLIN";
    console.log(`Running writeNotOnInvoiceKeptClaimAPI.V1.2 in PROD with table: ${ENTCLMTable}...`);
  } else {
    ENTCLMTable = "ENTCLMDEV";
    console.log(`Running writeNotOnInvoiceKeptClaimAPI.V1.2 in DEV with table: ${ENTCLMTable}...`);
  }

  // ERROR CHECKS FOR MISSING DATA COMMING IN---------------------------------------------------------------------------
  if (!claimNumber) {
    returnObj.error = "Claim Number is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNotOnInvoiceKeptClaimAPI.V1.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  if (!itemNumberCheckDigit) {
    returnObj.error = "Item Number is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNotOnInvoiceKeptClaimAPI.V1.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  let checkDigitValidation = await validateCheckDigitAPP.validateCheckDigit(
    itemNumberCheckDigit
  );
  if (checkDigitValidation == false) {
    returnObj.error =
      "This item code does not seem to be valid, please double check that it is entered correctly.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNotOnInvoiceKeptClaimAPI.V1.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  if (!storeNumber) {
    returnObj.error = "Store Number is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNotOnInvoiceKeptClaimAPI.V1.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  if (!quantityClaimed || quantityClaimed == 0) {
    returnObj.error = "QuantityOnClaim needs to be greater than 0.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNotOnInvoiceKeptClaimAPI.V1.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  if (
    !userFirstName ||
    userFirstName == "" ||
    (!userLastName || userLastName == "")
  ) {
    returnObj.error = "Users first and last name is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  if (!claimDate) {
    returnObj.error = "A valid claim date is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeFreezerboxTotesAPI.V2.1",
      "Automated call during Claim",
      incomingDataForLogging,
      returnObj
    );
    response.status(400).send(returnObj);
    return;
  }

  if (!extendedCost || extendedCost == 0) {
    returnObj.error = "Extended Cost is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeFreezerboxTotesAPI.V2.1",
      "Automated call during Claim",
      incomingDataForLogging,
      returnObj
    );
    response.status(400).send(returnObj);
    return;
  }
  // END ERROR CHECKS FOR MISSING DATA COMMING IN---------------------------------------------------------------------------

  let itemNumber = Math.floor(itemNumberCheckDigit / 10);

  //Use dayjs to format the incomming date to YYMMDD and then convert it to a number
  let claimDateFormatted = dayjs(claimDate).format("YYMMDD");
  claimDateFormatted = parseInt(claimDateFormatted);

  //drop the first number of the claim number and set it to shortClaimNumber
  let shortClaimNumber = claimNumber.toString();
  shortClaimNumber = shortClaimNumber.substr(1, 5);
  shortClaimNumber = parseInt(shortClaimNumber);

  let sqlquery =
    "select " +
    "ITMCDE as ItemCode, " +
    "CHKDIG as CheckDigit, " +
    "ITMDSC as Description, " +
    "DEPT as Department, " +
    "PICODE as PickCode, " +
    "SLOTAI as SlotAisle, " +
    "SLOTLV as SlotLevel, " +
    "SLOTNO as SlotBin, " +
    "sell as StoreCost, " +
    "cost as Cost " +
    "from INVMST where ITMCDE = ?";

  gatheredClaimInfoArray = pjs.query(sqlquery, [itemNumber]);
  console.log(gatheredClaimInfoArray);

  if (gatheredClaimInfoArray.length < 1) {
    returnObj.error = `No item found for item number ${itemNumber}`;
    await ProfrontTxtlog.pushToTxTLog(
      "writeNotOnInvoiceKeptClaimAPI.V1.2",
      userName,
      incomingDataForLogging,
      returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }

  let department = gatheredClaimInfoArray[0]["department"];
  department = department.trim();

  ledgerNumber = await ledgerNumber.getLedgerNumber(department);

  if (ledgerNumber.Message) {
    await ProfrontTxtlog.pushToTxTLog(
      "writeNotOnInvoiceKeptClaimAPI.V1.2",
      userName,
      incomingDataForLogging,
      ledgerNumber.Message
    );
    returnObj.error = ledgerNumber.Message;
    response.status(400).send(returnObj);
    return;
  }
  ledgerNumber = ledgerNumber.LedgerNumber;

  const invoiceNumber = shortClaimNumber;
  const caseOrOnlyClaimed = "C";
  const restockCharge = 0;
  const creditType = ""; // setting default to "" to generate an invoice instead of a credit.
  const claimType = 5; // setting default to 5 for Not On Invoice: Kept
  const recordID = "S"; // record ID 'S'
  const tobaccoTax = 0; // tax if tobacco. we can stop passing this
  const documentNumber = 0; //document no. created when the file is read on 400.
  const container = 0;

  let storeCost = gatheredClaimInfoArray[0]["storecost"];
  let buyer = getBuyerNumber(itemNumber);
  let pickCode = gatheredClaimInfoArray[0]["pickcode"];
  let slot3 = gatheredClaimInfoArray[0]["slotbin"];
  let slotAisle = gatheredClaimInfoArray[0]["slotaisle"];
  let slotLevel = gatheredClaimInfoArray[0]["slotlevel"];
  let slot1 = slotAisle + slotLevel;
  slot1 = slot1.trim();
  let assignment = dayjs().format("YYMMDD");
  let invoiceDate = claimDateFormatted;
  let flag = "P";
  if (storeNumber == 700) flag = "";

  // /*
  // TURN ON FOR TESTING!!!!!!
  console.log(
    "recordID: " + recordID,
    "claimNumber: " + claimNumber,
    "claimDate: " + claimDate,
    "storeNumber: " + storeNumber,
    "itemNumber: " + itemNumber,
    "claimType: " + claimType,
    "creditType: " + creditType,
    "department: " + department,
    "ledgerNumber: " + ledgerNumber,
    "invNumOfOrder: " + invoiceNumber, //set to claim number
    "orderDate: " + invoiceDate, //set to claim date
    "pickCode: " + pickCode,
    "slot1: " + slot1,
    "slot3: " + slot3,
    "quantityClaimed: " + quantityClaimed,
    "caseOrOnlyClaimed: " + caseOrOnlyClaimed,
    "tobaccoTax: " + tobaccoTax,
    "storeCost: " + storeCost,
    "extendedCost: " + extendedCost,
    "flag: " + flag,
    "buyer: " + buyer,
    "restockCharge: " + restockCharge,
    "documentNumber: " + documentNumber,
    "extendedRetail: " + extendedRetail,
    "assignment: " + assignment,
    "container: " + container,
    "UserFirstName: " + userFirstName,
    "UserLastName: " + userLastName
  );
  // */
  // /*
  // TURN OFF FOR TESTING!!!!!

  //TH: 10-26-2023 - Adding in method to write to the new claim table with the long claim ID. PROCLM-25 in Jira
  let cmxrefpSQLQuery =
    "INSERT into CMXREFP (" +
    "cxclmlong, " + //Long Claim Number
    "cxclm, " + //Short Claim Number
    "cxstore, " + //Store Number
    "cxdate, " + //Claim Date
    "cxitem, " + //Item Number
    "cxinv " + //Invoice Number
    ") VALUES(?,?,?,?,?,?) with None";

  try {
    pjs.query(cmxrefpSQLQuery, [
      claimNumber,
      shortClaimNumber,
      storeNumber,
      claimDateFormatted,
      itemNumber,
      invoiceNumber
    ]);
    if (sqlstate >= "02000") {
      returnObj.error =
        "SQLstate error during CMXREFP Write. SQLstate: " + sqlstate;
      await ProfrontTxtlog.pushToTxTLog(
        "writeClaimAPI.V4.2",
        userName,
        incomingDataForLogging,
        returnObj.error
      );
      response.status(400).send(returnObj);
      return;
    }
  } catch (err) {
    returnObj.error = "Sql Insert catch error during CMXREFP Write.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeClaimAPI.V4.2",
      userName,
      incomingDataForLogging,
      returnObj.error + ":" + err
    );
    response.status(400).send(returnObj);
    return;
  }

  let entclmSQLQuery =
    `insert into ${ENTCLMTable} (` +
    "sclmrc, " +
    "sclmno, " +
    "sclmdt, " +
    "sclmst, " +
    "sclmit, " +
    "sclmty, " +
    "sclmcr, " +
    "sclmld, " +
    "sclmdp, " +
    "sordin, " +
    "sorddt, " +
    "sordpc, " +
    "sords1, " +
    "sords3, " +
    "sclmqt, " +
    "sclmco, " +
    "sclmtx, " +
    "sclmcs, " +
    "sclmex, " +
    "sclmfl, " +
    "sclmby, " +
    "sclstk, " +
    "scldoc, " +
    "sclret, " +
    "sclusr, " +
    "sclasn, " +
    "sclcnt, " +
    "sclnam" +
    ") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) with None";
  try {
    pjs.query(entclmSQLQuery, [
      recordID,
      shortClaimNumber,
      claimDateFormatted,
      storeNumber,
      itemNumber,
      claimType,
      creditType,
      ledgerNumber,
      department,
      invoiceNumber,
      invoiceDate,
      pickCode,
      slot1,
      slot3,
      quantityClaimed,
      caseOrOnlyClaimed,
      tobaccoTax,
      storeCost,
      extendedCost,
      flag,
      buyer,
      restockCharge,
      documentNumber,
      extendedRetail,
      userFirstName,
      assignment,
      container,
      userLastName
    ]);
    if (sqlstate >= "02000") {
      returnObj.error =
        "SQLstate error during ENTCLM write on writeNotOnInvoiceKeptClaim API. SQLstate: " +
        sqlstate;
      await ProfrontTxtlog.pushToTxTLog(
        "writeNotOnInvoiceKeptClaimAPI.V1.2",
        userName,
        incomingDataForLogging,
        returnObj.error + ":" + err
      );
      console.log(
        "Item Code " +
          itemNumber +
          " Store " +
          storeNumber +
          " Error Message: " +
          returnObj.error
      );
      response.status(400).send(returnObj);
      return;
    }
  } catch (err) {
    returnObj.error =
      "Sql Insert catch error during ENTCLM write on writeNotOnInvoiceKeptClaim API.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeNotOnInvoiceKeptClaimAPI.V1.2",
      userName,
      incomingDataForLogging,
      returnObj.error + ":" + err
    );
    console.log(
      "Item Code " +
        itemNumber +
        " Store " +
        storeNumber +
        " Error Message: " +
        returnObj.error
    );
    response.status(400).send(returnObj);
    return;
  }
  // */
  let successMsg =
    "Entry Claim submitted to generate an invoice for the kept item.";
  returnObj.array = successMsg;
  console.log(
    `Item Code ${itemNumber} updated for Store ${storeNumber} for for Claim Date ${claimDate}.`
  );
  await ProfrontTxtlog.pushToTxTLog(
    "writeNotOnInvoiceKeptClaimAPI.V1.2",
    userName,
    incomingDataForLogging,
    successMsg
  );
  response.status(200).send(returnObj);
}
exports.run = writeNotOnInvoiceKeptClaimInfo;

//Get BUYER from INVMST based on Item Number. --------------------------------------------------------------------------------
function getBuyerNumber(y) {
  let itemNumber = y;
  let recordSet;
  let sqlquery = "select buyer from INVMST where ITMCDE = ?";
  recordSet = pjs.query(sqlquery, [itemNumber]);
  buyer = "";
  if (recordSet.length > 0 && recordSet != "") buyer = recordSet[0]["buyer"];
  return buyer;
}
