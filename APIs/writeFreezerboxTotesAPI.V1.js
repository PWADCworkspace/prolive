// NOTE: 11-21-22: This API is used ONLY for freezer box and tote claims.  It is not used for bale claims or other items.
// It should be ready for testing and should be called with or without the boxes/totes being checking in via the app. Test approval and call on store 70 in eClaims.

const dayjs = require("dayjs");

async function writeFBoxToteBale(request, response) {

  const directPathToProlive = "../../Prologging/";
  let ProfrontTxtlog = pjs.require(directPathToProlive + "PushToTxtLogAPP.js");

  let storeNumber = request.body.StoreNumber;
  let qtyOnClaim = request.body.QuantityOnClaim;
  let itemNumber = request.body.ItemNumber;
  let claimNumber = request.body.ClaimNumber;
  var userName = request.body.UserFirstName + " " + request.body.UserLastName;
  const incomingClaimDate = request.body.ClaimDate; // 11/10/2023 : WITH THE NEW FREEZERBOX PROCESS, ECLAIMS IS SENDING THE CLAIM DATE NOW SO WHY NOT UTILIZE IT PROCLM-40
  let errorMsg = "";

  const shortClaimNumber = String(claimNumber).slice(-5); // 11/10/2023: ADDED THIS SO WE WRITE SHORT CLAIM NUM TO ENTCLM FILE PROCLM-40

  let claimDate = dayjs(incomingClaimDate).format("YYMMDD"); // 11/10/2023: Formating claim date to be yymmdd format for enclm PROCLM-40
  claimDate = parseInt(claimDate);

  const originalDataForLogging =
    "StoreNumber: " +
    storeNumber +
    ", ItemNumber: " +
    itemNumber +
    ", QuantityOnClaim: " +
    qtyOnClaim +
    ", ClaimNumber: " +
    claimNumber +
    ", Short ClaimNumber: " +
    shortClaimNumber +
    ", Claim Date: " +
    incomingClaimDate +
    ", Formatted Claim Date: " +
    claimDate;

  //Setting Prod or Dev tables based on port---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  const currentPort = profound.settings.port;
  console.log(currentPort);

  let ENTCLMTable = "";

  if (currentPort === 80) {
    ENTCLMTable = "ENTCLM_CALLIN";
    console.log(`Running writeFreezerboxTotesAPI.V1 in PROD with Table: ${ENTCLMTable}.`);
  } else {
    ENTCLMTable = "ENTCLMDEV";
    console.log(`Running writeFreezerboxTotesAPI.V1 in DEV with Table: ${ENTCLMTable}.`);
  }
  
  errorMsg = 'This API is currently turned off for the new freezer box process';
  response.status(400).send(errorMsg);
  return;

  
  if (itemNumber !== 70 && itemNumber !== 30) {
    errorMsg =
      "An Item Number of either 70 (Freezer Box) or 30 (Tote) is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeFreezerboxTotesAPI.V1",
      userName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  if (!storeNumber || storeNumber.toString().length > 3) {
    errorMsg = "A valid Store Number is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeFreezerboxTotesAPI.V1",
      userName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  if (!qtyOnClaim) {
    errorMsg = "A valid quantity is required.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeFreezerboxTotesAPI.V1",
      userName,
      originalDataForLogging,
      errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  let recordID = "S";
  // let claimDate = formatDate();
  const claimType = 2;
  const creditType = "C";
  const ledgerNumber = 213;
  const department = "J";
  const invNumOfOrder = 0;
  const orderDate = 0;
  const pickCode = 0;
  const slot1 = 0;
  const slot3 = 0;
  const caseOrOnly = "";
  const tobaccoTax = 0.0;
  let perCaseCost;

  // ----------------SET THIS TO 'P' for auto processing-----------------------------------------------------------------------------------------
  let flag = "P";
  if (storeNumber == 700) flag = "";
  // ----------------SET THIS TO 'P' for auto processing-----------------------------------------------------------------------------------------

  const buyer = "";
  const restockCharge = 0.0;
  const documentNumber = 0;
  const extendedRetail = 0.0;
  let extendedCost = 0.0;
  let sclUser = request.body.UserFirstName;
  const assignment = 0;
  const container = 0;
  let callingName = request.body.UserLastName;

  if (itemNumber == 70) {
    //Freezer Boxes @ $500.00
    perCaseCost = 500.0;
  }

  if (itemNumber == 30) {
    //Totes @ $20.00
    perCaseCost = 20.0;
  }

  extendedCost = perCaseCost * qtyOnClaim;

  console.log(
    "recordID: " + recordID, //'s'
    "Incoming claimNumber: " + claimNumber, //generated on front end. 5s0
    "Short claimNumber: " + shortClaimNumber,
    "Incoming claimDate: " + incomingClaimDate,
    "claimDate: " + claimDate, //YYMMDD
    "STNUM: " + storeNumber,
    "itemNumber: " + itemNumber, //based on claim type
    "claimType: " + claimType, //this is a 2 then it is a return and we may charge a restock fee (SCLSTK)
    "creditType: " + creditType, //In CLMHST this is a 'C'
    "ledgerNumber: " + ledgerNumber, //In CLMHST this is a 213
    "department: " + department, // In CLMHST this is a 'J'
    "invNumOfOrder: " + invNumOfOrder, //In CLMHST this is usually 0
    "orderDate: " + orderDate, //In CLMHST this is 0
    "pickCode: " + pickCode, //In CLMHST this blank
    "slot1: " + slot1, //In CLMHST this blank
    "slot3: " + slot3, //In CLMHST this blank
    "qtyOnClaim: " + qtyOnClaim,
    "caseOrOnly: " + caseOrOnly, //In CLMHST this blank
    "tobaccoTax: " + tobaccoTax, //0.00
    "perCaseCost: " + perCaseCost, //according to item #, 500 or 20.
    "extendedCost: " + extendedCost, //perCaseCost * quantity on claim
    "flag: " + flag, //defauly for auto-process = 'P'
    "buyer: " + buyer, //blank
    "restockCharge: " + restockCharge, //0.00
    "documentNumber: " + documentNumber, //0. assigned later
    "extendedRetail: " + extendedRetail, //0.00
    "sclUser: " + sclUser, //user approving claim
    "assignment: " + assignment, //0
    "container: " + container, //0
    "callingName: " + callingName //we fill it with last name, not used otherwise.
  );

  //Note: Tyler Hobbs 03/11/24: Turning off the update process to CMPFRZBX as it is no longer needed and was replaced with the LucasFBGetR.js process and FBCLAIMLOG table
  // console.log(`Updating cmpfrzbx table.`);
  // const updateCMPFRZBXsqlquery =
  //   "UPDATE cmpfrzbx SET fbpndqty = fbpndqty + ? where fbstore = ? and fbitem = ?"; // chnage back to prod

  // try {
  //   pjs.query(updateCMPFRZBXsqlquery, [qtyOnClaim, storeNumber, itemNumber]);
  //   if (sqlstate >= "02000") {
  //     errorMsg = "SQL error during update to cmpfrzbx. SQLstate: " + sqlstate;
  //     await ProfrontTxtlog.pushToTxTLog(
  //       "writeFreezerboxTotesAPI.V1",
  //       userName,
  //       originalDataForLogging,
  //       errorMsg
  //     );
  //     console.log(
  //       "Item Code " +
  //         itemNumber +
  //         " Store " +
  //         storeNumber +
  //         " Error Message: " +
  //         errorMsg
  //     );
  //     response.status(400).send(errorMsg);
  //     return;
  //   }
  // } catch (err) {
  //   errorMsg = err;
  //   await ProfrontTxtlog.pushToTxTLog(
  //     "writeFreezerboxTotesAPI.V1",
  //     userName,
  //     originalDataForLogging,
  //     errorMsg
  //   );
  //   response.status(400).send(errorMsg);
  //   return;
  // }

  //SG: 11-13-2023 - Adding in method to write to the new claim table with the long claim ID. PROCLM-40 in Jira--------------------------------------------------
  console.log(`Inserting new record in CMXREFP table`);
  try {
    pjs.query(
      "INSERT into CMXREFP (" +
      "cxclmlong, " + //Long Claim Number
      "cxclm, " + //Short Claim Number
      "cxstore, " + //Store Number
      "cxdate, " + //Claim Date
      "cxitem " + //Item Number
        ") VALUES(?,?,?,?,?) with None",
      [claimNumber, shortClaimNumber, storeNumber, claimDate, itemNumber]
    );
    if (sqlstate >= "02000") {
      errorMsg = "SQLstate error during CMXREFP Write. SQLstate: " + sqlstate;
      await ProfrontTxtlog.pushToTxTLog(
        "writeFreezerboxTotesAPI.V1",
        userName,
        originalDataForLogging,
        errorMsg
      );
      response.status(400).send(errorMsg);
      return;
    }
  } catch (err) {
    console.log(err);

    errorMsg = "Sql Insert catch error during CMXREFP Write.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeFreezerboxTotesAPI.V1",
      userName,
      originalDataForLogging,
      errorMsg + ":" + err
    );
    response.status(400).send(errorMsg);
    return;
  }

  //Writing to entclm file------------------------------------------------------------------------------------------------------------------------------------------
  console.log(`Inserting record into ENTCLM file.`);
  // /*
  var sqlquery =
    `insert into ${ENTCLMTable} (` +
    "sclmrc, " + // record ID
    "sclmno, " + // Claim No.
    "sclmdt, " + // Claim Date
    "sclmst, " + // Store
    "sclmit, " + // Item Number
    "sclmty, " + // Claim Type
    "sclmcr, " + // Credit
    "sclmld, " + // Ledger Number
    "sclmdp, " + // Department
    "sordin, " + // Invoice Number of Order
    "sorddt, " + // Order Date
    "sordpc, " + // Pick Code
    "sords1, " + // Slot 1
    "sords3, " + // Slot 3
    "sclmqt, " + // Quantity on Claim
    "sclmco, " + // Case or Only
    "sclmtx, " + // Tax if Tobacco
    "sclmcs, " + // Per Case Cost
    "sclmex, " + // Extended Cost
    "sclmfl, " + // Flag
    "sclmby, " + // Buyer
    "sclstk, " + // Restock Charge
    "scldoc, " + // Document Number
    "sclret, " + // Extended Retail
    "sclusr, " + // User
    "sclasn, " + // Assignment
    "sclcnt, " + // Container
    "sclnam" + // Callin Name
    ") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) with None";
  try {
    pjs.query(sqlquery, [
      recordID,
      shortClaimNumber, // 11/10/23: Change to short claim number  PROCLM-40
      claimDate,
      storeNumber,
      itemNumber,
      claimType,
      creditType,
      ledgerNumber,
      department,
      invNumOfOrder,
      orderDate,
      pickCode,
      slot1,
      slot3,
      qtyOnClaim,
      caseOrOnly,
      tobaccoTax,
      perCaseCost,
      extendedCost,
      flag,
      buyer,
      restockCharge,
      documentNumber,
      extendedRetail,
      sclUser,
      assignment,
      container,
      callingName
    ]);
    if (sqlstate >= "02000") {
      errorMsg = "SQL error during write. SQLstate: " + sqlstate;
      await ProfrontTxtlog.pushToTxTLog(
        "writeFreezerboxTotesAPI.V1",
        userName,
        originalDataForLogging,
        errorMsg
      );
      console.log(
        "Item Code " +
          itemNumber +
          " Store " +
          storeNumber +
          " Error Message: " +
          errorMsg
      );
      response.status(400).send(errorMsg);
      return;
    }
  } catch (err) {
    errorMsg = "Sql Insert catch error during Write.";
    await ProfrontTxtlog.pushToTxTLog(
      "writeFreezerboxTotesAPI.V1",
      userName,
      originalDataForLogging,
      errorMsg
    );
    console.log(
      "Item Code " +
        itemNumber +
        " Store " +
        storeNumber +
        " Error Message: " +
        errorMsg
    );
    response.status(400).send(errorMsg);
    return;
  }

  //API Response----------------------------------------------------------------------------------------------------------------------------------------------------
  const successMsg = "Entry Claim/s submitted.";
  await ProfrontTxtlog.pushToTxTLog(
    "writeFreezerboxTotesAPI.V1",
    userName,
    originalDataForLogging,
    successMsg
  );
  console.log(
    "Item Code " +
      itemNumber +
      " updated to Store " +
      storeNumber +
      " for Claim Date " +
      claimDate
  );
  response.status(200).send(successMsg);
}
exports.run = writeFBoxToteBale;
