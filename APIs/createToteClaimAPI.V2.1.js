/*

  Request Model:
    {
      "StoreNumber" : 700,
      "ItemNumber": 301,
      "UserFirstName" : "Tyler",
      "UserLastName" : "Hobbs",
      "QuantityOnClaim" : 3
    }

  app.post("/v2.1/createToteClaim/", 
  profoundjs.express("prolive/APIs/createToteClaimAPI.V2.1"));

  Created By: Samuel Gray
  Date Created: 12/24/2023
  Date Modified: 08/26/2024
  Modified By: Samuel Gray

  Purpose: Calculates the tote claim amount based on current price per tote and quantity on the claim

  Change Log:
  JIRA TICKET PROCLM-38:
    -- Replaced UserKeyingClaim to UserFirstName for consistency
    -- Replaced CallingName to UserLastName for consistency
    -- Added check for users name
    -- Added logic to grab claim amount to return back to eClaims
    
  JIRA TICKET PROCLM-45: 
    -- Added in dev table and adjusted sql queries to use template literals using the claimLogTable.
    -- Renamed array to 'success' and made it a string.
    -- Replaces all instances of returnObj with returnObj.error when writing to txt log.

  JIRA TICKET PROCLM-83:
    -- Removed any logic the pertained to freezer boxes
    -- Updated API request model to exclude fb variables
    -- Renamed API from createFreezerBoxToteClaim to createToteClaim

  Work Flow:
    - Retrieves incoming data from eClaims
    - Performs error checks on incoming data
    - Grabs current price per tote from TRSHPITP file and calculates claim amount using (tote current price * quantity on claim)
    - Returns extended cost and unit cost back to eClaims to create tote claim
*/

async function createToteClaim(request, response) {

  //Setting modules
  const dayjs = require("dayjs");

  //Setting paths
  const directPathToProlive = "../../prolive/APPs/";
  const directPathToProtest = "../../protest/APPs/";
  const directPathToProlog = "../../prologging/";

  //Setting APPS
  let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

  //Grabbing current port
  const currentPort = profound.settings.port;
  const programName = "createToteAPI.V2.1";

  //Setting API response
  let returnObj = {};
  returnObj.success = ""; 
  returnObj.error = ""; 
  returnObj.amount = 0;
  returnObj.perCaseCost = 0;

  console.log(`${programName} is running on ${currentPort}...${dayjs().format('YYYY-MM-DD HH:mm:ss')} `);

  //Grabbing incoming request body
  const storeNumber = request.body.StoreNumber;
  const itemNumber = request.body.ItemNumber;
  const quantityOnClaim = request.body.QuantityOnClaim;
  const userFirstName = request.body.UserFirstName; 
  const userLastName = request.body.UserLastName; 

  const userName = userFirstName + " " + userLastName;

  const incomingDataForLogging =  `StoreNumber: ${storeNumber}, ItemNumber: ${itemNumber}, QuantityOnClaim: ${quantityOnClaim}, UserName: ${userName}
  `;

  console.log(`Performing error checks on incoming data...`);

  if (!storeNumber || storeNumber.toString().length > 3) {
    returnObj.error = "A valid store number is required.";
    console.log(returnObj.error);
    await ProfrontTxtlog.pushToTxTLog(programName, incomingDataForLogging, returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }

  if (itemNumber !== 30) {
    returnObj.error = "An item number of 30 (Tote) is required.";
    console.log(returnObj.error);
    await ProfrontTxtlog.pushToTxTLog(programName, incomingDataForLogging, returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }

  if (!quantityOnClaim || quantityOnClaim == 0) {
    returnObj.error = "The Quantity on claim needs to be greater than 0.";
    console.log(returnObj.error);
    await ProfrontTxtlog.pushToTxTLog(programName, incomingDataForLogging, returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }

  if (!userFirstName || userFirstName == "" ) { // PROCLM-38 in Jira: Added check for users name: https://pwadc.atlassian.net/browse/PROCLM-34 
    returnObj.error = "The current user's first and last name is required.";
    console.log(returnObj.error);
    await ProfrontTxtlog.pushToTxTLog(programName, incomingDataForLogging, returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }

  console.log(`Grabbing current price for totes...`);

  try{
    var totePriceResult = pjs.query("SELECT tprice FROM trshpitp WHERE titem = 30");

    if (sqlstate >= "02000") {
      returnObj.error = `SQL state error occurred while grabbing current price for totes. SQL state: ${sqlstate}`;
      console.log(returnObj.error);
      await ProfrontTxtlog.pushToTxTLog(programName, incomingDataForLogging, returnObj.error);
      response.status(400).send(returnObj.error);
      return;
    }

  } catch (err){
    returnObj.error = "CATCH SQL occurred while grabbing current price for totes.";
    console.log(returnObj.error);
    await ProfrontTxtlog.pushToTxTLog(programName, incomingDataForLogging, returnObj.error + ":" + err);
    response.status(400).send(returnObj.error);
    return;
  }

  const totePerCaseCost = totePriceResult[0]["tprice"];

  console.log(`The current price per tote is ${totePerCaseCost}...`);
  console.log(`Calculating extended cost for tote claim using current price of ${totePerCaseCost} and claiming quantity of ${quantityOnClaim}...`);

  returnObj.perCaseCost = totePerCaseCost;
  returnObj.amount = totePerCaseCost * quantityOnClaim;
  returnObj.success = `The tote claim is prepared and ready to be created for an amount of ${returnObj.amount}.`;

  await ProfrontTxtlog.pushToTxTLog(programName, incomingDataForLogging, returnObj);

  console.log(returnObj.success);
  console.log(`${programName} is done running on ${currentPort}...${dayjs().format('YYYY-MM-DD HH:mm:ss')} `);

  response.status(200).send(returnObj);
  return;  
}
exports.run = createToteClaim;
