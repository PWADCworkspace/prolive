async function queryMobileFrameDetail(request, response) {
  console.log('mobileFrameDetails API called.');
  
  // const directPathToProlive = "../../prolive/APPs/";
  // const directPathToProtest = "../../protest/APPs/";
  const directPathToProlog = "../../Prologging/";
  const ProfrontTxtlog = pjs.require(directPathToProlog + 'PushToTxtLogAPP.js');

  let orderNumber = request.body.OrderNo;
  let type = request.body.Type.toUpperCase();
  let params = { orderNum: orderNumber };
  let getOrderDetails = [];
  const WAREHOUSE = 'WAREHOUSE';
  const TAG = 'TAG';
  const CROSSDOCK = 'CROSSDOCK';
  const originalDataForLogging = 'orderNumber: ' + orderNumber + ', type: ' + type;

  if (!orderNumber || orderNumber == '') {
    errorMsg = 'Order Number is required.';
    await ProfrontTxtlog.pushToTxTLog("MobileFrameDetailsQuery", "automated call from eClaims", originalDataForLogging, errorMsg);
    response.status(400).send(errorMsg);
    return;
  }

  if (type === WAREHOUSE || type === TAG) {
    sqlquery =
      'Select oh.ID, oh.ORDER_DT, oh.ORDER_TYPE, oh.VENDOR_NO, od.STORE, od.LINE_ITEM_NO, itm.UPC, itm.ITEM_DESC, itm.CURR_CASE_COST, od.QUANTITY ' +
      'From PW_ORDER_DETAIL od Inner Join PW_ITEM itm On od.LINE_ITEM_NO = itm.ITEM_CODE Inner Join PW_ORDER_HEADER oh On oh.GUID = od.ORDER_HEADER_GUID ' +
      'Where oh.[deleted_by] < 0 AND od.[deleted_by] < 0 AND itm.[deleted_by] < 0 AND oh.ID = @orderNum';
  } else if (type === CROSSDOCK) { //WE CAN ENTER FUTURE CROSSDOCK SQL QUERY HERE ---------------------------------------------------------------
    sqlquery =
      'Select oh.ID, oh.ORDER_DT, oh.ORDER_TYPE, oh.VENDOR_NO, od.STORE, od.LINE_ITEM_NO, od.QUANTITY From PW_ORDER_DETAIL od ' +
      'Inner Join PW_ORDER_HEADER oh On oh.GUID = od.ORDER_HEADER_GUID Where oh.[deleted_by] < 0 AND od.[deleted_by] < 0 AND oh.ID = @orderNum';  
  } else { //DEFAULT QUERY (currently same as above)----------------------------------------------------------
    sqlquery =
      'Select oh.ID, oh.ORDER_DT, oh.ORDER_TYPE, oh.VENDOR_NO, od.STORE, od.LINE_ITEM_NO, od.QUANTITY From PW_ORDER_DETAIL od ' +
      'Inner Join PW_ORDER_HEADER oh On oh.GUID = od.ORDER_HEADER_GUID Where oh.[deleted_by] < 0 AND od.[deleted_by] < 0 AND oh.ID = @orderNum';  
  }

  try {
    getOrderDetails = pjs.query(pjs.getDB('Server2012-MFS'), sqlquery, params);
    // console.log(getOrderDetails);

    if (getOrderDetails && getOrderDetails.length > 0) {
      await ProfrontTxtlog.pushToTxTLog("MobileFrameDetailsQuery", "automated call from eClaims", originalDataForLogging, "Success.");
      response.json(getOrderDetails);
    } else {
      response.status(400).send('Error, no records found for Order Number: ' + orderNumber);
      return;
    }
  } catch (err) {
    await ProfrontTxtlog.pushToTxTLog("MobileFrameDetailsQuery", "automated call from eClaims", originalDataForLogging, err);
    response.status(400).send('Error in the database query.' + err);
    return;
  }
}
exports.run = queryMobileFrameDetail;
