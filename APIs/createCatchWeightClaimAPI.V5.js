/* Incoming INFO:
{
    "InvoiceNumber" : 3,
    "StoreNumber" : 700,
    "ItemNumber" : 840082,
    "UserKeyingClaim": "Tyler",
    "CallingName": "Hobbs",
    "CatchWeight": 93.80,
    "QuantityOnClaim" : 1,
    "CaseOrOnly" : "C",
    "PendingClaims" : [
      { "ClaimID" : 12345,
        "CaseOrOnly" : "C",
        "QuantityOnClaim" : 1
      },
      { "ClaimID" : 23456,
        "CaseOrOnly" : "C",
        "QuantityOnClaim" : 2
      },
      { "ClaimID" : 34567,
        "CaseOrOnly" : "C",
        "QuantityOnClaim" : 1
      },
      { "ClaimID" : 45678,
        "CaseOrOnly" : "O",
        "QuantityOnClaim" : 4
      }
    ]
}
*/
const addSubtractDate = require("add-subtract-date");

async function createCWClaim(request, response) {
    //Set pathing for Apps.
    const directPathToProlive = "../../prolive/APPs/";
    const directPathToProtest = "../../protest/APPs/";
    const directPathToProlog = "../../prologging/";
    
    let CatchWeightFicheD01 = pjs.require(directPathToProlive + "CatchWeightFicheD01APP.V2.js");
    let CatchWeightEntClm = pjs.require(directPathToProlive + "CatchWeightEntClmAPP.V2.js");
    let CatchWeightClmHst = pjs.require(directPathToProlive + "CatchWeightClmHstAPP.V2.js");
    let CatchWeightClmEnt = pjs.require(directPathToProlive + "CatchWeightClmEntAPP.V2.js");
    let validateCheckDigitAPP = pjs.require(directPathToProlive + "ValidateCheckDigitAPP.js");
    let selector = pjs.require(directPathToProlive + "PickSelectorAPP.V3.js");
    let buyerName = pjs.require(directPathToProlive + "BuyerNameAPP.js");
    let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");
  
    //Predefine variables.
    const invoiceNumber = request.body.InvoiceNumber;
    const storeNumber = request.body.StoreNumber;
    let itemNumberCheckDigit = request.body.ItemNumber;
    // const userName = request.body.UserKeyingClaim + " " + request.body.CallingName;

    let userFirstName = request.body.UserKeyingClaim;
    let userLastName = request.body.CallingName;
    userFirstName = userFirstName.trim().toUpperCase();
    userLastName = userLastName.trim().toUpperCase();
    const userName = userFirstName + " " + userLastName;

    const incommingCatchWeight = request.body.CatchWeight;
    const quantityOnClaim = request.body.QuantityOnClaim;
    const caseOrOnly = request.body.CaseOrOnly;
    const eClaimsPendingClaimsArray = request.body.PendingClaims;

    let isMarkout = false;

    var returnObj = {};
    returnObj.array = [];
    returnObj.error = '';

    //Set Item Number and Check Digit
    itemNumberCheckDigit = itemNumberCheckDigit.toString();
    let itemNumber = itemNumberCheckDigit.slice(0, -1);
    let CheckDigit = itemNumberCheckDigit.slice(-1);

    //get count of pending claims
    const pendingClaimsCount = eClaimsPendingClaimsArray.length;
    console.log("pendingClaimsCount: " + pendingClaimsCount);

    //use forEach to get the claimId from each object in the PendingClaims array
    let claimIDArray = [];
    eClaimsPendingClaimsArray.forEach(claim => claimIDArray.push(claim.ClaimID));
    console.log("claimIDArray: " + claimIDArray);
  
    const originalDataForLogging = 
    'InvoiceNumber: '+ invoiceNumber +
    ', StoreNumber: '+ storeNumber +
    ', ItemNumber: '+ itemNumberCheckDigit +
    ', IncomingCatchWeight: '+ incommingCatchWeight +
    ', QuantityOnClaim: '+ quantityOnClaim +
    ', CaseOrOnly: '+ caseOrOnly +
    ', PendingClaimCount: '+ pendingClaimsCount;

    console.log("originalDataForLogging: " + originalDataForLogging);
  
// ERROR CHECKS FOR MISSING DATA COMMING IN--------------------------------------------------------------------------------------------------------------------
    if (!invoiceNumber) {
      returnObj.error = "Invoice Number is required";
      await ProfrontTxtlog.pushToTxTLog(
        "createCatchWeightClaimAPI.V5", 
        userName, 
        originalDataForLogging,
        returnObj.error
      );
      response.status(400).send(returnObj.error);
      return;
    }

    if (invoiceNumber.toString().length > 5) {
      returnObj.error =
          "Invoice Number: " +
          invoiceNumber +
          " may be incorrect. It should have a length of 5";
        await ProfrontTxtlog.pushToTxTLog(
          "createCatchWeightClaimAPI.V5", 
          userName, 
          originalDataForLogging,
          returnObj.error
        );
        response.status(400).send(returnObj.error);
        return;
    }
  
    if (!itemNumberCheckDigit) {
      returnObj.error = "Item Number is required";
      await ProfrontTxtlog.pushToTxTLog(
        "createCatchWeightClaimAPI.V5", 
        userName,
        originalDataForLogging,
        returnObj.error
      );
      response.status(400).send(returnObj.error);
      return;
    }
  
    if (!storeNumber) {
      returnObj.error = "Store Number is required";
      await ProfrontTxtlog.pushToTxTLog(
        "createCatchWeightClaimAPI.V5", 
        userName,
        originalDataForLogging,
        returnObj.error
      );
      response.status(400).send(returnObj.error);
      return;
    }
  
    if (!quantityOnClaim || quantityOnClaim == 0) {
      returnObj.error = "QuantityOnClaim needs to be greater than 0.";
        await ProfrontTxtlog.pushToTxTLog(
            "createCatchWeightClaimAPI.V5",
            userName,
            originalDataForLogging,
            returnObj.error
        );
        response.status(400).send(returnObj.error);
        return;
    }
   
    if(quantityOnClaim > 1){
      returnObj.error = "Quantity on Claim must be 1 for Catch Weight Items.";
        await ProfrontTxtlog.pushToTxTLog(
          "createCatchWeightClaimAPI.V5",
          userName,
          originalDataForLogging,
          returnObj.error
        );
        response.status(400).send(returnObj.error);
        return;
    }

    let checkDigitValidation = await validateCheckDigitAPP.validateCheckDigit(itemNumberCheckDigit);
  
    if (checkDigitValidation == false) {
      returnObj.error = "This item code does not seem to be valid, please double check that it is entered correctly.";
      await ProfrontTxtlog.pushToTxTLog(
        "createCatchWeightClaimAPI.V5", 
        userName, 
        originalDataForLogging,
        returnObj.error
      );
      response.status(400).send(returnObj.error);
      return;
    }

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

  // call getBuyerNameApp to get Buyer first and last name and apply it to buyerNum to add into returned array.
  buyerName = await buyerName.getBuyerName(itemNumber);
  if (buyerName.errorMsg) {
    returnObj.error = buyerName.errorMsg;
    await ProfrontTxtlog.pushToTxTLog(
      "createCatchWeightClaimAPI.V5",
      userName, 
      originalDataForLogging,        
      buyerName.errorMsg
    );
    response.status(400).send(returnObj.error);
    return;
  }

  // get Pick Selector Info.
  selector = await selector.PickSelectorApp_V3(storeNumber,invoiceNumber,itemNumberCheckDigit,itemNumber, 'Y',incommingCatchWeight);
  if (selector.ErrMessage) selector.Message = "Selector not available.";

  // Query NEW Catch Weight FicheD01 to get Item info off of Invoice.
  CatchWeightFicheD01 = await CatchWeightFicheD01.getCatchWeightFicheD01Info(invoiceNumber,storeNumber,itemNumber,incommingCatchWeight);

  if (CatchWeightFicheD01.Message) {
    returnObj.error = CatchWeightFicheD01.Message;
    await ProfrontTxtlog.pushToTxTLog("createCatchWeightClaimAPI.V5", userName, originalDataForLogging, CatchWeightFicheD01.Message);
    console.log("Error on CatchWeightFicheD01 querry:" + CatchWeightFicheD01.Message);
    response.status(400).send(returnObj.error);
    return;
  }

  console.log("records gathered from CatchWeightFicheD01.");
  var numberOfItemsOnInvoice = Object.keys(CatchWeightFicheD01.Array).length;
  console.log(`member key size: ${numberOfItemsOnInvoice}`);

  //get date and run formatDate function to +1 the day.
  let invoiceDate = CatchWeightFicheD01.Array[0]["fddate"];
  let deliveryDate = formatDate(invoiceDate);
  
  //  CHECK HERE TO COMPARE DELIVERYDATE TO TODAYS DATE. IF IT IS MORE THAN 2 DAYS APART THEY CANNOT FILE A CLAIM
  // var checkDateOfEntry = dateOfEntryApproval(deliveryDate);

  // if (checkDateOfEntry == false && storeNumber != 700){ //700 is the test store.
  //   returnObj.error = 'The deadline for filing the claim ended at 12 PM, two business days after delivery.';
  //   await ProfrontTxtlog.pushToTxTLog(
  //   "createClaimAPI.V5",
  //   userName, 
  //   originalDataForLogging,        
  //   returnObj.error
  //   );
  //   response.status(400).send(returnObj.error);
  //   return;
  // }

  //  CHECK HERE TO COMPARE DELIVERYDATE TO TODAYS DATE. IF IT IS MORE THAN 2 DAYS APART THEY CANNOT FILE A CLAIM
  let deadlineDate = dateOfEntryApproval(deliveryDate);
  console.log(`deadlineDate: ${deadlineDate}`);

  var today = new Date();
  console.log(`today: ${today}`);

  //query table for approved users
  // let approvedUsers = pjs.query("SELECT eaauth FROM tyler.eclaimauth WHERE eausrnam = ?", userName);

  userFirstName = "%" + userFirstName + "%";
  userLastName = "%" + userLastName + "%";

  let approvedUsers = pjs.query("SELECT eaauth FROM eclaimauth WHERE UPPER(eauserfn) LIKE ? and UPPER(eauserln) LIKE ?", [userFirstName,userLastName]);
  console.log('approvedUsers:');
  console.log(approvedUsers);

  if (approvedUsers.length == 0 || approvedUsers[0]['eaauth'] < 80) { //60 is the minimum authorization level to create tokens. in the table, 80 allows bypass of 2 day limit and 100 allows approval of non-standard claims
    if (today > deadlineDate) {
      let displayDate = addSubtractDate.subtract(deadlineDate, 1, "day");
      displayDate = displayDate.toString().slice(0, 15);
      returnObj.error = `The deadline for store users to file a claim for this invoice ended at 11:59 PM on ${displayDate}, two business days after delivery. \n You can still contact warehouse personnel to have the claim created. `;
      await ProfrontTxtlog.pushToTxTLog("createClaimAPI.V5", userName, originalDataForLogging, returnObj.error);
      response.status(400).send(returnObj.error);
      return;
    }
  }
  
  const quantityShipped = CatchWeightFicheD01.Array.reduce((accumulator, item) => {
    return accumulator + item.fdqtys;
    }, 0);

  console.log("Total quantityShipped: " + quantityShipped);

  if (quantityShipped <= 0) {
    returnObj.error = 'The quantity shipped was zero meaning the item was an Out and a claim cannot be filed on such. Any questions please call 205-209-6309.';
    await ProfrontTxtlog.pushToTxTLog("createCatchWeightClaimAPI.V5", userName, originalDataForLogging, returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  } 

  extendedCostForQueries = CatchWeightFicheD01.Array[0]["fdecst"];
  console.log(`CatchWeight Extended Cost: ${extendedCostForQueries}`);

//Run querries to check for previous claims--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  
  // START Query ENT-CLAIM to see if a claim is currently filed for processing.---------------------------------------------------------------------
    console.log(`Calling CatchWeightEntClm query for item: ${itemNumber}`);
    let CatchWeightEntClmCall = await CatchWeightEntClm.getCatchWeightEntClmInfo(invoiceNumber,storeNumber,itemNumber,extendedCostForQueries);
    var numberOfItemsInENTCLM = Object.keys(CatchWeightEntClmCall.Array).length;
    //Grab all the pending approved claim numbers from the query and put them into an Array to display on the screen. 
    let CatchWeightEntClmClaimIDsArray = [];
    CatchWeightEntClmCall.Array.forEach(claim => CatchWeightEntClmClaimIDsArray.push(claim.sclmno));
    console.log(`CatchWeightEntClm member key size: ${numberOfItemsInENTCLM}`);
  // END Query ENT-CLAIM to see if a claim is currently filed for processing.---------------------------------------------------------------------

  // START Query CLAIM-ENT to see if a Markout claim was filed in house.---------------------------------------------------------------------
    console.log(`Calling CatchWeightClmEnt query for item: ${itemNumber}`);
    let CatchWeightClmEntCall = await CatchWeightClmEnt.getCatchWeightClmEntInfo(invoiceNumber,storeNumber,itemNumber,extendedCostForQueries);
    // var numberOfItemsInCLMENT = Object.keys(CatchWeightClmEntCall.Array).length;
    var numberOfItemsInCLMENT = 0;
    //Grab all the pending Markout claim numbers from the query and put them into an Array to display on the screen. 
    // CatchWeightClmEntCall.Array.forEach(claim => CatchWeightEntClmClaimIDsArray.push(claim.clmno));

    CatchWeightClmEntCall.Array.forEach(claim => {
      numberOfItemsInCLMENT += claim.QuantityOnClaim;
      CatchWeightEntClmClaimIDsArray.push(claim.ClaimID);
    });

    console.log(`CLMENT member key size: ${numberOfItemsInCLMENT}`);
  // END Query CLAIM-ENT to see if a Markout claim was filed in house.---------------------------------------------------------------------

  // START Query CLAIM-HISTORY to see if a claim has already been filed.---------------------------------------------------------------------
    console.log(`Calling CatchWeightClmHst query for item: ${itemNumber}`);
    let CatchWeightClmHstCall = await CatchWeightClmHst.getCatchWeightClmHstInfo(invoiceNumber,storeNumber,itemNumber,extendedCostForQueries);
    var numberOfItemsInCLMHST = Object.keys(CatchWeightClmHstCall.Array).length;
    //Grab all the claim numbers from the query and put them into an Array to display on the screen.
    let CatchWeightClmHstClaimIDsArray = [];  
    CatchWeightClmHstCall.Array.forEach(claim => CatchWeightClmHstClaimIDsArray.push(claim.hclmno));
    console.log(`CLMHST member key size: ${numberOfItemsInCLMHST}`);
  // END Query CLAIM-HISTORY to see if a claim has already been filed.---------------------------------------------------------------------

  console.log(`CatchWeightEntClm Claim IDs: ${CatchWeightEntClmClaimIDsArray}`);
  console.log(`CatchWeightClmHst Claim IDs: ${CatchWeightClmHstClaimIDsArray}`);

  // let numberOfPaidClaims = numberOfItemsInCLMHST + numberOfItemsInCLMENT;

  let numberOfClaimsFiled = numberOfItemsInENTCLM + numberOfItemsInCLMENT + numberOfItemsInCLMHST + pendingClaimsCount;
  console.log(`Number of claims filed: ${numberOfClaimsFiled}`);

  let maxNumberOfClaimableItems = numberOfItemsOnInvoice - numberOfClaimsFiled; //remainder that can be claimed.

  if (quantityOnClaim > maxNumberOfClaimableItems) {
    // returnObj.error = `It looks like a credit has been given for this item, please check your markouts sheets under eDocs. Any questions please call 205-209-6309.`;

    returnObj.error = 
      'A total of ' + numberOfItemsOnInvoice + ' units shipped for item ' + itemNumberCheckDigit + ' on Invoice ' + invoiceNumber + ' at a weight of ' + incommingCatchWeight +' lbs. ' +
      (CatchWeightClmHstClaimIDsArray.length == 0 ? '' : 'Claim IDs: ' + CatchWeightClmHstClaimIDsArray + ' have already been given credit. ') +
      (CatchWeightEntClmClaimIDsArray.length == 0 ? '' : 'Claim IDs: ' + CatchWeightEntClmClaimIDsArray + ' have been submitted to accounting for payment. ') +
      (claimIDArray.length == 0 ? '' : 'Claim IDs: ' + claimIDArray + ' are pending approval in eClaims.');

    await ProfrontTxtlog.pushToTxTLog("createCatchWeightClaimAPI.V5", userName, originalDataForLogging, returnObj.error);
    response.status(400).send(returnObj.error);
    return;
  }

  // get Pack Size and run formatPackSize to just get pack amount.
  let packSize = CatchWeightFicheD01.Array[0]["fdpksz"];
  packSize = formatPackSize(packSize);

  let CatchWeightInPounds = CatchWeightFicheD01.Array[0]["fdcwlb"];
  let fdsrp = CatchWeightFicheD01.Array[0]["fdsrp"]
  let calculatedExtendedRetail = ((fdsrp * 100) * CatchWeightInPounds) / 100;
    calculatedExtendedRetail = +calculatedExtendedRetail.toFixed(2);
  console.log("calculatedExtendedRetail: " + calculatedExtendedRetail);

  // POPULATE THE returnObj.Array WITH ALL RELEVENT DATA SET TO NULL SO THAT WE SEND EVERYHTING TO THE FRONT END REGARDLESS OF FILE QUERIES LATER.
  returnObj.array.push({
    Selector: "",
    Buyer: "",
    Status: "",
    AppMessage: "",
    StoreNumber: 0,
    InvoiceNumber: 0,
    OrderDate: 0,
    DeliveryDate: 0,
    ItemNumber: 0,
    CheckDigit: 0,
    PackSize: 0,
    Description: "",
    QuantityShipped: 0,
    Department: "",
    Slot: "",
    StoreCost: 0,
    ExtendedCost: 0,
    Buyer: "",
    ExtendedRetail: 0,
    CatchWeightInPounds: 0,
    User: "",
    CallingName: "",
    CaseOrOnly: "",
    isMarkout : false
  });

  //PUSH ALL COLLECTED DATA THUS FAR TO ARRAY BEFORE RUNNING QUERIES
  returnObj.array[0]["Status"] = "READY-TO-WRITE-NEW-CLAIM"; //DO NOT CHANGE - MAGIC STRING --------------------------------------------------------------------------------------------
  returnObj.array[0]["AppMessage"] = "INVOICE ORDER FOUND WITH NO CLAIM FILED YET.";
  returnObj.array[0]["StoreNumber"] = CatchWeightFicheD01.Array[0]["fdstor"];
  returnObj.array[0]["InvoiceNumber"] = CatchWeightFicheD01.Array[0]["fdinv"];
  returnObj.array[0]["OrderDate"] = CatchWeightFicheD01.Array[0]["fddate"];
  returnObj.array[0]["DeliveryDate"] = deliveryDate;
  returnObj.array[0]["ItemNumber"] = itemNumber;
  returnObj.array[0]["CheckDigit"] = CheckDigit;
  returnObj.array[0]["Description"] = CatchWeightFicheD01.Array[0]["fddesc"];
  returnObj.array[0]["Buyer"] = buyerName.name;
  returnObj.array[0]["Selector"] = selector.Message;
  returnObj.array[0]["QuantityShipped"] = quantityShipped;
  returnObj.array[0]["PackSize"] = packSize;
  returnObj.array[0]["Department"] = CatchWeightFicheD01.Array[0]["fddept"].trim();
  returnObj.array[0]["Slot"] = CatchWeightFicheD01.Array[0]["fdslot"];
  returnObj.array[0]["StoreCost"] = CatchWeightFicheD01.Array[0]["fdsell"];
  returnObj.array[0]["StoreSRP"] = fdsrp;
  returnObj.array[0]["ExtendedRetail"] = calculatedExtendedRetail;
  returnObj.array[0]["ExtendedCost"] = extendedCostForQueries;
  returnObj.array[0]["CatchWeightInPounds"] = CatchWeightFicheD01.Array[0]["fdcwlb"];
  returnObj.array[0]["VendorNumber"] = CatchWeightFicheD01.Array[0]["vendno"];
  returnObj.array[0]["VendorName"] = CatchWeightFicheD01.Array[0]["vndn"];


  await ProfrontTxtlog.pushToTxTLog(
    "createCatchWeightClaimAPI.V5",
    userName, 
    originalDataForLogging,        
    "Success Status: " + returnObj.array[0]["Status"]
  );
  response.status(200).send(returnObj);
  return;
}
exports.run = createCWClaim;

  function formatDate(date) {
    let s = date.toString();
    let invoiceYear = s.substr(0, 4);
    let invoiceMonth = s.substr(4, 2);
    let invoiceDay = s.substr(6, 2);
    let invoiceDate = new Date(invoiceYear, invoiceMonth, invoiceDay);
    deliveryDate = addSubtractDate.subtract(invoiceDate, 1, "month"); //Becuase .js months start at 00.
    deliveryDate = addSubtractDate.add(invoiceDate, 1, "day"); //Delivery date is always one day after the invoice date.
    deliveryDate = deliveryDate.toISOString();
    deliveryDate = deliveryDate.replace(/-|T|:/g, "");
    deliveryDate = deliveryDate.substring(0, 8);
    deliveryDate = parseInt(deliveryDate);
    return deliveryDate;
  }
  
  function formatPackSize(packSize) {
    packSize = packSize.toString();
    var packDash = packSize.indexOf("/");
    packSize = packSize.slice(0, packDash);
    packSize = packSize.trim();
    packSize = parseInt(packSize);
    return packSize;
  }
  
  // Check that todays date is not more than 2 days after the invoice becuase they cannot place a claim more than 2 days later.
  function dateOfEntryApproval(deliveryDateIn) {
    console.log(`deliveryDateIn: ${deliveryDateIn}`);
    let baseDate = deliveryDateIn.toString();
    let year = baseDate.substr(0, 4);
    let month = baseDate.substr(4, 2);
    let day = baseDate.substr(6, 2);
    let compareDate = new Date(year, month, day);
    let comparisonDeliveryDate = addSubtractDate.subtract(compareDate,1,"month"); //Becuase .js months start at 00.
    console.log(`comparisonDeliveryDate: ${comparisonDeliveryDate}`);
  
    const comparisonDayOfWeek = comparisonDeliveryDate.getDay();
  
    if (comparisonDayOfWeek == 6) {
      var deadlineDateOut = addSubtractDate.add(comparisonDeliveryDate, 4, "day");    
    } else if (comparisonDayOfWeek == 4 || comparisonDayOfWeek == 5) {
      deadlineDateOut = addSubtractDate.add(comparisonDeliveryDate, 5, "day");
    } else {
      deadlineDateOut = addSubtractDate.add(comparisonDeliveryDate, 3, "day");
    }
    // } else if (comparisonDayOfWeek == 4) {
    //   deadlineDateOut = addSubtractDate.add(comparisonDeliveryDate, 5, "day");
    // } else if (comparisonDayOfWeek == 5) {
    //   deadlineDateOut = addSubtractDate.add(comparisonDeliveryDate, 5, "day");
  
    //0 sun = +3
    //1 mon = +3
    //2 tue = +3
    //3 wed = +3
    //4 Thur = +5
    //5 fri = +5
    //6 sat = +4
  
    console.log(`deadlineDateOut: ${deadlineDateOut}`);

    return deadlineDateOut;
  
    // var today = new Date();
    // console.log(`today: ${today}`);
  
    // if (today > deadlineDate) {
    //   return false;
    // } else {
    //   return true;
    // }
  }
  