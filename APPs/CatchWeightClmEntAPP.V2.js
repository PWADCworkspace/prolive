//var ClmEnt = pjs.require('C:/tyler/modules/protest/APPs/ClmEntAPP.js');

function getCatchWeightClmEntInfo(
  invoiceNumberIn,
  storeNumberIn,
  itemNumberIn,
  extendedCostIn
) {
  const invoice = invoiceNumberIn;
  const store = storeNumberIn;
  const item = itemNumberIn;
  const extendedCost = extendedCostIn;
  let returnObj = {};
  returnObj.Array = [];

  console.log(
    `running Catch Weight CLMENT V2 query for item: ${item} on invoice: ${invoice} for store: ${store} with extended cost: ${extendedCost}.`
  );

  // const sqlquery =
  //   "select * from CLMENT where ORDINV = ? and CLMSTR = ? and CLMITM = ? and CLMEXT = ? and CLMTYP = 7 and CLMCRD = 'C'";
  // returnObj.Array = pjs.query(sqlquery, [invoice, store, item, extendedCost]);
  // // console.log(returnObj.Array);

  const clmentMarkoutQuery =
    "select * from CLMENT where ORDINV = ? and CLMSTR = ? and CLMITM = ? and CLMEXT = ? and CLMTYP = 7 and CLMCRD = 'C'";
  let catchWeightClmentQuery = pjs.query(clmentMarkoutQuery, [
    invoice,
    store,
    item,
    extendedCost
  ]);
  console.log(catchWeightClmentQuery);

  const catchWeightClmhstMarkoutQuery =
    "select * from CLMHST where HORDIN = ? and HCLMST = ? and HCLMIT = ? and HCLMEX = ? and HCLMTY = 7 and HCLMCR = 'C'";
  let catchWeightClmhstQuery = pjs.query(catchWeightClmhstMarkoutQuery, [
    invoice,
    store,
    item,
    extendedCost
  ]);
  console.log(catchWeightClmhstQuery);

  catchWeightClmentQuery.forEach(claim => {
    returnObj.Array.push({
      ClaimID: claim.clmno,
      CaseOrOnly: claim.clmcro,
      QuantityOnClaim: claim.clmqty
    });
  });

  catchWeightClmhstQuery.forEach(claim => {
    returnObj.Array.push({
      ClaimID: claim.hclmno,
      CaseOrOnly: claim.hclmco,
      QuantityOnClaim: claim.hclmqt
    });
  });
  console.log("catchWeightClmEntQuery returnObj");
  console.log(returnObj);
  return returnObj;
}
exports.getCatchWeightClmEntInfo = getCatchWeightClmEntInfo;
