/* 
  Description: App Program to write processes to text log in /Profront/log/ProfrontLog.txt.
  Calling method for other programs is: var ProfrontTxtlog = pjs.require("C:/profront/modules/protest/APPs/PushToTxtLogAPP.js");
  Creation Date: 05-17-2022
  Author: Tyler Hobbs
*/
const fs = require("fs");

function pushToTxTLog(program, userName, data, message) {
  console.log(`PushToTxtLogAPP.js is running for ${program}...`);
  var filePath = ".\\profront-test\\logs\\ProfrontLog.txt";
  
  let d = new Date();
  date = d.toISOString();

    let logMessage = {};
    logMessage.userName = userName;
    logMessage.program = program;
    logMessage.date = date;
    logMessage.rawData = data;
    logMessage.programMessage = message;

    logMessage = JSON.stringify(logMessage)

  // Write text to ProfrontLog.txt
  fs.appendFile(filePath, `${logMessage} \n`, { flag: "a+" },
    (err) => {
      if (err) {
        console.error(`Error when trying to write text log message: ${err}`);
        return;
      }else {
        console.log(`New record written to TxtLog on ${date}.`);
      }
    });
}
exports.pushToTxTLog = pushToTxTLog;
      