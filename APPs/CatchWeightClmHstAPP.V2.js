//var ClmHst = pjs.require('C:/tyler/modules/protest/APPs/ClmHstAPP.js');

// VERSION V2-------------------------------------------------------------------------------------------------------------------------------------------------------
// Including extended cost intake from the writeAPI and querry based on strick extended cost entry.
// This is to prevent duplicate claims from being created for that specific weight/extended cost.

// VERSION V3-------------------------------------------------------------------------------------------------------------------------------------------------------
// This should include a way to return to the getClaim API and the front end, the values of the markout claim that was found.
/*
    select 
      hclmno as ClaimNumber, 
      hclmdt as ClaimDate, 
      hclmst as Store, 
      hclmit as Item, 
      hclmty as ClaimType, 
      hclmcr as Credit, 
      hordin as InvoiceNumber, 
      horddt as OrderDate, 
      hclmqt as QuantityOnClaim,
      hclmco as CaseOrOnly,
      hclmcs as PerCaseCost,
      hclmex as ExtendedCost,
      hclstk as RestockCharge,
      hclret as Retail
    from pwafil.clmhst where hclmdt > 20221100
  */

function getCatchWeightClmHstInfo(
  invoiceNumber,
  storeNumber,
  itemNumber,
  incommingExtendedCost
) {
  const invoice = invoiceNumber;
  const store = storeNumber;
  const item = itemNumber;
  const extendedCost = incommingExtendedCost;
  let sqlquery;
  let returnObj = {};
  returnObj.Array = [];
  // returnObj.Message = "";

  console.log(`running Catch Weight CLMHST V2 query for item: ${item}`);

  console.log(
    `Running extended cost HISTORY query for item ${item} on invoice ${invoice} at store ${store} with an extended cost of ${extendedCost}.`
  );

  //create comparison date that is 1 week ago from today as YYYMMDD format
  let comparisonDate = new Date();
  comparisonDate.setDate(comparisonDate.getDate() - 180);
  comparisonDate = comparisonDate.toISOString().slice(0, 10).replace(/-/g, "");
  // console.log(comparisonDate);

  sqlquery =
    "select * from CLMHST where HORDIN = ? and HCLMST = ? and HCLMIT = ? and HCLMEX = ? and HCLMTY != 7 and HCLMCR = 'C'";
  returnObj.Array = pjs.query(sqlquery, [
    invoice,
    store,
    item,
    extendedCost,
  ]);
  // console.log(returnObj.Array);

  return returnObj;
}
exports.getCatchWeightClmHstInfo = getCatchWeightClmHstInfo;
