function validateCheckDigit(itemCodeIn) {
    
    fullItemCode = padLeadingZeros(itemCodeIn, 6);
    console.log(`Running validateCheckDigit on ${fullItemCode}`);

    let firstDigit = fullItemCode.toString().slice(0, 1); //0
    let secondDigit = fullItemCode.toString().slice(1, 2); //0
    let thirdDigit = fullItemCode.toString().slice(2, 3);   //3
    let fourthDigit = fullItemCode.toString().slice(3, 4); //3
    let fifthDigit = fullItemCode.toString().slice(4, 5); //0 
    let checkDigit = fullItemCode.toString().slice(5, 6); //1

    secondDigit = parseInt(secondDigit); //0
    fourthDigit = parseInt(fourthDigit); //3

    let firstDigitx2 = padLeadingZeros(firstDigit * 2, 2); //00
    let thirdDigitx2 = padLeadingZeros(thirdDigit * 2, 2); //06
    let fifthDigitx2 = padLeadingZeros(fifthDigit * 2, 2); //00

    if (firstDigitx2 >= 10){
        let firstDigitx2Left = firstDigitx2.toString().slice(0, 1);
        let firstDigitx2Right = firstDigitx2.toString().slice(1, 2);
        firstDigitx2 = parseInt(firstDigitx2Left) + parseInt(firstDigitx2Right); 
    }

    if (thirdDigitx2 >= 10){
        let thirdDigitx2Left = thirdDigitx2.toString().slice(0, 1);
        let thirdDigitx2Right = thirdDigitx2.toString().slice(1, 2);
        thirdDigitx2 = parseInt(thirdDigitx2Left) + parseInt(thirdDigitx2Right);
    }

    if (fifthDigitx2 >= 10){
        let fifthDigitx2Left = fifthDigitx2.toString().slice(0, 1);
        let fifthDigitx2Right = fifthDigitx2.toString().slice(1, 2);
        fifthDigitx2 = parseInt(fifthDigitx2Left) + parseInt(fifthDigitx2Right);
    }

    firstDigitx2 = parseInt(firstDigitx2);
    thirdDigitx2 = parseInt(thirdDigitx2);
    fifthDigitx2 = parseInt(fifthDigitx2);

    let numberToSubtractFrom10 = firstDigitx2 + secondDigit + thirdDigitx2 + fourthDigit + fifthDigitx2; 
    if (numberToSubtractFrom10 >= 10) numberToSubtractFrom10 = numberToSubtractFrom10.toString().slice(-1);

    let checkDigitToCompare = 10 - numberToSubtractFrom10;
    if (checkDigitToCompare == 10) checkDigitToCompare = 0;
    if (checkDigitToCompare == checkDigit) return true;
    else return false;

}
exports.validateCheckDigit = validateCheckDigit;

function padLeadingZeros(number, length) {
    let my_string = "" + number;
    while (my_string.length < length) {
      my_string = "0" + my_string;
    }
    return my_string;
  }
