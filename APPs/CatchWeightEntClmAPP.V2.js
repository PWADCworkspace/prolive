function getCatchWeightEntClmInfo(
  invoiceNumberIn,
  storeNumberIn,
  itemNumberIn,
  extendedCostIn
) {
  const invoice = invoiceNumberIn;
  const store = storeNumberIn;
  const item = itemNumberIn;
  const extendedCost = extendedCostIn;
  let returnObj = {};
  returnObj.Array = [];

  //Setting Prod or Dev tables based on port---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  const currentPort = profound.settings.port;
  console.log(currentPort);

  let ENTCLMTable = "";

  if (currentPort === 80) {
    ENTCLMTable = "ENTCLM_CALLIN";
    console.log(`Running CatchWeightEntClmAPP.V2 in PROD with table: ${ENTCLMTable} for item: ${item} on invoice: ${invoice} for store: ${store} with extended cost: ${extendedCost}.`);
  } else {
    ENTCLMTable = "ENTCLMDEV";
    console.log(`Running CatchWeightEntClmAPP.V2 in DEV with table: ${ENTCLMTable} for item: ${item} on invoice: ${invoice} for store: ${store} with extended cost: ${extendedCost}.`);
  }

  sqlquery =
    `select * from ${ENTCLMTable} where SORDIN = ? and SCLMST = ? and SCLMIT = ? and SCLMEX = ? and SCLMCR = 'C'`;
  returnObj.Array = pjs.query(sqlquery, [invoice, store, item, extendedCost]);
  console.log(returnObj.Array);

  // if (returnObj.Array.length < 1) {
  //   returnObj.Message = `No Claim for Catch Weight Item ${item} on invoice ${invoice} for store ${store} has been filed in ENTCLM.`;
  //   return returnObj;
  // } else {
  //   returnObj.Message = "CLAIM-ALREADY-EXISTS";
  //   return returnObj;
  // }

  console.log("entclm returnObj");
  console.log(returnObj);
  return returnObj;
}
exports.getCatchWeightEntClmInfo = getCatchWeightEntClmInfo;
