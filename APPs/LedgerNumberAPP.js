//var ledgerNumber = pjs.require("C:/profront/modules/protest/APPs/LedgerNumberAPP.js");

function getLedgerNumber(Department) {
  var Value = Department.trim();

  var returnObj = {};
  returnObj.LedgerNumber = '';
  returnObj.Message = '';

  if (!Value) {
    returnObj.LedgerNumber = '0210';
    returnObj.Message = 'No Department Code recieved.';
    return returnObj;
  } else if (Value) {
    switch (Value) {
      case 'A':
        returnObj.LedgerNumber = '0210';
        break;
      case 'B':
        returnObj.LedgerNumber = '0210';
        break;
      case 'C':
        returnObj.LedgerNumber = '0212';
        break;
      case 'D':
        returnObj.LedgerNumber = '0211';
        break;
      case 'E':
        returnObj.LedgerNumber = '0210';
        break;
      case 'F':
        returnObj.LedgerNumber = '0214';
        break;
      case 'G':
        returnObj.LedgerNumber = '0218';
        break;
      case 'H':
        returnObj.LedgerNumber = '0216';
        break;
      case 'I':
        returnObj.LedgerNumber = '0216';
        break;
      case 'J':
        returnObj.LedgerNumber = '0213';
        break;
      case 'K':
        returnObj.LedgerNumber = '0210';
        break;
      case 'M':
        returnObj.LedgerNumber = '0220';
        break;
      case 'N':
        returnObj.LedgerNumber = '0220';
        break;
      case 'P':
        returnObj.LedgerNumber = '0221';
        break;
      case 'S':
        returnObj.LedgerNumber = '0215';
        break;
      case 'X':
        returnObj.LedgerNumber = '0215';
        break;
      case 'Y':
        returnObj.LedgerNumber = '0220';
        break;
      case 'Z':
        returnObj.LedgerNumber = '0215';
        break;
      default:
        returnObj.LedgerNumber = '0210';
    }
    return returnObj;
  } else {
    returnObj.LedgerNumber = '0210';
    returnObj.Message = 'Error fetching Ledger Number.';
    return returnObj;
  }
}
exports.getLedgerNumber = getLedgerNumber;
