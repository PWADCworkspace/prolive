
const dayjs = require('dayjs');

function PickSelectorApp_V3(storeNum, invoiceNum, itemNumCheckDigit2, itemNum, isCW, cwAmnt) {

    const programName = 'PickSelectorApp_V3';
    const currentPort = profound.settings.port;

    console.log(`${programName} is running on ${currentPort}....${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);

    //Set incomming paraemeters
    const storeNumber = storeNum
    const itemNumber = itemNum;
    const itemNumberCheck = itemNumCheckDigit2;
    const isCWItem = isCW;
    const cwAmount = cwAmnt;
    let invoiceNumber = invoiceNum;
    
    //Initialize pgm response object
    var returnObj = {};
    returnObj.Message = '';
    returnObj.ErrMessage = '';

    let recordSet = [];
    let assignmentNumber = [];
      
    //If the invoice and item numbers are missing throw an error
    if (!invoiceNumber || !itemNumber) {
        returnObj.ErrMessage = 'Invoice Number and Item Number are required.';
        console.log(returnObj.ErrMessage);
        return returnObj;
    } 

    if(isCWItem) {
        console.log(`Retrieving pick code from fiched01 file to retrieve pick code for CW ${itemNumber}`);

        console.log(`SELECT fdcwpk FROM PWAFIL.FICHED01 WHERE fdstor = ${storeNumber} AND fdinv = ${invoiceNumber} AND fditem = ${itemNumber} AND fdcwlb = ${cwAmount}`);

        let cwPickCode = pjs.query('SELECT fdcwpk FROM PWAFIL.FICHED01 WHERE fdstor = ? AND fdinv = ? AND fditem = ? AND fdcwlb = ? LIMIT 1',[storeNumber,invoiceNumber,itemNumber,cwAmount]);
        
        if(cwPickCode.length == 0){
            returnObj.ErrMessage = 'No assignment was found for catchweight item.';
            console.log(returnObj.ErrMessage);
            return returnObj;       
        }

        if(cwPickCode.length > 1){
            returnObj.ErrMessage = 'There were multiple assignments found for this catchweight item.';
            console.log(returnObj.ErrMessage);
            return returnObj;             
        }

        console.log(cwPickCode);

        invoiceNumber = cwPickCode[0].fdcwpk;
    }

    try {
        assignmentNumber = pjs.query('SELECT CRASM# from PWAFIL.ASMCROSS where CRINV# = ? and CRITEM = ?', [invoiceNumber, itemNumber]);
    } catch (err) {
        returnObj.ErrMessage = err.sqlstate + ' ' + err.sqlMessage;
        console.log(returnObj.ErrMessage);
        return returnObj;
    }
     
    //If the assignment number was not found throw an error
    if (assignmentNumber.length == 0) {
        returnObj.ErrMessage = 'No Assignment Number Found.';  
        console.log(returnObj.ErrMessage);
        return returnObj;
    } 
        
    //add '0' to the front if assignment number to 8 charecters
    let formattedAssignmentNumber = assignmentNumber[0]['crasm#'].toString();
    formattedAssignmentNumber = formattedAssignmentNumber.padStart(8,0);
  
    //add '0' to the front if itmnum to 6 charecters
    let formattedItemNumberCheck = itemNumberCheck.toString().padStart(6,0)

    let getPickerIDQuery = 'SELECT trim(xpkpst) as Status, trim(xpkopr) as Operator, trim(xpkdat) as Datetime FROM PWAFIL.LUCYPCBK where xpkano = ? and xpkitm = ?';
    
    if(isCWItem) {
        getPickerIDQuery+= ` AND xpkvwg = ${cwAmount}`;
    }

    try{
        recordSet = pjs.query(getPickerIDQuery,[formattedAssignmentNumber, formattedItemNumberCheck]);
    } catch (err) {
        returnObj.ErrMessage = `Catch error occurred while grabbing picker ID from LUCYPCBK table.`;
        console.log(returnObj.ErrMessage);
        return returnObj;
    }


    if(recordSet.length == 0){
        returnObj.ErrMessage = `Item ${formattedItemNumberCheck} was not found for invoice number/pick no ${invoiceNumber}`;
        console.log(returnObj.ErrMessage);
        return returnObj;
    }

    for (let i = 0; i < recordSet.length; i++) {
        let Status = recordSet[i]['status'].toString();
        let Operator = recordSet[i]['operator'].toString();
        let Datetime = recordSet[i]['datetime'].toString();
        Datetime = Datetime.slice(0, 8);
        let operatorName = queryLucas(Operator);

        if(operatorName == null) {
            returnObj.Message += `${itemNumber} ${Status} by OperatorID:  ${operatorName} at ${Datetime};`;
        } else {
            returnObj.Message += `${itemNumber} ${Status} by OperatorID: ${operatorName} at ${Datetime};`;
        }

    }

    returnObj.Message = returnObj.Message.slice(-255);
    console.log(returnObj);
    console.log(`${programName} is done running on ${currentPort}....${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);

    return returnObj;
  }
  
  exports.PickSelectorApp_V3 = PickSelectorApp_V3;
// exports.run = PickSelectorApp_V3;

  
function queryLucas(Operator) {
    
    console.log(`Querying lucas to retrieve operator's name for ID ${Operator}`);

    var logging = Operator;
    let fullName = "";
  
    let sqlquery3 = 'select [fullname] from [JenX].[Users] where Login = @logg';
    let params = { logg: logging };
  
    let getFullName = pjs.query(pjs.getDB('Lucas'), sqlquery3, params); 
    if(getFullName.length > 0) fullName = getFullName[0]['fullname'];
  
    return fullName;
}
  