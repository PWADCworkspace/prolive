// // var FicheD01 = pjs.require("C:/profront/modules/protest/APPs/FicheD01APP.js");

// // VERSION V2-------------------------------------------------------------------------------------------------------------------------------------------------------
//   // Including Catch Weight intake and querry based on strick catch weight entry.

// function getFicheD01Info(invoiceNumber, storeNumber, itemNumber, incommingCatchWeight) {
//   const i = invoiceNumber;
//   const s = storeNumber;
//   const t = itemNumber;
//   let sqlquery;
//   let returnObj = {};
//   returnObj.array = [];
//   returnObj.Message = '';

//   if (!i || !s || !t) {
//     returnObj.Message ='Invoice Number, Store Number and Item Number are required.';
//     return returnObj; 
//   }
  
//   if(s.toString().length > 3){ 
//     returnObj.Message ='Store Number is to long.';
//     return returnObj; 
//   } 
  
//   console.log(`Running non-catch weight query for item ${t} on invoice ${i} at store ${s}`);

//   //OLD METHOD - TH - 4/12/2023
//   // sqlquery = 'select * from PWAFIL.FICHED01 where FDINV = ? and FDSTOR = ? and FDITEM = ?';
  
//   //select * from pwafil,fiched01, order by invoice date. Then select only the records that match the first invoice date.
//   sqlquery = 'select * from PWAFIL.FICHED01 where FDINV = ? and FDSTOR = ? and FDITEM = ? and FDDATE = (select max(FDDATE) from PWAFIL.FICHED01 where FDINV = ? and FDSTOR = ? and FDITEM = ?)';
//   //FDITEM, FDINV, FDCHKD, FDDATE, FDPKSZ, FDDESC, FDQTYS, FDDEPT, FDSLOT, FDSRP, FDSELL, FDECST, FDCWLB

//   returnObj.array = pjs.query(sqlquery, [i, s, t, i, s, t]);
//   // console.log(returnObj.array);

//   if (returnObj.array.length < 1) {
//     returnObj.Message = `No item found for invoice ${i} at store ${s} with item number ${t}`;
//     return returnObj;
//   } 

//   //check if FDCWFG is "C", if yes then this is a catch weight item and the returnObj.Message needs to say so.
//   if(returnObj.array[0].fdcwfg == "C" || returnObj.array[0].fdcwfg == "c"){
//     returnObj.Message = "This is a catch weight item and needs to be processed as such. Please make sure there is a CatchWeight value greater than 0.";
//   }

//   return returnObj;
  
  
// }
// exports.getFicheD01Info = getFicheD01Info;
