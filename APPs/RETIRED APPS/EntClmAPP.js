// //var EntClm = pjs.require('C:/tyler/modules/protest/APPs/EntClmAPP.js');

// function getEntClmInfo(invoiceNumber, storeNumber, itemNumber) {
//   const inv = invoiceNumber;
//   const s = storeNumber;
//   const t = itemNumber;
//   let sqlquery;
//   let returnObj = {};
//   returnObj.ObjectArray = [];
//   returnObj.ClaimedArray = [];
//   returnObj.Message = "";

//   if (!inv || !s || !t) {
//     returnObj.Message =
//       "Invoice Number, Store Number and Item Number are required.";
//     return returnObj;
//   } else {
//     sqlquery =
//       "select * from PROFRONT.ENTCLM_CALLIN where SORDIN = ? and SCLMST = ? and SCLMIT = ?";
//     returnObj.ObjectArray = pjs.query(sqlquery, [inv, s, t]);

//     //get total quantity claimed for sclmno = "C" from the array
//     let totalCasesClaimed = 0;
//     for (var i = 0; i < returnObj.ObjectArray.length; i++) {
//       if (
//         returnObj.ObjectArray[i].sclmco == "C" ||
//         returnObj.ObjectArray[i].sclmco == "c"
//       ) {
//         totalCasesClaimed += returnObj.ObjectArray[i].sclmqt;
//       }
//     }

//     //get total quantity claimed for sclmno = "O" from the array
//     let totalUnitsClaimed = 0;
//     for (var i = 0; i < returnObj.ObjectArray.length; i++) {
//       if (
//         returnObj.ObjectArray[i].sclmco == "O" ||
//         returnObj.ObjectArray[i].sclmco == "o"
//       ) {
//         totalUnitsClaimed += returnObj.ObjectArray[i].sclmqt;
//       }
//     }

//     returnObj.ClaimedArray.push({
//       totalCasesClaimed: totalCasesClaimed,
//       totalUnitsClaimed: totalUnitsClaimed
//     });

//     // if (returnObj.array.length < 1) {
//     //   returnObj.Message =
//     //     'Item ' +
//     //     t +
//     //     ' was not a part of Invoice Number ' +
//     //     i +
//     //     ' for Store Number ' +
//     //     s +
//     //     '.';
//     //   return returnObj;
//     // } else {
//     //   returnObj.Message = 'CLAIM-ALREADY-EXISTS';
//     return returnObj;
//     // }
//   }
// }
// exports.getEntClmInfo = getEntClmInfo;
