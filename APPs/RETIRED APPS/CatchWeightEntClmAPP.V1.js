// //var EntClm = pjs.require('C:/tyler/modules/protest/APPs/EntClmAPP.js');

// // Adjust query to pull based on catch weight or extended cost depending.

// function getCatchWeightEntClmInfo(invoiceNumberIn, storeNumberIn, itemNumberIn, extendedCostIn) {
//   const i = invoiceNumberIn;
//   const s = storeNumberIn;
//   const t = itemNumberIn;
//   const e = extendedCostIn;
//   let returnObj = {};
//   returnObj.array = [];
//   returnObj.Message = '';

//   console.log(`running ENTCLM query for item: ${t} on invoice: ${i} for store: ${s} with extended cost: ${e}.`);

//   if (!i || !s || !t || !e) {
//     returnObj.Message =
//       'Invoice Number, Store Number, Item Number and Extended Cost are required.';
//     return returnObj;
//   } else {
//     sqlquery =
//       'select * from PROFRONT.ENTCLM_CALLIN where SORDIN = ? and SCLMST = ? and SCLMIT = ? and SCLMEX = ?';
//     returnObj.array = pjs.query(sqlquery, [i, s, t, e]);
//     // console.log(returnObj.array);

//     if (returnObj.array.length < 1) {
//       returnObj.Message = `No Claim for Catch Weight Item ${t} on invoice ${i} for store ${s} has been filed in ENTCLM.`;
//       return returnObj;
//     } else {
//       returnObj.Message = 'CLAIM-ALREADY-EXISTS';
//       return returnObj;
//     }
//   }
// }
// exports.getCatchWeightEntClmInfo = getCatchWeightEntClmInfo;
