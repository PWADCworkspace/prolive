//var ClmHst = pjs.require('C:/tyler/modules/protest/APPs/ClmHstAPP.js');

function getClmHstInfo(invoiceNumber, storeNumber, itemNumber) {
  var i = invoiceNumber;
  var s = storeNumber;
  var t = itemNumber;
  var sqlquery;
  var returnObj = {};
  returnObj.array = [];
  returnObj.Message = '';

  if (!i || !s || !t) {
    returnObj.Message =
      'Invoice Number, Store Number and Item Number are required.';
    return returnObj;
  } else {
    sqlquery =
      'select * from PWAFIL.CLMHST where HORDIN = ? and HCLMST = ? and HCLMIT = ? and HCLMTY < 8';
      returnObj.array = pjs.query(sqlquery, [i, s, t]);
    // console.log(returnObj.array);
    if (returnObj.array.length < 1) {
      returnObj.Message =
        'Item ' +
        t +
        ' was not a part of Invoice Number ' +
        i +
        ' for Store Number ' +
        s +
        '.';
      return returnObj;
    } else if (returnObj.array[0]['hclmty'] == 7) {
      returnObj.Message = 'MARKOUT-CLAIM-ALREADY-EXISTS';
      return returnObj;
    } else {
      returnObj.Message = 'CLAIM-ALREADY-EXISTS';
      return returnObj;
    }
  }
}
exports.getClmHstInfo = getClmHstInfo;
