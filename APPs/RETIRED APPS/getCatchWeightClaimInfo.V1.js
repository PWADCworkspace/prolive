// /* Incoming INFO:
// {
//     "invoiceNumber" : 00000,
//     "storeNumber" : 000,
//     "itemNumber" : 000000,
//     "UserKeyingClaim": "Tyler",
//     "CallingName": "Hobbs"
//     "CatchWeight": 00.00
// }
// */

// //VERSION V4.0-------------------------------------------------------------------------------------------------------------------------------------------------------
// //Include check against eclaims to see if there is currently a claim on the item that is pending approval.
// //option1: create array fo item, store, inv, weight and cost and flag field. When each app is called, update that array accordingly. What ever is left can have a claim filied agianst it
// //option2: call the apps first. Gather results of where all the items/claims are at currently on the 400, then compare that against the invoice to see what is left to file a claim on.

// const addSubtractDate = require("add-subtract-date");
// async function getCatchWeightClaimInfo(
//   storeNumberIncomming,
//   invoiceNumberIncomming,
//   itemNumberCheckDigitIncomming,
//   userNameIncomming,
//   incommingCatchWeightIncomming,
//   quantityOnClaimIncomming
// ) {
//  console.log("getCatchWeightClaimInfo API V1 Running.");

//   const directPathToProlive = "../../prolive/APPs/";
//   const directPathToProtest = "../../protest/APPs/";
//   const directPathToProlog = "../../Prologging/";
  
//   let buyerName = pjs.require(directPathToProlive + "BuyerNameAPP.js");
//   let FicheD01 = pjs.require(directPathToProlive + "CatchWeightFicheD01APP.V1.js");
//   let EntClm = pjs.require(directPathToProlive + "CatchWeightEntClmAPP.V1.js");
//   let ClmHst = pjs.require(directPathToProlive + "CatchWeightClmHstAPP.V1.js");
//   let ClmEnt = pjs.require(directPathToProlive + "CatchWeightClmEntAPP.V1.js");
//   let selector = pjs.require(directPathToProlive + "PickSelectorAPPV2.js");
//   // let log = pjs.require(directPathToProlog + "PushToLogAPP.js");
//   let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

//   //Predefine variables.
//   const invoiceNumber = invoiceNumberIncomming;
//   const storeNumber = storeNumberIncomming;
//   const itemNumberCheckDigit = itemNumberCheckDigitIncomming;
//   const userName = userNameIncomming;
//   const incommingCatchWeight = incommingCatchWeightIncomming;
//   let extendedCostForQueries;
//   const quantityOnClaim = quantityOnClaimIncomming; //this would always equal 1 for right now unless we start to allow them to file a claim on multiple catch weights at the same time.
//   var returnObj = {};
//   returnObj.array = [];
//   returnObj.error = '';

//   const originalDataForLogging = 
//     'InvoiceNumber: '+ invoiceNumber +
//     ', StoreNumber: '+ storeNumber +
//     ', ItemNumber: '+ itemNumberCheckDigit +
//     ', IncomingCatchWeight: '+ incommingCatchWeight +
//     ', QuantityOnClaim: '+ quantityOnClaim;

//   // POPULATE THE returnObj.array WITH ALL RELEVENT DATA SET TO NULL SO THAT WE SEND EVERYHTING TO THE FRONT END REGARDLESS OF FILE QUERIES LATER.
//   returnObj.array.push({
//     Selector: "",
//     Buyer: "",
//     Status: "",
//     AppMessage: "",
//     StoreNumber: 0,
//     InvoiceNumber: 0,
//     OrderDate: 0,
//     DeliveryDate: 0,
//     ItemNumber: 0,
//     CheckDigit: 0,
//     PackSize: 0,
//     Description: "",
//     QuantityShipped: 0,
//     Department: "",
//     Slot: "",
//     StoreCost: 0,
//     ExtendedCost: 0,
//     Buyer: "",
//     ExtendedRetail: 0,
//     CatchWeightInPounds: 0,
//     User: "",
//     CallingName: "",
//     CaseOrOnly: "",
//   });

//   let itemNumberBase = padLeadingZeros(itemNumberCheckDigit, 6);
//   let CheckDigit = itemNumberBase.slice(5, 6);
//     CheckDigit = parseInt(CheckDigit);
//   let itemNumber = itemNumberBase.slice(0, 5);
//     itemNumber = parseInt(itemNumber);

//   // call getBuyerNameApp to get Buyer first and last name and apply it to buyerNum to add into returned array.
//   buyerName = await buyerName.getBuyerName(itemNumber);
//   if (buyerName.errorMsg) {
//     returnObj.error = buyerName.errorMsg;
//     await ProfrontTxtlog.pushToTxTLog(
//       "getCatchWeightClmInfoAPI.V1",
//       userName, 
//       originalDataForLogging,        
//       buyerName.errorMsg
//     );
//     // await log.pushToLog(storeNumber, userName, buyerName.errorMsg);
//     // response.status(400).send(buyerName.errorMsg);
//     return returnObj;
//   }

//   // get Pick Selector Info.
//   selector = await selector.getPickSelectorInfo(invoiceNumber,itemNumberCheckDigit,itemNumber);
//   if (selector.ErrMessage) selector.Message = "Selector not available.";

//   // Query NEW Catch Weight FicheD01 to get Item info off of Invoice.
//   FicheD01 = await FicheD01.getCatchWeightFicheD01Info(invoiceNumber,storeNumber,itemNumber,incommingCatchWeight);
//   console.log("records gathered from FICHED01.");
//   var numberOfItemsOnInvoice = Object.keys(FicheD01.array).length;
//   console.log(`member key size: ${numberOfItemsOnInvoice}`);

//   if (FicheD01.Message) {
//     returnObj.error = FicheD01.Message;
//     await ProfrontTxtlog.pushToTxTLog(
//       "getCatchWeightClmInfoAPI.V1",
//       userName, 
//       originalDataForLogging,        
//       FicheD01.Message
//     );
//     // await log.pushToLog(storeNumber, userName, FicheD01.Message);
//     console.log("Error on FicheD01 querry:" + FicheD01.Message);
//     return returnObj;
//   }

//   //get date and run formatDate function to +1 the day.
//   let invoiceDate = FicheD01.array[0]["fddate"];
//   let deliveryDate = formatDate(invoiceDate);
  
//   //  CHECK HERE TO COMPARE DELIVERYDATE TO TODAYS DATE. IF IT IS MORE THAN 2 DAYS APART THEY CANNOT FILE A CLAIM
//   var checkDateOfEntry = dateOfEntryApproval(deliveryDate);
//   if (checkDateOfEntry == false && storeNumber != 700){ //700 is the test store.
//     errorMsg = 'The deadline for filing the claim ended at 12 PM, two business days after delivery.';
//     returnObj.error = errorMsg;
//     await ProfrontTxtlog.pushToTxTLog(
//       "getCatchWeightClmInfoAPI.V1",
//       userName, 
//       originalDataForLogging,        
//       errorMsg
//     );
//     return returnObj;
//   }

//   const quantityShipped = FicheD01.array.reduce((accumulator, item) => {
//     return accumulator + item.fdqtys;
//     }, 0);

//   console.log("Total quantityShipped: " + quantityShipped);

//   if (quantityShipped <= 0) {
//     errorMsg = 'The quantity shipped was zero meaning the item was an Out and a claim cannot be filed on such. Any questions please call 205-209-6309.';
//     returnObj.error = errorMsg;
//     await ProfrontTxtlog.pushToTxTLog(
//       "getCatchWeightClmInfoAPI.V1",
//       userName, 
//       originalDataForLogging,        
//       errorMsg
//     );
//     // response.status(400).send(errorMsg);
//     return returnObj;
//   } 

//   extendedCostForQueries = FicheD01.array[0]["fdecst"];
//   console.log(`CatchWeight Extended Cost: ${extendedCostForQueries}`);

//   // Query ENTCLM to see if a claim is currently filed.
//   console.log(`Calling Catch Weight ENTCLM query for item: ${itemNumber}`);
//   let EntClmCall = await EntClm.getCatchWeightEntClmInfo(invoiceNumber,storeNumber,itemNumber,extendedCostForQueries);
//   var numberOfItemsInENTCLM = Object.keys(EntClmCall.array).length;
//   console.log(`ENTCLM member key size: ${numberOfItemsInENTCLM}`);

//   // Query CLMHST to see if a claim has already been filed.
//   console.log(`Calling Catch Weight CLMHST query for item: ${itemNumber}`);
//   let ClmHstCall = await ClmHst.getCatchWeightClmHstInfo(invoiceNumber,storeNumber,itemNumber,extendedCostForQueries);
//   var numberOfItemsInCLMHST = Object.keys(ClmHstCall.array).length;
//   console.log(`CLMHST member key size: ${numberOfItemsInCLMHST}`);

//   // Query CLMENT to see if a Markout claim was filed in house.
//   console.log(`Calling Catch Weight CLMENT query for item: ${itemNumber}`);
//   let ClmEntCall = await ClmEnt.getCatchWeightClmEntInfo(invoiceNumber,storeNumber,itemNumber,extendedCostForQueries);
//   var numberOfItemsInCLMENT = Object.keys(ClmEntCall.array).length;
//   console.log(`CLMENT member key size: ${numberOfItemsInCLMENT}`);

//   let numberOfClaimsFiled = numberOfItemsInENTCLM + numberOfItemsInCLMHST + numberOfItemsInCLMENT;
//   console.log(`Number of claims filed: ${numberOfClaimsFiled}`);

//   let maxNumberOfClaimableItems = numberOfItemsOnInvoice - numberOfClaimsFiled; //remainder that can be claimed.

//   if (quantityOnClaim > maxNumberOfClaimableItems) {

//     returnObj.error = `It looks like a credit has been given for this item, please check your markouts sheets under eDocs. Any questions please call 205-209-6309.`;

//     await ProfrontTxtlog.pushToTxTLog(
//       "getCatchWeightClmInfoAPI.V1",
//       userName, 
//       originalDataForLogging,        
//       returnObj.error
//     );
//     return returnObj;
//   }

//   // get Pack Size and run formatPackSize to just get pack amount.
//   let packSize = FicheD01.array[0]["fdpksz"];
//   packSize = formatPackSize(packSize);

//   let CatchWeightInPounds = FicheD01.array[0]["fdcwlb"];
//   let calculatedExtendedRetail = ((FicheD01.array[0]["fdsrp"] * 100) * CatchWeightInPounds) / 100;
//     calculatedExtendedRetail = +calculatedExtendedRetail.toFixed(2);
//   console.log("calculatedExtendedRetail: " + calculatedExtendedRetail);

//   //PUSH ALL COLLECTED DATA THUS FAR TO ARRAY BEFORE RUNNING QUERIES
//   returnObj.array[0]["Status"] = "READY-TO-WRITE-NEW-CLAIM"; //DO NOT CHANGE - MAGIC STRING --------------------------------------------------------------------------------------------
//   returnObj.array[0]["AppMessage"] = "INVOICE ORDER FOUND WITH NO CLAIM FILED YET.";
//   returnObj.array[0]["StoreNumber"] = FicheD01.array[0]["fdstor"];
//   returnObj.array[0]["InvoiceNumber"] = FicheD01.array[0]["fdinv"];
//   returnObj.array[0]["OrderDate"] = FicheD01.array[0]["fddate"];
//   returnObj.array[0]["DeliveryDate"] = deliveryDate;
//   returnObj.array[0]["ItemNumber"] = itemNumber;
//   returnObj.array[0]["CheckDigit"] = CheckDigit;
//   returnObj.array[0]["Description"] = FicheD01.array[0]["fddesc"];
//   returnObj.array[0]["Buyer"] = buyerName.value;
//   returnObj.array[0]["Selector"] = selector.Message;
//   returnObj.array[0]["QuantityShipped"] = quantityShipped;
//   returnObj.array[0]["PackSize"] = packSize;
//   returnObj.array[0]["Department"] = FicheD01.array[0]["fddept"];
//   returnObj.array[0]["Slot"] = FicheD01.array[0]["fdslot"];
//   returnObj.array[0]["StoreCost"] = FicheD01.array[0]["fdsell"];
//   returnObj.array[0]["ExtendedRetail"] = calculatedExtendedRetail;
//   returnObj.array[0]["ExtendedCost"] = extendedCostForQueries;
//   returnObj.array[0]["CatchWeightInPounds"] = FicheD01.array[0]["fdcwlb"];

//   await ProfrontTxtlog.pushToTxTLog(
//     "getCatchWeightClmInfoAPI.V1",
//     userName, 
//     originalDataForLogging,        
//     "Success Status: " + returnObj.array[0]["Status"]
//   );
//   return returnObj;
// }
// exports.getCatchWeightClaimInfo = getCatchWeightClaimInfo;

// function formatDate(date) {
//   let s = date.toString();
//   let invoiceYear = s.substr(0, 4);
//   let invoiceMonth = s.substr(4, 2);
//   let invoiceDay = s.substr(6, 2);
//   let invoiceDate = new Date(invoiceYear, invoiceMonth, invoiceDay);
//   deliveryDate = addSubtractDate.subtract(invoiceDate, 1, "month"); //Becuase .js months start at 00.
//   deliveryDate = addSubtractDate.add(invoiceDate, 1, "day"); //Delivery date is always one day after the invoice date.
//   deliveryDate = deliveryDate.toISOString();
//   deliveryDate = deliveryDate.replace(/-|T|:/g, "");
//   deliveryDate = deliveryDate.substring(0, 8);
//   deliveryDate = parseInt(deliveryDate);
//   return deliveryDate;
// }

// function formatPackSize(packSize) {
//   packSize = packSize.toString();
//   var packDash = packSize.indexOf("/");
//   packSize = packSize.slice(0, packDash);
//   packSize = packSize.trim();
//   packSize = parseInt(packSize);
//   return packSize;
// }

// function padLeadingZeros(num, size) {
//   var s = num + "";
//   while (s.length < size) s = "0" + s;
//   return s;
// }

// // Check that todays date is not more than 2 days after the invoice becuase they cannot place a claim more than 2 days later.
// function dateOfEntryApproval(delvryDte) {
//   console.log(`delvryDte: ${delvryDte}`);
//   var b = delvryDte.toString();
//   var y = b.substr(0, 4);
//   var m = b.substr(4, 2);
//   var d = b.substr(6, 2);
//   var comparisonDDate = new Date(y, m, d);
//   var comparisonDeliveryDate = addSubtractDate.subtract(
//     comparisonDDate,
//     1,
//     "month"
//   ); //Becuase .js months start at 00.
//   console.log(`comparisonDeliveryDate: ${comparisonDeliveryDate}`);

//   const comparisonDayOfWeek = comparisonDeliveryDate.getDay();

//   if (comparisonDayOfWeek == 6) {
//     var deadlineDate = addSubtractDate.add(comparisonDeliveryDate, 3, "day");
//   } else if (comparisonDayOfWeek == 4) {
//     deadlineDate = addSubtractDate.add(comparisonDeliveryDate, 4, "day");
//   } else if (comparisonDayOfWeek == 5) {
//     deadlineDate = addSubtractDate.add(comparisonDeliveryDate, 4, "day");
//   } else {
//     deadlineDate = addSubtractDate.add(comparisonDeliveryDate, 2, "day");
//   }
//   //0 sun = +2
//   //1 mon = +2
//   //2 tue = +2
//   //3 wed = +2
//   //4 Thur = +4
//   //5 fri = +4
//   //6 sat = +3

//   console.log(`deadlineDate: ${deadlineDate}`);

//   var today = new Date();
//   console.log(`today: ${today}`);

//   if (today > deadlineDate) {
//     return false;
//   } else {
//     return true;
//   }
// }
