// /* Incoming INFO:
// {
//     "invoiceNumber" : 00000,
//     "storeNumber" : 000,
//     "itemNumber" : 000000,
//     "UserKeyingClaim": "Tyler",
//     "CallingName": "Hobbs"
//     "CatchWeight": 00.00,
// }
// */

// // VERSION V3.2-------------------------------------------------------------------------------------------------------------------------------------------------------
//   // Including Catch Weight intake and calculations for extended cost.

//   const addSubtractDate = require('add-subtract-date');
//   function getShortClaimInfo(request, response) {
  
//     const directPathToProlive = 'C:/profront/modules/prolive/APPs/';
//     const directPathToProtest = 'C:/profront/modules/protest/APPs/';
  
//     var FicheD01 = pjs.require(directPathToProtest + 'FicheD01APP.V2.js');
//     var buyerName = pjs.require(directPathToProlive + 'BuyerNameAPP.js');
//     var EntClm = pjs.require(directPathToProlive + 'EntClmAPP.js');
//     var ClmHst = pjs.require(directPathToProtest + 'ClmHstAPP.V2.js');
//     var ClmEnt = pjs.require(directPathToProlive + 'ClmEntAPP.js');
//     var log = pjs.require(directPathToProlive + 'PushToLogAPP.js');
//     var selector = pjs.require(directPathToProlive + 'PickSelectorAPPV2.js');
//     var ProfrontTxtlog = pjs.require(directPathToProlive + 'PushToTxtLogAPP.js');
  
//     var errorMsg;
//     var ItemInfoListArray = [];
//     var invoiceNumber = request.body.invoiceNumber;
//     var storeNumber = request.body.storeNumber;
//     var itemNumberCheckDigit = request.body.itemNumber;
//     var userName = request.body.UserKeyingClaim + ' ' + request.body.CallingName;
//     var incommingCatchWeight = request.body.CatchWeight;
//     console.log(`incommingCatchWeight: ${incommingCatchWeight}`);
  
//     // POPULATE THE ItemInfoListArray WITH ALL RELEVENT DATA SET TO NULL SO THAT WE SEND EVERYHTING TO THE FRONT END REGARDLESS OF FILE QUERIES LATER.
//     ItemInfoListArray.push({
//       Selector: '',
//       Buyer: '',
//       Status: '',
//       AppMessage: '',
//       StoreNumber: 0,
//       InvoiceNumber: 0,
//       OrderDate: 0,
//       DeliveryDate: 0,
//       ItemNumber: 0,
//       CheckDigit: 0,
//       PackSize: 0,
//       Description: '',
//       DocumentNumber: 0,
//       RecordID: 0,
//       ClaimNumber: 0,
//       ClaimDate: 0,
//       ClaimType: 0,
//       QuantityShipped: 0,
//       QuantityOnClaim: 0,
//       CreditType: '',
//       LedgerNumber: 0,
//       Department: '',
//       PickCode: '',
//       Slot: '',
//       Slot1: '',
//       Slot3: '',
//       TobaccoTax: 0,
//       perCaseCost: 0,
//       ExtendedCost: 0,
//       Flag: '',
//       Buyer: '',
//       RestockCharge: 0,
//       ExtendedRetail: 0,
//       CatchWeightInPounds: 0,
//       User: '',
//       CallingName: '',
//       CaseOrOnly: '',
//       Assignment: 0,
//       Container: 0,
//     });
  
//   // ERROR CHECKS FOR MISSING DATA COMMING IN---------------------------------------------------------------------------
//     if (!invoiceNumber) {
//       errorMsg = 'Invoice Number is required';
//       ProfrontTxtlog.pushToTxTLog("getClmInfoAPI.V3",userName, errorMsg);
//       log.pushToLog(storeNumber, userName, errorMsg);
//       response.status(400).send(errorMsg);
//       return;
  
//     } else if (!itemNumberCheckDigit) {
//       errorMsg = 'Item Number is required';
//       ProfrontTxtlog.pushToTxTLog("getClmInfoAPI.V3",userName, errorMsg);
//       log.pushToLog(storeNumber, userName, errorMsg);
//       response.status(400).send(errorMsg);
//       return;
  
//     } else if (!storeNumber) {
//       errorMsg = 'Store Number is required';
//       ProfrontTxtlog.pushToTxTLog("getClmInfoAPI.V3",userName, errorMsg);
//       log.pushToLog(storeNumber, userName, errorMsg);
//       response.status(400).send(errorMsg);
//       return;
  
//     } else if (invoiceNumber.toString().length > 5) {
//       errorMsg = 'Invoice Number: ' + invoiceNumber + ' may be incorrect. It should have a length of 5';
//       ProfrontTxtlog.pushToTxTLog("getClmInfoAPI.V3",userName, errorMsg);
//       log.pushToLog(storeNumber, userName, errorMsg);
//       response.status(400).send(errorMsg);
//       return;
  
//     } else {
//       var itemNumberBase = padLeadingZeros(itemNumberCheckDigit, 6);
  
//       var CheckDigit = itemNumberBase.slice(5, 6);
//       CheckDigit = parseInt(CheckDigit);
  
//       var itemNumber = itemNumberBase.slice(0, 5);
//       itemNumber = parseInt(itemNumber);
  
//       // call getBuyerNameApp to get Buyer first and last name and apply it to buyerNum to add into returned array.
//       buyerName = buyerName.getBuyerName(itemNumber);
//       if (buyerName.errorMsg) {
//         ProfrontTxtlog.pushToTxTLog("getClmInfoAPI.V3",userName, buyerName.errorMsg);
//         log.pushToLog(storeNumber, userName, buyerName.errorMsg);
//         response.status(400).send(buyerName.errorMsg);
//         return;
//       }
  
//       // get Pick Selector Info.
//       selector = selector.getPickSelectorInfo(invoiceNumber,itemNumberCheckDigit,itemNumber);
//       if (selector.ErrMessage) selector.Message = 'Selector not available.';
  
//       // Query FicheD01 to get Item info off of Invoice.
//       FicheD01 = FicheD01.getFicheD01Info(invoiceNumber, storeNumber, itemNumber, incommingCatchWeight);
//       console.log('records gathered from FICHED01.');
  
//       if (FicheD01.Message) {
//         ProfrontTxtlog.pushToTxTLog("getClmInfoAPI.V3",userName, FicheD01.Message);
//         log.pushToLog(storeNumber, userName, FicheD01.Message);
//         response.status(400).send(FicheD01.Message);
//         return;
//       }
  
//       if (FicheD01.array > 0 && FicheD01.array[0]['fdqtys'] <= 0) {
//         errorMsg = 'Quantity was zero meaning Item was an Out and a Claim cannot be filed on such.';
//         ProfrontTxtlog.pushToTxTLog("getClmInfoAPI.V3",userName, errorMsg);
//         log.pushToLog(storeNumber, userName, errorMsg);
//         response.status(400).send(errorMsg);
//       } 
  
//       // get Pack Size and run formatPackSize to just get pack amount.
//       var packSize;
//       packSize = FicheD01.array[0]['fdpksz'];
//       packSize = formatPackSize(packSize);
  
//       let fdsrp = FicheD01.array[0]['fdsrp'];
//       let fdmulti = FicheD01.array[0]['fdmult'];
//       let calculatedExtendedRetail;
  
//       //IF ELSE to seperate processing for Catch Weight Items.
//       if(FicheD01.array[0]['fdcwfg'] == 'C'){ 
//         console.log('Item is a Catch Weight Item.');
//         var CatchWeightInPounds = FicheD01.array[0]['fdcwlb']; 
//           calculatedExtendedRetail = ((fdsrp*100) * CatchWeightInPounds)/100;
//         var extendedCostForQueries = FicheD01.array[0]['fdecst'];
//         console.log(`CatchWeight Extended Cost: ${extendedCostForQueries}`);
//       } else {
//         console.log('Item is NOT a Catch Weight Item.');
//         if(fdmulti != 0) { 
//           calculatedExtendedRetail = (((fdsrp*100)/fdmulti) * packSize)/100;
//         } else {
//           calculatedExtendedRetail = ((fdsrp*100) * packSize)/100;
//         }
//         extendedCostForQueries = 0; //place holder for the file quieres to no look for CW item based on extended cost.
//       }
  
//       calculatedExtendedRetail = +calculatedExtendedRetail.toFixed(2);
//       console.log('calculatedExtendedRetail: ' + calculatedExtendedRetail);
  
//       var quantityShipped;
//       quantityShipped = FicheD01.array[0]['fdqtys'];
  
//       //get date and run formatDate function to +1 the day.
//       var invoiceDate;
//       invoiceDate = FicheD01.array[0]['fddate'];
//       var deliveryDate;
//       deliveryDate = formatDate(invoiceDate);
  
//       //  CHECK HERE TO COMPARE DELIVERYDATE TO TODAYS DATE. IF IT IS MORE THAN 2 DAYS APART THEY CANNOT FILE A CLAIM
//         // var checkDateOfEntry = dateOfEntryApproval(deliveryDate);
//         // if (checkDateOfEntry == false && storeNumber != 700){ //700 is the test store.
//         //   errorMsg = 'It has been more than 2 days since that invoice was delivered and it is to late to submit a claim for that invoice.';
//         //   ProfrontTxtlog.pushToTxTLog("getClmInfoAPI.V3",userName, errorMsg);
//         //   log.pushToLog(storeNumber, userName, errorMsg);
//         //   response.status(400).send(errorMsg);
//         // return;}
  
//       // Query ENTCLM to see if a claim is currently filed.
//         console.log('running ENTCLM query.');
//         EntClm = EntClm.getEntClmInfo(invoiceNumber, storeNumber, itemNumber);
//         //console.log(EntClm.array);
  
//       // Query CLMHST to see if a claim has already been filed.
//         console.log('running CLMHST query.');
//         ClmHst = ClmHst.getClmHstInfo(invoiceNumber, storeNumber, itemNumber, extendedCostForQueries);
//         //console.log(ClmHst.array);
  
//       // Query CLMENT to see if a Markout claim was filed in house.
//         console.log('running CLMENT query.');
//         ClmEnt = ClmEnt.getClmEntInfo(invoiceNumber, storeNumber, itemNumber);
//         //console.log(ClmEnt);
  
//         console.log('ItemInfoListArray pre queries:');
//         // console.log(ItemInfoListArray);
  
//         //PUSH ALL COLLECTED DATA THUS FAR TO ARRAY BEFORE RUNNING QUERIES
//         ItemInfoListArray[0]['ItemNumber'] = itemNumber;
//         ItemInfoListArray[0]['CheckDigit'] = CheckDigit;
//         ItemInfoListArray[0]['Buyer'] = buyerName.value;
//         ItemInfoListArray[0]['Selector'] = selector.Message;
//         ItemInfoListArray[0]['DeliveryDate'] = deliveryDate;
//         ItemInfoListArray[0]['QuantityShipped'] = quantityShipped;
//         ItemInfoListArray[0]['PackSize'] = packSize;
//         ItemInfoListArray[0]['StoreRetailPrice'] = fdsrp;
//         ItemInfoListArray[0]['ExtendedRetail'] = calculatedExtendedRetail;
  
//       // STOPHERE-----------------------------------------------------------------------------------------------------------------------------------------------------------
  
//         if (ClmEnt.array.length > 0) {
//           console.log('Markout claim found in ClmEnt.');
  
//           ItemInfoListArray[0]['Status'] = 'MARKOUT-CLAIM-ALREADY-EXISTS.'; //DO NOT CHANGE - MAGIC STRING --------------------------------------------------------------------------------------------
//           ItemInfoListArray[0]['AppMessage'] = ClmEnt.Message;
//           ItemInfoListArray[0]['RecordID'] = ClmEnt.array[0]['clmrec'];
//           ItemInfoListArray[0]['ClaimNumber'] = ClmEnt.array[0]['clmmno'];
//           ItemInfoListArray[0]['ClaimDate'] = ClmEnt.array[0]['clmdt'];
//           ItemInfoListArray[0]['StoreNumber'] = ClmEnt.array[0]['clmstr'];
//           ItemInfoListArray[0]['ItemNumber'] = ClmEnt.array[0]['clmitm'];
//           ItemInfoListArray[0]['ClaimType'] = ClmEnt.array[0]['clmtyp'];
//           ItemInfoListArray[0]['CreditType'] = ClmEnt.array[0]['clmcrd'];
//           ItemInfoListArray[0]['LedgerNumber'] = ClmEnt.array[0]['clmldg'];
//           ItemInfoListArray[0]['Department'] = ClmEnt.array[0]['clmdpt'];
//           ItemInfoListArray[0]['InvoiceNumber'] = ClmEnt.array[0]['clminv'];
//           ItemInfoListArray[0]['OrderDate'] = ClmEnt.array[0]['clmdte'];
//           ItemInfoListArray[0]['PickCode'] = ClmEnt.array[0]['ordpic'];
//           ItemInfoListArray[0]['Slot1'] = ClmEnt.array[0]['ordsl1'];
//           ItemInfoListArray[0]['Slot3'] = ClmEnt.array[0]['ordsl3'];
//           ItemInfoListArray[0]['QuantityOnClaim'] = ClmEnt.array[0]['clmqty'];
//           ItemInfoListArray[0]['CaseOrOnly'] = ClmEnt.array[0]['clmcro'];
//           ItemInfoListArray[0]['TobaccoTax'] = ClmEnt.array[0]['clmtax'];
//           ItemInfoListArray[0]['perCaseCost'] = ClmEnt.array[0]['clmcst'];
//           ItemInfoListArray[0]['ExtendedCost'] = ClmEnt.array[0]['clmext'];
//           ItemInfoListArray[0]['Flag'] = ClmEnt.array[0]['clmflg'];
//           ItemInfoListArray[0]['Buyer'] = ClmEnt.array[0]['clmbyr'];
//           ItemInfoListArray[0]['RestockCharge'] = ClmEnt.array[0]['clmstk'];
//           ItemInfoListArray[0]['DocumentNumber'] = ClmEnt.array[0]['clmdoc'];
//           ItemInfoListArray[0]['User'] = ClmEnt.array[0]['clmusr'];
//           ItemInfoListArray[0]['Assignment'] = ClmEnt.array[0]['clmasn'];
//           ItemInfoListArray[0]['Container'] = ClmEnt.array[0]['clmcnt'];
  
//           ProfrontTxtlog.pushToTxTLog("getClmInfoAPI.V3",userName, 'Success Status: ' + ItemInfoListArray[0]['Status']);
//           response.json(ItemInfoListArray);
  
//         } else if (ClmHst.array.length > 0) {
//           console.log('Claim found in Claim History.');
  
//           ItemInfoListArray[0]['Status'] = 'CLAIM-ALREADY-EXISTS'; //DO NOT CHANGE - MAGIC STRING --------------------------------------------------------------------------------------------
//           ItemInfoListArray[0]['AppMessage'] = ClmHst.Message;
//           ItemInfoListArray[0]['RecordID'] = ClmHst.array[0]['hclmrc']; // record ID 'S'
//           ItemInfoListArray[0]['ClaimNumber'] = ClmHst.array[0]['hclmno']; //claim number
//           ItemInfoListArray[0]['ClaimDate'] = ClmHst.array[0]['hclmdt']; //claim date
//           ItemInfoListArray[0]['StoreNumber'] = ClmHst.array[0]['hclmst'];
//           ItemInfoListArray[0]['ItemNumber'] = ClmHst.array[0]['hclmit'];
//           ItemInfoListArray[0]['ClaimType'] = ClmHst.array[0]['hclmty']; // claim type '2'
//           ItemInfoListArray[0]['CreditType'] = ClmHst.array[0]['hclmcr'];
//           ItemInfoListArray[0]['LedgerNumber'] = ClmHst.array[0]['hclmld'];
//           ItemInfoListArray[0]['Department'] = ClmHst.array[0]['hclmdp'];
//           ItemInfoListArray[0]['InvoiceNumber'] = ClmHst.array[0]['hordin'];
//           ItemInfoListArray[0]['OrderDate'] = ClmHst.array[0]['horddt'];
//           ItemInfoListArray[0]['PickCode'] = ClmHst.array[0]['hordpc'];
//           ItemInfoListArray[0]['Slot1'] = ClmHst.array[0]['hords1'];
//           ItemInfoListArray[0]['Slot3'] = ClmHst.array[0]['hords3'];
//           ItemInfoListArray[0]['QuantityOnClaim'] = ClmHst.array[0]['hclmqt'];
//           ItemInfoListArray[0]['CaseOrOnly'] = ClmHst.array[0]['hclmco'];
//           ItemInfoListArray[0]['TobaccoTax'] = ClmHst.array[0]['hclmtx'];
//           ItemInfoListArray[0]['perCaseCost'] = ClmHst.array[0]['hclmcs'];
//           ItemInfoListArray[0]['ExtendedCost'] = ClmHst.array[0]['hclmex'];
//           ItemInfoListArray[0]['Flag'] = ClmHst.array[0]['hclmfl'];
//           ItemInfoListArray[0]['Buyer'] = ClmHst.array[0]['hclbyr'];
//           ItemInfoListArray[0]['RestockCharge'] = ClmHst.array[0]['hclstk'];
//           ItemInfoListArray[0]['DocumentNumber'] = ClmHst.array[0]['hcldoc'];
//           ItemInfoListArray[0]['User'] = ClmHst.array[0]['hclusr'];
//           ItemInfoListArray[0]['Assignment'] = ClmHst.array[0]['hclasn'];
//           ItemInfoListArray[0]['Container'] = ClmHst.array[0]['hclcnt'];
//           ItemInfoListArray[0]['CallingName'] = ClmHst.array[0]['hclnam'];
  
//           ProfrontTxtlog.pushToTxTLog("getClmInfoAPI.V3",userName, 'Success Status: ' + ItemInfoListArray[0]['Status']);
//           response.json(ItemInfoListArray);
          
//         } else if (EntClm.array.length > 0) {
//           console.log('Claim found in ENTCLM.');
  
//           ItemInfoListArray[0]['Status'] = 'CLAIM-ALREADY-EXISTS'; //DO NOT CHANGE - MAGIC STRING --------------------------------------------------------------------------------------------
//           ItemInfoListArray[0]['AppMessage'] = EntClm.Message;
//           ItemInfoListArray[0]['RecordID'] = EntClm.array[0]['sclmrc']; // record ID 'S'
//           ItemInfoListArray[0]['ClaimNumber'] = EntClm.array[0]['sclmno']; //claim number
//           ItemInfoListArray[0]['ClaimDate'] = EntClm.array[0]['sclmdt']; //claim date
//           ItemInfoListArray[0]['StoreNumber'] = EntClm.array[0]['sclmst']; //store number 'session.StoreNum#'
//           ItemInfoListArray[0]['ItemNumber'] = EntClm.array[0]['sclmit']; //item Number '70'
//           ItemInfoListArray[0]['ClaimType'] = EntClm.array[0]['sclmty']; // claim type '2'
//           ItemInfoListArray[0]['CreditType'] = EntClm.array[0]['sclmcr']; //creit: 'C'
//           ItemInfoListArray[0]['LedgerNumber'] = EntClm.array[0]['sclmld']; //ledger number '213'
//           ItemInfoListArray[0]['Department'] = EntClm.array[0]['sclmdp']; //department 'J'
//           ItemInfoListArray[0]['InvoiceNumber'] = EntClm.array[0]['sordin']; //invoice number of order '0'
//           ItemInfoListArray[0]['OrderDate'] = EntClm.array[0]['sorddt']; //order date'0'
//           ItemInfoListArray[0]['PickCode'] = EntClm.array[0]['sordpc']; //pick code ''
//           ItemInfoListArray[0]['Slot1'] = EntClm.array[0]['sords1'];
//           ItemInfoListArray[0]['Slot3'] = EntClm.array[0]['sords3'];        
//           ItemInfoListArray[0]['QuantityOnClaim'] = EntClm.array[0]['sclmqt']; //Qty on claim
//           ItemInfoListArray[0]['CaseOrOnly'] = EntClm.array[0]['sclmco']; //case or only
//           ItemInfoListArray[0]['TobaccoTax'] = EntClm.array[0]['sclmtx']; // tax if tobacco
//           ItemInfoListArray[0]['perCaseCost'] = EntClm.array[0]['sclmcs']; //per case cost
//           ItemInfoListArray[0]['ExtendedCost'] = EntClm.array[0]['sclmex']; //extended cost
//           ItemInfoListArray[0]['Flag'] = EntClm.array[0]['sclmfl']; //flag
//           ItemInfoListArray[0]['Buyer'] = EntClm.array[0]['sclmby']; //buyer
//           ItemInfoListArray[0]['RestockCharge'] = EntClm.array[0]['sclstk']; //restock charge
//           ItemInfoListArray[0]['DocumentNumber'] = EntClm.array[0]['scldoc']; //document no.
//           ItemInfoListArray[0]['ExtendedRetail'] = EntClm.array[0]['sclret']; //extended retail
//           ItemInfoListArray[0]['User'] = EntClm.array[0]['sclusr']; //user
//           ItemInfoListArray[0]['Assignment'] = EntClm.array[0]['sclasn']; //assignment
//           ItemInfoListArray[0]['Container'] = EntClm.array[0]['sclcnt']; //container        
//           ItemInfoListArray[0]['CallingName'] = EntClm.array[0]['sclnam']; //calling name
  
//           ProfrontTxtlog.pushToTxTLog("getClmInfoAPI.V3",userName, 'Success Status: ' + ItemInfoListArray[0]['Status']);
//           response.json(ItemInfoListArray);
  
//         } else if (FicheD01.array.length > 0) {
//           console.log('Claim found in FicheD01.');
  
//           ItemInfoListArray[0]['Status'] = 'READY-TO-WRITE-NEW-CLAIM'; //DO NOT CHANGE - MAGIC STRING --------------------------------------------------------------------------------------------
//           ItemInfoListArray[0]['AppMessage'] = 'INVOICE ORDER FOUND WITH NO CLAIM FILED YET.';
//           ItemInfoListArray[0]['StoreNumber'] = FicheD01.array[0]['fdstor']; //store number 'session.StoreNum#'
//           ItemInfoListArray[0]['InvoiceNumber'] = FicheD01.array[0]['fdinv'];
//           ItemInfoListArray[0]['OrderDate'] = FicheD01.array[0]['fddate'];
//           ItemInfoListArray[0]['ItemNumber'] = FicheD01.array[0]['fditem'];
//           ItemInfoListArray[0]['CheckDigit'] = FicheD01.array[0]['fdchkd'];
//           ItemInfoListArray[0]['Description'] = FicheD01.array[0]['fddesc'];
//           ItemInfoListArray[0]['Department'] = FicheD01.array[0]['fddept'];
//           ItemInfoListArray[0]['Slot'] = FicheD01.array[0]['fdslot'];
//           ItemInfoListArray[0]['StoreCost'] = FicheD01.array[0]['fdsell'];
//           ItemInfoListArray[0]['ExtendedCost'] = FicheD01.array[0]['fdecst'];
//           ItemInfoListArray[0]['CatchWeightInPounds'] = FicheD01.array[0]['fdcwlb'];
//           ItemInfoListArray[0]['TobaccoTax'] = FicheD01.array[0]['fdttax'];
  
//           ProfrontTxtlog.pushToTxTLog("getClmInfoAPI.V3",userName, 'Success Status: ' + ItemInfoListArray[0]['Status']);
//           response.json(ItemInfoListArray);
  
//       } else {
//         errorMsg = 'Error, no records found.'
//         // response.status(404).send(errorMsg);
//         response.json(errorMsg);
//         ProfrontTxtlog.pushToTxTLog("getClmInfoAPI.V3",userName, '404: ' + errorMsg);
//         log.pushToLog(storeNumber, userName, '404 Error, no records found.');
//       }
//     }
//   }
//   exports.run = getShortClaimInfo;
  
//   function formatDate(date) {
//     let s = date.toString();
//     let invoiceYear = s.substr(0, 4);
//     let invoiceMonth = s.substr(4, 2);
//     let invoiceDay = s.substr(6, 2);
//     let invoiceDate = new Date(invoiceYear, invoiceMonth, invoiceDay);
//     deliveryDate = addSubtractDate.subtract(invoiceDate, 1, 'month'); //Becuase .js months start at 00.
//     deliveryDate = addSubtractDate.add(invoiceDate, 1, 'day'); //Delivery date is always one day after the invoice date.
//     deliveryDate = deliveryDate.toISOString();
//     deliveryDate = deliveryDate.replace(/-|T|:/g, '');
//     deliveryDate = deliveryDate.substring(0, 8);
//     deliveryDate = parseInt(deliveryDate);
//     return deliveryDate;
//   }
  
//   function formatPackSize(packSize) {
//     packSize = packSize.toString();
//     var packDash = packSize.indexOf('/');
//     packSize = packSize.slice(0, packDash);
//     packSize = packSize.trim();
//     packSize = parseInt(packSize);
//     return packSize;
//   }
  
//   function padLeadingZeros(num, size) {
//     var s = num + '';
//     while (s.length < size) s = '0' + s;
//     return s;
//   }
  
//   // Check that todays date is not more than 2 days after the invoice becuase they cannot place a claim more than 2 days later.
//   function dateOfEntryApproval(delvryDte){
//     console.log(`delvryDte: ${delvryDte}`);
//     var b = delvryDte.toString();
//     var y = b.substr(0, 4);
//     var m = b.substr(4, 2);
//     var d = b.substr(6, 2);
//     var comparisonDDate = new Date(y,m,d);
//     var comparisonDeliveryDate = addSubtractDate.subtract(comparisonDDate, 1, 'month'); //Becuase .js months start at 00.
//     console.log(`comparisonDeliveryDate: ${comparisonDeliveryDate}`);
  
//     const comparisonDayOfWeek = comparisonDeliveryDate.getDay();
    
//     if(comparisonDayOfWeek == 6){
//       var deadlineDate = addSubtractDate.add(comparisonDeliveryDate, 3, 'day'); 
//     }else if (comparisonDayOfWeek == 4){
//       deadlineDate = addSubtractDate.add(comparisonDeliveryDate, 4, 'day');
//     }else if (comparisonDayOfWeek == 5){
//       deadlineDate = addSubtractDate.add(comparisonDeliveryDate, 4, 'day');
//     }else {
//       deadlineDate = addSubtractDate.add(comparisonDeliveryDate, 2, 'day');
//     }
//     //0 sun = +2
//     //1 mon = +2
//     //2 tue = +2
//     //3 wed = +2
//     //4 Thur = +4
//     //5 fri = +4
//     //6 sat = +3
  
//     console.log(`deadlineDate: ${deadlineDate}`);
  
//     var today = new Date();
//     console.log(`today: ${today}`);
    
//     if (today > deadlineDate){
//       return false;
//     }else {
//       return true;
//     }
//   }
  