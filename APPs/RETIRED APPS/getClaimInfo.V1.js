// /* Incoming INFO:
// {
//     "invoiceNumber" : 3,
//     "storeNumber" : 700,
//     "itemNumber" : 895243,
//     "UserKeyingClaim": "Tyler",
//     "CallingName": "Hobbs",
//     "CatchWeight": 44.90,
//     "QuantityOnClaim" : 1,
//     "CaseOrOnly" : "C"
// }
// */

// const addSubtractDate = require("add-subtract-date");
// async function getClaimInfo(
//   storeNumberIncomming,
//   invoiceNumberIncomming,
//   itemNumberCheckDigitIncomming,
//   userNameIncomming,
//   quantityOnClaimIncomming,
//   caseOrOnlyIncomming
// ) {
//   //Set pathing for Apps.
//     //local
//     // const directPathToProlive = "C:/tyler/modules/prolive/APPs/";
//     // const directPathToProtest = "C:/tyler/modules/protest/APPs/";
//     //production
//     // const directPathToProlive = "C:/profront/modules/prolive/APPs/";
//     // const directPathToProtest = "C:/profront/modules/protest/APPs/";

//     const directPathToProlive = "../../prolive/APPs/";
//     const directPathToProtest = "../../protest/APPs/";
//     const directPathToProlog = "../../Prologging/";

//   let buyerName = pjs.require(directPathToProlive + "BuyerNameAPP.js");
//   let FicheD01 = pjs.require(directPathToProlive + "FicheD01APP.V2.js"); //New app call is set
//   let EntClm = pjs.require(directPathToProlive + "EntClmAPP.js"); //New app call is set
//   let ClmHst = pjs.require(directPathToProlive + "ClmHstAPP.V2.js"); //New app call is set
//   let ClmEnt = pjs.require(directPathToProlive + "ClmEntAPP.V2.js"); //New app call is set
//   let selector = pjs.require(directPathToProlive + "PickSelectorAPPV2.js");
//   let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

//   //Predefine variables.
//   let errorMsg;
//   // let returnObj.array = [];
//   const invoiceNumber = invoiceNumberIncomming;
//   const storeNumber = storeNumberIncomming;
//   const itemNumberCheckDigit = itemNumberCheckDigitIncomming;
//   const userName = userNameIncomming;
//   const quantityOnClaim = quantityOnClaimIncomming;
//   const caseOrOnly = caseOrOnlyIncomming;
//   var returnObj = {};
//   returnObj.array = [];
//   returnObj.error = '';

//   const originalDataForLogging = 
//   'InvoiceNumber: '+ invoiceNumber +
//   ', StoreNumber: '+ storeNumber +
//   ', ItemNumber: '+ itemNumberCheckDigit +
//   ', QuantityOnClaim: '+ quantityOnClaim +
//   ', CaseOrOnly: '+ caseOrOnly;

//   // POPULATE THE returnObj.array WITH ALL RELEVENT DATA SET TO NULL SO THAT WE SEND EVERYHTING TO THE FRONT END REGARDLESS OF FILE QUERIES LATER.
//   returnObj.array.push({
//     Selector: "",
//     Buyer: "",
//     Status: "",
//     AppMessage: "",
//     StoreNumber: 0,
//     InvoiceNumber: 0,
//     OrderDate: 0,
//     DeliveryDate: 0,
//     ItemNumber: 0,
//     CheckDigit: 0,
//     PackSize: 0,
//     Description: "",
//     QuantityShipped: 0,
//     Department: "",
//     Slot: "",
//     StoreCost: 0,
//     ExtendedCost: 0,
//     Buyer: "",
//     ExtendedRetail: 0,
//     CatchWeightInPounds: 0,
//     User: "",
//     CallingName: "",
//     CaseOrOnly: "",
//   });

//   let itemNumberBase = padLeadingZeros(itemNumberCheckDigit, 6);
//   let CheckDigit = itemNumberBase.slice(5, 6);
//   CheckDigit = parseInt(CheckDigit);
//   let itemNumber = itemNumberBase.slice(0, 5);
//   itemNumber = parseInt(itemNumber);

//   // call getBuyerNameApp to get Buyer first and last name and apply it to buyerNum to add into returned array.
//   buyerName = await buyerName.getBuyerName(itemNumber);
//   if (buyerName.errorMsg) {
//     returnObj.error = buyerName.errorMsg;
//     ProfrontTxtlog.pushToTxTLog(
//       "getClaimInfo.V1",
//       userName, 
//       originalDataForLogging,        
//       buyerName.errorMsg
//     );
//     return returnObj;
//   }

//   // get Pick Selector Info.
//   selector = await selector.getPickSelectorInfo(invoiceNumber,itemNumberCheckDigit,itemNumber);
//   if (selector.ErrMessage) selector.Message = "Selector not available.";

//   // Query FicheD01 to get Item info off of Invoice.
//   FicheD01 = await FicheD01.getFicheD01Info(invoiceNumber, storeNumber, itemNumber);
//   console.log("records gathered from FICHED01.");
//   console.log(FicheD01.array);

//   if (FicheD01.Message) {
//     returnObj.error = FicheD01.Message;
//     await ProfrontTxtlog.pushToTxTLog(
//       "getClaimInfo.V1",
//       userName, 
//       originalDataForLogging,        
//       FicheD01.Message
//     );
//     console.log("Error on FicheD01 querry:" + FicheD01.Message);
//     return returnObj;
//   }

//   let numberOfItemsOnInvoice = FicheD01.array[0]["fdqtys"];
//   console.log(`member key size: ${numberOfItemsOnInvoice}`);

//   extendedCostForQueries = FicheD01.array[0]["fdecst"];
//   console.log(`Extended Cost: ${extendedCostForQueries}`);

//   //get date and run formatDate function to +1 the day.
//   var invoiceDate;
//   invoiceDate = FicheD01.array[0]["fddate"];
//   var deliveryDate;
//   deliveryDate = formatDate(invoiceDate);

//   //ADD IN A CHECK FOR USER id TO SEE IF APPROVED USERS CAN BYPASS THE DATE LIMIT.
  
//   //  CHECK HERE TO COMPARE DELIVERYDATE TO TODAYS DATE. IF IT IS MORE THAN 2 DAYS APART THEY CANNOT FILE A CLAIM
//   var checkDateOfEntry = dateOfEntryApproval(deliveryDate);
//   if (checkDateOfEntry == false && storeNumber != 700){ //700 is the test store.
//     errorMsg = 'The deadline for filing the claim ended at 12 PM, two business days after delivery.';
//     returnObj.error = errorMsg;
//     await ProfrontTxtlog.pushToTxTLog(
//       "getClaimInfo.V1",
//       userName, 
//       originalDataForLogging,        
//       errorMsg
//     );
//   return returnObj;}

//   const quantityShipped = FicheD01.array.reduce((accumulator, item) => {
//     return accumulator + item.fdqtys;
//     }, 0);

//   console.log("Total quantityShipped: " + quantityShipped);

//   if (FicheD01.array > 0 && quantityShipped <= 0) {
//     returnObj.error = "Quantity was zero meaning Item was an Out and a Claim cannot be filed on such.";
//     await ProfrontTxtlog.pushToTxTLog("getClaimInfo.V1", userName, originalDataForLogging, errorMsg);
//     // response.status(400).send(errorMsg);
//     return returnObj;
//   }

//   // Query ENTCLM to see if a claim is currently filed.
//   console.log(`Calling ENTCLM query for item: ${itemNumber}`);
//   let EntClmCall = await EntClm.getEntClmInfo(invoiceNumber, storeNumber, itemNumber);
//   // console.log(EntClmCall);

//   let totalENTCLMCasesClaimed = 0;
//   if (EntClmCall.ClaimedArray.length > 0) {
//     totalENTCLMCasesClaimed = EntClmCall.ClaimedArray.reduce((accumulator, item) => {
//       return accumulator + item.totalCasesClaimed;
//     }, 0);
//   }

//   let totalENTCLMUnitsClaimed = 0;
//   if (EntClmCall.ClaimedArray.length > 0) {
//     totalENTCLMUnitsClaimed = EntClmCall.ClaimedArray.reduce((accumulator, item) => {
//       return accumulator + item.totalUnitsClaimed;
//     }, 0);
//   }

//   // Query CLMHST to see if a claim has already been filed.
//   console.log(`Calling CLMHST query for item: ${itemNumber}`);
//   let ClmHstCall = await ClmHst.getClmHstInfo(invoiceNumber, storeNumber, itemNumber);
//   // console.log(ClmHstCall);

//   let totalCLMHSTCasesClaimed = 0
//   if (ClmHstCall.ClaimedArray.length > 0) {
//     totalCLMHSTCasesClaimed = ClmHstCall.ClaimedArray.reduce((accumulator, item) => {
//       return accumulator + item.totalCasesClaimed;
//     }, 0);
//   }

//   let totalCLMHSTUnitsClaimed = 0
//   if (ClmHstCall.ClaimedArray.length > 0) {
//     totalCLMHSTUnitsClaimed = ClmHstCall.ClaimedArray.reduce((accumulator, item) => {
//       return accumulator + item.totalUnitsClaimed;
//     }, 0);
//   }

//   // Query CLMENT to see if a Markout claim was filed in house.
//   console.log(`Calling CLMENT query for item: ${itemNumber}`);
//   let ClmEntCall = await ClmEnt.getClmEntInfo(invoiceNumber, storeNumber, itemNumber);
//   // console.log(ClmEntCall);

//   let totalCLMENTCasesClaimed
//   if (ClmEntCall.ClaimedArray.length > 0) {
//     totalCLMENTCasesClaimed = ClmEntCall.ClaimedArray.reduce((accumulator, item) => {
//       return accumulator + item.totalCasesClaimed;
//     }, 0);
//   }

//   let totalCLMENTUnitsClaimed;
//   if (ClmEntCall.ClaimedArray.length > 0) {
//     totalCLMENTUnitsClaimed = ClmEntCall.ClaimedArray.reduce((accumulator, item) => {
//       return accumulator + item.totalUnitsClaimed;
//     }, 0);
//   }
  
//   let numberOfCaseClaimsFiled = totalENTCLMCasesClaimed + totalCLMHSTCasesClaimed + totalCLMENTCasesClaimed;
//   console.log(`Number of case claims filed: ${numberOfCaseClaimsFiled}`);

//   let numberOfUnitClaimsFiled = totalENTCLMUnitsClaimed + totalCLMHSTUnitsClaimed + totalCLMENTUnitsClaimed;
//   console.log(`Number of unit claims filed: ${numberOfUnitClaimsFiled}`);

//   // get Pack Size and run formatPackSize to just get pack amount.
//   let packSize;
//   packSize = FicheD01.array[0]["fdpksz"];
//   packSize = formatPackSize(packSize);

//   //convert case climes filed to units by multiplying by pack size.
//   let numberOfTotalUnitClaimsFiled = (numberOfCaseClaimsFiled * packSize) + numberOfUnitClaimsFiled;
//   console.log(`Number of total unit claims filed: ${numberOfTotalUnitClaimsFiled}`);

//   //number of units shipped:
//   let numberOfUnitsShipped = quantityShipped * packSize;
//   console.log(`Number of units shipped: ${numberOfUnitsShipped}`);

//   //number of units remaining to be claimed:
//   let numberOfUnitsRemainingToBeClaimed = numberOfUnitsShipped - numberOfTotalUnitClaimsFiled;
//   console.log(`Number of units remaining to be claimed: ${numberOfUnitsRemainingToBeClaimed}`);

//   // if caseOrOnly is "O" then take pack size * maxNumberOfClaimableItems to get new maxNumberOfClaimableItems.
//   if (caseOrOnly == "O" || caseOrOnly == "o") {
//     if(quantityOnClaim > numberOfUnitsRemainingToBeClaimed){

//       returnObj.error = `It looks like a credit has been given for this item, please check your markouts sheets under eDocs. Any questions please call 205-209-6309.`;

//       await ProfrontTxtlog.pushToTxTLog("getClaimInfoAPI.V1",userName, originalDataForLogging, 'Quantity on claim is greater than the pack size.');
//       return returnObj;
//     }
//   }

//   if (caseOrOnly == "C" || caseOrOnly == "c") {
//     if (numberOfUnitsRemainingToBeClaimed < packSize) {

//       returnObj.error = `It looks like a credit has been given for this item, please check your markouts sheets under eDocs. Any questions please call 205-209-6309.`;

//       await ProfrontTxtlog.pushToTxTLog("getClaimInfoAPI.V1",userName, originalDataForLogging, 'There are not enough units remaining to claim a full case.');
//       return returnObj;
//     }

//     if (quantityOnClaim > numberOfItemsOnInvoice){
        
//       returnObj.error = `It looks like you are trying to file claims that exceed the number of cases shipped. Any questions please call 205-209-6309.`;
  
//       await ProfrontTxtlog.pushToTxTLog(
//         "getClaimInfoAPI.V1",
//         userName,
//         originalDataForLogging,
//         'Quantity on claim is greater than the number of cases on the invoice.'
//       );
//       return returnObj;
//     }
//   }
//   //STOP HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

//   let fdsrp = FicheD01.array[0]["fdsrp"];
//   let fdmulti = FicheD01.array[0]["fdmult"];
//   let calculatedExtendedRetail;

//   if (fdmulti != 0) {
//     calculatedExtendedRetail = fdsrp * 100 / fdmulti * packSize / 100;
//   } else {
//     calculatedExtendedRetail = fdsrp * 100 * packSize / 100;
//   }

//   calculatedExtendedRetail = +calculatedExtendedRetail.toFixed(2);
//   console.log("calculatedExtendedRetail: " + calculatedExtendedRetail);

//   console.log("returnObj.array pre queries:");

//     //PUSH ALL COLLECTED DATA THUS FAR TO ARRAY BEFORE RUNNING QUERIES
//     returnObj.array[0]["Status"] = "READY-TO-WRITE-NEW-CLAIM"; //DO NOT CHANGE - MAGIC STRING --------------------------------------------------------------------------------------------
//     returnObj.array[0]["AppMessage"] = "INVOICE ORDER FOUND WITH NO CLAIM FILED YET.";
//     returnObj.array[0]["StoreNumber"] = FicheD01.array[0]["fdstor"];
//     returnObj.array[0]["InvoiceNumber"] = FicheD01.array[0]["fdinv"];
//     returnObj.array[0]["OrderDate"] = FicheD01.array[0]["fddate"];
//     returnObj.array[0]["DeliveryDate"] = deliveryDate;
//     returnObj.array[0]["ItemNumber"] = itemNumber;
//     returnObj.array[0]["CheckDigit"] = CheckDigit;
//     returnObj.array[0]["Description"] = FicheD01.array[0]["fddesc"];
//     returnObj.array[0]["Buyer"] = buyerName.value;
//     returnObj.array[0]["Selector"] = selector.Message;
//     returnObj.array[0]["QuantityShipped"] = quantityShipped;
//     returnObj.array[0]["PackSize"] = packSize;
//     returnObj.array[0]["Department"] = FicheD01.array[0]["fddept"];
//     returnObj.array[0]["Slot"] = FicheD01.array[0]["fdslot"];
//     returnObj.array[0]["StoreCost"] = FicheD01.array[0]["fdsell"];
//     returnObj.array[0]["ExtendedRetail"] = calculatedExtendedRetail;
//     returnObj.array[0]["ExtendedCost"] = extendedCostForQueries;
//     returnObj.array[0]["CatchWeightInPounds"] = FicheD01.array[0]["fdcwlb"];

//     await ProfrontTxtlog.pushToTxTLog("getClaimInfo.V1",userName, originalDataForLogging, "Success Status: " + returnObj.array[0]["Status"]);
//   return returnObj;
// }
// exports.getClaimInfo = getClaimInfo;

// function formatDate(date) {
//   let s = date.toString();
//   let invoiceYear = s.substr(0, 4);
//   let invoiceMonth = s.substr(4, 2);
//   let invoiceDay = s.substr(6, 2);
//   let invoiceDate = new Date(invoiceYear, invoiceMonth, invoiceDay);
//   deliveryDate = addSubtractDate.subtract(invoiceDate, 1, "month"); //Becuase .js months start at 00.
//   deliveryDate = addSubtractDate.add(invoiceDate, 1, "day"); //Delivery date is always one day after the invoice date.
//   deliveryDate = deliveryDate.toISOString();
//   deliveryDate = deliveryDate.replace(/-|T|:/g, "");
//   deliveryDate = deliveryDate.substring(0, 8);
//   deliveryDate = parseInt(deliveryDate);
//   return deliveryDate;
// }

// function padLeadingZeros(num, size) {
//   var s = num + "";
//   while (s.length < size) s = "0" + s;
//   return s;
// }

// function formatPackSize(packSizeIn) {
//   pS = packSizeIn.toString();
//   var packDash = pS.indexOf("/");
//   pS = pS.slice(0, packDash);
//   pS = pS.trim();
//   pS = parseInt(pS);
//   return pS;
// }

// // Check that todays date is not more than 2 days after the invoice becuase they cannot place a claim more than 2 days later.
// function dateOfEntryApproval(delvryDte) {
//   console.log(`delvryDte: ${delvryDte}`);
//   var b = delvryDte.toString();
//   var y = b.substr(0, 4);
//   var m = b.substr(4, 2);
//   var d = b.substr(6, 2);
//   var comparisonDDate = new Date(y, m, d);
//   var comparisonDeliveryDate = addSubtractDate.subtract(
//     comparisonDDate,
//     1,
//     "month"
//   ); //Becuase .js months start at 00.
//   console.log(`comparisonDeliveryDate: ${comparisonDeliveryDate}`);

//   const comparisonDayOfWeek = comparisonDeliveryDate.getDay();

//   if (comparisonDayOfWeek == 6) {
//     var deadlineDate = addSubtractDate.add(comparisonDeliveryDate, 3, "day");
//   } else if (comparisonDayOfWeek == 4) {
//     deadlineDate = addSubtractDate.add(comparisonDeliveryDate, 4, "day");
//   } else if (comparisonDayOfWeek == 5) {
//     deadlineDate = addSubtractDate.add(comparisonDeliveryDate, 4, "day");
//   } else {
//     deadlineDate = addSubtractDate.add(comparisonDeliveryDate, 2, "day");
//   }
//   //0 sun = +2
//   //1 mon = +2
//   //2 tue = +2
//   //3 wed = +2
//   //4 Thur = +4
//   //5 fri = +4
//   //6 sat = +3

//   console.log(`deadlineDate: ${deadlineDate}`);

//   var today = new Date();
//   console.log(`today: ${today}`);

//   if (today > deadlineDate) {
//     return false;
//   } else {
//     return true;
//   }
// }
