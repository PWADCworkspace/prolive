// var FicheD01 = pjs.require("C:/profront/modules/protest/APPs/FicheD01APP.js");

function getFicheD01Info(invoiceNumber, storeNumber, itemNumber) {
  var i = invoiceNumber;
  var s = storeNumber;
  var t = itemNumber;
  var sqlquery;
  var returnObj = {};
  returnObj.array = [];
  returnObj.Message = '';

  if (!i || !s || !t) {
    returnObj.Message =
      'Invoice Number, Store Number and Item Number are required.';
    return returnObj; 
  } else if(s.toString().length > 3){ 
      returnObj.Message =
        'Store Number is to long.';
      return returnObj; 
  } else {
    sqlquery =
      'select * from PWAFIL.FICHED01 where FDINV = ? and FDSTOR = ? and FDITEM = ?';
    //FDITEM, FDINV, FDCHKD, FDDATE, FDPKSZ, FDDESC, FDQTYS, FDDEPT, FDSLOT, FDSRP, FDSELL, FDECST, FDCWLB
    returnObj.array = pjs.query(sqlquery, [i, s, t]);

    if (returnObj.array.length < 1) {
      returnObj.Message =
        'Item ' +
        t +
        ' was not a part of Invoice Number ' +
        i +
        ' for Store Number ' +
        s +
        '.';
      return returnObj;
    } else {
      return returnObj;
    }
  }
}
exports.getFicheD01Info = getFicheD01Info;
