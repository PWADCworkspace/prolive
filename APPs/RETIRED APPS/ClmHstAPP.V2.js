// //var ClmHst = pjs.require('C:/tyler/modules/protest/APPs/ClmHstAPP.js');

// // VERSION V2-------------------------------------------------------------------------------------------------------------------------------------------------------
// // Including extended cost intake from the writeAPI and querry based on strick extended cost entry.
// // This is to prevent duplicate claims from being created for that specific weight/extended cost.

// // VERSION V3-------------------------------------------------------------------------------------------------------------------------------------------------------
// // This should include a way to return to the getClaim API and the front end, the values of the markout claim that was found.
// /*
//     select 
//       hclmno as ClaimNumber, 
//       hclmdt as ClaimDate, 
//       hclmst as Store, 
//       hclmit as Item, 
//       hclmty as ClaimType, 
//       hclmcr as Credit, 
//       hordin as InvoiceNumber, 
//       horddt as OrderDate, 
//       hclmqt as QuantityOnClaim,
//       hclmco as CaseOrOnly,
//       hclmcs as PerCaseCost,
//       hclmex as ExtendedCost,
//       hclstk as RestockCharge,
//       hclret as Retail
//     from pwafil.clmhst where hclmdt > 20221100
//   */

// function getClmHstInfo(invoiceNumber, storeNumber, itemNumber) {
//   const inv = invoiceNumber;
//   const s = storeNumber;
//   const t = itemNumber;
//   let sqlquery;
//   let returnObj = {};
//   returnObj.ObjectArray = [];
//   returnObj.ClaimedArray = [];
//   returnObj.Message = "";

//   if (!inv || !s || !t) {
//     returnObj.Message =
//       "Invoice Number, Store Number and Item Number are required.";
//     return returnObj;
//   } else {
//     console.log(
//       `Running HISTORY query for item ${t} on invoice ${inv} at store ${s}`
//     );

//     //create comparison date that is 1 week ago from today as YYYMMDD format
//     let comparisonDate = new Date();
//     comparisonDate.setDate(comparisonDate.getDate() - 180);
//     comparisonDate = comparisonDate
//       .toISOString()
//       .slice(0, 10)
//       .replace(/-/g, "");
//     // console.log(comparisonDate);

//     sqlquery =
//       "select * from PWAFIL.CLMHST where HORDIN = ? and HCLMST = ? and HCLMIT = ? and HCLMDT > ? and HCLMTY < 8";
//     returnObj.ObjectArray = pjs.query(sqlquery, [inv, s, t, comparisonDate]);

//     //get total quantity claimed for sclmno = "C" from the array
//     let totalCasesClaimed = 0;
//     for (var i = 0; i < returnObj.ObjectArray.length; i++) {
//       if (
//         returnObj.ObjectArray[i].hclmco == "C" ||
//         returnObj.ObjectArray[i].hclmco == "c"
//       ) {
//         totalCasesClaimed += returnObj.ObjectArray[i].hclmqt;
//       }
//     }

//     //get total quantity claimed for sclmno = "O" from the array
//     let totalUnitsClaimed = 0;
//     for (var i = 0; i < returnObj.ObjectArray.length; i++) {
//       if (
//         returnObj.ObjectArray[i].hclmco == "O" ||
//         returnObj.ObjectArray[i].hclmco == "o"
//       ) {
//         totalUnitsClaimed += returnObj.ObjectArray[i].hclmqt;
//       }
//     }

//     returnObj.ClaimedArray.push({
//       totalCasesClaimed: totalCasesClaimed,
//       totalUnitsClaimed: totalUnitsClaimed
//     });
//     // console.log(returnObj.array);
//     // if (returnObj.array.length < 1) {
//     //   returnObj.Message =
//     //     'Item ' +
//     //     t +
//     //     ' was not a part of Invoice Number ' +
//     //     i +
//     //     ' for Store Number ' +
//     //     s +
//     //     '.';
//     //   return returnObj;
//     // } else if (returnObj.array[0]['hclmty'] == 7) {
//     //   returnObj.Message = 'MARKOUT-CLAIM-ALREADY-EXISTS';
//     //   return returnObj;
//     // } else {
//     //   returnObj.Message = 'CLAIM-ALREADY-EXISTS';
//     return returnObj;
//     // }
//   }
// }
// exports.getClmHstInfo = getClmHstInfo;
