//var ClmEnt = pjs.require('C:/tyler/modules/protest/APPs/ClmEntAPP.js');

function getClmEntInfo(invoiceNumber, storeNumber, itemNumber) {
    var i = invoiceNumber;
    var s = storeNumber;
    var t = itemNumber;
    var sqlquery;
    var returnObj = {};
    returnObj.array = [];
    returnObj.Message = '';
  
    if (!i || !s || !t) {
      returnObj.Message =
        'Invoice Number, Store Number and Item Number are required.';
      return returnObj;
    } else {
      sqlquery =
        'select * from PWAFIL.CLMENT where ORDINV = ? and CLMSTR = ? and CLMITM = ? and CLMTYP = 7';
      returnObj.array = pjs.query(sqlquery, [i, s, t]);
  
      if (returnObj.array.length < 1) {
        returnObj.Message =
          'Markout was not filed for Item ' +
          t +
          ' for Invoice Number ' +
          i +
          ' for Store Number ' +
          s +
          '.';
        return returnObj;
      } else {
        // console.log(returnObj.array);
        returnObj.Message = 'MARKOUT-CLAIM-ALREADY-EXISTS.';
        return returnObj;
      }
    }
  }
  exports.getClmEntInfo = getClmEntInfo;
  