// // var FicheD01 = pjs.require("C:/profront/modules/protest/APPs/FicheD01APP.js");

// /*---------------------------------------------------------------------------------------------------------------------------------------------------------------
// function testRun() {
//   //get Pick Selector Info.
//   var invoiceNumber = 40531;
//   var itemNumberCheckDigit = 582957;
//   var itemNumber = 58295;
//   var selectorInfo;

//   selectorInfo = getPickSelectorInfo(
//     invoiceNumber,
//     itemNumberCheckDigit,
//     itemNumber
//   );
//   console.log(selectorInfo);
// }
// exports.run = testRun;
// */

// function getPickSelectorInfo(invoiceNumber, itemNumberCheckDigit, itemNumber) {
//   var i = invoiceNumber;
//   var t = itemNumber;
//   var t2 = itemNumberCheckDigit;

//   var zeroItemNumber;
//   var AssignmentNumber;
//   var sqlquery;
//   var returnObj = {};
//   returnObj.Message = '';
//   returnObj.ErrMessage = '';

//   if (!i || !t2) {
//     returnObj.ErrMessage = 'Invoice Number and Item Number are required.';
//     return returnObj;
//   } else {
//     sqlquery =
//       'SELECT CRASM# from PWAFIL.ASMCROSS where CRINV# = ? and CRITEM = ?';
//     AssignmentNumber = pjs.query(sqlquery, [i, t]);

//     if (AssignmentNumber.length == 0) {
//       returnObj.ErrMessage = 'No Assignment Number Found.';
//       return returnObj;
//     } else {
//       //add '0' to the front if assignment number to 8 charecters
//       let a = AssignmentNumber[0]['crasm#'].toString();
//       zeroA = padLeadingZeros(a, 8);

//       //add '0' to the front if itmnum to 6 charecters
//       zeroItemNumber = padLeadingZeros(t2, 6);
//       // console.log(zeroA);
//       // console.log(zeroItemNumber);

//       sqlquery =
//         'SELECT trim(xpkpst) as Status, trim(xpkopr) as Operator, trim(xpkdat) as Datetime FROM PWAFIL.LUCYPCBK where xpkano = ? and xpkitm = ?';
//       recordSet = pjs.query(sqlquery, [zeroA, zeroItemNumber]);


//       if (recordSet.length < 1) {
//         returnObj.ErrMessage =
//           'Item ' + t + ' was not a part of Invoice Number ' + i + '.';
//         return returnObj;
//       } else {
//         for (let i = 0; i < recordSet.length; i++) {
//           let Status = recordSet[i]['status'].toString();
//           let Operator = recordSet[i]['operator'].toString();
//           let Datetime = recordSet[i]['datetime'].toString();
//           Datetime = Datetime.slice(0, 8);
//           returnObj.Message += 
//             t + ' ' + Status + ' by Operator ' + Operator + ' at ' + Datetime + '; ';
//           // console.log(returnObj.Message);

//         }
//         return returnObj;
//       }
//     }
//   }
// }

// exports.getPickSelectorInfo = getPickSelectorInfo;

// function padLeadingZeros(num, size) {
//   var s = num + '';
//   while (s.length < size) s = '0' + s;
//   return s;
// }
