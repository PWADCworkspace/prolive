// //var ClmHst = pjs.require('C:/tyler/modules/protest/APPs/ClmHstAPP.js');

// // VERSION V2-------------------------------------------------------------------------------------------------------------------------------------------------------
// // Including extended cost intake from the writeAPI and querry based on strick extended cost entry.
// // This is to prevent duplicate claims from being created for that specific weight/extended cost.

// // VERSION V3-------------------------------------------------------------------------------------------------------------------------------------------------------
// // This should include a way to return to the getClaim API and the front end, the values of the markout claim that was found.
// /*
//     select 
//       hclmno as ClaimNumber, 
//       hclmdt as ClaimDate, 
//       hclmst as Store, 
//       hclmit as Item, 
//       hclmty as ClaimType, 
//       hclmcr as Credit, 
//       hordin as InvoiceNumber, 
//       horddt as OrderDate, 
//       hclmqt as QuantityOnClaim,
//       hclmco as CaseOrOnly,
//       hclmcs as PerCaseCost,
//       hclmex as ExtendedCost,
//       hclstk as RestockCharge,
//       hclret as Retail
//     from pwafil.clmhst where hclmdt > 20221100
//   */

// function getCatchWeightClmHstInfo(
//   invoiceNumber,
//   storeNumber,
//   itemNumber,
//   incommingExtendedCost
// ) {
//   const i = invoiceNumber;
//   const s = storeNumber;
//   const t = itemNumber;
//   const e = incommingExtendedCost;
//   let sqlquery;
//   let returnObj = {};
//   returnObj.array = [];
//   returnObj.Message = "";

//   console.log(`running ENTCLM query for item: ${t}`);

//   if (!i || !s || !t) {
//     returnObj.Message =
//       "Invoice Number, Store Number and Item Number are required.";
//     return returnObj;
//   } else {
//     console.log(
//       `Running extended cost HISTORY query for item ${t} on invoice ${i} at store ${s} with an extended cost of ${e}.`
//     );

//     //create comparison date that is 1 week ago from today as YYYMMDD format
//     let comparisonDate = new Date();
//     comparisonDate.setDate(comparisonDate.getDate() - 180);
//     comparisonDate = comparisonDate
//       .toISOString()
//       .slice(0, 10)
//       .replace(/-/g, "");
//     // console.log(comparisonDate);

//     sqlquery =
//       "select * from PWAFIL.CLMHST where HORDIN = ? and HCLMST = ? and HCLMIT = ? and HCLMEX = ? and HCLMDT > ? and HCLMTY < 8";
//     returnObj.array = pjs.query(sqlquery, [i, s, t, e, comparisonDate]);
//     // console.log(returnObj.array);
//     if (returnObj.array.length < 1) {
//       returnObj.Message = `Item ${t} was not found for Invoice Number ${i} for Store Number ${s} with an extended cost of ${e}.`;
//       return returnObj;
//     } else if (returnObj.array[0]["hclmty"] == 7) {
//       returnObj.Message = `MARKOUT-CLAIM-ALREADY-EXISTS for Item ${t} on Invoice Number ${i} for Store Number ${s} with an extended cost of ${e}.`;
//       return returnObj;
//     } else {
//       returnObj.Message = `CLAIM-ALREADY-EXISTS for Item ${t} on Invoice Number ${i} for Store Number ${s} with an extended cost of ${e}.`;
//       return returnObj;
//     }
//   }
// }
// exports.getCatchWeightClmHstInfo = getCatchWeightClmHstInfo;
