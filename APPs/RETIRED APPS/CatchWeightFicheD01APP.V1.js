// // var FicheD01 = pjs.require("C:/profront/modules/protest/APPs/FicheD01APP.js");

// // VERSION V2-------------------------------------------------------------------------------------------------------------------------------------------------------
//   // Including Catch Weight intake and querry based on strick catch weight entry.

// function getCatchWeightFicheD01Info(invoiceNumber, storeNumber, itemNumber, incommingCatchWeight) {
//   const i = invoiceNumber;
//   const s = storeNumber;
//   const t = itemNumber;
//   const cw = incommingCatchWeight;
//   let sqlquery;
//   let returnObj = {};
//   returnObj.array = [];
//   returnObj.Message = '';

//   if (!i || !s || !t || !cw) {
//     returnObj.Message =
//       'Invoice Number, Store Number, Item Number and Catch Weight are required.';
//     return returnObj; 
//   }
  
//   if(s.toString().length > 3){ 
//       returnObj.Message =
//         'Store Number is to long.';
//       return returnObj; 
//   } 
  
//   if (cw > 0) {
//     console.log(`Running catch weight query for item ${t} on invoice ${i} at store ${s} with a catch weight of ${cw}`);

//     //OLD METHOD - TH - 4/12/2023
//     // sqlquery = 'select * from PWAFIL.FICHED01 where FDINV = ? and FDSTOR = ? and FDITEM = ? and FDCWLB = ?';

//     //select * from pwafil,fiched01, order by invoice date. Then select only the records that match the first invoice date.
//     sqlquery = 'select * from PWAFIL.FICHED01 where FDINV = ? and FDSTOR = ? and FDITEM = ? and FDCWLB = ? and FDDATE = (select max(FDDATE) from PWAFIL.FICHED01 where FDINV = ? and FDSTOR = ? and FDITEM = ? and FDCWLB = ?)';
//     //FDITEM, FDINV, FDCHKD, FDDATE, FDPKSZ, FDDESC, FDQTYS, FDDEPT, FDSLOT, FDSRP, FDSELL, FDECST, FDCWLB
    
//     returnObj.array = pjs.query(sqlquery, [i, s, t, cw, i, s, t, cw]);

//     if (returnObj.array.length < 1) {
//       returnObj.Message = `No Catch Weight item found for invoice ${i} at store ${s} with item number ${t} and catch weight ${cw}`;
//       return returnObj;
//     } else {
//       return returnObj;
//     }
//   } else {
//     let errorMessage = `Catch weight was a zero value.`;
//     console.log(errorMessage);
//     returnObj.Message = errorMessage;
//     return returnObj;
//   }
// }
// exports.getCatchWeightFicheD01Info = getCatchWeightFicheD01Info;
