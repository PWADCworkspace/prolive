// var FicheD01 = pjs.require("C:/profront/modules/protest/APPs/FicheD01APP.js");

// VERSION V2-------------------------------------------------------------------------------------------------------------------------------------------------------
  // Including Catch Weight intake and querry based on strick catch weight entry.

function getFicheD01Info(invoiceNumber, storeNumber, itemNumber) {
  const invoice = invoiceNumber;
  const store = storeNumber;
  const item = itemNumber;
  let returnObj = {};
    returnObj.array = [];
    returnObj.Message = '';
  
  if(store.toString().length > 3){ 
    returnObj.Message ='Store Number is to long.';
    return returnObj; 
  } 
  
  console.log(`Running FICHED01 V2 query for item ${item} on invoice ${invoice} at store ${store}`);
  
  //12/7/2023: https://pwadc.atlassian.net/browse/PROCLM-36 - Updated sql query to grab vendor number and vendor name to return back to eClaims
  let sqlquery = "select F.*, TRIM(I.vendno) AS vendno, TRIM(V.vndn) AS vndn " +
           "from FICHED01 AS F " +
           "LEFT JOIN invmst as I ON f.fditem = i.itmcde " +
           "LEFT JOIN vendpi as V ON v.vndnu = i.vendno " +
           "where FDINV = ? and FDSTOR = ? and FDITEM = ? " +
           "and FDDATE = (select max(FDDATE) from FICHED01 " +
           "where FDINV = ? and FDSTOR = ? and FDITEM = ?) LIMIT 1;";
           
  returnObj.array = pjs.query(sqlquery, [invoice, store, item, invoice, store, item]);

  if (returnObj.array.length < 1) {
    returnObj.Message = `No item found for invoice ${invoice} at store ${store} with item number ${item}`;
    return returnObj;
  } 

  //check if FDCWFG is "C", if yes then this is a catch weight item and the returnObj.Message needs to say so.
  if(returnObj.array[0].fdcwfg == "C" || returnObj.array[0].fdcwfg == "c"){
    returnObj.Message = "This is a catch weight item and needs to be processed as such. Please make sure there is a CatchWeight value greater than 0.";
  }

  return returnObj;
  
  
}
exports.getFicheD01Info = getFicheD01Info;
