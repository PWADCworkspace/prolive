//var ClmEnt = pjs.require('C:/tyler/modules/protest/APPs/ClmEntAPP.js');

function getClmEntInfo(invoiceNumber, storeNumber, itemNumber) {
  const invoice = invoiceNumber;
  const store = storeNumber;
  const item = itemNumber;
  let returnObj = {};
  returnObj.Array = [];

  console.log(
    `running CLMENT V2 query for item: ${item} on invoice: ${invoice} for store: ${store}.`
  );

  const clmentMarkoutQuery =
    "select * from CLMENT where ORDINV = ? and CLMSTR = ? and CLMITM = ? and CLMTYP = 7 and CLMCRD = 'C'";
  let clmentQuery = pjs.query(clmentMarkoutQuery, [invoice, store, item]);
  console.log(clmentQuery);

  const clmhstMarkoutQuery =
    "select * from CLMHST where HORDIN = ? and HCLMST = ? and HCLMIT = ? and HCLMTY = 7 and HCLMCR = 'C'";
  let clmhstQuery = pjs.query(clmhstMarkoutQuery, [invoice, store, item]);
  console.log(clmhstQuery);

  clmentQuery.forEach(claim => {
    returnObj.Array.push({
      ClaimID: claim.clmno,
      CaseOrOnly: claim.clmcro,
      QuantityOnClaim: claim.clmqty
    });
  });

  clmhstQuery.forEach(claim => {
    returnObj.Array.push({
      ClaimID: claim.hclmno,
      CaseOrOnly: claim.hclmco,
      QuantityOnClaim: claim.hclmqt
    });
  });

  console.log("clment returnObj");
  console.log(returnObj);
  return returnObj;
}
exports.getClmEntInfo = getClmEntInfo;
