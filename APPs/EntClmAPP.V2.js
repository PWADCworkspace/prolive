//var EntClm = pjs.require('C:/tyler/modules/protest/APPs/EntClmAPP.js');

function getEntClmInfo(invoiceNumber, storeNumber, itemNumber) {
  const invoice = invoiceNumber;
  const store = storeNumber;
  const item = itemNumber;
  let returnObj = {};
  returnObj.Array = [];

  //Setting Prod or Dev tables based on port---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  const currentPort = profound.settings.port;
  console.log(currentPort);

  let ENTCLMTable = "";

  if (currentPort === 80) {
    ENTCLMTable = "ENTCLM_CALLIN";
    console.log(`Running EntClmAPP.V2 in PROD with table: ${ENTCLMTable} for item: ${item} on invoice: ${invoice} for store: ${store}.`);
  } else {
    ENTCLMTable = "ENTCLMDEV";
    console.log(`Running EntClmAPP.V2 in DEV with table: ${ENTCLMTable} for item: ${item} on invoice: ${invoice} for store: ${store}.`);
  }

  const sqlquery =
    `select * from ${ENTCLMTable} where SORDIN = ? and SCLMST = ? and SCLMIT = ? and SCLMCR = 'C'`;
  let entclmQuery = pjs.query(sqlquery, [invoice, store, item]);

  entclmQuery.forEach(claim => {
    returnObj.Array.push({
      ClaimID: claim.sclmno,
      CaseOrOnly: claim.sclmco,
      QuantityOnClaim: claim.sclmqt
    });
  });

  return returnObj;
}
exports.getEntClmInfo = getEntClmInfo;
