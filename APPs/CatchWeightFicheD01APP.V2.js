function getCatchWeightFicheD01Info(
  invoiceNumber,
  storeNumber,
  itemNumber,
  incommingCatchWeight
) {
  const invoice = invoiceNumber;
  const store = storeNumber;
  const item = itemNumber;
  const catchWeight = incommingCatchWeight;
  let sqlquery;
  let returnObj = {};
  returnObj.Array = [];
  returnObj.Message = "";

  if (!invoice || !store || !item || !catchWeight) {
    returnObj.Message =
      "Invoice Number, Store Number, Item Number and Catch Weight are required.";
    return returnObj;
  }

  if (store.toString().length > 3) {
    returnObj.Message = "Store Number is to long.";
    return returnObj;
  }

  console.log(
    `Running Catch Weight FICHED01 V2 query for item ${item} on invoice ${invoice} at store ${store} with a catch weight of ${catchWeight}`
  );
  //OLD METHOD - TH - 4/12/2023
  // sqlquery = 'select * from FICHED01 where FDINV = ? and FDSTOR = ? and FDITEM = ? and FDCWLB = ?';

  //select * from fiched01, order by invoice date. Then select only the records that match the first invoice date.
  sqlquery = 'select * from FICHED01 where FDINV = ? and FDSTOR = ? and FDITEM = ? and FDCWLB = ? and FDDATE = (select max(FDDATE) from FICHED01 where FDINV = ? and FDSTOR = ? and FDITEM = ? and FDCWLB = ?)';
  //FDITEM, FDINV, FDCHKD, FDDATE, FDPKSZ, FDDESC, FDQTYS, FDDEPT, FDSLOT, FDSRP, FDSELL, FDECST, FDCWLB
  returnObj.Array = pjs.query(sqlquery, [invoice, store, item, catchWeight, invoice, store, item, catchWeight]);

  if (returnObj.Array.length < 1) {
    returnObj.Message = `No Catch Weight item found for invoice ${invoice} at store ${store} with item number ${item} and catch weight ${catchWeight}`;
  }

  return returnObj;
  
}
exports.getCatchWeightFicheD01Info = getCatchWeightFicheD01Info;
