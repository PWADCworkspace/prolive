// var FicheD01 = pjs.require("C:/profront/modules/protest/APPs/FicheD01APP.js");

//---------------------------------------------------------------------------------------------------------------------------------------------------------------
// function testRun() {
//   //get Pick Selector Info.
//   var invoiceNumber = 40531;
//   var itemNumberCheckDigit = 582957;
//   var itemNumber = 58295;
//   var selectorInfo;
//   selectorInfo = getPickSelectorInfo(
//     invoiceNumber,
//     itemNumberCheckDigit,
//     itemNumber
//   );
//   console.log(selectorInfo);
// }
// exports.run = testRun;

function getPickSelectorInfo(invoiceNumber, itemNumberCheckDigit, itemNumber) {
  var i = invoiceNumber;
  var t = itemNumber;
  var t2 = itemNumberCheckDigit;
  var recordSet;
  var assignmentNumber;

  var returnObj = {};
  returnObj.Message = '';
  returnObj.ErrMessage = '';

  console.log('start of getPickSelectorInfo');

  if (!i || !t2) {
    returnObj.ErrMessage = 'Invoice Number and Item Number are required.';
    return returnObj;
  } else {
    let sqlquery =
      'SELECT CRASM# from ASMCROSS where CRINV# = ? and CRITEM = ?';
    try{
        assignmentNumber = pjs.query(sqlquery, [i, t]);
    } catch (err) {
      returnObj.ErrMessage = err.sqlstate + ' ' + err.sqlMessage;
        return returnObj;
    }

    if (assignmentNumber.length == 0) {
      returnObj.ErrMessage = 'No Assignment Number Found.';
      return returnObj;
    } else {
      //add '0' to the front if assignment number to 8 charecters
      let a = assignmentNumber[0]['crasm#'].toString();
      let zeroA = padLeadingZeros(a, 8);

      //add '0' to the front if itmnum to 6 charecters
      let zeroItemNumber = padLeadingZeros(t2, 6);

      let sqlquery2 =
        'SELECT trim(xpkpst) as Status, trim(xpkopr) as Operator, trim(xpkdat) as Datetime FROM LUCYPCBK where xpkano = ? and xpkitm = ?';
      try{
          recordSet = pjs.query(sqlquery2, [zeroA, zeroItemNumber]);
          console.log(recordSet);
      } catch (err) {
          return;
      }

      if (recordSet.length == 0) {
        returnObj.ErrMessage =
          'Item ' + t + ' was not a part of Invoice Number ' + i + '.';
        return returnObj;
      } else {
        for (let i = 0; i < recordSet.length; i++) {
          let Status = recordSet[i]['status'].toString();
          let Operator = recordSet[i]['operator'].toString();
          let Datetime = recordSet[i]['datetime'].toString();
          Datetime = Datetime.slice(0, 8);
          
          let operatorName = queryLucas(Operator);

          console.log('operatorName: ' + operatorName);
          if (operatorName == null) {
            returnObj.Message +=
              t +
              ' ' +
              Status +
              ' by OperatorID: ' + 
              Operator +
              ' at ' +
              Datetime +
              '; ';
            
          } else {
            returnObj.Message +=
              t +
              ' ' +
              Status +
              ' by Operator: ' +
              operatorName +
              ' at ' +
              Datetime +
              '; ';
          }
        }
        returnObj.Message = returnObj.Message.slice(-255);
        console.log(returnObj.Message);
        return returnObj;
      }
    }
  }
}

exports.getPickSelectorInfo = getPickSelectorInfo;

function padLeadingZeros(num, size) {
  var s = num + '';
  while (s.length < size) s = '0' + s;
  return s;
}

function queryLucas(Operator) {
  
  console.log('Operator: ' + Operator);
  var logging = Operator;
  let fullName = "";

  let sqlquery3 = 'select [fullname] from [JenX].[Users] where Login = @logg';
  let params = { logg: logging };

  let getFullName = pjs.query(pjs.getDB('Lucas'), sqlquery3, params); 
  if(getFullName.length > 0) fullName = getFullName[0]['fullname'];

//   console.log('fullname: ' + fullName);
  return fullName;
}
