//var ClmHst = pjs.require('C:/tyler/modules/protest/APPs/ClmHstAPP.js');

// VERSION V2-------------------------------------------------------------------------------------------------------------------------------------------------------
// Including extended cost intake from the writeAPI and querry based on strick extended cost entry.
// This is to prevent duplicate claims from being created for that specific weight/extended cost.

function getClmHstInfo(invoiceNumber, storeNumber, itemNumber) {
  const invoice = invoiceNumber;
  const store = storeNumber;
  const item = itemNumber;
  let returnObj = {};
  returnObj.Array = [];

  console.log(
    `running CLMHST V2 query for item: ${item} on invoice: ${invoice} for store: ${store}.`
  );

  //create comparison date that is 1 week ago from today as YYYMMDD format
  let comparisonDate = new Date();
  comparisonDate.setDate(comparisonDate.getDate() - 180);
  comparisonDate = comparisonDate.toISOString().slice(0, 10).replace(/-/g, "");
  console.log(comparisonDate);

  const sqlquery =
    "select * from CLMHST where HORDIN = ? and HCLMST = ? and HCLMIT = ?  and HCLMTY != 7 and HCLMCR = 'C'";
  let clmhstQuery = pjs.query(sqlquery, [invoice, store, item]);

  clmhstQuery.forEach(claim => {
    returnObj.Array.push({
      ClaimID: claim.hclmno,
      CaseOrOnly: claim.hclmco,
      QuantityOnClaim: claim.hclmqt
    });
  });

  return returnObj;
}
exports.getClmHstInfo = getClmHstInfo;
