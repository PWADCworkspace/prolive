//  var buyerName = pjs.require('C:/tyler/modules/protest/APPs/BuyerNameAPP.js');

function getBuyerName(itemNumber) {
  var t = itemNumber;
  var recordSet;
  var sqlquery;

  var returnObj = {};
  returnObj.name = '';
  returnObj.phone = 0;
  returnObj.errorMsg = '';

  if (!t) {
    returnObj.errorMsg = 'Item Number is required.';
    return returnObj;
  } else {
    sqlquery =
      'SELECT trim(BFNAME) as BFNAME, trim(BLNAME) as BLNAME, bphone as BPhone FROM BUYTAB INNER JOIN INVMST ON BUYTAB.BCODE = INVMST.BUYER WHERE INVMST.ITMCDE= ?';
      // process.env["PJS_SQL_DEBUG"] = "1";
      recordSet = pjs.query(sqlquery, [t]);

    if (recordSet < 1) {
      returnObj.errorMsg = `No Buyer was found for item ${t} meaning that Item Number is either invalid or no longer in use.`;
      return returnObj;
    } else {
      buyerName = recordSet[0]['bfname'];
      buyerLast = recordSet[0]['blname']
      buyerPhone = recordSet[0]['bphone'];
      returnObj.name = buyerName + ' ' + buyerLast;
      returnObj.phone = buyerPhone;

      return returnObj;
    }
  }
}
exports.getBuyerName = getBuyerName;
