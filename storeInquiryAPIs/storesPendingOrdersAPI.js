async function storesPendingOrdersAPI(request, response){
    console.log("storePendingOrdersAPI");
    //Set pathing for Apps.
    //local
    //   const directPathToProlive = "C:/tyler/modules/prolive/APPs/";
    //production
    // const directPathToProlive = "C:/profront/modules/prolive/APPs/";
    //test
    // const directPathToProlive = "C:/profront-test/modules/prolive/APPs/";

    // const ProfrontTxtlog = pjs.require(directPathToProlive + "PushToTxtLogAPP.js");

    const directPathToProlog = "../../Prologging/";
  
    let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

    let httpStatusCode = 200;
    let returnObj = {
        array: [],
        error: ""
    }
    let arrayObj = {};

    pjs.define("uuid", { type: 'char', length: 36 });
    pjs.define("StoreNumber", { type: 'packed decimal', length: 3, decimals: 0 });
    pjs.define("ItemNumberWithCheckDigit", { type: 'packed decimal', length: 6, decimals: 0 });
    pjs.define("ItemNumber", { type: 'packed decimal', length: 5, decimals: 0 });
    pjs.define("Department", { type: 'char', length: 1 });
    pjs.define("VendorNumber", { type: 'packed decimal', length: 4, decimals: 0 });
    pjs.define("OrderDate", { type: 'packed decimal', length: 8, decimals: 0 });
    pjs.define("ErrorMessageOut", { type: 'char', length: 256 });

    uuid = pjs.newUUID();
    StoreNumber =  request.body.StoreNumber;
    ItemNumberWithCheckDigit =  request.body.ItemNumber;
    Department =  request.body.Department;
    VendorNumber =  request.body.VendorNumber;
    OrderDate =  request.body.OrderDate;
    ErrorMessageOut = "";

    const originalDataForLogging = 
    'Store: '+ StoreNumber +
    ', ItemNumberWithCheckDigit: '+ ItemNumber +
    ', Department: '+ Department +
    ', VendorNumber: '+ VendorNumber +
    ', OrderDate: '+ OrderDate;

    //require at least StoreNumber
    if (StoreNumber == 0){
        returnObj.error = "Error: A Store Number is required.";
        await ProfrontTxtlog.pushToTxTLog("storesPendingOrdersAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
        response.status(400).send(returnObj);
        return;
    }

    //require at least ItemNumber or Department or OrderHeader
    if (ItemNumberWithCheckDigit == 0 && Department == " " && VendorNumber == 0 && OrderDate == 0){
        returnObj.error = "Error: Item Number, Department, Vendor Number or Order Date is required.";
        await ProfrontTxtlog.pushToTxTLog("storesPendingOrdersAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
        response.status(400).send(returnObj);
        return;
    }

    ItemNumber = Math.floor(ItemNumberWithCheckDigit / 10);

    try{
    pjs.call("ZARSTORD", //done
        pjs.parm("uuid"),
        pjs.parm("StoreNumber"),
        pjs.parm("ItemNumber"),
        pjs.parm("Department"),
        pjs.parm("VendorNumber"),
        pjs.parm("OrderDate"),
        pjs.refParm("ErrorMessageOut")
        );
    }
    catch(err){
    returnObj.error = "Error: Call to RPG program ZARSTORD failed. Make sure all libraries needed by this program are in the config.js path." + err;
    await ProfrontTxtlog.pushToTxTLog("storesPendingOrdersAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
    httpStatusCode = 400; 
    }

    if (ErrorMessageOut.trim().length > 0){
        returnObj.error = "Error from RPG program ZARSTORD: " + ErrorMessageOut.trim();
        await ProfrontTxtlog.pushToTxTLog("storesPendingOrdersAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
        httpStatusCode = 400; 
    }

    let recordSet
    let query = "select zadata from ZAPSTORD where zaguid = ?";
    try{recordSet = pjs.query(query, [uuid]);
    }
    catch{
        returnObj.error = "Error: DB2 SQL Error Code " + sqlcod + "   https://www.ibm.com/docs/en/db2-for-zos/12?topic=codes-sql-error" ;
        await ProfrontTxtlog.pushToTxTLog("storesPendingOrdersAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
        httpStatusCode = 400; 
    }
    
    if (recordSet && recordSet.length>0)
    {
        for(var i = 0 ; i<recordSet.length ;  i++)
        {
            try{
                arrayObj = JSON.parse(recordSet[i]["zadata"]);
                returnObj.array.push(arrayObj);
            }
            catch{
                returnObj.error = "Error: This JSON returned from RPG program failed to parse. " + recordSet[i]["zadata"];
                await ProfrontTxtlog.pushToTxTLog("storesPendingOrdersAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
                httpStatusCode = 400;  
                console.log("bad json:"+recordSet[i]["zadata"]);
            }
        }
    }

    await ProfrontTxtlog.pushToTxTLog("storesPendingOrdersAPI", `Store: ${StoreNumber}`, originalDataForLogging, "Successful Call");
    response.status(httpStatusCode).send(returnObj);
}

exports.run = storesPendingOrdersAPI;