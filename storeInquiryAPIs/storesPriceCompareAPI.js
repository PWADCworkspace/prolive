async function storePriceCompareAPI(request, response){
    console.log("storePriceCompareAPI");
    //Set pathing for Apps.
    //local
    //   const directPathToProlive = "C:/tyler/modules/prolive/APPs/";
    //production
    // const directPathToProlive = "C:/profront/modules/prolive/APPs/";
    //test
    // const directPathToProlive = "C:/profront-test/modules/prolive/APPs/";

    // const ProfrontTxtlog = pjs.require(directPathToProlive + "PushToTxtLogAPP.js");

    const directPathToProlog = "../../Prologging/";
  
    let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

    let httpStatusCode = 200;
    let returnObj = {
        array: [],
        error: ""
    };
    let arrayObj = {};

    pjs.define("uuid", { type: 'char', length: 36 });
    pjs.define("ControllingStoreNumber", { type: 'packed decimal', length: 3, decimals: 0 });
    pjs.define("Store2", { type: 'packed decimal', length: 3, decimals: 0 });
    pjs.define("Store3", { type: 'packed decimal', length: 3, decimals: 0 });
    pjs.define("Store4", { type: 'packed decimal', length: 3, decimals: 0 });
    pjs.define("Store5", { type: 'packed decimal', length: 3, decimals: 0 });
    pjs.define("Store6", { type: 'packed decimal', length: 3, decimals: 0 });
    pjs.define("Store7", { type: 'packed decimal', length: 3, decimals: 0 });
    pjs.define("ItemNumberWithCheckDigit", { type: 'packed decimal', length: 6, decimals: 0 });
    pjs.define("ItemNumber", { type: 'packed decimal', length: 5, decimals: 0 });
    pjs.define("Department", { type: 'char', length: 1 });
    pjs.define("OrderHeader", { type: 'packed decimal', length: 4, decimals: 0 });
    pjs.define("ErrorMessageOut", { type: 'char', length: 256 });

    uuid = pjs.newUUID();
    ControllingStoreNumber =  request.body.ControllingStoreNumber;
    Store2 =  request.body.CompareStore2;
    Store3 =  request.body.CompareStore3;
    Store4 =  request.body.CompareStore4;
    Store5 =  request.body.CompareStore5;
    Store6 =  request.body.CompareStore6;
    Store7 =  request.body.CompareStore7;
    ItemNumberWithCheckDigit =  request.body.ItemNumber;
    Department =  request.body.Department;
    OrderHeader =  request.body.OrderHeader;
    ErrorMessageOut = "";
    ItemNumber = Math.floor(ItemNumberWithCheckDigit / 10);

    const originalDataForLogging = 
    'Controlling Store: '+ ControllingStoreNumber +
    ', Store2: '+ Store2 +
    ', Store3: '+ Store3 +
    ', Store4: '+ Store4 +
    ', Store5: '+ Store5 +
    ', Store6: '+ Store6 +
    ', Store7: '+ Store7 +
    ', ItemNumberWithCheckDigit: '+ ItemNumber +
    ', Department: '+ Department +
    ', OrderHeader: '+ OrderHeader;

    //require at least ControllingStoreNumber and Store2
    if (ControllingStoreNumber == 0 || Store2 == 0){
        returnObj.error = "Error: At least a Controlling Store Number and Store 2 are required.";
        await ProfrontTxtlog.pushToTxTLog("storesPriceCompareAPI", `Controlling Store: ${ControllingStoreNumber}`, originalDataForLogging, returnObj.error);
        response.status(400).send(returnObj);
        return;
    }

    //WILL TURN OFF ONCE DOTTI PUTS ARRAY RETURN LIMIT IN PLACE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //require at least ItemNumber or Department or OrderHeader
    if (ItemNumber == 0 && Department == " " && OrderHeader == 0){
        returnObj.error = "Error: Item Number, Department, or Order Header are required.";
        await ProfrontTxtlog.pushToTxTLog("storesPriceCompareAPI", `Controlling Store: ${ControllingStoreNumber}`, originalDataForLogging, returnObj.error);
        response.status(400).send(returnObj);
        return;
    }

    try{
    pjs.call("ZARPRCMP", //done
        pjs.parm("uuid"),
        pjs.parm("ControllingStoreNumber"),
        pjs.parm("Store2"),
        pjs.parm("Store3"),
        pjs.parm("Store4"),
        pjs.parm("Store5"),
        pjs.parm("Store6"),
        pjs.parm("Store7"),
        pjs.parm("ItemNumber"),
        pjs.parm("Department"),
        pjs.parm("OrderHeader"),
        pjs.refParm("ErrorMessageOut")
        );
    }
    catch(err){
    returnObj.error = "Error: Call to RPG program ZARPRCMP failed. Make sure all libraries needed by this program are in the config.js path." + err;
    await ProfrontTxtlog.pushToTxTLog("storesPriceCompareAPI", `Controlling Store: ${ControllingStoreNumber}`, originalDataForLogging, returnObj.error);
    httpStatusCode = 400; 
    }

    if (ErrorMessageOut.trim().length > 0){
        returnObj.error = "Error from RPG program ZARPRCMP: " + ErrorMessageOut.trim();
        await ProfrontTxtlog.pushToTxTLog("storesPriceCompareAPI", `Controlling Store: ${ControllingStoreNumber}`, originalDataForLogging, returnObj.error);
        httpStatusCode = 400; 
    }

    let recordSet
    let query = "select zadata from zapprcmp where zaguid = ?";
    try{recordSet = pjs.query(query, [uuid]);
    }
    catch{
        returnObj.error = "Error: DB2 SQL Error Code " + sqlcod + "   https://www.ibm.com/docs/en/db2-for-zos/12?topic=codes-sql-error" ;
        await ProfrontTxtlog.pushToTxTLog("storesPriceCompareAPI", `Controlling Store: ${ControllingStoreNumber}`, originalDataForLogging, returnObj.error);
        httpStatusCode = 400; 
    }
    
    if (recordSet && recordSet.length>0) {
        for(var i = 0 ; i<recordSet.length ;  i++)
        {
            try{
                arrayObj = JSON.parse(recordSet[i]["zadata"]);
                returnObj.array.push(arrayObj);
            }
            catch{
                returnObj.error = "Error: This JSON returned from RPG program failed to parse. " + recordSet[i]["zadata"];
                await ProfrontTxtlog.pushToTxTLog("storesPriceCompareAPI", `Controlling Store: ${ControllingStoreNumber}`, originalDataForLogging, returnObj.error);
                httpStatusCode = 400;  
                console.log("bad json:"+recordSet[i]["zadata"]);
            }
        }
    } else {
        returnObj.error = "No difference were found within the given search Criteria.";
        await ProfrontTxtlog.pushToTxTLog("storesPriceCompareAPI", `Controlling Store: ${ControllingStoreNumber}`, originalDataForLogging, returnObj.error);
        httpStatusCode = 404;
    }

    await ProfrontTxtlog.pushToTxTLog("storesPriceCompareAPI", `Controlling Store: ${ControllingStoreNumber}`, originalDataForLogging, "Successful Call");
    response.status(httpStatusCode).send(returnObj);
}

exports.run = storePriceCompareAPI;

//Inbound parms:
//
//Store 3,0 – always included
//Item 5,0 – 0 place holder
//Invoice # 5,0 – 0 place holder
//Dept 1A – ‘ ’ place holder
//From date (or single date) 8,0 – 0 place holder
//To date 8,0 – 0 place holder


//Outbound data: (always send these with place holders if blank)
//
//Store# 3,0
//Item 5,0
//Description 30A
//Packsize 10A
//Dept 1A
//Order qty 5,0
//Ship qty 5,0
//Catchweight lbs  5,2
//Ext cost  9,4
//Invoice # 5,0
//Date 8,0
