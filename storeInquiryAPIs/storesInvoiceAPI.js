const dayjs = require("dayjs");

async function storeinvoiceAPI(request, response){
    console.log(`storeinvoiceAPI is running... ${dayjs()}`);

    //Set pathing for Apps.
    //local
    // const directPathToProlive = "C:/tyler/modules/prolive/APPs/";
    //production
    // const directPathToProlive = "C:/profront/modules/prolive/APPs/";
    //test
    // const directPathToProlive = "C:/profront-test/modules/prolive/APPs/";

    // const ProfrontTxtlog = pjs.require(directPathToProlive + "PushToTxtLogAPP.js");

    const directPathToProlog = "../../Prologging/";
  
    let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

    let httpStatusCode = 200;
    let returnObj = {
        array: [],
        error: ""
    };
    let arrayObj = {};

    pjs.define("uuid", { type: 'char', length: 36 });
    pjs.define("StoreNumber", { type: 'packed decimal', length: 3, decimals: 0 });
    pjs.define("ItemNumberWithCheckDigit", { type: 'packed decimal', length: 6, decimals: 0 });
    pjs.define("ItemNumber", { type: 'packed decimal', length: 5, decimals: 0 });
    pjs.define("InvoiceNumber", { type: 'packed decimal', length: 5, decimals: 0 });
    pjs.define("Department", { type: 'char', length: 1 });
    pjs.define("FromDate", { type: 'packed decimal', length: 8, decimals: 0 });
    pjs.define("ToDate", { type: 'packed decimal', length: 8, decimals: 0 });
    pjs.define("ErrorMessageOut", { type: 'char', length: 256 });

    StoreNumber =  request.body.StoreNumber;
    ItemNumberWithCheckDigit =  request.body.ItemNumber;
    InvoiceNumber =  request.body.InvoiceNumber;
    let incomingDepartment =  request.body.Department.trim();
    FromDate =  request.body.FromDate;
    ToDate =  request.body.ToDate;
    ErrorMessageOut = "";

    const originalDataForLogging = 
    'StoreNumber: '+ StoreNumber +
    ' ItemNumber: '+ ItemNumberWithCheckDigit +
    ' InvoiceNumber: '+ InvoiceNumber +
    ' incomingDepartment: '+ incomingDepartment +
    ' FromDate: '+ FromDate +
    ' ToDate: '+ ToDate;

    console.log(`storeinvoiceAPI Incoming Data: ${originalDataForLogging}`);

    if (!StoreNumber || StoreNumber <= 0) {
        returnObj.error = "Error: Store Number is required.";
        console.log(returnObj.error);
        await ProfrontTxtlog.pushToTxTLog("storeInvoiceAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
        response.status(400).send(returnObj);
        return;
    }

    let departmentsArray = [];
    if(incomingDepartment == "All Deparments" || incomingDepartment == ""){ // valid
        departmentsArray.push("");
    } else if(incomingDepartment == "P - Produce" || incomingDepartment == "P"){ // valid
        departmentsArray.push("P");
    } else if (incomingDepartment == "A - Grocery"|| incomingDepartment == 'A'){ // valid - need to confirm if need to add more departments
        departmentsArray.push("A","E","B");
    } else if(incomingDepartment == "M - Meat"|| incomingDepartment == 'M'){ //valid - need to confirm if need to add more departments
        departmentsArray.push("M","N");
    } else if(incomingDepartment == "G - HBA"|| incomingDepartment == 'G'){
        departmentsArray.push("G");
    } else if(incomingDepartment == "F - Frozen"|| incomingDepartment == 'F'){
        departmentsArray.push("F");
    } else if(incomingDepartment == "Z - Deli"|| incomingDepartment == 'Z'){
        departmentsArray.push("Z","X","Z");

    } else if(incomingDepartment == "D - Dairy"|| incomingDepartment == 'D'){
        departmentsArray.push("D");
    }else{
        returnObj.error = "Please ensure to specify all departments or individual departments.";
        console.log(returnObj.error);
        await ProfrontTxtlog.pushToTxTLog("storeInvoiceAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
        response.status(400).send(returnObj);
        return;
    }
    
    let hasItem = (ItemNumberWithCheckDigit != 0) ? true : false;
    let hasInvoice = (InvoiceNumber != 0) ? true : false;

    ItemNumber = Math.floor(ItemNumberWithCheckDigit / 10);

    let itemFoundInAnyDepartment = false;
    let invoiceFoundInAnyDepartment = false;

    for (const department of departmentsArray) {
        uuid = pjs.newUUID();

        Department = department;


        try{
            console.log("Calling ZARSTINV Program with the following parameters: ");
            console.log(`uuid: ${uuid}, StoreNumber: ${StoreNumber}, ItemNumber: ${ItemNumber}, InvoiceNumber: ${InvoiceNumber}, Department: ${Department}, FromDate: ${FromDate}, ToDate: ${ToDate}, ErrorMessageOut: ${ErrorMessageOut}`);

        pjs.call("ZARSTINV",
            pjs.parm("uuid"),
            pjs.parm("StoreNumber"),
            pjs.parm("ItemNumber"),
            pjs.parm("InvoiceNumber"),
            pjs.parm("Department"),
            pjs.parm("FromDate"),
            pjs.parm("ToDate"),
            pjs.refParm("ErrorMessageOut")
            );
        }
        catch (err) {
            returnObj.error = "Error: Call to RPG program ZARSTINV failed. Make sure all libraries needed by this program are in the config.js path.";
            console.log(returnObj.error);
            await ProfrontTxtlog.pushToTxTLog("storeInvoiceAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
            console.log(err);
            httpStatusCode = 400; 
        }

        //If there is an error message and the error does not pertain no records found throw error
        if (ErrorMessageOut.trim().length > 0 && !ErrorMessageOut.includes("No records found")){
            returnObj.error = "Error from RPG program ZARSTINV: " + ErrorMessageOut.trim();
            console.log(returnObj.error);
            await ProfrontTxtlog.pushToTxTLog("storeInvoiceAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
            httpStatusCode = 400; 
        }

        let recordSet
        let query = "select zadata from zapstinv where zaguid = ?";

        try{
            recordSet = pjs.query(query, [uuid]);
        } catch{
            returnObj.error = "Error: DB2 SQL Error Code " + sqlcod + "   https://www.ibm.com/docs/en/db2-for-zos/12?topic=codes-sql-error" ;
            await ProfrontTxtlog.pushToTxTLog("storeInvoiceAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
            httpStatusCode = 400; 
        }

        
        if (recordSet && recordSet.length> 0)
        {
            for(var i = 0 ; i<recordSet.length ;  i++)
            {
                try{
                    arrayObj = JSON.parse(recordSet[i]["zadata"]);
                    returnObj.array.push(arrayObj);
                    itemFoundInAnyDepartment = true; // Mark that the item was found
                    invoiceFoundInAnyDepartment = true; //Mark that the invoice was found

                }
                catch{
                    returnObj.error = "Error: This JSON returned from RPG program failed to parse.    " + recordSet[i]["zadata"];
                    await ProfrontTxtlog.pushToTxTLog("storeInvoiceAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
                    httpStatusCode = 400;  
                    console.log("bad json:"+recordSet[i]["zadata"]);
                }
            }
        }
    }

    console.log(`Invoice records pulled from zapstinv table: ${returnObj.array.length }`);
    
    //After making requests and pulling invoice records, verify if the returning length is zero
    if(returnObj.array.length == 0){
        returnObj.error = "Error from RPG program ZARSTINV: No records found";
        console.log(returnObj.error);
        await ProfrontTxtlog.pushToTxTLog("storeInvoiceAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
        httpStatusCode = 400; 
    }

    // After checking all departments, verify if the item was found in any of them
    if (!invoiceFoundInAnyDepartment && hasInvoice) {
        returnObj.error = "Error: Invoice not found in any sub-department for the selected department.";
        console.log(returnObj.error);
        await ProfrontTxtlog.pushToTxTLog("storeInvoiceAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
        response.status(400).send(returnObj);
        return;
    }

    // After checking all departments, verify if the invoice was found in any of them
    if (!itemFoundInAnyDepartment && hasItem) {
        returnObj.error = "Error: Item not found in any sub-department for the selected department.";
        console.log(returnObj.error);
        await ProfrontTxtlog.pushToTxTLog("storeInvoiceAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
        response.status(400).send(returnObj);
        return;
    }    

    console.log(`storeinvoiceAPI is done running... ${dayjs()}`);

    await ProfrontTxtlog.pushToTxTLog("storeInvoiceAPI", `Store: ${StoreNumber}`, originalDataForLogging, "Successful Call");
    response.status(httpStatusCode).send(returnObj);
}

exports.run = storeinvoiceAPI;

//Inbound parms:
//
//Store 3,0 – always included
//Item 5,0 – 0 place holder
//Invoice # 5,0 – 0 place holder
//Dept 1A – ‘ ’ place holder
//From date (or single date) 8,0 – 0 place holder
//To date 8,0 – 0 place holder


//Outbound data: (always send these with place holders if blank)
//
//Store# 3,0
//Item 5,0
//Description 30A
//Packsize 10A
//Dept 1A
//Order qty 5,0
//Ship qty 5,0
//Catchweight lbs  5,2
//Ext cost  9,4
//Invoice # 5,0
//Date 8,0
