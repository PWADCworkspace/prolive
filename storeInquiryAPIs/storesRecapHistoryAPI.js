const dayjs = require("dayjs");

async function storeRecapHistoryAPI(request, response){
    console.log(`storeRecapHistoryAPI is running... ${dayjs()}`);
    //Set pathing for Apps.
    //local
    // const directPathToProlive = "C:/tyler/modules/prolive/APPs/";
    //production
    // const directPathToProlive = "C:/profront/modules/prolive/APPs/";
    //test
    // const directPathToProlive = "C:/profront-test/modules/prolive/APPs/";

    // const ProfrontTxtlog = pjs.require(directPathToProlive + "PushToTxtLogAPP.js");

    const directPathToProlog = "../../Prologging/";
  
    let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

    let httpStatusCode = 200;
    let returnObj = {
        array: [],
        error: ""
    };
    let arrayObj = {};

    pjs.define("uuid", { type: 'char', length: 36 });
    pjs.define("StoreNumber", { type: 'packed decimal', length: 3, decimals: 0 });
    pjs.define("DateOne", { type: 'packed decimal', length: 8, decimals: 0 });
    pjs.define("DateTwo", { type: 'packed decimal', length: 8, decimals: 0 });
    pjs.define("RecapNumber", { type: 'packed decimal', length: 5, decimals: 0 });
    pjs.define("ErrorMessageOut", { type: 'char', length: 256 });

    uuid = pjs.newUUID();
    StoreNumber =  request.body.StoreNumber;
    DateOne =  request.body.DateOne;
    DateTwo =  request.body.DateTwo;
    RecapNumber =  request.body.RecapNumber;
    ErrorMessageOut = "";

    const originalDataForLogging =
    'StoreNumber: '+ StoreNumber +
    ' DateOne: '+ DateOne +
    ' DateTwo: '+ DateTwo +
    ' RecapNumber: '+ RecapNumber;

    console.log(`storeRecapHistoryAPI Incoming Data: ${originalDataForLogging}`);

    if (!StoreNumber || StoreNumber <= 0) {
        returnObj.error = "Error: Store Number is required.";
        await ProfrontTxtlog.pushToTxTLog("storesRecapHistoryAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
        response.status(400).send(returnObj);
        return;
    }

    if (!DateOne && !RecapNumber) {
        returnObj.error = "Error: Either Date or Recap Number is required.";
        await ProfrontTxtlog.pushToTxTLog("storesRecapHistoryAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
        response.status(400).send(returnObj);
        return;
    }

    try{
        console.log("Calling ZARRCPHST program...");
        pjs.call("ZARRCPHST", //done
            pjs.parm("uuid"), //guid for ZARRCPHST
            pjs.parm("StoreNumber"), //store for ZARRCPHST
            pjs.parm("DateOne"), //date one for ZARRCPHST in YYMMDD format
            pjs.parm("DateTwo"), //date two for ZARRCPHST in YYMMDD format
            pjs.parm("RecapNumber"), //recap number for ZARRCPHST
            pjs.refParm("ErrorMessageOut") //error for ZARRCPHST
            );
    }
    catch{
    returnObj.error = "Error: Call to RPG program ZARRCPHST failed. Make sure all libraries needed by this program are in the config.js path.";
    await ProfrontTxtlog.pushToTxTLog("storesRecapHistoryAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
    httpStatusCode = 400; 
    }

    if (ErrorMessageOut.trim().length > 0)
    {
        returnObj.error = "Error from RPG program ZARRCPHST: " + ErrorMessageOut.trim();
        await ProfrontTxtlog.pushToTxTLog("storesRecapHistoryAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
        httpStatusCode = 400; 
    }

    let recordSet
    let query = "select zadata from ZAPRCPHST where zaguid = ?";
    try{recordSet = pjs.query(query, [uuid]);
    }
    catch{
        returnObj.error = "Error: DB2 SQL Error Code " + sqlcod + " https://www.ibm.com/docs/en/db2-for-zos/12?topic=codes-sql-error" ;
        await ProfrontTxtlog.pushToTxTLog("storesRecapHistoryAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
        httpStatusCode = 400; 
    }
    
    if (recordSet && recordSet.length>0)
    {
        for(var i = 0 ; i<recordSet.length ;  i++)
        {
            try{
                arrayObj = JSON.parse(recordSet[i]["zadata"]);
                returnObj.array.push(arrayObj);
            }
            catch{
                returnObj.error = "Error: This JSON returned from RPG program failed to parse." + recordSet[i]["zadata"];
                await ProfrontTxtlog.pushToTxTLog("storesRecapHistoryAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
                httpStatusCode = 400;  
                console.log("bad json:"+recordSet[i]["zadata"]);
            }
        }
    }

    console.log(`storeRecapHistoryAPI is done running... ${dayjs()}`);

    await ProfrontTxtlog.pushToTxTLog("storesRecapHistoryAPI", `Store: ${StoreNumber}`, originalDataForLogging, "Successful Call");
    response.status(httpStatusCode).send(returnObj);
}

exports.run = storeRecapHistoryAPI;
