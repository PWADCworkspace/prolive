const dayjs = require("dayjs");

async function storeClaimHistoryAPI(request, response){
    console.log(`storeClaimHistoryAPI is running.... ${dayjs()}`);
    //Set pathing for Apps.
    //local
    // const directPathToProlive = "C:/tyler/modules/prolive/APPs/";
    //production
    // const directPathToProlive = "C:/profront/modules/prolive/APPs/";
    //test
    // const directPathToProlive = "C:/profront-test/modules/prolive/APPs/";
    
    // const ProfrontTxtlog = pjs.require(directPathToProlive + "PushToTxtLogAPP.js");

    const directPathToProlog = "../../Prologging/";
  
    let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

    let httpStatusCode = 200;
    let returnObj = {
        array: [],
        error: ""
    };
    let arrayObj = {};

    pjs.define("uuid", { type: 'char', length: 36 });
    pjs.define("StoreNumber", { type: 'packed decimal', length: 3, decimals: 0 });
    pjs.define("ItemNumberWithCheckDigit", { type: 'packed decimal', length: 6, decimals: 0 });
    pjs.define("ItemNumber", { type: 'packed decimal', length: 5, decimals: 0 });
    pjs.define("InvoiceNumber", { type: 'packed decimal', length: 5, decimals: 0 });
    // pjs.define("ClaimNumber", { type: 'packed decimal', length: 5, decimals: 0 });
    pjs.define("ClaimNumber", { type: 'packed decimal', length: 6, decimals: 0 }); //changed from 5 to 6 becuase eClaims clain ids have gone above 99,999.
    pjs.define("DocumentNumber", { type: 'packed decimal', length: 5, decimals: 0 });
    pjs.define("ClaimDate", { type: 'packed decimal', length: 8, decimals: 0 });
    pjs.define("OrderDate", { type: 'packed decimal', length: 8, decimals: 0 });
    pjs.define("GlAcct", {type: 'packed decimal', length : 4, decimals : 0});
    pjs.define("ErrorMessageOut", { type: 'char', length: 256 });

    uuid = pjs.newUUID();
    StoreNumber =  request.body.StoreNumber;
    ItemNumberWithCheckDigit =  request.body.ItemNumber;
    InvoiceNumber =  request.body.InvoiceNumber;
    ClaimNumber =  request.body.ClaimNumber;
    DocumentNumber = request.body.DocumentNumber;
    ClaimDate = request.body.ClaimDate;
    OrderDate = request.body.OrderDate;
    GlAcct = request.body.GLAcctNumber;
    ErrorMessageOut = "";

    if(GlAcct === undefined) {
        console.log(`The account number was not passed. Setting it to zero`);
        GlAcct = 0;
    }

    const originalDataForLogging = 
    'StoreNumber: '+ StoreNumber +
    ' ItemNumber: '+ ItemNumber +
    ' InvoiceNumber: '+ InvoiceNumber +
    ' ClaimNumber: '+ ClaimNumber +
    ' DocumentNumber: '+ DocumentNumber +
    ' ClaimDate: '+ ClaimDate +
    ' OrderDate: '+ OrderDate +
    ' GlAcct: ' + GlAcct;

    console.log(`storeClaimHistoryAPI Incoming data: ${originalDataForLogging}`);

    if (!StoreNumber || StoreNumber <= 0) {
        returnObj.error = "Error: Store Number is required.";
        await ProfrontTxtlog.pushToTxTLog("storesClaimHistoryAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
        response.status(400).send(returnObj);
        return;
    }

    if (!ItemNumberWithCheckDigit && !ClaimNumber && !InvoiceNumber && !DocumentNumber) {
        returnObj.error = "Error: Item Number, Claim Number, Invoice Number, or Document Number is required.";
        await ProfrontTxtlog.pushToTxTLog("storesClaimHistoryAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
        response.status(400).send(returnObj);
        return;
    }

    ItemNumber = Math.floor(ItemNumberWithCheckDigit / 10);

    //NOTE: 05162023 - This addition is to trim claim IDs that are greater than 99,999 (5 digits) to fit our system.
    let updatedClaimNumber = ClaimNumber.toString();

        if (updatedClaimNumber.length > 5) {
            updatedClaimNumber = updatedClaimNumber.substring(1);
            ClaimNumber= parseInt(updatedClaimNumber);
        } 

    pjs.define("ClaimNumber", { type: 'packed decimal', length: 9, decimals: 0 }); //changed from 5 to 6 becuase eClaims clain ids have gone above 99,999.
    // pjs.define("GlAcct", {type: 'packed decimal', length : 4, decimals : 0}); //Need to move it to the top

    
        //consoloe.log all parms going into the pjs.call
        console.log(`uuid ${uuid}`);
        console.log(`ErrorMessageOut ${ErrorMessageOut}`);

    try{
        console.log("Calling ZarClmhst program...");
    pjs.call("ZARCLMHST", //done
        pjs.parm("uuid"), //guid for zarclmhst
        pjs.parm("StoreNumber"), //store for zarclmhst
        pjs.parm("ItemNumber"), //item for zarclmhst
        pjs.parm("InvoiceNumber"), //invoice for zarclmhst
        pjs.parm("ClaimNumber"), //claim for zarclmhst
        pjs.parm("DocumentNumber"), //doc for zarclmhst
        pjs.parm("ClaimDate"), //claimdate for zarclmhst
        pjs.parm("OrderDate"), //orderdate for zarclmhst
        pjs.parm("GlAcct"), //gl account number for zarclmhst
        pjs.refParm("ErrorMessageOut") //error for zarclmhst
        );
    }
    catch(err){
    // returnObj.error = "Error: Call to RPG program ZARCLMHST failed. Make sure all libraries needed by this program are in the config.js path.";
    returnObj.error = err;
    await ProfrontTxtlog.pushToTxTLog("storesClaimHistoryAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
    httpStatusCode = 400; 
    }

    if (ErrorMessageOut.trim().length > 0)
    {
        returnObj.error = "Error from RPG program ZARCLMHST: " + ErrorMessageOut.trim();
        await ProfrontTxtlog.pushToTxTLog("storesClaimHistoryAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
        httpStatusCode = 400; 
    }

    let recordSet
    let query = "select zadata from ZAPCLMHST where zaguid = ?";

    try{recordSet = pjs.query(query, [uuid]);
    }
    catch (err) {
        returnObj.error = "Error: DB2 SQL Error Code " + sqlcod + " https://www.ibm.com/docs/en/db2-for-zos/12?topic=codes-sql-error" + err;
        await ProfrontTxtlog.pushToTxTLog("storesClaimHistoryAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
        httpStatusCode = 400; 
    }
    
    if (recordSet && recordSet.length>0)
    {
        for(var i = 0 ; i<recordSet.length ;  i++)
        {
            try{
                arrayObj = JSON.parse(recordSet[i]["zadata"]);
                returnObj.array.push(arrayObj);
            }
            catch{
                returnObj.error = "Error: This JSON returned from RPG program failed to parse.    " + recordSet[i]["zadata"];
                await ProfrontTxtlog.pushToTxTLog("storesClaimHistoryAPI", `Store: ${StoreNumber}`, originalDataForLogging, returnObj.error);
                httpStatusCode = 400;  
                console.log("bad json:"+recordSet[i]["zadata"]); 
            }
        }
    }

    console.log(`storeClaimHistoryAPI is done running.... ${dayjs()}`);

    await ProfrontTxtlog.pushToTxTLog("storesClaimHistoryAPI", `Store: ${StoreNumber}`, originalDataForLogging, "Successful Call");
    response.status(httpStatusCode).send(returnObj);
}

exports.run = storeClaimHistoryAPI;
