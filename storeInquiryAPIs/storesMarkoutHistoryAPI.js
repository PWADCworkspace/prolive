const dayjs = require("dayjs");

async function storesMarkout(request,response){
    console.log(`storesMarkoutHistory API is running...${dayjs()}`);

    //Setting Paths
    const directPathToProlog = "../../Prologging/";
    let ProfrontTxtlog = pjs.require(directPathToProlog + "PushToTxtLogAPP.js");

    // //Setting API response
    let returnArray = [];
    let returnObj = {};
    returnObj.errorMessage = "";


    //Defining variable types
    pjs.define("uuid", { type: 'char', length: 36 });
    pjs.define("error", {type: 'char', length: 256});
    pjs.define("store", {type: 'packed decimal', length: 3, decimals: 0});
    pjs.define("item", {type: 'packed decimal', length: 5, decimals: 0});
    pjs.define("invoice", {type: 'packed decimal', length: 5, decimals: 0});
    pjs.define("buyer", {type: 'char', length: 1});
    pjs.define("dept", {type: 'char', length: 1});
    pjs.define("invoicedate", {type: 'packed decimal', length: 8, decimals: 0});

    //Setting Table and Program
    const table = `ZAPMARKOUT`;
    const program = `ZARMARKOUT`;
    let dataQuery = `select zadata from ${table} where zaguid = ?`;
    let data;
    let arrayObj = {};



    //Grabbing request parameters
    const storeNum = request.query.storeNum; //Will be either item code or upc
    const itemNum = request.query.itemNum;
    const invoiceNum = request.query.invoiceNum;
    // const buyerNum = request.query.buyerNum;
    const deptNum = request.query.deptNum;
    const invoiceDate = request.query.invoiceDate;


    // const incomingDataForLogging = 
    // `store : ${storeNum}, item: ${itemNum}, invoice: ${invoiceNum}, buyer: ${buyerNum}, dept: ${deptNum}, invoiceDate: ${invoiceDate}`;
    const incomingDataForLogging = 
    `store : ${storeNum}, item: ${itemNum}, invoice: ${invoiceNum},  dept: ${deptNum}, invoiceDate: ${invoiceDate}`;

    console.log("storesMarkoutHistory V1 Incoming Data: " + incomingDataForLogging);

    if(!storeNum || storeNum > 999){
        returnObj.errorMessage = "A valid max 3 digit store number is required.";
        console.log(returnObj.errorMessage);
        await ProfrontTxtlog.pushToTxTLog(
          "storesMarkoutHistory V1",
          `STORE: ${storeNum}`,
          incomingDataForLogging,
          returnObj.errorMessage
        );
        response.status(400).send(returnObj);
        return;
    }

    if(itemNum > 999999){
        returnObj.errorMessage = "The item number can not be greater 6 digits.";
        console.log(returnObj.errorMessage);
        await ProfrontTxtlog.pushToTxTLog(
          "storesMarkoutHistory V1",
          `ITEM: ${itemNum}`,
          incomingDataForLogging,
          returnObj.errorMessage
        );
        response.status(400).send(returnObj);
        return;
    }

    if(invoiceNum > 99999){
        returnObj.errorMessage = "The invoice number can not be greater 5 digits.";
        console.log(returnObj.errorMessage);
        await ProfrontTxtlog.pushToTxTLog(
          "storesMarkoutHistory V1",
          `INVOICE: ${invoiceNum}`,
          incomingDataForLogging,
          returnObj.errorMessage
        );
        response.status(400).send(returnObj);
        return;
    }

    // if(buyerNum.length > 2){
    //     errorMessage = "The buyer code must be one character value.";
    //     console.log(errorMessage);
    //     await ProfrontTxtlog.pushToTxTLog(
    //       "storesMarkoutHistory V1",
    //       `BUYER: ${buyerNum}`,
    //       incomingDataForLogging,
    //       errorMessage
    //     );
    //     response.status(400).send(errorMessage);
    //     return;
    // }

    if(deptNum.length > 2){
        returnObj.errorMessage = "The department code must be one character value.";
        console.log(returnObj.errorMessage);
        await ProfrontTxtlog.pushToTxTLog(
          "storesMarkoutHistory V1",
          `DEPARTMENT: ${deptNum}`,
          incomingDataForLogging,
          returnObj.errorMessage
        );
        response.status(400).send(returnObj);
        return;
    }

    //Setting RPG params to incoming data
    uuid = pjs.newUUID();
    store = storeNum;
    item = Math.floor(itemNum / 10);
    invoice = invoiceNum;
    buyer = "";
    dept = deptNum;
    invoicedate = invoiceDate;
    error = "";

    const rpgParms = `
    uuid: ${uuid},
    store : ${store}, 
    item: ${item}, 
    invoice: ${invoice}, 
    buyer: ${buyer}, 
    dept: ${dept}, 
    invoiceDate: ${invoicedate},
    error: ${error}
    `;
  console.log("storesMarkoutHistory V1 RPG Parms: " + rpgParms);
    
    try{
        console.log(`Calling ${program} Program...`);
     pjs.call(`${program}`,
        pjs.parm("uuid"),
        pjs.parm("store"),
        pjs.parm("item"),
        pjs.parm("invoice"),
        pjs.parm("buyer"),
        pjs.parm("dept"),
        pjs.parm("invoicedate"),
        pjs.refParm("error")
        );
    }
    catch(err){
        returnObj.errorMessage = `Program Call Catch error. If the issue persists please contact retail systems.`;
        console.log(returnObj.errorMessage + err);
        await ProfrontTxtlog.pushToTxTLog(
          "storesMarkoutHistory V1",
          incomingDataForLogging,
          returnObj.errorMessage + err
        );
        response.status(400).send(returnObj);
        return;
    }

    
    if (error.trim().length > 0) {
        returnObj.errorMessage = error.trim();
        console.log(`${program} error response: ${returnObj.errorMessage}`);
        await ProfrontTxtlog.pushToTxTLog(
          "storesMarkoutHistory V1",
          incomingDataForLogging,
          returnObj.errorMessage
        );
        response.status(404).send(returnObj);
        return;
    
      } 

      console.log(`Querying ${table} table for results....`);
      try{
        data = pjs.query(dataQuery, [uuid]);

        if (sqlstate >= "02000") {
            returnObj.errorMessage = `SQL error while grabbing results. If the issue persists please contact retail systems. SQLSTATE: ${sqlstate}.`
            console.log(returnObj.errorMessage);
            await ProfrontTxtlog.pushToTxTLog(
              "storesMarkoutHistory V1",
              incomingDataForLogging,
              returnObj.errorMessage
            );
            response.status(400).send(returnObj);
            return;
        }
        } catch(err){
            returnObj.errorMessage = `SQL catch error while grabbing results. If the issue persists please contact retail systems.`;
    
            await ProfrontTxtlog.pushToTxTLog(
                "storesMarkoutHistory V1",
                incomingDataForLogging,
                returnObj.errorMessage + err
              );
              response.status(400).send(returnObj);
              return;
        }

        console.log(`Length of records: ${data.length}`);
        if (data && data.length>0)
        {
            console.log(`Parsing results from table....`);
            for(var i = 0 ; i<data.length ;  i++)
            {
                try{
                  arrayObj = JSON.parse(data[i]["zadata"]);
                  returnArray.push(arrayObj);
                }
                catch(err){
                  returnObj.errorMessage = "JSON parse error while parsing data. If the issue persists please contact retail systems.";
                    console.log(returnObj.errorMessage + "Error" + err + "Data" + data[i]["zadata"]);
                    await ProfrontTxtlog.pushToTxTLog("storesMarkoutHistory", incomingDataForLogging, returnObj.errorMessage);
                    response.status(400).send(returnObj);
                    return;
                }
            }
        }
      
      await ProfrontTxtlog.pushToTxTLog("storesMarkoutHistory", incomingDataForLogging, "Successful Call");
      response.status(200).send(returnArray);

     console.log(`storesMarkoutHistory API is done running...${dayjs()}`);

}
exports.run = storesMarkout;
